<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registrations extends Model
{
    protected $guard = 'user';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id','meta','status','user_id','step','type','submitted_at'
    ];
}
