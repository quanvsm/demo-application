<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FAQs extends Model
{
  protected $guard = 'admin';
  public $table = "faqs";

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'question', 'answer', 'template_id',
  ];
}
