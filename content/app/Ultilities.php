<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ultilities extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'cut_off_date'
  ];
}
