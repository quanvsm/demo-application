<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsCategories extends Model
{
  protected $guard = 'admin';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'title', 'slug',
  ];
}
