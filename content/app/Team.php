<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
  protected $guard = 'admin';
  public $table = 'team';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'image', 'image_mime', 'position', 'additional_position', 'phone', 'email', 'quote', 'bio', 'sort', 'template_id',
  ];
}
