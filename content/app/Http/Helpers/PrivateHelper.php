<?php

namespace App\Http\Helpers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use App\PostsCategories;
use App\Templates;
use App\FAQs;
use App\Team;
use Illuminate\Support\Facades\DB;
use Auth;

class PrivateHelper extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

        // If no valid guard is logged in.
        if(!Auth::check()){
            header('Location: '.route('home'));
            die;
        }
    }

    public function get_category($id)
    {
        if($id!=null){
            $category = PostsCategories::findOrFail($id);
            return $category->title;
        }
    }

    public function get_category_by_slug($slug)
    {
        if($slug!=null){
            $category = PostsCategories::where('slug','=',$slug)->first();
            return $category->title;
        }
    }

    public function get_template_info($template_id){
        $template = Templates::where('id', '=', $template_id)->first();
        return $template;
    }

    public function get_FAQS($template_id){
        $FAQs = FAQs::where('template_id', '=', $template_id)->orderBy('sort','asc')->get();
        return $FAQs;
    }

    public function get_team($template_id){
        $team = Team::where('template_id', '=', $template_id)->orderBy('sort','asc')->get();
        return $team;
    }

    public function save_template_attributes($page, $request){

        $template = Templates::where('page_id', '=', $page->id)->first();
        if($template->template_type == 1){
            self::save_faq_order($request);
        }elseif($template->template_type == 2){
            self::save_team_order($request);
        }
    }

    public function save_faq_order(PageRequest $request){
      // Clear the sort colomn.
      FAQs::where('template_id', '=', request('template_id'))->update(array('sort' => null));

      // This goes through each input in the request.
      foreach ($request->request as $name => $value) {
        // If the input name contains sort_.
        if (strpos($name, 'sort_') !== false && $value != null) {
          $id = substr($name,5);
          $FAQ = FAQs::findOrFail($id);
          $FAQ->sort = $value;
          $FAQ->save();
        }
      }
    }

    public function save_team_order(PageRequest $request){
      // Clear the sort colomn.
      Team::where('template_id', '=', request('template_id'))->update(array('sort' => null));

      // This goes through each input in the request.
      foreach ($request->request as $name => $value) {
        // If the input name contains sort_.
        if (strpos($name, 'sort_') !== false && $value != null) {
          $id = substr($name,5);
          $team = Team::findOrFail($id);
          $team->sort = $value;
          $team->save();
        }
      }
    }
}
