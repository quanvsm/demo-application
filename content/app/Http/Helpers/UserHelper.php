<?php

namespace App\Http\Helpers;

use App\Http\Controllers\Controller;
use App\User;
use Auth;

class UserHelper extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:user');

        if(!Auth::guard('user')->check()){
            header('Location: '.route('user.dashboard'));
            die;
        }
    }

    public static function get_name()
    {
        $user = User::findOrFail(Auth::id());
        return $user->first_name;
    }

}
