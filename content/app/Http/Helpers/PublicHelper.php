<?php

namespace App\Http\Helpers;

use App\Page;
use App\FAQs;
use App\Team;
use Session;

class PublicHelper
{

    public static function get_slug($id)
    {
        $pages = Page::where('id', '=', $id)->first();
        return $pages->slug;
    }

    public static function get_info($id)
    {
        $page = Page::where('id', '=', $id)->first();
        return $page;
    }

    public static function get_faqs($template_id)
    {
        return FAQs::orderBy('sort','asc')->where('template_id', '=', $template_id)->get();
    }

    public static function get_team_members($template_id){
        $team = Team::where('template_id', '=', $template_id)->orderBy('sort','asc')->get();
        return $team;
    }

    public static function show_excerpt($content)
    {
        $content = substr($content,0,200);
        return $content;
    }

    public static function format_phone_number($phone_number){
    	$n = str_split($phone_number);
        try {
            // Any phone number less then 10 digits will cause an error.
            $formatted_number = $n[0].$n[1].$n[2].'.'.$n[3].$n[4].$n[5].'.'.$n[6].$n[7].$n[8].$n[9];
        }catch(\Exception $e) {
            // If phone number is less then 10 digits.
            return $phone_number;
        }

    	return $formatted_number;
    }
}
