<?php

namespace App\Http\Helpers;

use App\Http\Controllers\Controller;
use App\Admin;
use Auth;

class AdminHelper extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');

        if(!Auth::guard('admin')->check()){
            header('Location: '.route('home'));
            die;
        }
    }

    public static function get_admin_name()
    {
        $admin = Admin::findOrFail(Auth::id());
        return $admin->first_name;
    }

}
