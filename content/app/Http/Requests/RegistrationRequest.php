<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade' => 'sometimes|required|max:255',
            'int_grade' => 'sometimes|required|max:255',
            'stu_lname' => 'required|max:255',
            'stu_fname' => 'required|max:255',
            'stu_month_of_birth' => 'required|max:255',
            'stu_date_of_birth' => 'required|max:255',
            'stu_year_of_birth' => 'required|max:255',
            'stu_gender' => 'required|max:255',
            'stu_street' => 'required|max:255',
            'stu_city' => 'required|max:255',
            'stu_province' => 'required|max:255',
            'stu_postal' => 'required|max:255',
            'stu_phone' => 'required|max:255',
            'stu_email' => 'required|email|max:255',
            'stu_citizenship_status' => 'required|max:255',
            'doctor_name' => 'required|max:255',
            'doctor_phone' => 'required|max:255',
            'health_card' => 'required|max:255',
            'immunization' => 'required|max:255',
            'stu_medical' => 'required|max:255',
            'emergency_name' => 'required|max:255',
            'emergency_rela' => 'required|max:255',
            'emergency_phone' => 'required|max:255',
            'medical_consent' => 'required|max:255',
            'pa_sig_medical' => 'sometimes|required|max:255',
            'pa_sig_medical_date' => 'sometimes|required|max:255',
            'mo_sig_medical' => 'sometimes|required|max:255',
            'mo_sig_medical_date' => 'sometimes|required|max:255',
            'pa_sig_transfer' => 'sometimes|required|max:255',
            'pa_sig_transfer_date' => 'sometimes|required|max:255',
            'mo_sig_transfer' => 'sometimes|required|max:255',
            'mo_sig_transfer_date' => 'sometimes|required|max:255',
            'pa_sig_accuracy' => 'sometimes|required|max:255',
            'pa_sig_accuracy_date' => 'sometimes|required|max:255',
            'mo_sig_accuracy' => 'sometimes|required|max:255',
            'mo_sig_accuracy_date' => 'sometimes|required|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'grade.required' => 'Grade is a required field (Step 1).',
            'int_grade.required' => 'Grade is a required field (Step 1).',
            'stu_lname.required' => 'Student Last Name is a required field (Step 1).',
            'stu_fname.required' => 'Student First Name is a required field (Step 1).',
            'stu_month_of_birth.required' => 'Student Birthday is a required field (Step 1).',
            'stu_date_of_birth.required' => 'Student Birthday is a required field (Step 1).',
            'stu_year_of_birth.required' => 'Student Birthday is a required field (Step 1).',
            'stu_gender.required' => 'Student Gender is a required field (Step 1).',
            'stu_street.required' => 'Student Address is a required field (Step 1).',
            'stu_city.required' => 'Student Address is a required field (Step 1).',
            'stu_province.required' => 'Student Address is a required field (Step 1).',
            'stu_postal.required' => 'Student Address is a required field (Step 1).',
            'stu_phone.required' => 'Student Phone Nmber is a required field (Step 1).',
            'stu_email.email' => 'Invalid Student Email Address (Step 1).',
            'stu_email.required' => 'Student Email is a required field (Step 1).',
            'stu_citizenship_status.required' => 'Student Immigrant Status is a required field (Step 1).',
            'doctor_name.required' => 'Doctor Name is a required field (Step 2).',
            'doctor_phone.required' => 'Doctor Phone Number is a required field (Step 2).',
            'health_card.required' => 'Student Health Card is a required field (Step 1).',

            'immunization.required' => 'immunization is a required field (Step 2).',
            'stu_medical.required' => 'Student Medical Condition is a required field (Step 2).',
            'emergency_name.required' => 'Emergency Contact is a required field (Step 2).',
            'emergency_rela.required' => 'Emergency Contact Card is a required field (Step 2).',
            'emergency_phone.required' => 'Emergency Contact Card is a required field (Step 2).',
            'medical_consent.required' => 'Emergency Medical Consent is a required field (Step 2).',
            'pa_sig_medical.required' => 'Parent Name is a required field (Step 2).',
            'pa_sig_medical_date.required' => 'Parent Name is a required field (Step 2).',
            'mo_sig_medical.required' => 'Parent Name is a required field (Step 2).',
            'mo_sig_medical_date.required' => 'Parent Name is a required field (Step 2).',
            'pa_sig_transfer.required' => 'Parent Name is a required field (Step 7).',
            'pa_sig_transfer_date.required' => 'Parent Name is a required field (Step 7).',
            'mo_sig_transfer.required' => 'Parent Name is a required field (Step 7).',
            'mo_sig_transfer_date.required' => 'Parent Name is a required field (Step 7).',
            'pa_sig_accuracy.required' => 'Parent Name is a required field.',
            'pa_sig_accuracy_date.required' => 'Parent Name is a required field.',
            'mo_sig_accuracy.required' => 'Parent Name is a required field.',
            'mo_sig_accuracy_date.required' => 'Parent Name is a required field.',
        ];
    }
}
