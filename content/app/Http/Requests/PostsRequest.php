<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use File;

class PostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // Gets the name of every public folder.
        $public_dirs = File::directories(public_path());
        foreach ($public_dirs as $index => $file) {
            $public_dirs[$index] = substr(strrchr($file,'/'), 1);
        }
        $public_dirs = implode($public_dirs, ',');

        return [
            'img' => 'mimes:jpeg,png,gif,jpeg',
            'slug' => 'required|unique:posts,slug,'.$this->get('id').'|unique:posts_categories,slug|not_in:'.config('app.not_in').','.$public_dirs
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'img.mimes' => 'Invalid file type. Accepted types: jpeg, png, gif, jpeg',
            'slug.required' => 'Slug is a required field.',
            'slug.unique' => 'Slug is already taken, please enter a different slug.',
            'slug.not_in' => 'Invalid Slug, please enter a different slug.',
        ];
    }
}
