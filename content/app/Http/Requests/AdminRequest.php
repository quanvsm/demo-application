<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|max:255|unique:admins,email,'.$this->get('id'),
            'active' => 'required_with:active == yes',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'First Name is a required field.',
            'last_name.required' => 'Last Name is a required field.',
            'email.required' => 'Email is a required field.',
            'email.email' => 'Invalid Email Address',
            'email.max' => 'Exceeded maximum email length',
            'email.unique' => 'Email is already in use, please enter a different email address.',
            'active.required_with' => 'Active is a required field.',
        ];
    }
}
