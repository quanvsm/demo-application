<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grade' => 'sometimes|required|max:255',
            'int_grade' => 'sometimes|required|max:255',
            'stu_lname' => 'required|max:255',
            'stu_fname' => 'required|max:255',
            'stu_month_of_birth' => 'required|max:255',
            'stu_date_of_birth' => 'required|max:255',
            'stu_year_of_birth' => 'required|max:255',
            'stu_gender' => 'required|max:255',
            'stu_street' => 'required|max:255',
            'stu_city' => 'required|max:255',
            'stu_province' => 'required|max:255',
            'stu_postal' => 'required|max:255',
            'stu_phone' => 'required|max:255',
            'stu_email' => 'required|email|max:255',
            'stu_citizenship_status' => 'required|max:255',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'grade.required' => 'Grade is a required field (Step 2).',
            'int_grade.required' => 'Grade is a required field (Step 2).',
            'stu_lname.required' => 'Student Last Name is a required field (Step 2).',
            'stu_fname.required' => 'Student First Name is a required field (Step 2).',
            'stu_month_of_birth.required' => 'Student Birthday is a required field (Step 2).',
            'stu_date_of_birth.required' => 'Student Birthday is a required field (Step 2).',
            'stu_year_of_birth.required' => 'Student Birthday is a required field (Step 2).',
            'stu_gender.required' => 'Student Gender is a required field (Step 2).',
            'stu_street.required' => 'Student Address is a required field (Step 2).',
            'stu_city.required' => 'Student Address is a required field (Step 2).',
            'stu_province.required' => 'Student Address is a required field (Step 2).',
            'stu_postal.required' => 'Student Address is a required field (Step 2).',
            'stu_phone.required' => 'Student Phone Nmber is a required field (Step 2).',
            'stu_email.email' => 'Invalid Student Email Address (Step 2).',
            'stu_email.required' => 'Student Email is a required field (Step 2).',
            'stu_citizenship_status.required' => 'Student Immigrant Status is a required field (Step 2).',
        ];
    }
}
