<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6|same:password_confirmation',
            'password_confirmation' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email is a required field.',
            'email.email' => 'Invalid Email Address',
            'email.max' => 'Exceeded maximum email length',
            'password.required' => 'Password is a required field.',
            'password.min' => 'Password must be at least 6 characters long.',
            'password.same' => 'Password does not match confirmation',
            'password_confirmation.required' => 'Password Confirmation is a required field.',
        ];
    }
}
