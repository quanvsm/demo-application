<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostsCategoriesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slug' => 'required|unique:posts_categories,slug,'.$this->get('id').'|unique:posts,slug|not_in:admin,posts',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'slug.required' => 'Slug is a required field.',
            'slug.unique' => 'Slug is already taken, please enter a different slug.',
            'slug.not_in' => 'Invalid Slug, please enter a different slug.',
        ];
    }
}
