<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\Payments;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use Auth;

class PaypalController extends Controller
{
    private $_api_context;
    
    public function __construct()
    {
        $this->middleware('auth:user');
        $paypal_configuration = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_configuration['client_id'], $paypal_configuration['secret']));
        $this->_api_context->setConfig($paypal_configuration['settings']);
    }

    public function payWithPaypal()
    {
        return redirect()->route('user.dashboard');
    }

    public function postPaymentWithpaypal(Request $request)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

    	$item_1 = new Item();

        $item_1->setName('Product 1')
            ->setCurrency('CAD')
            ->setQuantity(1)
            ->setPrice(request('amount'));

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('CAD')
            ->setTotal(request('amount'));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Enter Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('status'))
            ->setCancelUrl(URL::route('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));            
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error','Connection timeout');
                return Redirect::route('paywithpaypal');                
            } else {
                \Session::put('error','Some error occur, sorry for inconvenient');
                return Redirect::route('paywithpaypal');                
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        Session::put('paypal_payment_id', $payment->getId());
        if(request("type_id")){
            Session::put('application_id', $request->form_id);
            unset($request['_token']);
            Session::put('post_data', $request->all());
        }
        else{
            Session::put('registration_id', $request->form_id);
        }

        if(isset($redirect_url)) {            
            return Redirect::away($redirect_url);
        }

        \Session::put('error','Unknown error occurred');
    	return Redirect::route('paywithpaypal');
    }

    public function getPaymentStatus(Request $request)
    {        
        $payment_id=$request->paymentId;

        Session::forget('paypal_payment_id');
        if (empty($request->PayerID) || empty($request->token)) {
            // \Session::put('error','Payment failed');
            return redirect()->route('user.form')->with('error', 'Payment failed !');
        }
        $payment = Payment::get($payment_id, $this->_api_context);        
        $execution = new PaymentExecution();
        $execution->setPayerId($request->input('PayerID'));        
        $result = $payment->execute($execution, $this->_api_context);
        $transactions = $payment->getTransactions();
        $relatedResources = $transactions[0]->getRelatedResources();
        $sale = $relatedResources[0]->getSale();
        $payer = $payment->getPayer()->getPayerInfo();
        $saleId = $sale->getId();
        
        if ($result->getState() == 'approved') {     
            $isPaymentExist = Payments::where('transaction_id', $saleId)->first(); 
            if(!$isPaymentExist)
                {
                    $payment = new Payments;
                    $payment->method = "Paypal";
                    $payment->transaction_id = $saleId;
                    $payment->user_id = Auth::id();
                    if(Session::has('application_id')){
                        $payment->application_id =Session::get('application_id');
                    }
                    if(Session::has('registration_id')){
                        $payment->registration_id =Session::get('registration_id');
                    }              
                    $payment->payer_email = $payer->getEmail();
                    $payment->amount = $sale->getAmount()->getTotal();
                    $payment->currency = $sale->getAmount()->getCurrency();
                    $payment->payment_status = $result->getState();
                    $payment->save();
                }   
            // \Session::put('success','Payment success !!');
            if(Session::has('application_id')){
                $application =  Session::get('application_id');
                Session::forget('application_id');
                return redirect()->route('user.update.payment',['id'=>$payment->id, 'application'=>$application] );
            }
            if(Session::has('registration_id')){
                $registration_id = Session::get('registration_id');
                Session::forget('registration_id');
                return redirect()->route('user.registration.payment',$registration_id)->with('success',"Your payment has been completed. Thank you.");
            }  
        }

        // \Session::put('error','Payment failed !!');
		return redirect()->route('user.form')->with('error', 'Error Please Try Again !');
    }
}