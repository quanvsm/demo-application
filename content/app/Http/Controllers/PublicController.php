<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Templates;
use Auth;
use App\Applications;
use App\Registrations;
use App\Invoices;
use App\Documents;
use Storage;
use Crypt;
use Exception;

class PublicController extends Controller
{
  private $visible;

  public function showHomePage(){
      $page = null;
      return view('theme.home', compact('page'));
  }

  public function showForm(){
    return view('theme.application-form', compact('page'));
}

  public function show($slug){
    $this->check_admin_login();

    $page = Page::where('slug', $slug)
                 ->where('parent_id', '=', null)
                 ->whereRaw($this->visible)
                 ->first();

    if($page){
      // slug exists in the database with null parent_id.
      // Note* null parent_id does not necessarily means the page
      // is a parent page.

      return $this->find_view($page);
    }else{
      // slug does not exists in the database with null parent_id.

      $page = Page::where('slug', $slug)
                   ->where('parent_id', '!=', null)
                   ->whereRaw($this->visible)
                   ->first();
      if($page){
        // Page is a sub page.
        $parent = Page::where('id', $page->parent_id)->first();
        return redirect()->route('public.parent', ['parent'=>$parent->slug, 'slug'=>$page->slug]);
      }
    }

    $page = null;
    return view('theme.404', compact('page'));
  }

  public function showParent($parent_slug, $slug){
    $this->check_admin_login();

    $parent = Page::where('slug', $parent_slug)
                   ->where('parent', '=', 1)
                   ->whereRaw($this->visible)
                   ->first();
    if($parent){
      // Parent page does exist
      $sub_page = Page::where('slug', $slug)
                       ->where('parent_id', '=', $parent->id)
                       ->whereRaw($this->visible)
                       ->first();
      if($sub_page){
        // Parent has sub pages.
        $page = $sub_page;
        return $this->find_view($page);
      }
    }

    $page = null;
    return view('theme.404', compact('page'));
  }

  public function find_view($page){
      if($page->hard_code == 1){
          return view($page->hard_code_view, compact('page'));
      }elseif($page->template_id == null){
          return view('theme.content', compact('page'));
      }elseif($page->template_id != null){
          $template = Templates::where('page_id', '=', $page->id)->first();
          return view('theme.'.$template->view.'', compact('page'));
      }
  }

  private function check_admin_login(){
      if (Auth::guard('admin')->check()){
          $this->visible = "visible IS NOT NULL";
      }elseif(Auth::guard('user')->check()){
          $this->visible = "visible IS NOT NULL";
      }else{
          $this->visible = "visible = 'published' ";
      }
  }

  public function downloadInvoice($id){
    ini_set('memory_limit', '-1');
    try{
    $invoice = Invoices::get()->where('id',$id);
    
   foreach($invoice as $item){
    $file = decrypt(Storage::get($item->path));

    return response()->streamDownload(function() use ($file,$item) {
        ob_start();
        header('Content-Type: application/{$item->file_mime}');
        echo $file;
        ob_end_flush();
        exit;
    }, $item->name);
   }
  }
  catch (Exception $e) {
      return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
      return false;
  }
}


public function downloadDocument($id){
  ini_set('memory_limit', '-1');
  try{
  $documents = Documents::get()->where('id',$id);
  
 foreach($documents as $item){
  $file = decrypt(Storage::get($item->path));

  return response()->streamDownload(function() use ($file,$item) {
      ob_start();
      header('Content-Type: application/{$item->file_mime}');
      echo $file;
      ob_end_flush();
      exit;
  }, $item->name);
 }
}
catch (Exception $e) {
    return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
    return false;
}
}

public function uploadDocument(Request $request){
  ini_set('memory_limit', '-1');
  try{
  $target_dir = 'Upload_Files/Documents/';
  if ($request->hasfile('document')) {
          $file = $request->file('document');
          $name = $file->getClientOriginalName();
          $new_file_name =  SHA1(rand());
              Storage::put($target_dir . $new_file_name, Crypt::encrypt($file->get()));
              $document = Documents::updateOrCreate([
                  'path' => $target_dir . $new_file_name,
                  'name' => $name,
                  'size' => $this->getFileSize($file->getsize()),
                  'mime' => $file->getMimeType(),
              ]);
              $saved = $document->save();
              if($saved){
                  return redirect()->route('admin.users')->with('success', 'Document Uploaded!');
              }
              else{
                  return redirect()->route('admin.users')->with('error', 'Document Was Not Uploaded!');
              }
  }
  else{
      return redirect()->route('admin.users')->with('error', 'Please select a file!');
  }
}
catch (Exception $e) {
    return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
    return false;
}
  
}

public function getFileSize($size)
{
    // Bytes
    $file_size = $size . ' Bytes';

    if ($file_size >= 1000) {
        // Kilobytes
        $file_size = round($size / 1024, 1) . ' KB';
    }

    if ($file_size >= 1000) {
        // Megabytes
        $file_size = round($size / 1024 / 1024, 1) . ' MB';
    }

    return $file_size;
}

  //Export 1 application
  public function exportCSV($id,$type){
    
try{
    $applications = Applications::get()->where("id",$id);
      if(isset($applications)){
        // Setup Headers to Export the CSV
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Application.csv');
        $output = fopen('php://output', 'w');
        if($type==1){
          fputcsv($output, array('Type','Status','Grade','Student First Name','Student Middle Name','Student Last Name','Gender','Birthday','Address','Email','Home Phone','Citizenship / Immigration Status',
                                  'Language Spoken at Home','Country of Birth','Current School','City and Country','Medical Conditions or Allergies','Parent/Guardian 2 Relationship','Parent/Guardian 1 Last Name ',
                                  'Parent/Guardian 1 First Name','Parent/Guardian 1 Address','Parent/Guardian 1 Home Phone','Parent/Guardian 1 Mobile Phone','Parent/Guardian 1 Email','Parent/Guardian 1 Occupation',
                                   'Parent/Guardian 1 Employer','Parent/Guardian 2 Relationship','Parent/Guardian 2 Last Name ','Parent/Guardian 2 First Name','Parent/Guardian 2 Address','Parent/Guardian 2 Home Phone',
                                   'Parent/Guardian 2 Mobile Phone','Parent/Guardian 2 Email','Parent/Guardian 2 Occupation', 'Parent/Guardian 2 Employer','Parent Status','Student Lives With','Church Name','Reference Name',
                                   'Reference Phone','Relationship to Applicant','Heard From'));
        }
        else{
          fputcsv($output, array('Type','Status','Grade','Student First Name','Student Middle Name','Student Last Name','Gender','Birthday','Address','Email','Home Phone','Citizenship / Immigration Status',
                                  'Language Spoken at Home','Country of Birth','Current School','City and Country','Medical Conditions or Allergies','Parent/Guardian 2 Relationship','Parent/Guardian 1 Last Name ',
                                  'Parent/Guardian 1 First Name','Parent/Guardian 1 Address','Parent/Guardian 1 Home Phone','Parent/Guardian 1 Mobile Phone','Parent/Guardian 1 Email','Parent/Guardian 1 Occupation', 
                                  'Parent/Guardian 1 Employer','Parent/Guardian 2 Relationship','Parent/Guardian 2 Last Name ','Parent/Guardian 2 First Name','Parent/Guardian 2 Address','Parent/Guardian 2 Home Phone',
                                  'Parent/Guardian 2 Mobile Phone','Parent/Guardian 2 Email','Parent/Guardian 2 Occupation', 'Parent/Guardian 2 Employer','Parent Status','Student Lives With','Church Name','Custodian Legal First Name',
                                  'Custodian Legal Middle Name','Custodian Legal Last Name','Commonly Goes by',' Custodian Date of Birth','Relationship to Applicant','Winnipeg Address','Custodian Home Phone','Custodian Alternate Phone',
                                  'Custodian Email','Heard From'));
        }
      
        foreach ($applications as $app){
          $data = json_decode($app->meta);
          $heard_from='';
          $father_address='';
          $mother_address='';
          if(isset($data->heard_from)){
            foreach ($data->heard_from as $item){
              $heard_from .= $item.",";
          }
          }

          if(isset($data->pa_addr_check_box) && $data->pa_addr_check_box == 'on'){
            $father_address = $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
          }
          else{
            $father_address = $data->pa_street.' '.$data->pa_city.' '.$data->pa_province;
          }

          if(isset($data->mo_addr_check_box) && $data->mo_addr_check_box == 'on'){
            $mother_address =  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
          }
          else{
            $mother_address = $data->mo_street.' '.$data->mo_city.' '.$data->mo_province;
          }
          if($type == 1 ){
            fputcsv($output,
            array(
              "Canadian",
                  $app->status??'',
                  $data->grade_in ?? ( $data->grade??''),
                  $data->stu_lname??'',
                  $data->stu_mname??'',
                  $data->stu_fname??'',
                  $data->stu_gender??'',
                  $data->stu_month_of_birth.'/'.$data->stu_date_of_birth.'/'.$data->stu_year_of_birth,
                  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province,
                  $data->stu_email??'',
                  $data->stu_phone??'',
                 $data->stu_citizenship_status??'',
                 $data->stu_lang??'',
                 $data->stu_country_of_birth??'',
                 $data->stu_curr_school??'',
                 $data->stu_city_country??'',
                 ($data->stu_allegies_details??'No'),
                 $data->father??'',
                 $data->pa_lname??'',
                 $data->pa_fname??'',
                 $father_address,
                //  $data->pa_addr_check_box??'',
                 $data->pa_hphone??'',
                 $data->pa_mphone??'',
                 $data->pa_email??'',
                 $data->pa_occupation??'',
                 $data->pa_employer??'',
                 $data->mother??'',
                 $data->mo_lname??'',
                 $data->mo_fname??'',
                 $mother_address??'',
                 $data->mo_hphone??'',
                 $data->mo_mphone??'',
                 $data->mo_email??'',
                 $data->mo_occupation??'',
                 $data->mo_employer??'',
                 $data->marriage_status??'',
                 $data->custody_status??"",
                 $data->church??'',
                 $data->ref_name??'',
                 $data->ref_phone??"",
                 $data->ref_rela??'',
                 $heard_from
            )
          );
          }
          else{
            fputcsv($output,
            array(
              "International",
              $app->status??'',
              $data->int_grade??'',
              $data->stu_lname??'',
              $data->stu_mname??'',
              $data->stu_fname??'',
              $data->stu_gender??'',
              $data->stu_month_of_birth.'/'.$data->stu_date_of_birth.'/'.$data->stu_year_of_birth,
              $data->stu_street.' '.$data->stu_city.' '.$data->stu_province,
              $data->stu_email??'',
              $data->stu_phone??'',
             $data->stu_citizenship_status??'',
             $data->stu_lang??'',
             $data->stu_country_of_birth??'',
             $data->stu_curr_school??'',
             $data->stu_city_country??'',
             ($data->stu_allegies_details??'No'),
             $data->father??'',
             $data->pa_lname??'',
             $data->pa_fname??'',
             $father_address??'',
             $data->pa_hphone??'',
             $data->pa_mphone??'',
             $data->pa_email??'',
             $data->pa_occupation??'',
             $data->pa_employer??'',
             $data->mother??'',
             $data->mo_lname??'',
             $data->mo_fname??'',
             $mother_address??'',
             $data->mo_hphone??'',
             $data->mo_mphone??'',
             $data->mo_email??'',
             $data->mo_occupation??'',
             $data->mo_employer??'',
             $data->marriage_status??'',
             $data->custody_status??'',
             $data->church??'',
             $data->custodian_fname??'',
             $data->custodian_mname??'',
             $data->custodian_lname??'',
             $data->custodian_cname??'',
             $data->custodian_birthday??'',
             $data->custodian_rela??'',
             $data->custodian_street . " ".$data->custodian_city." ".$data->custodian_province." ".$data->custodian_country." ".$data->custodian_postal,
             $data->custodian_phone??'',
             $data->custodian_alt_phone??'',
             $data->custodian_email??'',
             $heard_from
            )
          );
          }
        }
      }
    exit;
  }
  catch (Exception $e) {
      return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
      return false;
  }
  }

  public function exportIndividualRegistrationCSV($id,$type){
    try{
    $registrations = Registrations::get()->where("id",$id);
      if(isset($registrations)){
        // Setup Headers to Export the CSV
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=Registration.csv');
        $output = fopen('php://output', 'w');
        if($type==1){
          fputcsv($output, array('Type','Status','Grade','Student First Name','Student Middle Name','Student Last Name','Gender','Birthday','Address','Email','Home Phone','Citizenship / Immigration Status',
                                  'Language Spoken at Home','Current School','City and Country','Parent/Guardian 2 Relationship','Parent/Guardian 1 Last Name ',
                                  'Parent/Guardian 1 First Name','Parent/Guardian 1 Address','Parent/Guardian 1 Home Phone','Parent/Guardian 1 Mobile Phone','Parent/Guardian 1 Email','Parent/Guardian 1 Occupation',
                                   'Parent/Guardian 1 Employer','Parent/Guardian 2 Relationship','Parent/Guardian 2 Last Name ','Parent/Guardian 2 First Name','Parent/Guardian 2 Address','Parent/Guardian 2 Home Phone',
                                   'Parent/Guardian 2 Mobile Phone','Parent/Guardian 2 Email','Parent/Guardian 2 Occupation', 'Parent/Guardian 2 Employer','Parent Status','Student Lives With','Church Name','Doctor name',
                                   'Doctor Phone','Manitoba Health Card','Immunizations Up To Date','Medical Conditions','Medical Details','Emergency Contact Name',"Relationship",'Emergency Contact Day Phone','Emergency Contact Mobile Phone','Indigenous Identity',
                                   "Description",'Cultural-linguistic Identity'));
        }
        else{
          fputcsv($output, array('Type','Status','Grade','Student First Name','Student Middle Name','Student Last Name','Gender','Birthday','Address','Email','Home Phone','Citizenship / Immigration Status',
                                  'Language Spoken at Home','Current School','City and Country','Parent/Guardian 2 Relationship','Parent/Guardian 1 Last Name ',
                                  'Parent/Guardian 1 First Name','Parent/Guardian 1 Address','Parent/Guardian 1 Home Phone','Parent/Guardian 1 Mobile Phone','Parent/Guardian 1 Email','Parent/Guardian 1 Occupation', 
                                  'Parent/Guardian 1 Employer','Parent/Guardian 2 Relationship','Parent/Guardian 2 Last Name ','Parent/Guardian 2 First Name','Parent/Guardian 2 Address','Parent/Guardian 2 Home Phone',
                                  'Parent/Guardian 2 Mobile Phone','Parent/Guardian 2 Email','Parent/Guardian 2 Occupation', 'Parent/Guardian 2 Employer','Parent Status','Student Lives With','Church Name','Doctor name',
                                  'Doctor Phone','Manitoba Health Card','Immunizations Up To Date','Medical Conditions','Medical Details','Emergency Contact Name',"Relationship",'Emergency Contact Day Phone','Emergency Contact Mobile Phone'));
        }
      
        foreach ($registrations as $app){
          $data = json_decode($app->meta);
          $father_address='';
          $mother_address='';
          $medical_conditions = '';
          $indigenous_child = '';
          $cultural_linguistic ='';
          if(isset($data->stu_medical )){
            foreach ($data->stu_medical as $item){
              $medical_conditions .= $item.",";
          }
          }

          if(isset($data->indigenous_child )){
            foreach ($data->indigenous_child as $item){
              $indigenous_child .= $item.",";
          }
          }

          if(isset($data->cultural_linguistic )){
            foreach ($data->cultural_linguistic as $item){
              $cultural_linguistic .= $item.",";
          }
          }

          if(isset($data->pa_addr_check_box) && $data->pa_addr_check_box == 'on'){
            $father_address = $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
          }
          else{
            $father_address = $data->pa_street.' '.$data->pa_city.' '.$data->pa_province;
          }

          if(isset($data->mo_addr_check_box) && $data->mo_addr_check_box == 'on'){
            $mother_address =  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
          }
          else{
            $mother_address = $data->mo_street.' '.$data->mo_city.' '.$data->mo_province;
          }
          if($type == 1 ){
            fputcsv($output,
            array(
              "Canadian",
                  $app->status??'',
                  $data->grade_in ?? ( $data->grade??''),
                  $data->stu_lname??'',
                  $data->stu_mname??'',
                  $data->stu_fname??'',
                  $data->stu_gender??'',
                  $data->stu_month_of_birth.'/'.$data->stu_date_of_birth.'/'.$data->stu_year_of_birth,
                  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province,
                  $data->stu_email??'',
                  $data->stu_phone??'',
                 $data->stu_citizenship_status??'',
                 $data->stu_lang??'',
                 $data->stu_curr_school??'',
                 $data->stu_city_country??'',
                 $data->father??'',
                 $data->pa_lname??'',
                 $data->pa_fname??'',
                 $father_address,
                 $data->pa_hphone??'',
                 $data->pa_mphone??'',
                 $data->pa_email??'',
                 $data->pa_occupation??'',
                 $data->pa_employer??'',
                 $data->mother??'',
                 $data->mo_lname??'',
                 $data->mo_fname??'',
                 $mother_address??'',
                 $data->mo_hphone??'',
                 $data->mo_mphone??'',
                 $data->mo_email??'',
                 $data->mo_occupation??'',
                 $data->mo_employer??'',
                 $data->marriage_status??'',
                 $data->custody_status??"",
                 $data->church??'',
                 $data->doctor_name??'',
                 $data->doctor_phone??"",
                 $data->health_card??'',
                 $data->immunization??'',
                 $medical_conditions,
                 $data->medical_detail??"",
                 $data->emergency_name??'',
                 $data->emergency_rela??'',
                 $data->emergency_phone??'',
                 $data->emergency_mobile??'',
                 $data->indigenous_declaration??'',
                 $indigenous_child,
                 $cultural_linguistic,

            )
          );
          }
          else{
            fputcsv($output,
            array(
              "International",
              $app->status??'',
              $data->int_grade??'',
              $data->stu_lname??'',
              $data->stu_mname??'',
              $data->stu_fname??'',
              $data->stu_gender??'',
              $data->stu_month_of_birth.'/'.$data->stu_date_of_birth.'/'.$data->stu_year_of_birth,
              $data->stu_street.' '.$data->stu_city.' '.$data->stu_province,
              $data->stu_email??'',
              $data->stu_phone??'',
             $data->stu_citizenship_status??'',
             $data->stu_lang??'',
             $data->stu_curr_school??'',
             $data->stu_city_country??'',
             $data->father??'',
             $data->pa_lname??'',
             $data->pa_fname??'',
             $father_address??'',
             $data->pa_hphone??'',
             $data->pa_mphone??'',
             $data->pa_email??'',
             $data->pa_occupation??'',
             $data->pa_employer??'',
             $data->mother??'',
             $data->mo_lname??'',
             $data->mo_fname??'',
             $mother_address??'',
             $data->mo_hphone??'',
             $data->mo_mphone??'',
             $data->mo_email??'',
             $data->mo_occupation??'',
             $data->mo_employer??'',
             $data->marriage_status??'',
             $data->custody_status??'',
             $data->church??'',
             $data->doctor_name??'',
             $data->doctor_phone??"",
             $data->health_card??'',
             $data->immunization??'',
             $medical_conditions,
             $data->medical_detail??"",
             $data->emergency_name??'',
             $data->emergency_rela??'',
             $data->emergency_phone??'',
             $data->emergency_mobile??'',
            )
          );
          }
        }
      }
    exit;
    }
    catch (Exception $e) {
      return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
    return false;
}
  }
  
  //Export All applications
  public function exportAllCSV($id){
    try{
      if($id =="all"){
        $canadian_applications = Applications::get()->where('status','!=' ,'Pending')->where('type','=',1);
        $international_applications = Applications::get()->where('status','!=' ,'Pending')->where('type','=',2);
      }
      else{
        $idsArray = explode(',', $id);
        $canadian_applications = Applications::where('type','=',1)->whereIn('id' ,$idsArray)->get();
        $international_applications = Applications::where('type','=',2)->whereIn('id' ,$idsArray)->get();
      }
  
      header('Content-Type: text/csv; charset=utf-8');
      header('Content-Disposition: attachment; filename=Applications.csv');
      $output = fopen('php://output', 'w');
      if(isset($canadian_applications)){
        // Setup Headers to Export the CSV
        fputcsv($output,array("Canadian Applications"));
        fputcsv($output, array('Type','Status','Grade','Student First Name','Student Middle Name','Student Last Name','Gender','Birthday','Address','Email','Home Phone','Citizenship / Immigration Status',
                                'Language Spoken at Home','Country of Birth','Current School','City and Country','Medical Conditions or Allergies','Parent/Guardian 2 Relationship','Parent/Guardian 1 Last Name ',
                                'Parent/Guardian 1 First Name','Parent/Guardian 1 Address','Parent/Guardian 1 Home Phone','Parent/Guardian 1 Mobile Phone','Parent/Guardian 1 Email','Parent/Guardian 1 Occupation',
                                  'Parent/Guardian 1 Employer','Parent/Guardian 2 Relationship','Parent/Guardian 2 Last Name ','Parent/Guardian 2 First Name','Parent/Guardian 2 Address','Parent/Guardian 2 Home Phone',
                                  'Parent/Guardian 2 Mobile Phone','Parent/Guardian 2 Email','Parent/Guardian 2 Occupation', 'Parent/Guardian 2 Employer','Parent Status','Student Lives With','Church Name','Reference Name',
                                  'Reference Phone','Relationship to Applicant','Heard From'));
  
        foreach ($canadian_applications as $app){
          $data = json_decode($app->meta);
          $heard_from='';
            $father_address='';
            $mother_address='';
            if(isset($data->heard_from )){
              foreach ($data->heard_from as $item){
                $heard_from .= $item.",";
            }
            }
            if(isset($data->pa_addr_check_box) && $data->pa_addr_check_box == 'on'){
              $father_address = $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
            }
            else{
              $father_address = $data->pa_street.' '.$data->pa_city.' '.$data->pa_province;
            }
  
            if(isset($data->mo_addr_check_box) && $data->mo_addr_check_box == 'on'){
              $mother_address =  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
            }
            else{
              $mother_address = $data->mo_street.' '.$data->mo_city.' '.$data->mo_province;
            }
          fputcsv($output,
          array(
            "Canadian",
            $app->status??'',
            $data->stu_lname??'',
            $data->stu_mname??'',
            $data->stu_fname??'',
            $data->stu_gender??'',
            $data->stu_month_of_birth.' '.$data->stu_date_of_birth.','.$data->stu_year_of_birth,
            $data->stu_street.' '.$data->stu_city.' '.$data->stu_province,
            $data->stu_email??'',
            $data->stu_phone??'',
           $data->stu_citizenship_status??'',
           $data->stu_lang??'',
           $data->stu_country_of_birth??'',
           $data->stu_curr_school??'',
           $data->stu_city_country??'',
           ($data->stu_allegies_details??'No'),
           $data->father??'',
           $data->pa_lname??'',
           $data->pa_fname??'',
           $father_address??'',
           $data->pa_addr_check_box??'',
           $data->pa_hphone??'',
           $data->pa_mphone??'',
           $data->pa_email??'',
           $data->pa_occupation??'',
           $data->pa_employer??'',
           $data->mother??'',
           $data->mo_lname??'',
           $data->mo_fname??'',
           $mother_address??'',
           $data->mo_hphone??'',
           $data->mo_mphone??'',
           $data->mo_email??'',
           $data->mo_occupation??'',
           $data->mo_employer??'',
           $data->marriage_status??'',
           $data->custody_status??"",
           $data->church??'',
           $data->ref_name??'',
           $data->ref_phone??"",
           $data->ref_rela??'',
           $heard_from
          )
        );
        }
        fputcsv($output,array("\r"));
        fputcsv($output,array("International Applications"));
      }
  
      if(isset($international_applications)){
        fputcsv($output, array('Type','Status','Grade','Student First Name','Student Middle Name','Student Last Name','Gender','Birthday','Address','Email','Home Phone','Citizenship / Immigration Status',
        'Language Spoken at Home','Country of Birth','Current School','City and Country','Medical Conditions or Allergies','Parent/Guardian 2 Relationship','Parent/Guardian 1 Last Name ',
        'Parent/Guardian 1 First Name','Parent/Guardian 1 Address','Parent/Guardian 1 Home Phone','Parent/Guardian 1 Mobile Phone','Parent/Guardian 1 Email','Parent/Guardian 1 Occupation', 
        'Parent/Guardian 1 Employer','Parent/Guardian 2 Relationship','Parent/Guardian 2 Last Name ','Parent/Guardian 2 First Name','Parent/Guardian 2 Address','Parent/Guardian 2 Home Phone',
        'Parent/Guardian 2 Mobile Phone','Parent/Guardian 2 Email','Parent/Guardian 2 Occupation', 'Parent/Guardian 2 Employer','Parent Status','Student Lives With','Church Name','Custodian Legal First Name',
        'Custodian Legal Middle Name','Custodian Legal Last Name','Commonly Goes by',' Custodian Date of Birth','Relationship to Applicant','Winnipeg Address','Custodian Home Phone','Custodian Alternate Phone',
        'Custodian Email','Heard From'));
  
        foreach ($international_applications as $app){
          $data = json_decode($app->meta);
          $heard_from='';
            $father_address='';
            $mother_address='';
            if(isset($data->heard_from )){
              foreach ($data->heard_from as $item){
                $heard_from .= $item.",";
            }
            }
            if(isset($data->pa_addr_check_box) && $data->pa_addr_check_box == 'on'){
              $father_address = $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
            }
            else{
              $father_address = $data->pa_street.' '.$data->pa_city.' '.$data->pa_province;
            }
  
            if(isset($data->mo_addr_check_box) && $data->mo_addr_check_box == 'on'){
              $mother_address =  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
            }
            else{
              $mother_address = $data->mo_street.' '.$data->mo_city.' '.$data->mo_province;
            }
          fputcsv($output,
          array(
            "International",
                $app->status??'',
                $data->int_grade??'',
                $data->stu_lname??'',
                $data->stu_mname??'',
                $data->stu_fname??'',
                $data->stu_gender??'',
                $data->stu_month_of_birth.' '.$data->stu_date_of_birth.','.$data->stu_year_of_birth,
                $data->stu_street.' '.$data->stu_city.' '.$data->stu_province,
                $data->stu_email??'',
                $data->stu_phone??'',
               $data->stu_citizenship_status??'',
               $data->stu_lang??'',
               $data->stu_country_of_birth??'',
               $data->stu_curr_school??'',
               $data->stu_city_country??'',
               ($data->stu_allegies_details??'No'),
               $data->father??'',
               $data->pa_lname??'',
               $data->pa_fname??'',
               $father_address??'',
               $data->pa_hphone??'',
               $data->pa_mphone??'',
               $data->pa_email??'',
               $data->pa_occupation??'',
               $data->pa_employer??'',
               $data->mother??'',
               $data->mo_lname??'',
               $data->mo_fname??'',
               $mother_address??'',
               $data->mo_hphone??'',
               $data->mo_mphone??'',
               $data->mo_email??'',
               $data->mo_occupation??'',
               $data->mo_employer??'',
               $data->marriage_status??'',
               $data->custody_status??'',
               $data->church??'',
               $data->custodian_fname??'',
               $data->custodian_mname??'',
               $data->custodian_lname??'',
               $data->custodian_cname??'',
               $data->custodian_birthday??'',
               $data->custodian_rela??'',
               $data->custodian_street . " ".$data->custodian_city." ".$data->custodian_province." ".$data->custodian_country." ".$data->custodian_postal,
               $data->custodian_phone??'',
               $data->custodian_alt_phone??'',
               $data->custodian_email??'',
               $heard_from
          )
        );
        }
      }
      exit; 
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
  }

  public function exportAllRegistrationsCSV($id){
    try{
    if($id =="all"){
      $canadian_registrations = Registrations::get()->where('status','!=' ,'Pending')->where('type','=',1);
      $international_registrations = Registrations::get()->where('status','!=' ,'Pending')->where('type','=',2);
    }
    else{
      $idsArray = explode(',', $id);
      $canadian_registrations = Registrations::where('type','=',1)->whereIn('id' ,$idsArray)->get();
      $international_registrations = Registrations::where('type','=',2)->whereIn('id' ,$idsArray)->get();
    }

    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=Registrations.csv');
    $output = fopen('php://output', 'w');
    if(isset($canadian_registrations)){
      // Setup Headers to Export the CSV
      fputcsv($output,array("Canadian Registrations"));
      fputcsv($output, array('Type','Status','Grade','Student First Name','Student Middle Name','Student Last Name','Gender','Birthday','Address','Email','Home Phone','Citizenship / Immigration Status',
      'Language Spoken at Home','Current School','City and Country','Parent/Guardian 2 Relationship','Parent/Guardian 1 Last Name ',
      'Parent/Guardian 1 First Name','Parent/Guardian 1 Address','Parent/Guardian 1 Home Phone','Parent/Guardian 1 Mobile Phone','Parent/Guardian 1 Email','Parent/Guardian 1 Occupation',
       'Parent/Guardian 1 Employer','Parent/Guardian 2 Relationship','Parent/Guardian 2 Last Name ','Parent/Guardian 2 First Name','Parent/Guardian 2 Address','Parent/Guardian 2 Home Phone',
       'Parent/Guardian 2 Mobile Phone','Parent/Guardian 2 Email','Parent/Guardian 2 Occupation', 'Parent/Guardian 2 Employer','Parent Status','Student Lives With','Church Name','Doctor name',
       'Doctor Phone','Manitoba Health Card','Immunizations Up To Date','Medical Conditions','Medical Details','Emergency Contact Name',"Relationship",'Emergency Contact Day Phone','Emergency Contact Mobile Phone','Indigenous Identity',
       "Description",'Cultural-linguistic Identity'));

      foreach ($canadian_registrations as $app){
        $data = json_decode($app->meta);
        $heard_from='';
          $father_address='';
          $mother_address='';
          $medical_conditions = '';
          $indigenous_child = '';
          $cultural_linguistic ='';
          if(isset($data->stu_medical )){
            foreach ($data->stu_medical as $item){
              $medical_conditions .= $item.",";
          }
          }

          if(isset($data->indigenous_child )){
            foreach ($data->indigenous_child as $item){
              $indigenous_child .= $item.",";
          }
          }

          if(isset($data->cultural_linguistic )){
            foreach ($data->cultural_linguistic as $item){
              $cultural_linguistic .= $item.",";
          }
          }
          if(isset($data->pa_addr_check_box) && $data->pa_addr_check_box == 'on'){
            $father_address = $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
          }
          else{
            $father_address = $data->pa_street.' '.$data->pa_city.' '.$data->pa_province;
          }

          if(isset($data->mo_addr_check_box) && $data->mo_addr_check_box == 'on'){
            $mother_address =  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
          }
          else{
            $mother_address = $data->mo_street.' '.$data->mo_city.' '.$data->mo_province;
          }
        fputcsv($output,
        array(
          "Canadian",
                  $app->status??'',
                  $data->grade_in ?? ( $data->grade??''),
                  $data->stu_lname??'',
                  $data->stu_mname??'',
                  $data->stu_fname??'',
                  $data->stu_gender??'',
                  $data->stu_month_of_birth.'/'.$data->stu_date_of_birth.'/'.$data->stu_year_of_birth,
                  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province,
                  $data->stu_email??'',
                  $data->stu_phone??'',
                 $data->stu_citizenship_status??'',
                 $data->stu_lang??'',
                 $data->stu_curr_school??'',
                 $data->stu_city_country??'',
                 $data->father??'',
                 $data->pa_lname??'',
                 $data->pa_fname??'',
                 $father_address,
                 $data->pa_hphone??'',
                 $data->pa_mphone??'',
                 $data->pa_email??'',
                 $data->pa_occupation??'',
                 $data->pa_employer??'',
                 $data->mother??'',
                 $data->mo_lname??'',
                 $data->mo_fname??'',
                 $mother_address??'',
                 $data->mo_hphone??'',
                 $data->mo_mphone??'',
                 $data->mo_email??'',
                 $data->mo_occupation??'',
                 $data->mo_employer??'',
                 $data->marriage_status??'',
                 $data->custody_status??"",
                 $data->church??'',
                 $data->doctor_name??'',
                 $data->doctor_phone??"",
                 $data->health_card??'',
                 $data->immunization??'',
                 $medical_conditions,
                 $data->medical_detail??"",
                 $data->emergency_name??'',
                 $data->emergency_rela??'',
                 $data->emergency_phone??'',
                 $data->emergency_mobile??'',
                 $data->indigenous_declaration??'',
                 $indigenous_child,
                 $cultural_linguistic,
        )
      );
      }
      fputcsv($output,array("\r"));
      fputcsv($output,array("International Registrations"));
    }

    if(isset($international_registrations)){
      fputcsv($output, array('Type','Status','Grade','Student First Name','Student Middle Name','Student Last Name','Gender','Birthday','Address','Email','Home Phone','Citizenship / Immigration Status',
      'Language Spoken at Home','Current School','City and Country','Parent/Guardian 2 Relationship','Parent/Guardian 1 Last Name ',
      'Parent/Guardian 1 First Name','Parent/Guardian 1 Address','Parent/Guardian 1 Home Phone','Parent/Guardian 1 Mobile Phone','Parent/Guardian 1 Email','Parent/Guardian 1 Occupation', 
      'Parent/Guardian 1 Employer','Parent/Guardian 2 Relationship','Parent/Guardian 2 Last Name ','Parent/Guardian 2 First Name','Parent/Guardian 2 Address','Parent/Guardian 2 Home Phone',
      'Parent/Guardian 2 Mobile Phone','Parent/Guardian 2 Email','Parent/Guardian 2 Occupation', 'Parent/Guardian 2 Employer','Parent Status','Student Lives With','Church Name','Doctor name',
      'Doctor Phone','Manitoba Health Card','Immunizations Up To Date','Medical Conditions','Medical Details','Emergency Contact Name',"Relationship",'Emergency Contact Day Phone','Emergency Contact Mobile Phone'));

      foreach ($international_registrations as $app){
        $data = json_decode($app->meta);
        $heard_from='';
          $father_address='';
          $mother_address='';
          $medical_conditions = '';
          $indigenous_child = '';
          $cultural_linguistic ='';
          if(isset($data->stu_medical )){
            foreach ($data->stu_medical as $item){
              $medical_conditions .= $item.",";
          }
          }
          if(isset($data->pa_addr_check_box) && $data->pa_addr_check_box == 'on'){
            $father_address = $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
          }
          else{
            $father_address = $data->pa_street.' '.$data->pa_city.' '.$data->pa_province;
          }

          if(isset($data->mo_addr_check_box) && $data->mo_addr_check_box == 'on'){
            $mother_address =  $data->stu_street.' '.$data->stu_city.' '.$data->stu_province;
          }
          else{
            $mother_address = $data->mo_street.' '.$data->mo_city.' '.$data->mo_province;
          }
        fputcsv($output,
        array(
          "International",
              $app->status??'',
              $data->int_grade??'',
              $data->stu_lname??'',
              $data->stu_mname??'',
              $data->stu_fname??'',
              $data->stu_gender??'',
              $data->stu_month_of_birth.'/'.$data->stu_date_of_birth.'/'.$data->stu_year_of_birth,
              $data->stu_street.' '.$data->stu_city.' '.$data->stu_province,
              $data->stu_email??'',
              $data->stu_phone??'',
             $data->stu_citizenship_status??'',
             $data->stu_lang??'',
             $data->stu_curr_school??'',
             $data->stu_city_country??'',
             $data->father??'',
             $data->pa_lname??'',
             $data->pa_fname??'',
             $father_address??'',
             $data->pa_hphone??'',
             $data->pa_mphone??'',
             $data->pa_email??'',
             $data->pa_occupation??'',
             $data->pa_employer??'',
             $data->mother??'',
             $data->mo_lname??'',
             $data->mo_fname??'',
             $mother_address??'',
             $data->mo_hphone??'',
             $data->mo_mphone??'',
             $data->mo_email??'',
             $data->mo_occupation??'',
             $data->mo_employer??'',
             $data->marriage_status??'',
             $data->custody_status??'',
             $data->church??'',
             $data->doctor_name??'',
             $data->doctor_phone??"",
             $data->health_card??'',
             $data->immunization??'',
             $medical_conditions,
             $data->medical_detail??"",
             $data->emergency_name??'',
             $data->emergency_rela??'',
             $data->emergency_phone??'',
             $data->emergency_mobile??'',
        )
      );
      }
    }
    exit; 
  }
  catch (Exception $e) {
      return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
      return false;
  }
}
}
