<?php

namespace App\Http\Controllers;

use App\Posts;
use App\PostsCategories;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Auth;

class PublicPostsController extends Controller
{
  private $visible;

  public function index(){
    $this->check_login();
    $post_items = Posts::orderBy('created_at','desc')->whereRaw($this->visible)->paginate(5);
    $categories = PostsCategories::get();
    return view('theme.posts', compact('post_items','categories'));
  }

  public function category($cat_slug){
    $this->check_login();
    $category = PostsCategories::where('slug', '=', $cat_slug)->first();
    if($category){
        $post_items = Posts::where('category_id', '=', $category->id)->whereRaw($this->visible)->orderBy('created_at','desc')->paginate(5);
        $categories = PostsCategories::get();
        return view('theme.posts', compact('post_items','categories','cat_slug'));
    }else{
        return $this->single($cat_slug);
    }
  }

  public function page($page){
    $this->check_login();
    Paginator::currentPageResolver(function() use ($page) {
      return $page;
    });

    $post_items = Posts::orderBy('created_at','desc')->whereRaw($this->visible)->paginate(5);
    $categories = PostsCategories::get();
    return view('theme.posts', compact('post_items','categories'));
  }

  public function categoryPage($cat_slug,$page){
    $this->check_login();
    Paginator::currentPageResolver(function() use ($page) {
      return $page;
    });

    $category = PostsCategories::where('slug', '=', $cat_slug)->first();
    $post_items = Posts::where('category_id', '=', $category->id)->whereRaw($this->visible)->orderBy('created_at','desc')->paginate(5);
    $categories = PostsCategories::get();
    return view('theme.posts', compact('post_items','categories','cat_slug'));
  }

  public function single($slug){
    $this->check_login();
    $post = Posts::where('slug', '=', $slug)->whereRaw($this->visible)->first();
    // This is for the layout to show seo data.
    $page = Posts::where('slug', '=', $slug)->whereRaw($this->visible)->first();
    $categories = PostsCategories::get();
    $other_posts = Posts::orderBy('created_at','desc')->paginate(3);

    if(!$post){
        return redirect()->route('public.posts');
        die;
    }
    return view('theme.posts-single', compact('post','categories','other_posts','page'));
  }

  private function check_login(){
      if (Auth::guard('admin')->check()){
          $this->visible = "visible IS NOT NULL";
      }else{
          $this->visible = "visible = 'published' ";
      }
  }

}
