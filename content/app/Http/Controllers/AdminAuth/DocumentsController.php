<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Documents;
use Storage;
use Crypt;
use Exception;


class DocumentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function documents()
    {
        $documents = Documents::orderBy('created_at','desc')->get();
        return view('admin.modules.documents.index', compact('documents'));
    }

    public function downloadDocument($id){
        ini_set('memory_limit', '-1');
        try{
        $documents = Documents::get()->where('id',$id);
        
       foreach($documents as $item){
        $file = decrypt(Storage::get($item->path));
      
        return response()->streamDownload(function() use ($file,$item) {
            ob_start();
            header('Content-Type: application/{$item->file_mime}');
            echo $file;
            ob_end_flush();
            exit;
        }, $item->name);
       }
      }
      catch (Exception $e) {
          return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
          return false;
      }
      }
      
      public function uploadDocument(Request $request){
        ini_set('memory_limit', '-1');
        try{
        $target_dir = 'Upload_Files/Documents/';
        if ($request->hasfile('document')) {
                $file = $request->file('document');
                $name = $file->getClientOriginalName();
                $new_file_name =  SHA1(rand());
                    Storage::put($target_dir . $new_file_name, Crypt::encrypt($file->get()));
                    $document = Documents::updateOrCreate([
                        'path' => $target_dir . $new_file_name,
                        'name' => $name,
                        'size' => $this->getFileSize($file->getsize()),
                        'mime' => $file->getMimeType(),
                    ]);
                    $saved = $document->save();
                    if($saved){
                        return redirect()->route('admin.documents')->with('success', 'Document Uploaded!');
                    }
                    else{
                        return redirect()->route('admin.documents')->with('error', 'Document Was Not Uploaded!');
                    }
        }
        else{
            return redirect()->route('admin.documents')->with('error', 'Please select a file!');
        }
      }
      catch (Exception $e) {
          return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
          return false;
      }
        
      }


    public function delete(Request $request)
    {
      $doc = Documents::where('id', '=', request('id'))->first();
      if(!$doc){
          die;
      }

      if($doc->path != null){
        $old_path = $doc->path;
        Storage::delete($old_path);
      }

      Documents::where('id', '=', request('id'))->delete();
      // No return this function is called by ajax.
    }

    public function getFileSize($size)
    {
    // Bytes
    $file_size = $size . ' Bytes';

    if ($file_size >= 1000) {
        // Kilobytes
        $file_size = round($size / 1024, 1) . ' KB';
    }

    if ($file_size >= 1000) {
        // Megabytes
        $file_size = round($size / 1024 / 1024, 1) . ' MB';
    }

    return $file_size;
}
}
