<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Applications;
use App\FileUploads;
use App\Invoices;
use App\Payments;
use App\Registrations;
use App\Ultilities;
use Password;
use Mail;
use ZipArchive;
use Auth;
use Storage;
use Crypt;
use Exception;

class ApplicationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    protected function broker()
    {
      return Password::broker('users');
    }

    public function applications()
    {
        $applications = Applications::orderBy('submitted_at','desc')->get()->where('status','!=','Pending');
         $cut_off = Ultilities::get()->first();
         if($cut_off){
            $cut_off_date =  $cut_off->cut_off_date;
         }
         else{
            $cut_off_date = "";
         }
         
        $applications = json_decode($applications);
        return view('admin.admin_auth.modules.applications.index', compact('applications', 'cut_off_date'));
    }

    public function viewApplication($id){
      $cut_off='';
      $application = Applications::where('id', '=', $id)->first();
      $file_uploads = FileUploads::get()->where('application_id','=',$id);
      $invoices = Invoices::get()->where('application_id','=',$id);
      $payment = Payments::get()->where('application_id','=',$id)->first();
      $payment = json_decode($payment);
      $data =json_decode($application->meta);
      if(!$data){
          return redirect()->route('user.dashboard');
          die;
      }
      $step=6;
      $type=$data->type_id;
      $id=$id;
      $file_names=[];
      foreach($file_uploads as $file){
        $file_names[$file->field_name]= $file->file_name;
    }
      return view('admin.admin_auth.modules.applications.application-view', compact('application','data','step','type','id','file_names','cut_off','payment','invoices'));
    }

    public function create($id)
    {
        $cut_off='';
        $file_names = [];
        $type=$id;
        return view('admin.admin_auth.modules.applications.application-create', compact('type','file_names','cut_off'));
    }

    public function editApplication($id)
    {
        $cut_off='';
        $application = Applications::findOrFail(request('id'));
        $data =json_decode($application->meta);
        $file_uploads = FileUploads::get()->where('application_id','=',$id);
        $file_names = [];
        if(!$data){
            return redirect()->route('admin.applications');
            die;
        }
        foreach($file_uploads as $file){
          $file_names[$file->field_name]= $file->file_name;
        }
        $step=2;
        $edit=1;
        $type=$data->type_id;
        return view('admin.admin_auth.modules.applications.application-edit', compact('application', 'data','step','type','edit','file_names','id','cut_off'));
    }

    public function print($id)
    {
    try{
      $paid = '';
      $cut_off ='';
      $application = Applications::where('id', '=', $id)->first();
      $file_uploads = FileUploads::get()->where('application_id','=',$id);
      $data =json_decode($application->meta);
      if(!$data){
          return redirect()->route('user.dashboard');
          die;
      }
      $type=$data->type_id;
      $id=$id;
      $file_names=[];
      foreach($file_uploads as $file){
        $file_names[$file->field_name]= $file->file_name;
    }
      return view('auth.user_auth.modules.application.application-print', compact('application','data','type','id','file_names','paid','cut_off'));
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    public function save(Request $request)
    {
        try{
        unset($request['_token']);
        $current_date_time = date('Y-m-d H:i:s');
        $data1 = json_encode($request->all());
        $data = json_decode($data1);
        if (isset($data->form_id)) {
            $application = Applications::findOrFail($data->form_id);
            $application->meta = $data1;
            $application->save();
            $this->fileUpload($request, $application);
        } else {
            $application = Applications::create([
                'meta' => $data1,
                'status' => 'Submitted',
                'type' => $data->type_id,
                'user_id' => Auth::id(),
                'submitted_at' => $current_date_time,
            ]);
            $application->save();

            $this->fileUpload($request, $application);
            $payment = Payments::create([
                'method' => $data->payment??'',
                'payment_status' => 'approved',
                'application_id'=>$application->id
            ]);
            $payment->save();
       }
        return redirect()->route('admin.application.view', $application->id);
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    public function delete(Request $request)
    {
        try{
        $app = Applications::findOrFail(request('id'));
        $file_uploads = FileUploads::get()->where('application_id',request('id'));
        $file_data = json_decode($file_uploads);
        foreach ($file_data as $fd) {
            $old_path = $fd->file_path;
            Storage::delete($old_path);
        }
        if($app->registration_id){
            $reg = Registrations::findOrFail($app->registration_id);
            $reg_file_uploads = FileUploads::get()->where('registration_id',$reg->id);
            $reg_file_data = json_decode($reg_file_uploads);
            foreach ($reg_file_data as $fd) {
                $old_path = $fd->file_path;
                Storage::delete($old_path);
            }
            $reg_file_uploads->each->delete();
            $reg->delete();
        }
        $file_uploads->each->delete();
		$app->delete();

        return redirect()->route('admin.applications')->with('success', 'Application Deleted!');
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    public function updateStatus()
    {
        $app = Applications::find(request('id'));
        $app->status=request('value');
         $saved = $app->save();
         if($saved){
            if(request('value') == "Accepted"){
                $content = '<h4>Your Application Was Accepted.</h4>';
                $content .= '<p>Please login to your account to review and start the Registration process.</p>';
                $content .= '<p class="footer">Here is a link to login your account: '.route('user.login').'</p>';
    
                $title="Application Status Updated";
                $subject="Application Was Accepted";

                $user = User::find($app->user_id);
                $email = $user->email;
                $this->sendNotificationEmail($content,$title,$email,$subject);
            }
            return redirect()->route('admin.application.view',request('id'))->with('success', 'Status Updated!');
         }else{
            return redirect()->route('admin.application.view',request('id'))->with('error', 'Status Was Not Updated, Please Try Again!');
         }
    }

    public function setInterviewDate()
    {
        $app = Applications::find(request('id'));
        $app->interview_date=request('interview_date');
        $app->save();
        return redirect()->route('admin.application.view',request('id'))->with('success', 'Interview Date Updated!');
    }

    public function setCutOff()
    {
        try{
        $cut_off = Ultilities::get()->all();
        if($cut_off){
            foreach($cut_off as $cut){
                $cut->cut_off_date=request('cut_off');
                $cut->save();
            }
        }
        else{
            $new_cut_off = Ultilities::create([
                'cut_off_date'=>request('cut_off'),
            ]);
            $new_cut_off->save();
        }
        $apps = Applications::get()->all();
        foreach($apps as $app){
            if( $app->cut_off_date == null || $app->cut_off_date  > request('cut_off') ){
                $app->cut_off_date=request('cut_off');
                $app->save();
            }
        }
         return redirect()->route('admin.applications')->with('success', 'Cut-Off Date Updated!');
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    private function sendNotificationEmail($content, $title, $email,$subject)
    {
        $data = [
            'email' => $email,
            'title' => $title,
            'subject' => $subject,
            'content' => $content
        ];

        Mail::send('theme.email.general', $data , function($message) use ($data)
        {
            $message->to($data['email'], $data['email'])
                   ->from(config('mail.from.address'), 'VSM')
                   ->subject($data['subject'])
                   ->getHeaders()
                   ->addTextHeader('Reply-to', config('mail.reply_to.address'));
        });
    }

    public function uploadInvoice(Request $request, $id){
        ini_set('memory_limit', '-1');
        try{
        $target_dir = 'Upload_Files/Applications/Invoices/';
        $app = Applications::find($id);
        if ($request->hasfile('application_invoice')) {
                $file = $request->file('application_invoice');
                $name = $file->getClientOriginalName();
                $new_file_name =  SHA1(rand());
                    Storage::put($target_dir . $new_file_name, Crypt::encrypt($file->get()));
                    $invoice = Invoices::updateOrCreate([
                        'application_id' => $id,
                        'path' => $target_dir . $new_file_name,
                        'name' => $name,
                        'file_mime' => $file->getMimeType(),
                        'size' => $this->getFileSize($file->getsize()),
                        'user_id'=> $app->user_id,
                    ]);
                    $saved = $invoice->save();
                    if($saved){
                        $content = '<h4>An Important Document Was Uploaded.</h4>';
                        $content .= '<p>Please login to your account to review..</p>';
                        $content .= '<p class="footer">Here is a link to login your account: '.route('user.login').'</p>';
            
                        $title="Important Document ";
                        $subject="Important Document ";
        
                        $user = User::find($app->user_id);
                        $email = $user->email;
                        $this->sendNotificationEmail($content,$title,$email,$subject);
                        return redirect()->route('admin.application.view',$id)->with('success', 'Invoice Uploaded!');
                    }
                    else{
                        return redirect()->route('admin.application.view',$id)->with('error', 'Invoice Was Not Uploaded!');
                    }
        }
        else{
            return redirect()->route('admin.application.view',$id)->with('error', 'Please select a file!');
        }
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
        
    }

    public function fileUpload($request, $application){
        ini_set('memory_limit', '-1');
        try{
        $target_dir = 'Upload_Files/Applications/';
        if ($request->hasfile('file_uploads')) {
            foreach ($request->file('file_uploads') as $file => $val) {
                $name = $val->getClientOriginalName();
                $new_file_name =  SHA1(rand());
                $exist = FileUploads::get()->where('application_id', '=', $application->id)->where('field_name', '=', $file);
                $file_data = json_decode($exist);
                if ($exist->count() == 1) {
                    foreach ($file_data as $fd) {
                        $file_upload = FileUploads::findOrFail($fd->id);
                        $old_path = $fd->file_path;
                        Storage::delete($old_path);
                        $file_upload->field_name = $file;
                        $file_upload->file_path = $target_dir . $new_file_name;
                        $file_upload->file_name = $name;
                        $file_upload->file_mime = $val->getMimeType();
                        $file_upload->file_size = $this->getFileSize($val->getsize());
                        Storage::put($target_dir . $new_file_name, Crypt::encrypt($val->get()));
                        $file_upload->save();
                    }
                } else {
                    Storage::put($target_dir . $new_file_name, Crypt::encrypt($val->get()));
                    $file_upload = FileUploads::updateOrCreate([
                        'field_name' => $file,
                        'application_id' => $application->id,
                        'file_path' => $target_dir . $new_file_name,
                        'file_name' => $name,
                        'file_mime' => $val->getMimeType(),
                        'file_size' => $this->getFileSize($val->getsize()),
                    ]);
                    $file_upload->save();
                }
            }
        }
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    public function getFileSize($size)
    {
        // Bytes
        $file_size = $size . ' Bytes';

        if ($file_size >= 1000) {
            // Kilobytes
            $file_size = round($size / 1024, 1) . ' KB';
        }

        if ($file_size >= 1000) {
            // Megabytes
            $file_size = round($size / 1024 / 1024, 1) . ' MB';
        }

        return $file_size;
    }

    public function download($id ,$field_name){
        ini_set('memory_limit', '-1');
        try{
            $FileUploads = FileUploads::get()->where('field_name',$field_name)->where('application_id',$id);
        
            foreach($FileUploads as $item){
             $file = decrypt(Storage::get($item->file_path));
     
             return response()->streamDownload(function() use ($file,$item) {
                 ob_start();
                 header('Content-Type: application/{$item->file_mime}');
                 echo $file;
                 ob_end_flush();
              
             }, $item->file_name);
            }
            exit;
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"File not found!");
        return false;
    }
        
    }

    public function downloadAll($id){
    $FileUploads = FileUploads::get()->where('application_id',$id);
    if($FileUploads->isEmpty()){
        return redirect()->route('admin.application.view', $id)->with('error','No attachments found!');
    }
    else{
        foreach($FileUploads as $item){
            $file = decrypt(Storage::get($item->file_path));
    
            return response()->streamDownload(function() use ($file,$item) {
                ob_start();
                header('Content-Type: application/{$item->file_mime}');
                echo $file;
                ob_end_flush();
                exit;
            }, $item->file_name);
           }
    }     
    }
}
