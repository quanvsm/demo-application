<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\Page;

class TeamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->target_dir = public_path().env('APP_TEAM');
    }

    public function create(Request $request){
        $team = Team::create([
            'name' => request('name'),
            'template_id' => intval(request('template_id')),
        ]);
        $max = Team::where('template_id', '=', request('template_id'))->max('sort');
        $team->sort = $max+1;
        $team->save();

        return redirect()->route('admin.team.edit', ['id' => $team->id]);
    }

    public function edit($id){
        $team = Team::findOrFail($id);
        $page = Page::where('template_id', '=', $team->template_id)->first();
        if($page->parent_id){
          $parent_page = Page::findOrFail($page->parent_id);
        }
        return view('admin.modules.pages.team.edit', compact('team','page','parent_page'));
    }

    public function save(Request $request){
        $team = Team::findOrFail(request('id'));
        $team->update($request->all());

        if(request('remove_image')){
            if(file_exists($this->target_dir.$team->image)){
                unlink($this->target_dir.$team->image);
                $team->image = null;
                $team->image_mime = null;
            }
        }

        if($request->hasFile('image')) {
            $image = $request->image;
            $image_mime =  $image->getMimeType();
            $file_name = 'team-'.$team->id.'.'.$image->getClientOriginalExtension();

            $image->move($this->target_dir, $file_name);
            $team->image = $file_name;
            $team->image_mime = $image_mime;
        }

        $team->save();

        return redirect()->route('admin.team.edit', ['id' => $team->id])->with('success', 'The Team Member is Saved.');
    }

    public function delete(Request $request){
        $team = Team::where('id', '=', request('id'))->first();

        if(!$team){
            die;
        }

        if($team->image != null){
            if(file_exists($this->target_dir.$team->image)){
                unlink($this->target_dir.$team->image);
            }
        }

        Team::where('id', '=', request('id'))->delete();
        // No return this function is called by ajax.
    }
}
