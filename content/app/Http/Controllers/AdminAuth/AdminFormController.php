<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\SettingsEditRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Form;
use Mail;
use Auth;

class AdminFormController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->target_dir = public_path().env('APP_POST');
    }

    public function posts()
    {
        $post_items = Posts::orderBy('created_at','desc')->get();
        $categories = PostsCategories::get();
        $helper = new PrivateHelper;
        return view('admin.modules.posts.index', compact('post_items','categories','helper'));
    }

    public function postCat($slug)
    {
        $post_items = Posts::join('posts_categories', 'posts.category_id', '=', 'posts_categories.id')
                                ->where('posts_categories.slug','=',$slug)
                                ->orderBy('posts.created_at','desc')
                                ->get();

        $categories = PostsCategories::get();
        $helper = new PrivateHelper;
        return view('admin.modules.posts.index', compact('post_items','categories','slug','helper'));
    }

    public function create(PostsRequest $request)
    {
      $post = Posts::create([
          'title' => request('title'),
          'slug' => preg_replace('/[^A-Za-z0-9\_\-]/', '', strtolower(request('slug'))),
          'visible' => 'published',
      ]);
      $post->sort = $post->id;
      $post->save();

      return redirect()->route('admin.posts.edit', ['id' => $post->id]);
    }

    public function edit($id)
    {
      $post = Posts::where('id', '=', $id)->first();
      $categories = PostsCategories::get();

      if(!$post){
          return redirect()->route('admin.posts');
          die;
      }

      return view('admin.modules.posts.edit', compact('post','categories'));
    }

    public function save(PostsRequest $request)
    {
      $post = Posts::where('id', '=', request('id'))->first();

      if(!$post){
          return redirect()->route('admin.posts');
          die;
      }

      $post->title = request('title');
      $post->slug = preg_replace('/[^A-Za-z0-9\_\-]/', '', strtolower(request('slug')));
      $post->content = request('content');
      $post->excerpt = request('excerpt');
      $post->seo_title = request('seo-title');
      $post->seo_description = request('seo-description');
      $post->visible = request('visible');
      $post->created_at = Carbon::parse(request('created_at'));
      $post->save();

      if (request('category') != 'default') {
          $post->category_id = request('category');
          $post->save();
      }elseif(request('category') == 'default'){
          $post->category_id = null;
          $post->save();
      }

      if(request('remove_img')){
          if(file_exists($this->target_dir.$post->img)){
              unlink($this->target_dir.$post->img);
              $post->img = null;
              $post->save();
          }
      }

      if($request->hasFile('img')) {

          if($post->img != null){
              if(file_exists($this->target_dir.$post->img)){
                  unlink($this->target_dir.$post->img);
              }
          }

          $img = $request->img;
          $file_name = 'post-'.$post->id.'.'.$img->getClientOriginalExtension();
          $img->move($this->target_dir, $file_name);
          $post->img = $file_name;
          $post->save();
      }

      return redirect()->route('admin.posts.edit', ['id' => $post->id])->with('success', 'Post Item Saved.');
    }

    public function delete(Request $request)
    {
      $post = Posts::where('id', '=', request('id'))->first();
      if(!$post){
          die;
      }

      if($post->img != null){
          if(file_exists($this->target_dir.$post->img)){
              unlink($this->target_dir.$post->img);
          }
      }

      Posts::where('id', '=', request('id'))->delete();
      // No return this function is called by ajax.
    }

    public function deletePost(Request $request){
        $post = Posts::where('id', '=', request('id'))->first();
        if(!$post){
            return redirect()->route('admin.posts');
            die;
        }

        if($post->img != null){
            if(file_exists($this->target_dir.$post->img)){
                unlink($this->target_dir.$post->img);
            }
        }

        Posts::where('id', '=', request('id'))->delete();
        return redirect()->route('admin.posts')->with('success', 'Post item Deleted!');
    }

}
