<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\SettingsEditRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Password;
use Mail;
use Auth;

class AdminController extends Controller
{
    /**
     * This class is responsible for handling password reset emails.
     */
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * SendsPasswordResetEmails class needs this to know which table to look at.
     */
    protected function broker()
    {
      return Password::broker('admins');
    }

    public function admin()
    {
        $admins = Admin::get();
        return view('admin.admin_auth.modules.admins.index', compact('admins'));
    }

    public function newAdmin(){
        return view('admin.admin_auth.modules.admins.form');
    }

    public function createAdmin(AdminRequest $request){

        $admin = Admin::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => strtolower(request('email')),
            'active' => 'no',
        ]);

        $admin->hash = hash('haval160,4',time().rand(72,745).'xzy',false);
        $admin->save();
        $this->sendNotificationEmail($admin);

        return redirect()->route('admin.admins.edit', ['id' => $admin->id])->with('success', 'Admin Activation email Sent!');
    }

    private function sendNotificationEmail($admin)
    {
        $content = '<p>Welcome, please click the link below to activate your account.</p>';
        $content .= '<p><a href="'.route('admin.account', $admin->hash).'" class="button" target="_blank">Activate Account</a></p>';
        $content .= '<p class="footer">You can also activate your account at '.route('admin.account', $admin->hash).'</p>';

        $data = [
            'email' => $admin->email,
            'title' => 'Admin Activation',
            'subject' => 'Account Activation',
            'content' => $content
        ];

        Mail::send('theme.email.general', $data , function($message) use ($data)
        {
            $message->to($data['email'], $data['email'])
                   ->from(config('mail.from.address'), 'VSM')
                   ->subject($data['subject'])
                   ->getHeaders()
                   ->addTextHeader('Reply-to', config('mail.reply_to.address'));
        });
    }

    public function reSendActivationEmail(Request $request){
        $admin = Admin::findOrFail(request('id'));

        if($admin->active=='no' && !empty($admin->hash) && empty($admin->password)){
            $this->sendNotificationEmail($admin);
            echo '1';
        }else{
            echo '2';
        }

        // No return this function is called by ajax.
    }

    public function editAdmin($id)
    {
        if($id == Auth::id()){
            return redirect()->route('admin.settings');
        }else{
            $admin = Admin::findOrFail($id);
            return view('admin.admin_auth.modules.admins.form', compact('admin'));
        }
    }

    public function saveAdmin(AdminRequest $request){

        $admin = Admin::findOrFail(request('id'));
        $admin->first_name = request('first_name');
        $admin->last_name = request('last_name');
        $admin->active = request('active');

        if($request->email != $admin->email){
            $admin->email = strtolower(request('email'));
        }

        $admin->save();

        return redirect()->route('admin.admins.edit', ['id' => $admin->id])->with('success', 'Admin Updated.');
    }

    public function settings(){
        $admin = Admin::findOrFail(Auth::id());
        return view('admin.admin_auth.modules.admins.settings', compact('admin'));
    }

    public function saveSettings(SettingsEditRequest $request){

        $admin = Admin::findOrFail(Auth::id());
        $admin->first_name = request('first_name');
        $admin->last_name = request('last_name');
        $admin->active = request('active');

        if($request->email != $admin->email){
            $admin->email = strtolower(request('email'));
        }

        if($request->password){
            $admin->password = bcrypt($request->password);
        }

        $admin->save();

        return redirect()->route('admin.settings')->with('success', 'Admin Updated.');
    }

    public function delete(Request $request){
        Admin::where('id', '=', request('id'))->delete();
        // No return this function is called by ajax.
    }

    public function deleteAdmin(Request $request){
        Admin::where('id', '=', request('id'))->delete();
        return redirect()->route('admin.admins')->with('success', 'Admin Deleted!');
    }

}
