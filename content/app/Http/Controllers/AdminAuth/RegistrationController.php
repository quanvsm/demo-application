<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Applications;
use App\Registrations;
use App\FileUploads;
use App\Invoices;
use App\Http\Requests\RegistrationRequest;
use App\Payments;
use App\Ultilities;
use Password;
use Mail;
use ZipArchive;
use Auth;
use Storage;
use Crypt;
use Exception;

class RegistrationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    protected function broker()
    {
      return Password::broker('users');
    }

    public function registrations()
    {
        $registrations = Registrations::orderBy('submitted_at','desc')->get()->where('status','!=','Pending');
        
        $registrations = json_decode($registrations);
        return view('admin.admin_auth.modules.registrations.index', compact('registrations'));
    }

    public function viewRegistration($id){
      $registration = Registrations::where('id', '=', $id)->first();
      $application = Applications::find($registration->application_id);
      $application_data = json_decode($application->meta);
      $invoices = Invoices::get()->where('registration_id','=',$id);
      $file_uploads = FileUploads::get()->where('registration_id','=',$id);
      $payment = Payments::get()->where('registration_id','=',$id)->first();
      $payment = json_decode($payment);
      $academic_year = $this->nextAcademicYear();
      $registration_data =json_decode($registration->meta);
      if(!$registration_data){
          return redirect()->route('user.dashboard');
          die;
      }
      $type=$registration->type;
      return view('admin.admin_auth.modules.registrations.registration-view', compact('registration','registration_data','application_data','file_uploads','type','id','payment','invoices','academic_year'));
    }

    public function editRegistration($id)
    {
      $registration = Registrations::where('id', '=', $id)->first();
      $application = Applications::find($registration->application_id);
      $application_data = json_decode($application->meta);
      $file_uploads = FileUploads::get()->where('registration_id','=',$id);
      $payment = Payments::get()->where('registration_id','=',$id)->first();
      $payment = json_decode($payment);
      $registration_data =json_decode($registration->meta);
      if(!$registration_data){
          return redirect()->route('user.dashboard');
          die;
      }
      $type=$registration->type;
      return view('admin.admin_auth.modules.registrations.registration-edit', compact('registration','registration_data','application_data','file_uploads','type','id','payment'));
    }

    public function save(RegistrationRequest $request)
    {
        unset($request['_token']);
        $data1 = json_encode($request->all());
        $data = json_decode($data1);
        $registration = Registrations::find($data->form_id);
        $registration->meta = $data1;
        $registration->save();

        $this->fileUpload($request, $registration);

        return redirect()->route('admin.registration.view', $registration->id);
    }

    public function updateStatus()
    {
        $app = Registrations::find(request('id'));
        $app->status=request('value');
        $app->save();
        return redirect()->route('admin.registration.view',request('id'))->with('success', 'Status Updated!');
    }

    public function delete(Request $request)
    {
            $app = Registrations::find(request('id'));
            $application = Applications::find($app->application_id);
            $file_uploads = FileUploads::get()->where('registration_id',request('id'));
            $file_data = json_decode($file_uploads);
            foreach ($file_data as $fd) {
                $old_path = $fd->file_path;
                Storage::delete($old_path);
            }
            $file_uploads->each->delete();
            $application->registration_id = null;
            $application->save();
            $app->delete();
    
            return redirect()->route('admin.registrations')->with('success', 'Registration Deleted!');
    }

    public function uploadInvoice(Request $request, $id){
        ini_set('memory_limit', '-1');
        $target_dir = 'Upload_Files/Registrations/Invoices/';
        $user = User::find($id);
        if ($request->hasfile('registration_invoice')) {
                $file = $request->file('registration_invoice');
                $name = $file->getClientOriginalName();
                $new_file_name =  SHA1(rand());
                    Storage::put($target_dir . $new_file_name, Crypt::encrypt($file->get()));
                    $invoice = Invoices::updateOrCreate([
                        'path' => $target_dir . $new_file_name,
                        'name' => $name,
                        'file_mime' => $file->getMimeType(),
                        'size' => $this->getFileSize($file->getsize()),
                        'user_id'=> $id,
                    ]);
                    $saved = $invoice->save();
                    if($saved){
                        $registrations = Registrations::get()->where('user_id',$id)->where('status','=','Submitted');
                        foreach($registrations as $reg){
                            $reg->status = "Complete";
                            $reg->save();
                        }

                        $content = '<h4>An Important Document Was Uploaded.</h4>';
                        $content .= '<p>Please login to your account to review..</p>';
                        $content .= '<p class="footer">Here is a link to login your account: '.route('user.login').'</p>';
            
                        $title="Important Document ";
                        $subject="Important Document ";
        
                        $email = $user->email;
                        $this->sendNotificationEmail($content,$title,$email,$subject);
                        return redirect()->route('admin.users.edit',$id)->with('success', 'Invoice Uploaded!');
                    }
                    else{
                        return redirect()->route('admin.users.edit',$id)->with('error', 'Invoice Was Not Uploaded!');
                    }
        }
        else{
            return redirect()->route('admin.users.edit',$id)->with('error', 'Please select a file!');
        }
    }

    private function sendNotificationEmail($content, $title, $email,$subject)
    {
        $data = [
            'email' => $email,
            'title' => $title,
            'subject' => $subject,
            'content' => $content
        ];

        Mail::send('theme.email.general', $data , function($message) use ($data)
        {
            $message->to($data['email'], $data['email'])
                   ->from(config('mail.from.address'), 'VSM')
                   ->subject($data['subject'])
                   ->getHeaders()
                   ->addTextHeader('Reply-to', config('mail.reply_to.address'));
        });
    }

    public function print($id)
    {
        $file_uploads = [];
        $registration = $registration = Registrations::find($id);
        $academic_year = $this->nextAcademicYear();
        $file_uploads = FileUploads::get()->where('registration_id', '=', $registration->id);
        $application = Applications::find($registration->application_id);
        $application_data = json_decode($application->meta);
        $type = $application->type;
        $registration_data = json_decode($registration->meta);
        $application_id=$registration->application_id;
        $step=0;
      return view('auth.user_auth.modules.registration.registration-print', compact('application_id','type','registration','registration_data','file_uploads' ,'application_data','step','academic_year'));
    }


    public function fileUpload($request, $registration){
      ini_set('memory_limit', '-1');
      try {
          $target_dir = 'Upload_Files/Registrations/Photo';
          if ($request->hasfile('family_photo')) {
                  $file = $request->file('family_photo');
                  $name = $file->getClientOriginalName();
                  $new_file_name =  SHA1(rand());
                  $exist = FileUploads::get()->where('registration_id', '=', $registration->id);
                  $file_data = json_decode($exist);
                  if ($exist->count() == 1) {
                      foreach ($file_data as $fd) {
                          $file_upload = FileUploads::findOrFail($fd->id);
                          $old_path = $fd->file_path;
                          Storage::delete($old_path);
                          $file_upload->field_name = $file;
                          $file_upload->file_path = $target_dir . $new_file_name;
                          $file_upload->file_name = $name;
                          $file_upload->file_mime = $file->getMimeType();
                          $file_upload->file_size = $this->getFileSize($file->getsize());
                          Storage::put($target_dir . $new_file_name, Crypt::encrypt($file->get()));
                          $file_upload->save();
                      }
                  } else {
                      Storage::put($target_dir . $new_file_name, Crypt::encrypt($file->get()));
                      $file_upload = FileUploads::updateOrCreate([
                          'field_name' => $file,
                          'registration_id' => $registration->id,
                          'file_path' => $target_dir . $new_file_name,
                          'file_name' => $name,
                          'file_mime' => $file->getMimeType(),
                          'file_size' => $this->getFileSize($file->getsize()),
                      ]);
                      $file_upload->save();
                  }
          }
      } catch (Exception $e) {
          report($e);
  
          return false;
      }
  }

  public function nextAcademicYear() {
    $current_date_time = date('Y-m-d h:i:sa');
    $currentYear = date('Y');
    $cutoff = ($currentYear . '/07/31 23:59:59');
    if ($current_date_time < $cutoff) {
        return ($currentYear) . '-' . ($currentYear+1);
    }
    return ($currentYear+1) . '-' . ($currentYear+2);
}

  public function getFileSize($size)
  {
      // Bytes
      $file_size = $size . ' Bytes';

      if ($file_size >= 1000) {
          // Kilobytes
          $file_size = round($size / 1024, 1) . ' KB';
      }

      if ($file_size >= 1000) {
          // Megabytes
          $file_size = round($size / 1024 / 1024, 1) . ' MB';
      }

      return $file_size;
  }
}
