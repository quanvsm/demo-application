<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Page;
use App\FAQs;
use App\Templates;
use App\TemplateTypes;
use App\Http\Requests\PageRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\PrivateHelper;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function page()
    {
        $pages = Page::orderBy('sort','asc')->get();
        $helper = new PrivateHelper;
        return view('admin.modules.pages.index', compact('pages','helper'));
    }

    public function createPage(PageRequest $request)
    {
      $page = Page::create([
          'title' => request('title'),
          'slug' => preg_replace('/[^A-Za-z0-9\_\-]/', '', strtolower(request('slug'))),
          'parent' => 0,
          'visible' => 'published',
      ]);
      $page->sort = $page->id;
      $page->save();

      return redirect()->route('admin.page.edit', ['id' => $page->id]);
    }

    public function editPage($id)
    {
      // Hard coded pages cannot be edited here.
      $page = Page::where('id', '=', $id)
                  ->where('hard_code', '=', 0)
                  ->first();
      $template_types = TemplateTypes::get();
      $helper = new PrivateHelper;

      if(!$page){
          // If no page is found.
          return redirect()->route('admin.page');
          die;
      }

      if($page->parent_id){
        // If the page has a parent page.
        $parent_page = Page::findOrFail($page->parent_id);
      }

      if($page->template_id != null){
          // If the page has a template.
          $template = Templates::where('page_id', '=', $page->id)->first();
          return view('admin.modules.pages.edit-'.$template->view, compact('page','parent_page','template_types','helper'));
      }

      return view('admin.modules.pages.edit', compact('page','parent_page','template_types'));
    }

    public function savePage(PageRequest $request)
    {
      // Hard coded pages cannot be saved here.
      $page = Page::where('id', '=', request('id'))
                  ->where('hard_code', '=', 0)
                  ->first();
      $helper = new PrivateHelper;

      if(!$page){
          // If no page is found
          return redirect()->route('admin.page');
          die;
      }

      $page->title = request('title');
      $page->slug = preg_replace('/[^A-Za-z0-9\_\-]/', '-', strtolower(request('slug')));
      $page->visible = request('visible');
      $page->seo_title = request('seo-title');
      $page->seo_description = request('seo-description');
      if(request('content')){
        // Prevents content from being over written if a template is applied
        $page->content = request('content');
      }
      $page->save();

      if($page->template_id != null){
          // If the page has a template.
          $helper->save_template_attributes($page, $request);
      }

      if(request('select_template_id') != 'default') {
          // Sets the template.
          $this->has_template($page, request('select_template_id'));
      }elseif(request('select_template_id') == 'default'){
          $page->template_id = null;
          $page->save();
      }

      return redirect()->route('admin.page.edit', ['id' => $page->id])->with('success', 'The Page is Saved.');
    }
    public function has_template($page, $template_type_id){
        $template = Templates::where('page_id', '=', $page->id)->first();
        $template_type = TemplateTypes::where('id', '=', $template_type_id)->first();

        if($template){
            // For pages with exiting templates.
            $template->description = $page->title;
            $template->view = $template_type->view;
            $template->page_id = $page->id;
            $template->template_type = $template_type->id;
            $template->save();
        }else{
            $template = Templates::create([
                'description' => $page->title,
                'view' => $template_type->view,
                'page_id' => $page->id,
                'template_type' => $template_type->id,
            ]);
            $template->save();
        }

        $page->template_id = $template->id;
        $page->save();
    }

    public function createParent(PageRequest $request)
    {
      $parent = Page::create([
          'title' => request('title'),
          'slug' => preg_replace('/[^A-Za-z0-9\_\-]/', '', strtolower(request('slug'))),
          'parent' => 1,
          'visible' => 'published',
      ]);
      $parent->sort = $parent->id;
      $parent->save();
      return redirect()->back();
    }

    public function editPageSEO($id)
    {
      // Hard coded pages can only be edited here.
      $page = Page::where('id', '=', $id)
                  ->where('hard_code', '=', 1)
                  ->first();
      if(!$page){
          // If no page is found
          return redirect()->route('admin.page');
          die;
      }

      if($page->parent_id){
        // If the page has a parent page.
        $parent_page = Page::findOrFail($page->parent_id);
      }

      return view('admin.modules.pages.edit-seo', compact('page','parent_page'));
    }

    public function savePageSEO(Request $request)
    {
      // Hard coded pages can only be saved here.
      $page = Page::where('id', '=', request('id'))
                  ->where('hard_code', '=', 1)
                  ->first();
      if(!$page){
          // If no page is found
          return redirect()->route('admin.page');
          die;
      }

      // Only SEO content can be saed.
      $page->seo_title = request('seo-title');
      $page->seo_description = request('seo-description');
      $page->save();

      return redirect()->route('admin.page.seo', ['id' => $page->id])->with('success', 'The Page is Saved.');
    }

    public function delete(Request $request)
    {
      // Hard coded pages cannot be deleted.
      $page = Page::where('id', '=', request('id'))
                  ->where('hard_code', '=', 0)
                  ->first();
      if(!$page){
          // If no page is found
          die;
      }

      if($page->parent == 1){
        // If the page is a parent, this clears all the sub pages.
        // Sets the sub pages to stand alone pages.
        $sub_pages = DB::table('pages')
                         ->where('parent_id', '=', $page->id)
                         ->update(array('parent_id' => null));
      }
      Page::where('id', '=', request('id'))->delete();
      // No return this function is called by ajax.
    }

    public function deletePage(Request $request){
        // Hard coded pages cannot be deleted.
        $page = Page::where('id', '=', request('id'))
                    ->where('hard_code', '=', 0)
                    ->first();
        if(!$page){
            // If no page is found
            return redirect()->route('admin.page');
            die;
        }

        if($page->parent == 1){
          // If the page is a parent, this clears all the sub pages.
          // Sets the sub pages to stand alone pages.
          $sub_pages = DB::table('pages')
                           ->where('parent_id', '=', $page->id)
                           ->update(array('parent_id' => null));
        }
        Page::where('id', '=', request('id'))->delete();

        return redirect()->route('admin.page')->with('success', 'Page Deleted!');
    }

    public function saveOrder(Request $request){
      // Clear the sort colomn.
      $pages = DB::table('pages')->update(array('sort' => null));

      // This goes through each input in the request.
      foreach ($request->request as $name => $value) {
        // If the input name contains parent_id_.
        if (strpos($name, 'parent_id_') !== false && $value != null) {
          $id = substr($name,10);
          $page = Page::findOrFail($id);
          $page->parent_id = $value;
          $page->save();
        }elseif (strpos($name, 'parent_id_') !== false && $value == null) {
          $id = substr($name,10);
          $page = Page::findOrFail($id);
          $page->parent_id = null;
          $page->save();
        }

        // If the input name contains sort_.
        if (strpos($name, 'sort_') !== false && $value != null) {
          $id = substr($name,5);
          $page = Page::findOrFail($id);
          $page->sort = $value;
          $page->save();
        }
      }

      return redirect()->back()->with('success', 'The Page Order Saved.');
    }

}
