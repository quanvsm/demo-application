<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Password;
use App\Admin;
use App\User;

class AdminForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:admin');
    }

    protected function broker()
    {
      return Password::broker('admins');
    }

    public function showLinkRequestForm()
    {
        return view('admin.admin_auth.modules.login.passwords.email');
    }

    public function check(Request $request){
        $admin = Admin::where('email', request('email'))->first();
        if($admin){
            return $this->sendResetLinkEmail($request);
        }

        // Checks if the this is an user login.
        // $user = User::where('email', request('email'))->first();
        // if($user){
        //     return app()->call('App\Http\Controllers\UserAuth\UserForgotPasswordController@sendResetLinkEmail', $request->all());
        // }
    }
}
