<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Documents;
use App\Invoices;
use Password;

use Mail;
use Auth;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    protected function broker()
    {
      return Password::broker('users');
    }

    public function users()
    {
        $users = User::get();
        $documents= Documents::get()->all();
        return view('admin.admin_auth.modules.users.index', compact('users','documents'));
    }

    public function newUser(){
        return view('admin.admin_auth.modules.users.form');
    }

    public function createUser(UserRequest $request){

        $user = User::create([
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'email' => strtolower(request('email')),
            'active' => 'no',
            'approved' => 'no',
        ]);

        $user->hash = hash('haval160,4',time().rand(72,745).'xzy',false);
        $user->save();
        $this->sendNotificationEmail($user);

        return redirect()->route('admin.users.edit', ['id' => $user->id])->with('success', 'An activation email has been sent to you.');
    }

    private function sendNotificationEmail($user)
    {
        $content = '<p>Welcome. Please click the link below to activate your account.</p>';
        $content .= '<p><a href="'.route('user.activate', $user->hash).'" class="button" target="_blank">Activate Account</a></p>';
        $content .= '<p class="footer">You can also activate your account at '.route('user.activate', $user->hash).'</p>';

        $data = [
            'email' => $user->email,
            'title' => 'User Activation',
            'subject' => 'Account Activation',
            'content' => $content
        ];

        Mail::send('theme.email.general', $data , function($message) use ($data)
        {
            $message->to($data['email'], $data['email'])
                   ->from(config('mail.from.address'), 'VSM')
                   ->subject($data['subject'])
                   ->getHeaders()
                   ->addTextHeader('Reply-to', config('mail.reply_to.address'));
        });
    }

    public function reSendActivationEmail(Request $request){
        $user = User::findOrFail(request('id'));

        if($user->active=='no' && !empty($user->hash) && empty($user->password)){
            $this->sendNotificationEmail($user);
            echo '1';
        }else{
            echo '2';
        }
    }

    public function editUser($id)
    {
        $user = User::findOrFail($id);
        $invoices = Invoices::get()->where('user_id','=',$id);
        return view('admin.admin_auth.modules.users.form', compact('user','invoices'));
    }

    public function saveUser(UserRequest $request){

        $user = User::findOrFail(request('id'));
        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->active = request('active');
        if(request('active')=='yes'){
        $user->hash = null;
        }
        if($request->email != $user->email){
            $user->email = strtolower(request('email'));
        }

        $user->save();

        return redirect()->route('admin.users.edit', ['id' => $user->id])->with('success', 'User Updated.');
    }

    public function delete(Request $request){
        User::where('id', '=', request('id'))->delete();
        // No return this function is called by ajax.
    }

    public function deleteUser(Request $request){
        User::where('id', '=', request('id'))->delete();
        return redirect()->route('admin.users')->with('success', 'User Deleted!');
    }

}
