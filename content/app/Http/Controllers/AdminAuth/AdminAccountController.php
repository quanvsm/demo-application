<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use App\Http\Requests\AccountRequest;
use Auth;
use Mail;

class AdminAccountController extends Controller
{

    public function adminAccount($hash){
        Auth::guard('admin')->logout();

        $admin = Admin::where('hash', '=', $hash)
                        ->where('active', '=', 'no')
                        ->first();
        if($admin){
            return view('admin.admin_auth.modules.login.account', compact('admin'));
        }else{
            return redirect('/');
        }

    }

    public function adminCreate(AccountRequest $request, $hash){
        if(request('hash') == $hash){
            $admin = Admin::where('hash', '=', request('hash'))
                            ->where('active', '=', 'no')
                            ->first();

            if($admin->email == strtolower(request('email'))){
                $admin->active = 'yes';
                $admin->password = bcrypt($request->password);
                $admin->hash = null;
                $admin->save();

                // Attempt to log the user in.
                if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
                    return redirect()->intended(route('admin.applications'))->with('success', 'Success your account is now active!');
                }else{
                    return redirect()->route('admin.login')->with('success', 'Success your account is now active!');
                }
            }else{
                return redirect()->back()->withInput($request->only('hash'))->with('error', 'The Email you entered does not match our records.');
            }
        }else{
            return redirect()->back()->withInput($request->only('email', 'hash'))->with('error', 'An Error Occurred!');
        }
    }


}
