<?php

namespace App\Http\Controllers\AdminAuth;

use App\PostsCategories;
use Illuminate\Http\Request;
use App\Http\Requests\PostsCategoriesRequest;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PostsCategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {
      $categories = PostsCategories::orderBy('id','asc')->get();
      return view('admin.modules.posts.cat.index', compact('categories'));
    }

    public function create(PostsCategoriesRequest $request)
    {
      $category = PostsCategories::create([
          'title' => request('title'),
          'slug' => preg_replace('/[^A-Za-z0-9\_\-]/', '', strtolower(request('slug'))),
      ]);
      $category->save();

      return redirect()->route('admin.posts.cat.edit', ['id' => $category->id]);
    }

    public function edit($id)
    {
      $category = PostsCategories::where('id', '=', $id)->first();

      if(!$category){
          return redirect()->route('admin.posts.cat');
          die;
      }

      return view('admin.modules.posts.cat.edit', compact('category'));
    }

    public function save(PostsCategoriesRequest $request)
    {
      $category = PostsCategories::where('id', '=', request('id'))->first();

      if(!$category){
          return redirect()->route('admin.posts.cat');
          die;
      }

      $category->title = request('title');
      $category->slug = preg_replace('/[^A-Za-z0-9\_\-]/', '', strtolower(request('slug')));
      $category->save();


      return redirect()->route('admin.posts.cat.edit', ['id' => $category->id])->with('success', 'Post Category Saved.');
    }

    public function delete(Request $request)
    {
      $category = PostsCategories::where('id', '=', request('id'))->first();
      if(!$category){
          die;
      }

      PostsCategories::where('id', '=', request('id'))->delete();
      // No return this function is called by ajax.
    }

    public function deleteCategory(Request $request){
      $category = PostsCategories::where('id', '=', request('id'))->first();
      if(!$category){
          return redirect()->route('admin.posts.cat');
          die;
      }

      PostsCategories::where('id', '=', request('id'))->delete();
      return redirect()->route('admin.posts.cat')->with('success', 'Post Category Deleted!');
    }
}
