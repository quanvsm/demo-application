<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FAQs;
use App\Page;

class FAQController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function createFAQ(Request $request)
    {
      $FAQ = FAQs::create([
          'question' => request('question'),
          'template_id' => intval(request('template_id')),
      ]);
      $max = FAQs::where('template_id', '=', request('template_id'))->max('sort');
      $FAQ->sort = $max+1;
      $FAQ->save();

      return redirect()->route('admin.faq.edit', ['id' => $FAQ->id]);
    }

    public function editFAQ($id)
    {
      $FAQ = FAQs::findOrFail($id);
      $page = Page::where('template_id', '=', $FAQ->template_id)->first();
      if($page->parent_id){
        $parent_page = Page::findOrFail($page->parent_id);
      }
      return view('admin.modules.pages.faqs.edit', compact('FAQ','page','parent_page'));
    }

    public function saveFAQ(Request $request)
    {
      $FAQ = FAQs::findOrFail(request('id'));
      $FAQ->update($request->all());
      return redirect()->route('admin.faq.edit', ['id' => $FAQ->id])->with('success', 'The FAQ is Saved.');
    }

    public function delete(Request $request)
    {
      FAQs::where('id', '=', request('id'))->delete();
      // No return this function is called by ajax.
    }

}
