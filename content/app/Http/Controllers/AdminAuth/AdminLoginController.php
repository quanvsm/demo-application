<?php

namespace App\Http\Controllers\AdminAuth;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin;
use App\User;

class AdminLoginController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('guest:admin')->except('logout');
  }

  public function showLoginForm()
  {
    return view('admin.admin_auth.modules.login.login');
  }

  public function login(LoginRequest $request)
  {
    Auth::guard('user')->logout();

    // Checks if the Admin is active.
    $admin = Admin::where('email', request('email'))
                          ->where('active', '=', 'yes')
                          ->first();
    if($admin){
        // Attempt to log the user in.
        if(Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
          session_start();
          $_SESSION['APP_UPLOADS'] = env('APP_UPLOADS');
          $_SESSION['APP_UPLOADS_KEY'] = env('APP_UPLOADS_KEY');
          return redirect()->intended(route('admin.applications'));
        }
    }

    // Checks if the Admin is not active.
    $admin_not = Admin::where('email', request('email'))
                          ->where('active', '=', 'no')
                          ->first();
    if($admin_not){
       // Admin Account is not active.
       return redirect()->back()->withInput($request->only('email', 'remember'))->with('locked', 'This Account is Locked.');
    }

    // Checks if the this is an user login.
    // $user = User::where('email', request('email'))->first();
    // if($user){
    //     return app()->call('App\Http\Controllers\UserAuth\UserLoginController@login', $request->all());
    // }

    // If login attempt failed.
    return redirect()->back()->withInput($request->only('email', 'remember'));
  }

  public function logout(Request $request)
  {
      session_start();
      unset($_SESSION['APP_UPLOADS']);
      unset($_SESSION['APP_UPLOADS_KEY']);
      Auth::guard('admin')->logout();
      return redirect()->route('admin.login');
  }
}
