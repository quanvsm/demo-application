<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\User;
use App\Applications;
use App\Invoices;
use App\Documents;
use Password;
use Mail;
use Auth;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    protected function broker()
    {
      return Password::broker('users');
    }

    // public function users()
    // {
    //     $users = User::get();
    //     return view('admin.admin_auth.modules.users.index', compact('users'));
    // }

    public function profile($id)
    {
    $user = User::findOrFail($id);
    $invoices= Invoices::get()->where('user_id','=',$id);
    $documents= Documents::get()->all();
      return view('auth.user_auth.modules.profile.profile_view',compact('user','invoices','documents'));
    }

    public function updateProfile(Request $request){
        if($request->password == $request->password_confirm){
        $user = User::findOrFail(Auth::id());
            if($user){
                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->email = $request->email;
                $user->password = bcrypt($request->password);
                $saved = $user->save();
                if($saved){
                    return redirect()->route('user.profile', Auth::id())->with('success', 'Updated!');
                }
                else{
                    return redirect()->route('user.profile', Auth::id())->with('error', 'An error has been occurred. Please try again!');
                }
                
            }else{
                return redirect()->route('user.profile', Auth::id())->with('error', 'An error has been occurred. Please try again!');
            }   
        }
        else{
            return redirect()->route('user.profile', Auth::id())->with('error', 'Password does not match. Please try again!');
        }
    }

    private function sendNotificationEmail($user)
    {
        $content = '<p>Welcome. Please click the link below to activate your account.</p>';
        $content .= '<p><a href="'.route('user.activate', $user->hash).'" class="button" target="_blank">Activate Account</a></p>';
        $content .= '<p class="footer">You can also activate your account at '.route('user.activate', $user->hash).'</p>';

        $data = [
            'email' => $user->email,
            'title' => 'User Activation',
            'subject' => 'Account Activation',
            'content' => $content
        ];

        Mail::send('theme.email.general', $data , function($message) use ($data)
        {
            $message->to($data['email'], $data['email'])
                   ->from(config('mail.from.address'), 'VSM')
                   ->subject($data['subject'])
                   ->getHeaders()
                   ->addTextHeader('Reply-to', config('mail.reply_to.address'));
        });
    }

    public function reSendActivationEmail(Request $request){
        $user = User::findOrFail(request('id'));

        if($user->active=='no' && !empty($user->hash) && empty($user->password)){
            $this->sendNotificationEmail($user);
            echo '1';
        }else{
            echo '2';
        }

        // No return this function is called by ajax.
    }

    public function editUser($id)
    {
        $user = User::findOrFail($id);
        return view('admin.admin_auth.modules.users.form', compact('user'));
    }

    public function saveUser(UserRequest $request){

        $user = User::findOrFail(request('id'));
        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->active = request('active');

        if($request->email != $user->email){
            $user->email = strtolower(request('email'));
        }

        $user->save();

        return redirect()->route('admin.users.edit', ['id' => $user->id])->with('success', 'User Updated.');
    }

    public function updateApplicationPayment($id,$application){

        $user = User::findOrFail(Auth::id());
        $application  = Applications::findOrFail($application);
        // $application->status = 'Submitted';
        $user->application_payment_id = $id;

        $application->save();
        $user->save();

        return redirect()->route('user.form.payment',$application)->with('success',"Your payment has been completed. Thank you.");
    }

    public function delete(Request $request){
        User::where('id', '=', request('id'))->delete();
        // No return this function is called by ajax.
    }

    public function deleteUser(Request $request){
        User::where('id', '=', request('id'))->delete();
        return redirect()->route('admin.users')->with('success', 'User Deleted!');
    }
}
