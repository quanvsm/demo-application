<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\AccountRequest;
use Auth;
use Mail;

class UserAccountController extends Controller
{

    public function account($hash){
        Auth::guard('user')->logout();
        Auth::guard('admin')->logout();

        $user = User::where('hash', '=', $hash)
                        ->where('active', '=', 'no')
                        ->first();
        if($user){
            return view('admin.user_auth.modules.login.account', compact('user'));
        }else{
            return redirect('/');
        }

    }

    public function createUser(AccountRequest $request){
        $exist = User::get()->where("email",'', strtolower($request->email));
        if(count($exist) > 0){
            return redirect()->route('user.signup')->with('error', 'Email address has been used. Please try another email or reset password!');
        }
        else{
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' =>$request->last_name, request('last_name'),
                'email' => strtolower($request->email),
                'password' => bcrypt($request->password),
                'active' => 'no',
            ]);
    
            $user->hash = hash('haval160,4',time().rand(72,745).'xzy',false);
            $user->save();
            $this->sendNotificationEmail($user);
            return redirect()->route('user.login')->with('success', 'An activation email has been sent to you.');
        }     
    }

    private function sendNotificationEmail($user)
    {
        $content = '<h4>Welcome to VSM.Please select the link below to activate your account</h4>';
        $content .= '<p><a href="'.route('user.activate', $user->hash).'" class="button" target="_blank">Activate Account</a></p>';
        $content .= '<p class="footer">You can also activate your account at '.route('user.activate', $user->hash).'</p>';

        $data = [
            'email' => $user->email,
            'title' => 'Welcome to VSM!',
            'subject' => 'Account Activation',
            'content' => $content
        ];

        Mail::send('theme.email.general', $data , function($message) use ($data)
        {
            $message->to($data['email'], $data['email'])
                   ->from(config('mail.from.address'), 'VSM')
                   ->subject($data['subject'])
                   ->getHeaders()
                   ->addTextHeader('Reply-to', config('mail.reply_to.address'));
        });
    }

    public function create($hash){
        
        if(request('hash') == $hash){
            $user = User::where('hash', '=', $hash)
                            ->where('active', '=', 'no')
                            ->first();
                if($user){
                    $user->active = 'yes';
                    $user->hash = null;
                    $user->save();
                    return redirect()->route('user.login')->with('success', 'Success! your account is now active!');
                }   
                else{
                    return redirect()->route('user.login')->with('error', 'This account is activated or the activation link was expired! Please check again!');
                }
               
        }else{
            return redirect()->route('user.login')->with('error', 'An Error Occurred!');
        }
    }


}
