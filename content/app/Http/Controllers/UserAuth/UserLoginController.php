<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Applications;

class UserLoginController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest:user')->except('logout');
  }

  public function showLoginForm()
  {
    return view('auth.user_auth.modules.login.login');
  }

  public function showSignUp()
  {
    return view('auth.user_auth.modules.login.sign_up');
  }

  public function login(LoginRequest $request)
  {
    Auth::guard('admin')->logout();

    // Checks if the User is active.
    $user = User::where('email', request('email'))
                          ->where('active', '=', 'yes')
                          ->first();
    if($user){
        // Attempt to log the user in.
        if(Auth::guard('user')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
          session_start();
          $_SESSION['APP_UPLOADS'] = env('APP_UPLOADS');
          $_SESSION['APP_UPLOADS_KEY'] = env('APP_UPLOADS_KEY');
          $check_app = Applications::get()->where('user_id', $user->id);
          if($check_app->isEmpty()){
            return redirect()->route('user.form');
          }
          else{
            return redirect()->route('user.dashboard');
          }
        }
        else{
          return redirect()->route('user.login')->withInput($request->only('email'))->with('error', 'Incorrect Password!');
        }
    }

    // Checks if the user is not active.
    $user_not = User::where('email', request('email'))
                          ->where('active', '=', 'no')
                          ->first();
    if($user_not){
       // User Account is not active.
       return redirect()->route('user.login')->withInput($request->only('email', 'remember'))->with('error', 'Please activate your account!');
    }

    // If login attempt failed.
    return redirect()->route('user.login')->withInput($request->only('email', 'remember'));
  }


  public function logout(Request $request)
  {
      session_start();
      unset($_SESSION['APP_UPLOADS']);
      unset($_SESSION['APP_UPLOADS_KEY']);
      Auth::guard('user')->logout();
      return redirect()->route('home');
  }
}
