<?php

namespace App\Http\Controllers\UserAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Password;
use Auth;
use App\User;

class UserResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:user');
    }

    protected function guard()
    {
      return Auth::guard('user');
    }

    protected function broker()
    {
      return Password::broker('users');
    }

    public function showResetForm(Request $request, $token = null)
    {
        Auth::guard('admin')->logout();
        Auth::guard('user')->logout();

        return view('auth.user_auth.modules.login.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function resetUser(Request $request){
        $user = User::where('email', '=', request('email'))
                        ->where('active', '=', 'yes')
                        ->first();
        if($user){
            $this->reset($request);
            return redirect()->back();
        }else{
            return redirect()->back()->with('error', 'This Account is Locked.');
        }
    }
}
