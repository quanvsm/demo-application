<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Exceptions\Handler;
use App\User;
use Storage;
use Session;
use Crypt;
use App\Http\Requests\PageRequest;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Support\Facades\DB;
use App\Applications;
use App\Registrations;
use App\FileUploads;
use App\Payments;
use Auth;
use Mail;
use Exception;

class RegistrationFormController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:user');
    }

    public function form($id)
    {
         try{
            $file_uploads = [];
            $paid = "";
            $application_id = $id;
            $application = Applications::find($application_id);
            $application_data = json_decode($application->meta);
            $registration = '';
            $side_bar= true;
            $academic_year = $this->nextAcademicYear();
            $registration_data="";
            if($application->registration_id){
                $registration  = Registrations::find($application->registration_id);
                if($registration){
                    $registration_data = json_decode($registration->meta);
                    $file_uploads = FileUploads::get()->where('registration_id', '=', $registration->id);
                    $paid = $this->checkUserPayment($registration->user_id,$registration->id);
                }
            }
            $type=$application->type;
            if(isset($registration_data->step_id)){
                $step=$registration_data->step_id;
            }
            else{
                $step=0;
            }

            if(isset($registration->status)){
                if($registration->status != "Pending"){
                    if ($registration->type ==1){
                        $step=9;
                        $side_bar= false;
                    }
                    else{
                        $step=7;
                        $side_bar= false;
                    }
                }
            }
            
            return view('auth.user_auth.modules.registration.registration_form',compact('registration','registration_data','application_data','file_uploads','type','step','application_id','paid','side_bar','academic_year'));
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function createRegistration($request){
        $data1 = json_encode($request->all());
        $data = json_decode($data1);
        $application = Applications::find($data->application_id);
        $registration = Registrations::create([
            'meta' => $data1,
            'type' => $application->type,
            'status' => 'Pending',
            'application_id' => $data->application_id,
            'user_id' => Auth::id(),
        ]);
        $registration->save();
        $application->registration_id = $registration->id;
        $application->save();

        $this->fileUpload($request, $registration);
        return $registration->id;
    }

    public function updateRegistration($request){
        $data1 = json_encode($request->all());
        $data = json_decode($data1);
        $registration = Registrations::find($data->form_id);
        $registration->meta = $data1;
        $registration->application_id = $data->application_id;
        $registration->save();

        $application = Applications::find($data->application_id);
        $application->registration_id = $registration->id;
        $application->save();
        $this->fileUpload($request, $registration);
        return $registration->id;
    }

    public function saveBeforeSummary(RegistrationRequest $request)
    {
        try{
        unset($request['_token']);
        $data1 = json_encode($request->all());
        $data = json_decode($data1);
        if (isset($data->form_id)) {
            $registration_id = $this->updateRegistration($request);
        } else {
            $registration_id= $this->createRegistration($request);
        }
        return redirect()->route('user.registration.summary', $registration_id);
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function saveForLater(Request $request)
    {
        try{
        unset($request['_token']);
        $data1 = json_encode($request->all());
        $data = json_decode($data1);
        if (isset($data->form_id)) {
            $this->updateRegistration($request);
        } else {
            $this->createRegistration($request);
        }
        return redirect()->route('user.dashboard');
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function submit($id ,RegistrationRequest $request)
    {
        try{
        $current_date_time = date('Y-m-d H:i:s');
        $registration = Registrations::findOrFail($id);
        unset($request['_token']);
        $data1 = json_encode($request->all());
            $registration->status = "Submitted";
            $registration->meta = $data1;
            $registration->submitted_at = $current_date_time;
            $saved = $registration->save();
            if($saved){
                $this->sendNotificationEmail();
                return redirect()->route('user.dashboard')->with('success','Thank you for your registration.   A member of our Admission Office will contact you within five working days of your submission.');
                die;
            }
            else{
                return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
                die;
            }  
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function showSummary($id)
    {   
        try{
        $academic_year = $this->nextAcademicYear();
        $side_bar= true;
        $file_uploads = [];
        $registration = Registrations::find($id);
        $file_uploads = FileUploads::get()->where('registration_id', '=', $registration->id);
        $application = Applications::find($registration->application_id);
        $application_data = json_decode($application->meta);
        $paid = $this->checkUserPayment($registration->user_id , $id);
        $type = $application->type;
        $registration_data = json_decode($registration->meta);
        if ($type ==1){
            $step=9;
        }
        else{
            $step=7;
        }
        $application_id=$registration->application_id;
        return view('auth.user_auth.modules.registration.registration_form',compact('application_id','type','step','registration','registration_data','file_uploads' ,'application_data','paid','side_bar','academic_year'));
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function showPaymentStep($id)
    {   
        try{
        $side_bar= true;
        $academic_year = $this->nextAcademicYear();
        $file_uploads = [];
        $registration = $registration = Registrations::find($id);
        $file_uploads = FileUploads::get()->where('registration_id', '=', $registration->id);
        $application = Applications::find($registration->application_id);
        $application_data = json_decode($application->meta);
        $paid = $this->checkUserPayment($registration->user_id,$id);
        $type = $application->type;
        $registration_data = json_decode($registration->meta);
        if ($type ==1){
            $step=10;
        }
        else{
            $step=8;
        }
        $application_id=$registration->application_id;
        return view('auth.user_auth.modules.registration.registration_form',compact('application_id','type','step','registration','registration_data','file_uploads' ,'application_data','paid','side_bar','academic_year'));
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function fileUpload($request, $registration){
        ini_set('memory_limit', '-1');
        try {
            $target_dir = 'Upload_Files/Registrations/Photo/';
            if ($request->hasfile('family_photo')) {
                    $file = $request->file('family_photo');
                    $name = $file->getClientOriginalName();
                    $new_file_name =  SHA1(rand());
                    $exist = FileUploads::get()->where('registration_id', '=', $registration->id);
                    $file_data = json_decode($exist);
                    if ($exist->count() == 1) {
                        foreach ($file_data as $fd) {
                            $file_upload = FileUploads::findOrFail($fd->id);
                            $old_path = $fd->file_path;
                            Storage::delete($old_path);
                            $file_upload->field_name = $file;
                            $file_upload->file_path = $target_dir . $new_file_name;
                            $file_upload->file_name = $name;
                            $file_upload->file_mime = $file->getMimeType();
                            $file_upload->file_size = $this->getFileSize($file->getsize());
                            Storage::put($target_dir . $new_file_name, Crypt::encrypt($file->get()));
                            $file_upload->save();
                        }
                    } else {
                        Storage::put($target_dir . $new_file_name, Crypt::encrypt($file->get()));
                        $file_upload = FileUploads::updateOrCreate([
                            'field_name' => $file,
                            'registration_id' => $registration->id,
                            'file_path' => $target_dir . $new_file_name,
                            'file_name' => $name,
                            'file_mime' => $file->getMimeType(),
                            'file_size' => $this->getFileSize($file->getsize()),
                        ]);
                        $file_upload->save();
                    }
            }
        } catch (Exception $e) {
            return redirect()->back()->with('error',"Files Were Not Uploaded, Please Try Again!");

            return false;
        }
    }

    public function checkUserPayment($user_id, $registration_id)
    {
        $check_payment = Payments::where('user_id' ,$user_id)->where("registration_id",'!=', "null")->orderBy('created_at','desc')->first();
        if($check_payment){
            $current_date_time = date('Y-m-d h:i:s');
            $payment_date = strtotime($check_payment->created_at);
            $begin_of_purchased_academic_year = $this->beginAcademicYear($payment_date);
            // $end_of_purchased_academic_year = $this->endAcademicYear($payment_date);
            if($current_date_time < $begin_of_purchased_academic_year){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    public function print($id)
    {
        try{
            $file_uploads = [];
            $registration = $registration = Registrations::find($id);
            $academic_year = $this->nextAcademicYear();
            $file_uploads = FileUploads::get()->where('registration_id', '=', $registration->id);
            $application = Applications::find($registration->application_id);
            $application_data = json_decode($application->meta);
            $type = $application->type;
            $registration_data = json_decode($registration->meta);
            $application_id=$registration->application_id;
            $step=0;
          return view('auth.user_auth.modules.registration.registration-print', compact('application_id','type','registration','registration_data','file_uploads' ,'application_data','step','academic_year'));

        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function nextAcademicYear() {
        $current_date_time = date('Y-m-d h:i:s');
        $currentYear = date('Y');
        $cutoff = ($currentYear . '-06-30 23:59:59');
        if ($current_date_time < $cutoff) {
            return ($currentYear) . '-' . ($currentYear+1);
        }
        return ($currentYear+1) . '-' . ($currentYear+2);
    }

    public function beginAcademicYear($date) {
        $currentYear = date('Y', $date);
        $date = date('Y-m-d h:i:s', $date);     
        $cutoff = ($currentYear . '-08-31 23:59:59');
        if ($date < $cutoff) {
            return (($currentYear) . '-08-31 23:59:59');
        }
        return  (($currentYear+1) . '-08-31 23:59:59');
    }

    public function endAcademicYear($date) {
        $currentYear = date('Y', $date);
        $date = date('Y-m-d h:i:s', $date);
        $cutoff = ($currentYear . '-06-30 23:59:59');
        if ($date < $cutoff) {
            return (($currentYear+1) . '-06-30 23:59:59');
        }
        return  (($currentYear+2) . '-06-30 23:59:59');
    }


    public function getFileSize($size)
    {
        // Bytes
        $file_size = $size . ' Bytes';

        if ($file_size >= 1000) {
            // Kilobytes
            $file_size = round($size / 1024, 1) . ' KB';
        }

        if ($file_size >= 1000) {
            // Megabytes
            $file_size = round($size / 1024 / 1024, 1) . ' MB';
        }

        return $file_size;
    }

    public function download($id){
        ini_set('memory_limit', '-1');
        try{
            $FileUploads = FileUploads::get()->where('registration_id',$id);
            foreach($FileUploads as $item){
             $file = decrypt(Storage::get($item->file_path));
     
             return response()->streamDownload(function() use ($file,$item) {
                 ob_start();
                 header('Content-Type: application/{$item->file_mime}');
                 echo $file;
                 ob_end_flush();
                 exit;
             }, $item->file_name);
            }
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    private function sendNotificationEmail()
    {
        $content = '<h4>A new registration has been submitted.</h4>';
        $content .= '<p>Please login to the admin area to review.</p>';
        $content .= '<p class="footer">Here is a link to the admin area: '.route('admin.login').'</p>';

        $data = [
            'email' => 'quan@viewsource.ca',
            'title' => 'New Registration',
            'subject' => 'New Registration',
            'content' => $content
        ];

        Mail::send('theme.email.general', $data , function($message) use ($data)
        {
            $message->to($data['email'], $data['email'])
                   ->from(config('mail.from.address'), 'VSM')
                   ->subject($data['subject'])
                   ->getHeaders()
                   ->addTextHeader('Reply-to', config('mail.reply_to.address'));
        });
    }
}

// try{

// }
// catch (Exception $e) {
//     return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
//     return false;
// }
