<?php

namespace App\Http\Controllers\UserAuth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Exceptions\Handler;
use App\User;
use Storage;
use Session;
use Crypt;
use App\Http\Requests\PageRequest;
use Illuminate\Support\Facades\DB;
use App\Applications;
use App\Registrations;
use App\FileUploads;
use App\Payments;
use App\Invoices;
use App\Documents;
use Auth;
use Mail;
use Exception;

class FormController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:user');
    }
    
    public function dashboard()
    {
        $applications =  Applications::orderBy('updated_at','desc')->get()->where('user_id', '=', Auth::id());
        $registrations =  Registrations::orderBy('updated_at','desc')->get()->where('user_id', '=', Auth::id());
        $applications = json_decode($applications);
        $registrations = json_decode($registrations);
        $invoices= Invoices::get()->where('user_id','=',Auth::id());
        return view('auth.user_auth.modules.dashboard.index', compact('applications','registrations','invoices'));
    }

    public function documents()
    {
        $documents= Documents::orderBy('created_at','desc')->get()->all();
        return view('auth.user_auth.modules.documents.index', compact('documents'));
    }

    public function form()
    {
        session_start();
        if(Session::has('post_data')){
            Session::forget('post_data');
        }
        $paid = '';
        $step = 0;
        return view('auth.user_auth.modules.application.steps.application-step1', compact('step','paid'));
    }

    public function showForm($id)
    {
        $file_names = [];
        $step = 1;
        $type = $id;
        $cut_off='';
        return view('auth.user_auth.modules.application.application-form', compact('step', 'type', 'file_names','cut_off'));
    }

    public function editForm($id)
    {
        $paid = $this->checkUserPayment($id);
        $cut_off = $this->checkCutOff($id);
        $application = Applications::where('id', '=', $id)->first();
        if($cut_off == 1 || $application->status!="Pending"){
            return redirect()->route('user.form.summary',$id);
            die;
        }
        else{
            $file_uploads = FileUploads::get()->where('application_id', '=', $id);
            $data = json_decode($application->meta);
            if (!$data) {
                return redirect()->route('user.dashboard')->with('error','An error occurred. Please try agian!');
                die;
            }
            if($application->status == "Pending"){
                $step = $data->step_id;
            }
            else{
                $step =5;
            }
            $type = $data->type_id;
            $id = $id;
            $file_names = [];
            foreach ($file_uploads as $file) {
                $file_names[$file->field_name] = $file->file_name;
            }
            return view('auth.user_auth.modules.application.application-form', compact('application', 'data', 'step', 'type', 'id', 'file_names', 'paid','cut_off'));
        }
    }

    public function save(Request $request)
    {
        $request->validate([
            'grade' => 'sometimes|required',
            'int_grade' => 'sometimes|required',
            'stu_lname' => 'required|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_fname' => 'required|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_month_of_birth' => 'required',
            'stu_date_of_birth' => 'required',
            'stu_year_of_birth' => 'required',
            'stu_gender' => 'required',
            'stu_street' => 'required|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_city' => 'required|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_province' => 'required',
            'stu_postal' => 'required',
            'stu_phone' => 'required',
            'stu_email' => 'required|email',
            'stu_citizenship_status' => 'required',
            'allegies' => 'required',
            'stu_allegies_details' => 'nullable',
            'pa_lname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_fname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_street' => 'nullable',
            'pa_city' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_province' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_country' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_email' => 'nullable|email',
            'pa_occupation' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_employer' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_lname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_fname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_street' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_city' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_province' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_country' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_email' => 'nullable|email',
            'mo_occupation' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_employer' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'marriage_status' => 'required',
            'custody_status' => 'required',
            'heard_from' => 'required',
            'ref_name' => 'sometimes|required|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'ref_phone' => 'sometimes|required',
            'ref_rela' => 'sometimes|required|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
        ],[
            'email' => 'Email format is invalid.',
            'regex' => 'Fields can not contain special characters.',
            'required' => 'Please fill in all required fields.',
        ]);
        try{
        Session::forget('application_data');
        unset($request['_token']);
        $data1 = json_encode($request->all());
        $data = json_decode($data1);
        if (isset($data->form_id)) {
            $application = Applications::findOrFail($data->form_id);
            $application->meta = $data1;
            $application->save();
            $this->fileUpload($request, $application);
        } else {
            $application = Applications::create([
                'meta' => $data1,
                'status' => 'Pending',
                'type' => $data->type_id,
                'user_id' => Auth::id(),
            ]);
            $application->save();

            $this->fileUpload($request, $application);
        }
        return redirect()->route('user.form.summary', $application->id);
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    public function submit($id ,Request $request)
    {
        try{
        $current_date_time = date('Y-m-d H:i:s');
        $application = Applications::findOrFail($id);
        unset($request['_token']);
        $data1 = json_encode($request->all());
            $application->status = "Submitted";
            $application->meta = $data1;
            $application->submitted_at = $current_date_time;
            $application->save();
           $this->sendNotificationEmail();
            return redirect()->route('user.dashboard')->with('success','Thank you for your application.   A member of our Admission Office will contact you within five working days of your submission.');
            die;
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function saveForLater(Request $request)
    {
        $request->validate([
            'grade' => 'sometimes|nullable',
            'int_grade' => 'sometimes|nullable',
            'stu_lname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_fname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_month_of_birth' => 'nullable',
            'stu_date_of_birth' => 'nullable',
            'stu_year_of_birth' => 'nullable',
            'stu_gender' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_street' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_city' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_province' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'stu_postal' => 'nullable',
            'stu_phone' => 'nullable',
            'stu_email' => 'nullable|email',
            'stu_citizenship_status' => 'nullable',
            'pa_lname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_fname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_street' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_city' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_province' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_country' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_email' => 'nullable|email',
            'pa_occupation' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'pa_employer' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_lname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_fname' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_street' => 'nullable',
            'mo_city' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_province' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_country' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_email' => 'nullable|email',
            'mo_occupation' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',
            'mo_employer' => 'nullable|regex:/^[a-zA-Z0-9 \.\-\/]+$/u',    
         ],[
            'email' => 'Email format is invalid.',
            'regex' => 'Fields can not contain special characters.',
        ]);

        try{
        Session::forget('application_data');
        unset($request['_token']);
        $data1 = json_encode($request->all());
        $data = json_decode($data1);
        if (isset($data->form_id)) {
            $application = Applications::findOrFail($data->form_id);
            $application->meta = $data1;
            $application->save();
            $this->fileUpload($request, $application);
        } else {
            $application = Applications::updateOrCreate([
                'meta' => $data1,
                'status' => 'Pending',
                'type' => $data->type_id,
                'user_id' => Auth::id(),
            ]);
            $application->save();

            $this->fileUpload($request, $application);
        }
        return redirect()->route('user.dashboard')->with('success',"Your application has been saved!");
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    public function showSummary($id)
    {
    try{
        $paid = $this->checkUserPayment($id);
        $cut_off = $this->checkCutOff($id);
        $application = Applications::where('id', '=', $id)->first();
        if(Session::has('post_data')){
            $post_data=Session::get('post_data');
            $application->meta = json_encode($post_data);
            $application->save();
            Session::forget('post_data');
        }
        $file_uploads = FileUploads::get()->where('application_id','=',$id);
        $data =json_decode($application->meta);
        if(!$data){
            return redirect()->route('user.dashboard');
            die;
        }
        $step=5;
        $type=$data->type_id;
        $id=$id;
        $file_names=[];
        foreach($file_uploads as $file){
            $file_names[$file->field_name]= $file->file_name;
        }
        return view('auth.user_auth.modules.application.application-form', compact('application','data','step','type','id','file_names','paid','cut_off'));
    }catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }

    public function print($id)
    {
    try{
      $paid = '';
      $cut_off ='';
      $application = Applications::where('id', '=', $id)->first();
      $file_uploads = FileUploads::get()->where('application_id','=',$id);
      $data =json_decode($application->meta);
      if(!$data){
          return redirect()->route('user.dashboard');
          die;
      }
      $type=$data->type_id;
      $id=$id;
      $file_names=[];
      foreach($file_uploads as $file){
        $file_names[$file->field_name]= $file->file_name;
    }
      return view('auth.user_auth.modules.application.application-print', compact('application','data','type','id','file_names','paid','cut_off'));
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    public function showPaymentPage($id)
    {
        try{
        $paid = $this->checkUserPayment($id);
        $cut_off = $this->checkCutOff($id);
        $application = Applications::where('id', '=', $id)->first();
        $file_uploads = FileUploads::get()->where('application_id', '=', $id);
        if(Session::has('post_data')){
            $post_data=Session::get('post_data');
            $application->meta = json_encode($post_data);
            $application->save();
            Session::forget('post_data');
        }
        $data = json_decode($application->meta);
        if (!$data) {
            return redirect()->route('user.dashboard');
            die;
        }
        $step = 6;
        $type = $data->type_id;
        $id = $id;
        $file_names = [];
        foreach ($file_uploads as $file) {
            $file_names[$file->field_name] = $file->file_name;
        }
        return view('auth.user_auth.modules.application.application-form', compact('application', 'data', 'step', 'type', 'id', 'file_names', 'paid','cut_off'));
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    public function checkCutOff($id){
        $application = Applications::find($id);
        $cut_off_date = $application->cut_off_date;
        $today = date('Y-m-d');
        if($cut_off_date != null){
            if($cut_off_date < $today){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }   

    public function checkUserPayment($id)
    {
        $check_payment = Payments::get()->where('application_id' ,$id);
        if($check_payment->isEmpty()){
            return false;
        }
        else{
            return true;
        }
    }

    public function fileUpload($request, $application){
        ini_set('memory_limit', '-1');
        try {
            $target_dir = 'Upload_Files/Applications/';
            if ($request->hasfile('file_uploads')) {
                foreach ($request->file('file_uploads') as $file => $val) {
                    $name = $val->getClientOriginalName();
                    $new_file_name =  SHA1(rand());
                    $exist = FileUploads::get()->where('application_id', '=', $application->id)->where('field_name', '=', $file);
                    $file_data = json_decode($exist);
                    if ($exist->count() == 1) {
                        foreach ($file_data as $fd) {
                            $file_upload = FileUploads::findOrFail($fd->id);
                            $old_path = $fd->file_path;
                            Storage::delete($old_path);
                            $file_upload->field_name = $file;
                            $file_upload->file_path = $target_dir . $new_file_name;
                            $file_upload->file_name = $name;
                            $file_upload->file_mime = $val->getMimeType();
                            $file_upload->file_size = $this->getFileSize($val->getsize());
                            Storage::put($target_dir . $new_file_name, Crypt::encrypt($val->get()));
                            $file_upload->save();
                        }
                    } else {
                        Storage::put($target_dir . $new_file_name, Crypt::encrypt($val->get()));
                        $file_upload = FileUploads::updateOrCreate([
                            'field_name' => $file,
                            'application_id' => $application->id,
                            'file_path' => $target_dir . $new_file_name,
                            'file_name' => $name,
                            'file_mime' => $val->getMimeType(),
                            'file_size' => $this->getFileSize($val->getsize()),
                        ]);
                        $file_upload->save();
                    }
                }
            }
        } catch (Exception $e) {
            return redirect()->back()->with('error',"Files Were Not Uploaded, Please Try Again!");
            return false;
        }
    }

    public function getFileSize($size)
    {
        // Bytes
        $file_size = $size . ' Bytes';

        if ($file_size >= 1000) {
            // Kilobytes
            $file_size = round($size / 1024, 1) . ' KB';
        }

        if ($file_size >= 1000) {
            // Megabytes
            $file_size = round($size / 1024 / 1024, 1) . ' MB';
        }

        return $file_size;
    }

    public function statementDownload()
    {
        try{
       $file = Storage::get('doctrinal_statement/DoctrinalStatementofFaith2021.pdf');
        return response()->streamDownload(function () use ($file) {
            ob_start();
            header('Content-Type: application/pdf');
            echo $file;
            ob_end_flush();
            exit;
        }, 'Doctrinal Statement of Faith 2021.pdf');
        }
        catch (Exception $e) {
            return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
            return false;
        }
    }


    public function download($id ,$field_name){
        ini_set('memory_limit', '-1');
        try{
        $FileUploads = FileUploads::get()->where('field_name',$field_name)->where('application_id',$id);
        
       foreach($FileUploads as $item){
        $file = decrypt(Storage::get($item->file_path));

        return response()->streamDownload(function() use ($file,$item) {
            ob_start();
            header('Content-Type: application/{$item->file_mime}');
            echo $file;
            ob_end_flush();
            exit;
        }, $item->file_name);
       }
    }
    catch (Exception $e) {
        return redirect()->back()->with('error',"An Error Occurred Please Try Again!");
        return false;
    }
    }

    private function sendNotificationEmail()
    {
        $content = '<h4>A new application has been submitted.</h4>';
        $content .= '<p>Please login to the admin area to review.</p>';
        $content .= '<p class="footer">Here is a link to the admin area: '.route('admin.login').'</p>';

        $data = [
            'email' => 'quan@viewsource.ca',
            'title' => 'New Application',
            'subject' => 'New Application',
            'content' => $content
        ];

        Mail::send('theme.email.general', $data , function($message) use ($data)
        {
            $message->to($data['email'], $data['email'])
                   ->from(config('mail.from.address'), 'VSM')
                   ->subject($data['subject'])
                   ->getHeaders()
                   ->addTextHeader('Reply-to', config('mail.reply_to.address'));
        });
    }
}
