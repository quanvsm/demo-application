<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileUploads extends Model
{
    protected $guard = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_id','registration_id', 'file_path', 'file_name', 'file_size' , 'file_mime','field_name',
    ];

    public function application()
    {
    return $this->belongsTo('App\Applications','applications_id','id');
    }
}
