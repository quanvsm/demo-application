<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemplateTypes extends Model
{
  protected $guard = 'admin';
  public $table = 'template_types';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [

  ];
}
