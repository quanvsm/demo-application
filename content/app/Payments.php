<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $guard = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'method','transaction_id', 'user_id','payer_email', 'amount','currency','payment_status', 'application_id','registration_id'
    ];
}
