<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Templates extends Model
{
  protected $guard = 'admin';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'description', 'view', 'page_id', 'template_type'
  ];
}
