<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applications extends Model
{
    protected $guard = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meta', 'user_id','type', 'status','cut_off_date','interview_date','registration_id','submitted_at'
    ];

    public function user()
    {
    return $this->belongsTo('App\User');
    }

    public function fileUpload()
    {
    return $this->hasMany('App\FileUploads','application_id','id');
    }
}
