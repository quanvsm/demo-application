@extends('theme.layouts.public')
@section('content')
    <h1>{{($page->title ?? '')}}</h1>

    <? $all_team = PublicHelper::get_team_members($page->template_id); ?>
    @foreach($all_team as $team)

        @if($team->image)
            <img src="{{env('APP_TEAM').$team->image}}">
        @endif

        <h3>{{$team->name}}</h3>
        <p>{{$team->position}}</p>

        @if($team->additional_position)
            <p>{{$team->additional_position}}</p>
        @endif

        @if($team->phone)
            <p><a href="tel:+1{{$team->phone}}">{{PublicHelper::format_phone_number($team->phone)}}</a></p>
        @endif

        @if($team->email)
            <p><a href="mailto:{{$team->email}}">{{$team->email}}</a></p>
        @endif

        @if($team->quote)
            <p>{{$team->quote}}</p>
        @endif

        @if($team->bio)
            <p>{{$team->bio}}</p>
        @endif
    @endforeach

@endsection
