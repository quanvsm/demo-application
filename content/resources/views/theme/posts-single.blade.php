@extends('theme.layouts.public')
@section('content')
    <h1>{{$post->title}}.</h1>
    <p class="dateStamp">{{date_format($post->created_at,'F d, Y')}}</p>
    {!!($post->content ?? '')!!}

    <ul>
        @foreach ($categories as $category)
            <li class="{{($category->id == $post->category_id ? 'active' :'')}}">
                <a href="{{route('public.posts.cat', $category->slug)}}">{{$category->title}}</a>
            </li>
        @endforeach
    </ul>

    <a href="{{route('public.posts')}}">Back to Posts</a>

    @if(isset($post->img))
        <img src="{{env('APP_POST').$post->img}}">
    @endif

    <h3>Other Posts</h3>
    @foreach ($other_posts as $post)
        <a href="/posts/{{$post->slug}}">
            <p><strong>{{$post->title}}</strong></p>
            <p>{!! PublicHelper::show_excerpt($post->content); !!}</p>
        </a>
    @endforeach
@endsection
