@extends('theme.layouts.public')
@section('content')
    <h1>404 - Page Not Found</h1>
    <h2>Oops! That page can't be found.</h2>
    <p>Please go to the <a href="{{ route('home') }}">home page</a></p>
@endsection
