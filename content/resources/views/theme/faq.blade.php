@extends('theme.layouts.public')
@section('content')
    <h1>{{($page->title ?? '')}}</h1>

    <? $FAQs = PublicHelper::get_faqs($page->template_id); ?>
    @foreach($FAQs as $FAQ)
        <details>
            <summary>
                <span>{{$FAQ->question}}</span>
            </summary>
            <p>{!!$FAQ->answer!!}</p>
        </details>
    @endforeach
@endsection
