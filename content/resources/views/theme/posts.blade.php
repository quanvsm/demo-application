@extends('theme.layouts.public')
@section('content')
    <h1>Posts</h1>

    <ul>
        @foreach ($categories as $category)
            @if(isset($cat_slug) && $cat_slug == $category->slug)
                <li class="active"><a href="{{route('public.posts.cat', $category->slug)}}">{{$category->title}}</a></li>
            @else
                <li><a href="{{route('public.posts.cat', $category->slug)}}">{{$category->title}}</a></li>
            @endif
        @endforeach
    </ul>
    @if(isset($cat_slug))
        <a class="back" href="{{route('public.posts')}}">Back to Posts</a>
    @endif

    @foreach($post_items as $post)
        <a href="/posts/{{$post->slug}}">
            @if(isset($post->img))
                <img src="{{env('APP_POST').$post->img}}">
            @endif

            <h3>{{$post->title}}</h3>
            @if(isset($post->excerpt))
                <p>{{$post->excerpt}}</p>
            @else
                <p><?= PublicHelper::show_excerpt($post->content); ?></p>
            @endif
            <p>{{date_format($post->created_at,'F d, Y')}}</p>
        </a>
    @endforeach

    @if(isset($cat_slug))
        {{ $post_items->links('vendor.pagination.default',['cat_slug' => $cat_slug]) }}
    @else
        {{ $post_items->links('vendor.pagination.default') }}
    @endif
@endsection
