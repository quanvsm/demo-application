
    <div id="widget-bar">
        <img class="logo" src="img/vsm-logo.png" alt="Linden Christian School">
      </div>
      
      <div id="nav-bar">
        <nav>
          <div class="page-links">
            <a href="#" class="active">About LCS</a>
            <a href="#">Admissions</a>
            <a href="#">Spiritual Life</a>
            <a href="#">Academics</a>
            <a href="#">Athletics</a>
            <a href="#">The Arts</a>
            <a href="#">News</a>
            <a href="#">Alumni</a>
            <a href="#">Calendar</a>
            <a href="#">Give</a>
          </div>
          <div class="action-links">
            @if(!Auth::guard('user')->check()) 
            <span class="nav-drop-toggle sign-in" title="Sign In"></span>
            <div class="nav-drop-form">
              <form method="POST" action="{{ route('user.login.submit') }}">
                @csrf
                <input type="text" placeholder="username" name="email">
                <input type="password" placeholder="password" name="password">
                @if (Route::has('user.password.request'))
                <a href="{{ route('user.password.request') }}">
                    {{ __('Forgot Your Password?') }}
                </a>
                @endif
                <button>Submit</button>
              </form>
              @else
              <a class="nav-drop-toggle sign-out" title="Sign Out" href="{{route('user.logout')}}"></a>
              @endif
            </div>
            <span class="nav-drop-toggle search" title="Search"></span>
            <div class="nav-drop-form">
              <form>
                <input type="text" placeholder="search">
                <button>Go</button>
              </form>
            </div>
          </div>
        </nav>      
      </div>
