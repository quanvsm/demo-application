@extends('theme.layouts.public')
@section('content')
    <h1>Talk to us Today.</h1>
    <form>
      @csrf
      <input type="text" name="name" value="{{old('name')}}" placeholder="Full Name" required>
      <input type="text" name="company" value="{{old('company')}}" placeholder="Company">
      <input type="email" name="email" value="{{old('email')}}" placeholder="Email Address" required>
      <input type="tel" name="phone" value="{{old('phone')}}" placeholder="Telephone Number">
      <textarea name="message" placeholder="Message..." required>{{old('message')}}</textarea>
      <button type="submit" >Submit</button>
    </form>
@endsection
