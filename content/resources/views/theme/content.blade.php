@extends('theme.layouts.public')
@section('content')
    <h1>{{($page ? $page->title :'')}}</h1>
    <p>{!!($page ? $page->content :'')!!}</p>
@endsection
