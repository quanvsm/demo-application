@extends('theme.email.template')
@section('content')
    <div class="emailContainer center">
        <h1>{{ $title }}</h1>
        <p><?= $content ?></p>
    </div>
@endsection
