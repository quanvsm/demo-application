<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{env('APP_URL')}}/css/theme/mail.css" rel="stylesheet">
    </head>
    <body>
        @yield('content')
    </body>
</html>
