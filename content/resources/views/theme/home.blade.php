@extends('theme.layouts.public')
@section('content')
<main id="site-main">
    
    <aside id="sidebar">
      <nav>
        <a href="#overview">Overview</a>
        <a href="#administration">Administration</a>
        <a href="#mission-vision-values">Mission, Vision, Values</a>
        <a href="#history">History</a>
        <a href="#governance">Governance</a>
        <a href="#before-and-after-school-care">Before and After School Care</a>
        <a href="#school-of-fine-arts">School of Fine Arts</a>
        <a href="#employment">Employment</a>
        <a href="#contact-us">Contact Us</a>
        
        <a href="#widget-bar">Back To Top</a>
      </nav>
      
    </aside>
    
    <article id="content">
      <header>
        <h1>About</h1>
      </header>
      
      {{-- <section id="overview">
        <h2>Education Built on Relationships</h2>
        <p>We’re thankful for all that God has provided, and seek to partner with parents in providing the best education possible for our students. To learn more, visit our Admissions site <a href="https://joinlcs.ca/">joinlcs.ca</a>.<br>
        </p>
        <p><strong>Linden Christian School</strong> is a dynamic independent school inspired by faith, motivated by excellence and anchored by meaningful relationships. Our exceptional teaching staff boasts a rich variety of talents, skills and interests. With their guidance, Linden students grow to be <em><strong>insightful thinkers </strong></em>and <em><strong>discerning decision makers. </strong></em>We give them the tools, knowledge and opportunities to succeed; we expose them to extracurricular options so that they can develop new skills; we provide expert instruction and a lively, caring community.</p>
        <p>We also seek to impart the values that will serve as a foundation for a purposeful, meaningful life – helping students grow to be <em><strong>devoted followers of Christ</strong></em> and <em><strong>compassionate community members</strong></em>.</p>
        <p>Linden can help you achieve all that you want for the education of your children.</p>
        <ul>
          <li>We deliver the provincial curriculum and so much more as students are motivated to <strong>explore and excel in the classroom</strong>. We are a young school, but our graduates are already doing exceptional things in their careers.</li>
          <li>Our walls are full of banners and pennants that represent the success we’ve enjoyed in athletics. <strong>Through sports, we build character</strong> and community. We also build champions.</li>
          <li>We help our students discover and <strong>nurture their artistic talents</strong>. LCS choirs and bands are recognized for an exceptionally high performance standard. Our music students compete successfully and perform frequently as their skills grow.</li>
          <li>Our <strong>students look beyond themselves</strong>. Through chapel and community service, our students embrace biblical truth and seek to make this a better world. Whether the Linden family is collecting for a shoe box, gathering warm clothing for those less fortunate, or providing clean water overseas we seek to show the love of God and serve Christ.</li>
          <li>We have a remarkable <strong>facility</strong>, a strong <strong>church relationship</strong>, an increasingly dynamic<strong> alumni</strong> body.</li>
        </ul>
      </section>
      
      <section id="administration">
        <h2>Administration</h2>
        <p>As Principal and CEO of <strong>Linden Christian School</strong>, I am blessed to work with dynamic faculty and staff committed to providing students with a well-rounded, Christ-centered education. When I walk the halls of our facility and meet students, I see undeniable evidence that we are achieving academic excellence, celebrating creativity, and fulfilling our mission of equipping students to love and serve God.</p>
        <p>With the support of an active and engaged parent community, we strive for excellence in everything we do at LCS. Our students consistently score well above the median on provincial exams. Our sports teams win championships and promote leadership. Our artists create beauty.</p>
        <p>Central to our calling is a commitment to develop students with a strong Christian worldview. We long to shape students’ lives so that they graduate from LCS strong in their faith, eager to make a difference for Christ, and prepared to serve.</p>
        <p>LCS, one of the largest Christian schools in western Canada, has developed into a city-wide ministry representative of 100 churches. We are grateful to be a part of this community, and are thankful for the enthusiastic support and leadership of Grant Memorial Church. ~ <em>Robert Charach, Principal and CEO of Linden Christian School</em></p>
        
      </section>
      <section id="mission-vision-values">
        <h2>Mission, Vision, and Values</h2>
        <p><strong>Linden Christian School</strong> provides a Christ centred educational experience for over 975 students from Kindergarten through Grade 12. LCS strives for excellence in academics, athletics, and the arts. LCS faculty and staff comprise over 100 dedicated men and women who serve God as passionate Christian educators inspired and guided by the LCS mission, values and vision statement.</p>
        <h4>Mission</h4>
        <p>With a commitment to excellence, Linden Christian School provides a Christ-centred education that equips students to love and serve God.</p>
        <h4>Vision</h4>
        <p>Linden Christian School is a biblically-based learning community where all students grow to be devoted followers of Christ, insightful thinkers, discerning decision-makers, and compassionate community members.</p>
        <h4>Core Values</h4>
        <p>At Linden Christian School we value:</p>
        <ul>
        <li>a Christ-centered education based on a biblical world view;</li>
        <li>a commitment to excellence and the development of Godly character;</li>
        <li>a safe, caring, learning environment that focuses on the whole person;</li>
        <li>equipping students to serve in the local and global community.</li>
        </ul>
      </section>
      
      <section id="history">
        <h2>History</h2>
        <p>Linden Christian School was founded in 1987 as a ministry of Grant Memorial Baptist Church. It started with 33 students and three teachers, one of which was the founding principal Phyllis Cook.</p>
        <p>By 1992 enrollment had reached 250 students and Grades 7 and 8 were added to the school. LCS welcomed its first high school class in 1994. Grade 10 was added in 1997, Grade 11 in 1998 and Grade 12 in 1999. The first high school graduation was celebrated in 2000.</p>
        <p>September 2010 saw the opening of the Transitional Learning Classroom (TLC), at a downtown location. The TLC provided a unique learning setting for children who are newcomers to Canada. Students in Grades 1 through 5 learned the English language and Canadian culture within the Manitoba curriculum. This vibrant classroom was operational for 7 years.</p>
        <p>With a growing student population came the need for additional space. Over the years, Linden Christian School has undergone many expansions to add classrooms, a gym, and new band, choir and art facilities. Students at LCS also enjoy enhanced library and study space, a new, state of the art audio visual room and a new science lab. An outdoor Playscape was built in 2015 and field upgrades, including a new basketball court and outdoor fitness centre, were added in 2020.</p>
        <p>What has remained unchanged over the years is the commitment of all staff to provide an excellent Christ-centered education that equips students to love and serve God. LCS now has over 900 students from Kindergarten to Grade 12. The same emphasis on spiritual growth and relationship – with God, people and community – that Linden was founded on continues to provide for the development of the whole child.</p>
      </section>
      
      
      <section id="governance">
        <h2>Governance</h2>
        <p>Linden Christian School (LCS) is an incorporated non-profit charitable organization that operates independently as an affiliated ministry of Grant Memorial Baptist Church (GMBC).</p>
        <p>The Linden Christian School Board is affirmed annually by the GMBC Board of Deacons. The LCS School Board is responsible for approving the plans, budgets and policies of the school and ensuring their effective implementation by the school administration.</p>
        <p>The Principal is the Chief Executive Officer of the school and is responsible for the formation and implementation of the school’s administrative policy as well as the implementation of all of the educational programs, staffing and the day to day operations of the school. The Principal and CEO reports directly to the Linden Christian School Board. He is also part of the Joint Leadership Team that manages all components of the organization that includes Linden Christian School, Grant Memorial Baptist Church and the Lindenholm Seniors Complex. These senior officers prepare the annual plans, budgets and policies guided by the mandate approved by the GMBC Board of Deacons and within the governance policies set by the Linden Christian School Board.</p>
        <h4>2020-2021 School Board</h4>
        <ul>
        <li>Lowell Jones, Board Chair</li>
        <li>Rea Seitler, Vice – Chair</li>
        <li>Tanis Siemens, Secretary</li>
        <li>Mark Loewen, Provisional Treasurer</li>
        <li>Gerry Bettig, GMBC Member Representative</li>
        <li>Steve Martens, GMBC Interim CEO</li>
        <li>Bev Donald</li>
        <li>Arleigh Hefford</li>
        <li>Kelly Spiring</li>
        </ul>
      </section> --}}
      
      
      <section id="before-and-after-school-care">
      </section>
      
      
      <section id="employment">
      </section>
      
      
      <section id="contact-us">
      </section>
      
    </article>
      
  </main>  
  
<script>
  $(document).ready(function(){
    var sectionIds = $('#sidebar nav a');
    
    function activeSection() {
      sectionIds.each(function(){
        var container = $(this).attr('href');
        var containerOffset = $(container).offset().top;
        var containerHeight = $(container).outerHeight();
        var containerBottom = containerOffset + containerHeight;
        var scrollPosition = $(document).scrollTop();
        
        if(scrollPosition < containerBottom - 400 && scrollPosition >= containerOffset - 400){
          $(this).addClass('active');
        } else {
          $(this).removeClass('active');
        }
      });
    };
    
    $(document).ready(function(){
      activeSection();
    });
    
    $(document).scroll(function(){
      activeSection();
    });
    
    
		$('#sidebar').click(function(){ //open and close section navigation
			$('#sidebar').toggleClass('open');
      $('#nav-bar').removeClass('open');
		});
  
    
    $('#sidebar nav a').click(function(){ //smooth scroll
      $('#sidebar').removeClass('open'); //close section navigation
      if (window.innerWidth < 960) { //tablet and smaller
        $('html, body').animate({
          scrollTop: $( $(this).attr('href') )
          .offset().top - 100
        }, 700);
        return false;
      } else {
        $('html, body').animate({ //larger than tablet
          scrollTop: $( $(this).attr('href') )
          .offset().top
        }, 700);
      return false;
      }
    });
    
    
		$('.page-links').click(function(){ //open and close page navigation
			$('#nav-bar').toggleClass('open');
      $('#sidebar').removeClass('open');
      $('.nav-drop-toggle').removeClass('open');
		});
    
		$('.nav-drop-toggle').click(function(){ //drop down form toggles
      $('.nav-drop-toggle').not(this).removeClass('open');
      $('#nav-bar').removeClass('open');
			$(this).toggleClass('open');
		});   
  });
</script>
@endsection
