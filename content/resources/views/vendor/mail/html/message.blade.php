@component('mail::layout')

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                <p class='footer'>{{ $subcopy }}</p>
            @endcomponent
        @endslot
    @endisset

@endcomponent
