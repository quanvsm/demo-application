@if ($paginator->hasPages())
    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
        @if(isset($cat_slug))
            <a href="{{route('public.posts.cat.page',['cat_slug' => $cat_slug, 'page' => $paginator->currentPage() + 1])}}">older posts</a>
        @else
            <a href="{{route('public.posts.page', $paginator->currentPage() + 1)}}">older posts</a>
        @endif
    @endif

    {{-- Previous Page Link --}}
    @if (!$paginator->onFirstPage())
        @if(isset($cat_slug))
            <a href="{{route('public.posts.cat.page',['cat_slug' => $cat_slug, 'page' => $paginator->currentPage() - 1])}}">newer posts</a>
        @else
            <a href="{{route('public.posts.page', $paginator->currentPage() - 1)}}">newer posts</a>
        @endif
    @endif
@endif
