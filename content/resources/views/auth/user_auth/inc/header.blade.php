<div id="widget-bar">
    <img class="logo" src="/img/vsm-logo.png" alt="Vsm">
  </div>
  
  <div id="nav-bar">
    <nav>
      <div class="page-links">
        <a href="#">Home</a><!-- this goes to VSM public facing site -->
        <a href="{{ route('user.dashboard') }}" class="{{Request::is('user/dashboard')? "active": ""}}">Dashboard</a>
        <a href="{{ route('user.form') }}" class="{{Request::is('user/form*')? "active": ""}}">New Application</a>
        <a href="{{ route('user.documents') }}" class="{{Request::is('user/documents')? "active": ""}}">Documents</a>
        <a href="{{ route('user.profile',Auth::id()) }}" id="profile" class="{{Request::is('user/profile*')? "active": ""}}">Profile</a>
      </div>
      <div class="action-links">
        <a href="{{ route('user.logout') }}" class="sign-out" title="Sign Out"></a>
      </div>
    </nav>
  </div>

  <script>
    $(document).ready(function(){
  
      $('#sidebar').click(function(){ //open and close section navigation
        $('#sidebar').toggleClass('open');
        $('#nav-bar').removeClass('open');
      });
      
      $('.page-links').click(function(){ //open and close page navigation
        $('#nav-bar').toggleClass('open');
        $('#sidebar').removeClass('open');
        $('.nav-drop-toggle').removeClass('open');
      });
      
      $('.nav-drop-toggle').click(function(){ //drop down form toggles
        $('.nav-drop-toggle').not(this).removeClass('open');
        $('#nav-bar').removeClass('open');
        $(this).toggleClass('open');
      });

      
          
    });
  </script>