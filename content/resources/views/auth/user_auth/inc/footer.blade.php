@if(isset($application->status))
        @if($application->status === 'Pending')
        <a class="button sfl save">Save for Later</a>
        @else
        <a class="button summary save">Update</a>
        @endif
@else
    <a class="button sfl save">Save for Later</a>
@endif