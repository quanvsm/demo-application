<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>User Login</title>
  <link href="/css/theme/style.min.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
</head>

<body class="sign-in-page">
  <div class="box">
    <header>
      <img src="img/vsm-logo.png" alt="vsm">
    </header>
    @include('auth.user_auth.inc.messages')
    <form method="POST" action="{{ route('user.login.submit') }}">
      @csrf
      <form>
        <h1>Sign In</h1>
        <label>Email Address<input id="email" type="email" name="email" value="{{ old('email') }}" required
            autocomplete="email" autofocus></label>
        <label>Password<input id="password" type="password" name="password" required
            autocomplete="current-password"></label>

        <button type="submit" class="button green">Sign In</button>
      </form>
      <footer>
        {{-- <a class="button" href="{{ route('home') }}">Back</a>
        --}}
        @if(Route::has('user.password.request'))
        <a class="button" href="{{ route('user.password.request') }}">Forgot Password</a>
        @endif
        <a class="button" href="{{ route('user.signup') }}">Sign Up</a>
      </footer>
  </div>

  @error('email')
  <div id="notification">
    <p>{{ $message }}</p>
    <script>
      $("#notification").delay(1500).slideUp('slow');

    </script>
  </div>
  @enderror

  @error('password')
  <div id="notification">
    <p>{{ $message }}</p>
    <script>
      $("#notification").delay(1500).slideUp('slow');

    </script>
  </div>
  @enderror

  @if(Session::has('locked'))
  <div id="notification">
    <p>{{ Session::get('locked') }}</p>
    <script>
      $("#notification").delay(3000).slideUp('slow');

    </script>
  </div>
  @endif
</body>

</html>