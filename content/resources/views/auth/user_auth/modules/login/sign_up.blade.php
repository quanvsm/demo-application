<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sign Up</title>
    <link href="/css/theme/style.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
</head>

<body class="sign-in-page">
    <div class="box">
        <header>
            <img src="img/vsm-logo.png" alt="vsm">
        </header>
        @include('auth.user_auth.inc.messages')
        <form method="POST" action="{{ route('user.create') }}">
            @csrf
            <h1>Create New Account</h1>
            <label>First Name<input type="text" name="first_name" required></label>
            <label>Last Name<input type="text" name="last_name" required></label>
            <label>Email Address<input type="text" name="email" required></label>
            <label>Password<input type="password" name="password" required></label>
            <label>Confirm Password<input type="password" name="password_confirmation" required></label>
            <button type="submit" class="button green">Submit</button>
        </form>
        <footer>
            <a class="button" href="{{ route('home') }}">Back</a>
            <a class="button" href="{{ route('home') }}">Sign In</a>
        </footer>
    </div>

    @error('email')
    <div id="notification">
        <p>{{ $message }}</p>
        <script>
            $("#notification").delay(1500).slideUp('slow');

        </script>
    </div>
    @enderror

    @error('password')
    <div id="notification">
        <p>{{ $message }}</p>
        <script>
            $("#notification").delay(1500).slideUp('slow');

        </script>
    </div>
    @enderror

    @if(Session::has('locked'))
    <div id="notification">
        <p>{{ Session::get('locked') }}</p>
        <script>
            $("#notification").delay(3000).slideUp('slow');

        </script>
    </div>
    @endif
</body>

</html>