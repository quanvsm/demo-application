{{-- @extends('auth.user_auth.layouts.user_login')
@section('content')
  <form method="POST" action="{{ route('user.password.email') }}">
    @csrf
    <div class="loginContainer">
      <img class="logo" src="/css/admin/img/vsm-site-engine.png" alt="VSM Site Engine">
      <label>Email
        <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
      </label>
      <button type="submit">Send Password Reset Link</button>
    </div>
  </form>
@endsection --}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Forgot Password</title>
    <link href="/css/theme/style.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
</head>

<body class="sign-in-page">
    <div class="box">
        <header>
        <img src="/css/admin/img/vsm-logo.png"  alt="vsm">
        </header>
        @include('auth.user_auth.inc.messages')
        <form method="POST" action="{{ route('user.password.email') }}">
            @csrf
            <label>Email Address
              <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
            </label>
            <button class="button green" type="submit">Send Password Reset Link</button>
        </form>
        <footer>
            <a class="button" href="{{ route('home') }}">Back</a>
            <a class="button" href="{{ route('home') }}">Sign In</a>
        </footer>
    </div>
