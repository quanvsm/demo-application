{{-- @extends('admin.layouts.admin_login')
@section('content')
  <form method="POST" action="{{ route('user.password.check') }}">
    @csrf
    <div class="loginContainer">
      <img class="logo" src="/css/admin/img/vsm-logo.png" alt="VSM Site Engine">
      <input type="hidden" name="token" value="{{ $token }}">

      <label>Email
        <input id="email" type="email" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
      </label>
      <label>Password
        <input id="password" type="password" name="password" required autocomplete="new-password">
      </label>
      <label>Confirm Password
        <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
      </label>
      <button type="submit">Reset Password</button>
    </div>
  </form>
@endsection --}}

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reset Password</title>
    <link href="/css/theme/style.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
</head>

<body class="sign-in-page">
    <div class="box">
        <header>
            <img src="/css/admin/img/vsm-logo.png" alt="vsm">
        </header>
        @include('auth.user_auth.inc.messages')
        <form method="POST" action="{{ route('user.password.check') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">

            <label>Email
              <input id="email" type="email" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
            </label>
            <label>Password
              <input id="password" type="password" name="password" required autocomplete="new-password">
            </label>
            <label>Confirm Password
              <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
            </label>
            <button type="submit" class="button green">Reset Password</button>
        </form>
        <footer>
            <a class="button" href="{{ route('home') }}">Back</a>
            <a class="button" href="{{ route('home') }}">Sign In</a>
        </footer>
    </div>
