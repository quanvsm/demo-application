<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="{{(isset($page->seo_description) ? $page->seo_description :'')}}" />
        <title>{{(isset($page->seo_title) ? $page->seo_title :'vsm')}}</title>
        <link href="/css/theme/print-style.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    </head>

<body class="application-print">
    @include('auth.user_auth.modules.registration.steps.summary')
</body>
<script>
    $(document).ready(function(){
        $("footer").hide();
})
</script>
<script>window.print(); setTimeout(function(){ window.close(); }, 3000);</script>
</html>