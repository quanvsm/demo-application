<script>
    $('document').ready(function(){
     //save and go to summary + validation for all required fields
       //Step before Review,- Accuracy of Information -  Validation for the signature fields before going to the review.
     $('#summary_button').click(function(event){
       event.preventDefault();
       var fields_check = $('.accuracy').find("input:visible");
       var valid = 0;
       var continue_process = false;
       fields_check.each(function(){
         if($(this).val().length == 0){
           valid++;
           var error_span = $(this).attr("name")+"_error";
           $("#"+error_span).text("*Required Field");
           $("#"+error_span).prop('style',"color:#FF0000");
   
         }
         else{
           var error_span = $(this).attr("name")+"_error";
           $("#"+error_span).text("");
         }
       })
   
       if(valid==0){
         continue_process = true;
       }
   
       if(continue_process){
         var required_fields=[];
         $(':input[required]').each(function(){
             required_fields.push($(this).attr("name"));
         })
         for( i = 0; i < required_fields.length; ++i){
           if($(":input[name="+required_fields[i]+"]").val().length ==0){
               valid++;
           }
         }
         if(valid == 0 ){
             $('form').attr("action", "{{ route('user.registration.save') }}");  //change the form action
             $('form').submit(); 
         }
         else{
           alert("Please complete the required fields in all steps of the registration. Thank you.")
         }
       }
     })
   
     //save and go to dashboard- save for later
     $('.save').click(function(event){
       event.preventDefault();
       $('form').attr("action", "{{ route('user.registration.later.save') }}");  //change the form action
       $('form').submit(); 
     })
   
     //submit button
     $('.submit').click(function(event) {
       event.preventDefault();
       $('form').attr("action", "{{ route('user.registration.sutmit',$registration->id??0) }}");  //change the form action
       $('form').submit();  // submit the form
     });
   
     //jquery date picker for date fields
     $( ".jquery_datepicker" ).datepicker({ minDate:0,
       dateFormat: 'mm/dd/yy'});
   
     //turn off auto-fill
     $('input').attr("autocomplete",'nope');
   
     //mask phone number input. format: xxx-xxx-xxxx
     $('.phone_number_format')
       .keydown(function (e) {
         var key = e.which || e.charCode || e.keyCode || 0;
         $phone = $(this);
   
         if (key !== 8 && key !== 9) {
           if ($phone.val().length === 3) {
             $phone.val($phone.val() + '-');
           }			
           if ($phone.val().length === 7) {
             $phone.val($phone.val() + '-');
           }
         }
         return (key == 8 || 
             key == 9 ||
             key == 46 ||
             (key >= 48 && key <= 57) ||
             (key >= 96 && key <= 105));	
       })
   
       .bind('focus click', function () {
         $phone = $(this);
         
           var val = $phone.val();
           $phone.val('').val(val); 
       })
   
       .blur(function () {
         $phone = $(this);
         
       });

       $(".remove_sibling").click(function() {
        var par = $(this).parent();
            par.parent().remove();
        });

        $("#add_sibling").click(function() {
            var field = '<div class="row sibling"><fieldset> <label>Last Name<input type="text" name="sibling_lname[]"></label><label>First Name<input type="text" name="sibling_fname[]"></label></label><label>Date of Birth<input type="text" placeholder="MM/DD/YYYY" class="birthday_mask" maxlength="10" name="sibling_birthday[]"></label><label>School<input type="text" name="sibling_school[]"></label><span class="remove" id="remove_sibling"></span></fieldset></div>';
          
            $("#after-sibling").after(field);
            $( ".birthday_mask" ).datepicker({
            dateFormat: 'mm/dd/yy'});
            $("#remove_sibling").click(function() {
          var par = $(this).parent();
            par.parent().remove();
        });
        });
    })
   
    $('input[type=file]').change(function () {
       var id = $(this).attr('id')+"-show";
       var size = this.files[0].size;
           var val = $(this).val().toLowerCase(),
               regex = new RegExp("(.*?)\.(png|jpg|jpeg)$");
           if(size >31457280){
             $(this).val('');
             $('#'+id).empty()
               alert('Your files have not been uploaded. The maximum upload file size is 30 MB. Please check your file sizes and try again.');
           }
           if (!(regex.test(val))) {
               $(this).val('');
               $('#'+id).empty()
               alert('This file type is not accepted. Please save your file as one of the listed supported types.');
           }
           else{
             var file = $(this)[0].files[0].name;
             $('#'+id).empty().append(file);
           }
       });
    </script>
   
    <script>
     //form function script
     $('document').ready(function(){
       //step 1 - student information function
           // candadian -when form open - if the grade rad is selected, enable the grade- input + make required
           if($("#grade-rad").is(":checked")){
             $("#grade-input").prop("disabled",false);
             $("#grade-input").prop("required",true);
           }
           
          //candadian- grade change function - if grade is selected, enable grade input
          $("input[type='radio'][name='grade']").change(function(){
            if($(this).attr('id') == 'grade-rad'){
             $("#grade-input").prop("disabled",false);
             $("#grade-input").prop("required",true);
            }
            else{
             $("#grade-input").prop("disabled",true);
             $("#grade-input").prop("required",false);
             $("#grade-input").val("");
            }
          })
   
      //step 2 - Medical function
           //when form open -make consent with this emergency input required if select NO
           if($("input[type='radio'][name='medical_consent']:checked").val() =="No"){
             $("#medical_consent_detail").prop("disabled",false);
             $("#medical_consent_detail").prop("required",true);
           }
          //make consent with this emergency input required if select NO
          $("input[type='radio'][name='medical_consent']").change(function(){
            if($(this).val() == 'No'){
             $("#medical_consent_detail").prop("disabled",false);
             $("#medical_consent_detail").prop("required",true);
            }
            else{
             $("#medical_consent_detail").prop("disabled",true);
             $("#medical_consent_detail").prop("required",false);
             $("#medical_consent_detail").val("");
            }
          })
   
      //Step 3 - parent Information
          //function to ser father address required fields - condition : true/false
         function setFatherAddressRequiredFields(condition){
           $('#pa_street').prop('required',condition);
             $('#pa_city').prop('required',condition);
             $('#pa_province').prop('required',condition);
             $('#pa_country').prop('required',condition);
             $('#pa_postal').prop('required',condition);
         }
         
         //function to ser mother address required fields  
         function setMotherrAddressRequiredFields(condition){
           $('#mo_street').prop('required',condition);
             $('#mo_city').prop('required',condition);
             $('#mo_province').prop('required',condition);
             $('#mo_country').prop('required',condition);
             $('#mo_postal').prop('required',condition);
         }
   
           //when form open - enable/disable parent address fields if address checkbox if checked
           if ($('#mother-addr-checkbox').find('input:checkbox:checked').length > 0) {
               $("#mother-addr").prop("disabled", true);
               $("#mother-addr").prop("required", false);
           } else {
               $("#mother-addr").prop("disabled", false);
               $("#mother-addr").prop("required", true);
               if($("input[type='radio'][name='mother']:checked").length >0){
               setMotherrAddressRequiredFields(true);
             }
           }
   
           if ($('#father-addr-checkbox').find('input:checkbox:checked').length > 0) {
               $("#father-addr").prop("disabled", true);
               $("#father-addr").prop("required", false);
           } else {
               $("#father-addr").prop("disabled", false);
               $("#father-addr").prop("required", true);
               if($("input[type='radio'][name='father']:checked").length >0){
               setFatherAddressRequiredFields(true);
             }
           }
   
         //parent same as student address checkbox change function
         $('#mother-addr-checkbox').change(function(){
           if($(this).find('input:checkbox:checked').length >0){
             $("#mother-addr").prop("disabled",true);
             $("#mother-addr").prop("required",false);
             setMotherrAddressRequiredFields(false);
   
             $('#mo_street_error').text("").prop("style","");
             $('#mo_city_error').text("");
             $('#mo_province_error').text("");
             $('#mo_country_error').text("");
             $('#mo_postal_error').text("");
           }
           else{
             $("#mother-addr").prop("disabled",false);
             $("#mother-addr").prop("required",true);
             if($("input[type='radio'][name='mother']:checked").length >0){
               setMotherrAddressRequiredFields(true);
             }
           }
         });
   
         
         $('#father-addr-checkbox').change(function(){
           if($(this).find('input:checkbox:checked').length >0){
             $("#father-addr").prop("disabled",true);
             $("#father-addr").prop("required",false);
   
             setFatherAddressRequiredFields(false);
   
             $('#pa_street_error').text("");
             $('#pa_city_error').text("");
             $('#pa_province_error').text("");
             $('#pa_country_error').text("");
             $('#pa_postal_error').text("");
           }
           else{
             $("#father-addr").prop("disabled",false);
             $("#father-addr").prop("required",true);
             if($("input[type='radio'][name='father']:checked").length >0){
               setFatherAddressRequiredFields(true);
             }
           }
         });
   
         //parent select radio button change function
         $("input[type='radio'][name='father']").change(function(){
             $('#pa_fname').prop('required',true);
             $('#pa_lname').prop('required',true);
             if ($('#father-addr-checkbox').find('input:checkbox:checked').length == 0) {
               setFatherAddressRequiredFields(true);
             }
             $('#pa_hphone').prop('required',true);
             $('#pa_occupation').prop('required',true);
             $('#pa_employer').prop('required',true);
         })
         $("input[type='radio'][name='mother']").change(function(){                    
             $('#mo_fname').prop('required',true);
             $('#mo_lname').prop('required',true);
             if ($('#mother-addr-checkbox').find('input:checkbox:checked').length == 0) {
               setMotherrAddressRequiredFields(true);
             }
             $('#mo_hphone').prop('required',true);
             $('#mo_occupation').prop('required',true);
             $('#mo_employer').prop('required',true);
         })
   
         // Alumna + alumnus
             //when form open - toggle alumnus condition box
             var alumnus = $("input[name='alumnus']:checked").val();
                 if(alumnus == "yes"){
                   $("#alumnus_input").prop("disabled",false);
                     $("#alumnus_input").prop("required",true);
                 }
                 else{
                   $("#alumnus_input").prop("disabled",true);
                   $("#alumnus_input").prop("required",false);
                   $("#alumnus_input").val("")
                 }
   
               //toggle alumna condition box
               var alumna = $("input[name='alumna']:checked").val();
                 if(alumna == "yes"){
                   $("#alumna_input").prop("disabled",false);
                     $("#alumna_input").prop("required",true);
                 }
                 else{
                   $("#alumna_input").prop("disabled",true);
                   $("#alumna_input").prop("required",false);
                   $("#alumna_input").val("")
                 }
   
               //radio button change function
             $("input[type='radio']").click(function(){
               var alumnus = $("input[name='alumnus']:checked").val();
               var alumnus_check = $("input[name='alumnus']");
               if(alumnus_check.length >0){
                 if(alumnus == "yes"){
                 $("#alumnus_input").prop("disabled",false);
                 $("#alumnus_input").prop("required",true);
               }
               else{
                 $("#alumnus_input").prop("disabled",true);
                 $("#alumnus_input").prop("required",false);
                 $("#alumnus_input").val("")
               }
               }
               
               var alumna = $("input[name='alumna']:checked").val();
               var alumna_check = $("input[name='alumna']");
               if(alumna_check.length >0 ){
                 if(alumna == "yes"){
                 $("#alumna_input").prop("disabled",false);
                 $("#alumna_input").prop("required",true);
               }
               else{
                 $("#alumna_input").prop("disabled",true);
                 $("#alumna_input").prop("required",false);
                 $("#alumna_input").val("")
               }
               }
           });
         
         //Student lives with
             if($("input[name='custody_status']:checked").val() == "Other"){
                 $("#student_lives_with").prop("disabled",false);
                 $("#student_lives_with").prop("required",true);
               }
               else{
                 $("#student_lives_with").prop("disabled",true);
                 $("#student_lives_with").prop("required",false);
               }
               
           //enable Other input
           $("input[type='radio'][name='custody_status']").change(function(){
             console.log($(this))
             if($(this).val() == "Other"){
                 $("#student_lives_with").prop("disabled",false);
                 $("#student_lives_with").prop("required",true);
               }
               else{
                 $("#student_lives_with").prop("disabled",true);
                 $("#student_lives_with").prop("required",false);
                 $("#student_lives_with").val("");
               }
           })  
     })
    </script>