{{-- multi-step form script, show and hide step + form validation --}}
<script>
    var x = document.getElementsByClassName("tab");
    x[0].style.display = "none";
    var number_of_tabs = x.length;
    var currentStep = <?=$step?>; 
    showTab(currentStep);

    function showTab(n) {
    window.scrollTo(0,0);
    var x = document.getElementsByClassName("tab");
    x[currentStep].style.display = "none";
    currentStep=n;
    document.getElementById('step_id').value = currentStep;
    var x = document.getElementsByClassName("tab");
    if(currentStep == 0 ){
      x[n].style.display = "block";
    }
    else{
      x[n].style.display = "block";
    }
   
    fixStepIndicator(n)
    }
    function nextPrev(n,y) {              
    var x = document.getElementsByClassName("tab");
    if (n == 1 && !validateForm()) return false;
    x[currentStep].style.display = "none";
    currentStep = currentStep + n;
    document.getElementById('step_id').value = currentStep;
    if (currentStep >= x.length) {
        document.getElementsByClassName("continue").className += "disabled";
        return false;
    }
    showTab(currentStep);
    }

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    // all required input fields have it own <span> to display error. If the validation throws "innerHTML" error, check if the <span> is existing first.
    function validateForm() {
     
      var x, y, i, valid = true;
      x = document.getElementsByClassName("tab");
      y = x[currentStep].querySelectorAll("[required]")

      // A loop that checks every required input field in the current tab
      for (i = 0; i < y.length; i++) {
        // If a field is empty
        if (y[i].value == "" ) {
          var error_id = y[i].name+'_error';
          document.getElementById(error_id).innerHTML ="*Required Field";
          document.getElementById(error_id).style.color = "#FF0000";
          y[i].style.backgroundColor = "#ffdddd";
          y[i].focus();
          valid = false;
        }
        else{
          var error_id = y[i].name+'_error';
          document.getElementById(error_id).innerHTML ="";
          y[i].style.backgroundColor = "white";
        }
      }

      //step1 validation - student information

      //step 2 validation - Medical
        //student's immunizations radio button 
        if(currentStep == 1){
          var check_immunizations =  x[currentStep].querySelectorAll('input[name="immunization"]:checked')
          if(check_immunizations.length == 0){
            valid = false;
            document.getElementById('immunization_error').innerHTML ="*Required Field";
            document.getElementById('immunization_error').style.color = "#FF0000";
          }
          else{
            clearError("immunization_error")
          }

          //medical consition checkbox
          var check_medical = x[currentStep].querySelectorAll('input[name="stu_medical[]"]:checked')
          if(check_medical.length == 0){
              valid = false;
              document.getElementById('stu_medical_error').innerHTML ="*Required Field";
              document.getElementById('stu_medical_error').style.color = "#FF0000";
            }
            else{
              clearError("stu_medical_error")
            }

          //consent emergency procedure radio button 
          var check_medical_consent =  x[currentStep].querySelectorAll('input[name="medical_consent"]:checked')
          if(check_medical_consent.length == 0){
            valid = false;
            document.getElementById('medical_consent_error').innerHTML ="*Required Field";
            document.getElementById('medical_consent_error').style.color = "#FF0000";
          }
          else{
            clearError("medical_consent_error")
          }
        }

      //step 3 validation - Parent Validation

      //step 4 validation - Indigenous -(Oprional)
    
      //step 5 validation - Photo 
      //photo consent is step 4 in canadian registration and 3 in internatinal registration.
      var fix_step_photo = currentStep;
      if(number_of_tabs == 11){
        fix_step_photo = 4;
      }
      else{
        fix_step_photo = 3;
      }
      if(currentStep==fix_step_photo){
        //Media consent radio button 
          var check_media_consent=  x[currentStep].querySelectorAll('input[name="media_consent"]:checked')
          if(check_media_consent.length == 0){
            valid = false;
            document.getElementById('media_consent_error').innerHTML ="*Required Field";
            document.getElementById('media_consent_error').style.color = "#FF0000";
          }
          else{
            clearError("media_consent_error")
          }
          var check_uploaded_file= document.getElementById('family_photo-show');
          if(check_uploaded_file.innerText.length == 0 ){
            valid = false;
            document.getElementById('family_photo_error').innerHTML ="*Required Field";
            document.getElementById('family_photo_error').style.color = "#FF0000";
          }
          else{
            clearError("family_photo_error")
          }
      }

      //step 6 validation - email 
      var fix_step_email = currentStep;
      if(number_of_tabs == 11){
        fix_step_email = 5;
      }
      else{
        fix_step_email = 4;
      }
      if(currentStep==fix_step_email){
      var check_email_auth=  x[currentStep].querySelectorAll('input[name="email_auth"]:checked')
          if(check_email_auth.length == 0){
            valid = false;
            document.getElementById('email_auth_error').innerHTML ="*Required Field";
            document.getElementById('email_auth_error').style.color = "#FF0000";
          }
          else{
            clearError("email_auth_error")
          }
      }
      //step 7 validation - acknowledgement of transfer 
      if(currentStep==6){
          var check_acknowledgement_of_transfer = x[currentStep].querySelectorAll('input[name="acknowledgement_of_transfer"]:checked')
          if(check_acknowledgement_of_transfer.length == 0){
              valid = false;
              document.getElementById('acknowledgement_of_transfer_error').innerHTML ="*Required Field";
              document.getElementById('acknowledgement_of_transfer_error').style.color = "#FF0000";
            }
            else{
              clearError("acknowledgement_of_transfer_error")
            }
      }

      //step 8 validation - responsibility of payments
      var fix_step_payment = currentStep;
      if(number_of_tabs == 11){
        fix_step_payment = 7;
      }
      else{
        fix_step_payment = 5;
      }
        if(currentStep==fix_step_payment){
        //paypment consent checkbox
        var check_tuition_consent = x[currentStep].querySelectorAll('input[name="tuition_consent"]:checked')
        if(check_tuition_consent.length == 0){
            valid = false;
            document.getElementById('tuition_consent_error').innerHTML ="*Required Field";
            document.getElementById('tuition_consent_error').style.color = "#FF0000";
          }
          else{
            clearError("tuition_consent_error")
          }

        //refund policy consent checkbox
        var check_refund_consent = x[currentStep].querySelectorAll('input[name="refund_consent"]:checked')
        if(check_refund_consent.length == 0){
            valid = false;
            document.getElementById('refund_consent_error').innerHTML ="*Required Field";
            document.getElementById('refund_consent_error').style.color = "#FF0000";
          }
          else{
            clearError("refund_consent_error")
          }
        }

      if (valid) {
        document.getElementsByClassName("step")[currentStep].className += " finish";
        var class_name = document.getElementsByClassName("step")[currentStep].className.split(' active').join('')
        document.getElementsByClassName("step")[currentStep].className = class_name;
      }
      return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            if(i <= n){
                x[i].className = x[i].className.replace(" disabled", "");
                x[i].className = x[i].className.split(' active').join('');
                if(!x[i].classList.contains('active')){
                    x[n].className += " active";
                }
            }else{
                x[i].className = x[i].className.split(' active').join('');
                if(!x[i].classList.contains('disabled')){
                    x[i].className += " disabled";
                }
            }
        }
    }

    function clearError(field_name){
      document.getElementById(field_name).innerHTML ="";
      document.getElementById(field_name).style.color = "";
    }
 </script>  
