@extends('auth.user_auth.layouts.registration')

@section('content')
<article id="content" class="application-review">
    @include('auth.user_auth.inc.messages')
    <form method="POST" action="{!! URL::route('paypal',350) !!}" enctype="multipart/form-data">
        @csrf
        <div class="tab step1" style="display: none;">
          @include('auth.user_auth.modules.registration.steps.student-info')
        </div>

        <div class="tab step2" style="display: none;">
          @include('auth.user_auth.modules.registration.steps.medical')
        </div>

        <div class="tab step3" style="display: none;">
          @include('auth.user_auth.modules.registration.steps.family-info')
        </div>

        @if ($type==1)
            <div class="tab step4" style="display: none;">
              @include('auth.user_auth.modules.registration.steps.indigenous')
            </div>
        @endif

        <div class="tab step5" style="display: none;">
          @include('auth.user_auth.modules.registration.steps.photo')
        </div>

        <div class="tab step6" style="display: none;">
          @include('auth.user_auth.modules.registration.steps.email-authorization')
        </div>
        
        @if ($type==1)
        <div class="tab step7" style="display: none;">
          @include('auth.user_auth.modules.registration.steps.acknowledgement-of-transfers')
        </div>
    @endif
      <div class="tab step8" style="display: none;">
        @include('auth.user_auth.modules.registration.steps.tuition-consent')
      </div>

      <div class="tab step9" style="display: none;">
        @include('auth.user_auth.modules.registration.steps.accuracy')
      </div>
      
        <div class="tab summary" style="display: none;">
              @include('auth.user_auth.modules.registration.steps.summary')
        </div>

        <div class="tab visible" style="display: none;">
          @include('auth.user_auth.modules.registration.steps.payment')
        </div>
    </form>
</article>
@endsection