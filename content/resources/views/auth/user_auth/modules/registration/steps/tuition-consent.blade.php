<section>
    <h2>Responsibility for Payment of Registration, Capital and Tuition Fees</h2>
    <div class="row">
      <fieldset class="radio-block">
        <label><input type="checkbox" name="tuition_consent" {{ (isset($registration_data->tuition_consent)  && $registration_data->tuition_consent=="on") ? ' checked' : (old("tuition_consent" == "on" ? "checked" :"")) }}>I/We hereby acknowledge that as the parent(s)/guardian(s), I am/we are responsible for the fees.</label>
        <span id="tuition_consent_error"></span>
      </fieldset>
    </div>
    <h3>Withdrawal & Refund Policy</h3>
    <div class="row">
      <fieldset class="radio-block">
        <label><input type="checkbox" name="refund_consent" {{ (isset($registration_data->refund_consent)  && $registration_data->refund_consent=="on") ? ' checked' : (old("refund_consent" == "on" ? "checked" :"")) }}>I/We acknowledge that we have read and accept the refund policy and withdrawal procedures.</label>
        <span id="refund_consent_error"></span>
      </fieldset>
    </div>
    <div class="row">
      @if (isset($application_data->father))
      <div class="column">
        <label>Father/Guardian Initial: <input required type="text" id="pa_ini_tuition" name="pa_ini_tuition" value="{{$registration_data->pa_ini_tuition ?? (old("pa_ini_tuition" == "on" ? "checked" :""))}}"><span id='pa_ini_tuition_error'></span></label>
      </div>
      @endif

      @if (isset($application_data->mother))
      <div class="column">
        <label>Mother/Guardian Initial: <input required type="text" id="mo_ini_tuition" name="mo_ini_tuition" value="{{$registration_data->mo_ini_tuition ?? (old("mo_ini_tuition" == "on" ? "checked" :""))}}"><span id='mo_ini_tuition_error'></span></label>
      </div>
      @endif
    </div>
</section>
<footer>
  <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
  <a class="button save" id="save_for_later">Save for Later</a>
  <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
</footer>