<header>
  <h1>Indigenous</h1>
</header>
<section>
  <h2>Indigenous Identity Declaration</h2>
  <p><strong>Indigenous Identity Declaration helps to support the efforts of Manitoba Education and Advanced Learning to plan and improve programs in a way that is responsive to Indigenous learners.</strong></p>
  {{-- <p>Providing this personal information is voluntary and optional. It is being collected in compliance with section 36(1)(b) of The Freedom of Information and Protection of Privacy Act. If you have any questions regarding the collection of this personal information, please contact the school principal.</p> --}}
  <div class="row">
    <fieldset class="radio">
      <label><input type="radio" name="indigenous_declaration" value="I am submitting my child’s Indigenous Identity Declaration for the first time." {{(isset($registration_data->indigenous_declaration) && $registration_data->indigenous_declaration=='I am submitting my child’s Indigenous Identity Declaration for the first time.'? 'checked' : (old("indigenous_declaration")=='I am submitting my child’s Indigenous Identity Declaration for the first time.'? 'checked' :''))}}>I am submitting my child’s Indigenous Identity Declaration for the first time.</label>
      <label><input type="radio" name="indigenous_declaration" value="I am making changes to my child’s Indigenous Identity Declaration." {{(isset($registration_data->indigenous_declaration) && $registration_data->indigenous_declaration=='I am making changes to my child’s Indigenous Identity Declaration.'? 'checked' :(old("indigenous_declaration")=='I am making changes to my child’s Indigenous Identity Declaration.'? 'checked' :''))}}>I am making changes to my child’s Indigenous Identity Declaration.</label>
      <label><input type="radio" name="indigenous_declaration" value="I already submitted my child’s Indigenous Identity Declaration and have no further changes to make at this time." {{(isset($registration_data->indigenous_declaration) && $registration_data->indigenous_declaration=='I already submitted my child’s Indigenous Identity Declaration and have no further changes to make at this time.'? 'checked' :(old("indigenous_declaration")=='I already submitted my child’s Indigenous Identity Declaration and have no further changes to make at this time.'? 'checked' :''))}}>I already submitted my child’s Indigenous Identity Declaration and have no further changes to make at this time.</label>
      <span id="indigenous_declaration_error"></span>
    </fieldset>
  </div>
  <div class="row">
    <fieldset class="checkbox">
      <legend>If your child is an Indigenous person, please select the option(s) that best describe your child now.</legend>
      <label><input type="checkbox" name="indigenous_child[]" value="First Nations (North American Indian)*" {{ (isset($registration_data->indigenous_child)  && in_array('First Nations (North American Indian)*', $registration_data->indigenous_child)) ? ' checked' : (is_array(old('indigenous_child')) && in_array('First Nations (North American Indian)*', old("indigenous_child")) ? ' checked' : '') }}>First Nations (North American Indian)*</label>
      <label><input type="checkbox" name="indigenous_child[]" value="Métis" {{ (isset($registration_data->indigenous_child)  && in_array('Métis', $registration_data->indigenous_child)) ? ' checked' : (is_array(old('indigenous_child')) && in_array('Métis', old("indigenous_child")) ? ' checked' : '') }}>Métis</label>
      <label><input type="checkbox" name="indigenous_child[]" value="Inuk (Inuit)" {{ (isset($registration_data->indigenous_child)  && in_array('Inuk (Inuit)', $registration_data->indigenous_child)) ? ' checked' : (is_array(old('indigenous_child')) && in_array('Inuk (Inuit)', old("indigenous_child")) ? ' checked' : '') }}>Inuk (Inuit)</label>
    </fieldset>
  </div>
  <p><em>*includes Status and Non-Status Indians</em></p>
  <div class="row">
    <fieldset class="checkbox">
      <legend>Which best describes your child’s Indigenous cultural-linguistic identity? Please select up to two choices:</legend>
      <label><input type="checkbox" name="cultural_linguistic[]" value="Anishinaabe (Ojibway/Saulteaux)" {{ (isset($registration_data->cultural_linguistic)  && in_array('Anishinaabe (Ojibway/Saulteaux)', $registration_data->cultural_linguistic)) ? ' checked' : (is_array(old('cultural_linguistic')) && in_array('Anishinaabe (Ojibway/Saulteaux)', old("cultural_linguistic")) ? ' checked' : '') }}>Anishinaabe (Ojibway/Saulteaux)</label>
      <label><input type="checkbox" name="cultural_linguistic[]" value="Ininiw (Cree)"  {{ (isset($registration_data->cultural_linguistic)  && in_array('Ininiw (Cree)', $registration_data->cultural_linguistic)) ? ' checked' : (is_array(old('cultural_linguistic')) && in_array('Ininiw (Cree)', old("cultural_linguistic")) ? ' checked' : '')  }}>Ininiw (Cree)</label>
      <label><input type="checkbox" name="cultural_linguistic[]" value="Dene (Sayisi)"  {{ (isset($registration_data->cultural_linguistic)  && in_array('Dene (Sayisi)', $registration_data->cultural_linguistic)) ? ' checked' : (is_array(old('cultural_linguistic')) && in_array('Dene (Sayisi)', old("cultural_linguistic")) ? ' checked' : '')  }}>Dene (Sayisi)</label>
      <label><input type="checkbox" name="cultural_linguistic[]" value="Dakota"  {{ (isset($registration_data->cultural_linguistic)  && in_array('Dakota', $registration_data->cultural_linguistic)) ? ' checked' : (is_array(old('cultural_linguistic')) && in_array('Dakota', old("cultural_linguistic")) ? ' checked' : '')  }}>Dakota</label>
      <label><input type="checkbox" name="cultural_linguistic[]" value="Oji-Cree"  {{ (isset($registration_data->cultural_linguistic)  && in_array('Oji-Cree', $registration_data->cultural_linguistic)) ? ' checked' : (is_array(old('cultural_linguistic')) && in_array('Oji-Cree', old("cultural_linguistic")) ? ' checked' : '')  }}>Oji-Cree</label>
      <label><input type="checkbox" name="cultural_linguistic[]" value="Michif"  {{ (isset($registration_data->cultural_linguistic)  && in_array('Michif', $registration_data->cultural_linguistic)) ? ' checked' : (is_array(old('cultural_linguistic')) && in_array('Michif', old("cultural_linguistic")) ? ' checked' : '')  }}>Michif</label>
      <label><input type="checkbox" name="cultural_linguistic[]" value="Inuktitut"  {{ (isset($registration_data->cultural_linguistic)  && in_array('Inuktitut', $registration_data->cultural_linguistic)) ? ' checked' : (is_array(old('cultural_linguistic')) && in_array('Inuktitut', old("cultural_linguistic")) ? ' checked' : '')  }}>Inuktitut</label>
      <label><input type="checkbox" name="cultural_linguistic[]" value="Other"  {{ (isset($registration_data->cultural_linguistic)  && in_array('Other', $registration_data->cultural_linguistic)) ? ' checked' : (is_array(old('cultural_linguistic')) && in_array('Other', old("cultural_linguistic")) ? ' checked' : '')  }}>Other (specify)</label>
    </fieldset>
  </div>
  <div class="row">
    <label><textarea></textarea></label>
  </div>
</section>

<footer>
  <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
  <a class="button save" id="save_for_later">Save for Later</a>
  <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
</footer>
