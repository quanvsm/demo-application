<section class="accuracy">
    <h2>Accuracy Of Information</h2>
    <div class="row">
      <p>I/We affirm by initialling, signing and submitting this completed Registration Form, that I/we have thoroughly read and understand the information contained and confirm the accuracy of all responses.  </p>
    </div>
    <div class="row">
        @if (isset($application_data->father))
        <div class="column">
          <label>Father/Guardian Name: <input  type="text" id="pa_sig_accuracy" name="pa_sig_accuracy" value="{{$registration_data->pa_sig_accuracy ?? old("pa_sig_accuracy")}}"><span id='pa_sig_accuracy_error'></span></label>
          <label>Date<input  type="text" class="jquery_datepicker" id="pa_sig_accuracy_date" name="pa_sig_accuracy_date" value="{{($registration_data->pa_sig_accuracy_date ?? old("pa_sig_accuracy_date"))}}"><span id='pa_sig_accuracy_date_error'></span></label>
        </div>
        @endif
  
        @if (isset($application_data->mother))
        <div class="column">
          <label>Mother/Guardian Name: <input  type="text" id="mo_sig_accuracy" name="mo_sig_accuracy" value="{{$registration_data->mo_sig_accuracy ?? old("mo_sig_accuracy")}}"><span id='mo_sig_accuracy_error'></span></label>
          <label>Date<input class="jquery_datepicker" type="text" id="mo_sig_accuracy_date" name="mo_sig_accuracy_date" value="{{$registration_data->mo_sig_accuracy_date ?? old("mo_sig_accuracy_date")}}"><span id='mo_sig_accuracy_date_error'></span></label>
        </div>
        @endif
      </div>
      {{-- <p><strong>Linden Christian School respects your privacy. We protect personal information and adhere to all legislative requirements with respect to protecting privacy. We do not rent, sell, or trade our mailing lists. The information you provide will be used to deliver services and to keep you informed and up to date on the activities of Linden Christian School, including programs, services, special events, funding needs, opportunities to volunteer or to give, open houses and more through periodic contacts. If you wish to be removed from any of the contacts, please inform the Admissions Office in writing of your withdrawal of consent.</strong></p> --}}
</section>
<footer>
  <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
    <a class="button save" id="save_for_later">Save for Later</a>
    <a class="button green continue" id="summary_button">Continue to Review</a>
  </footer>