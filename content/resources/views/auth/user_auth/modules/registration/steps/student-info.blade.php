<header>
  <h1>Student Information</h1>
</header>
<section>
<div class="row">
  <input type="hidden" id="step_id" name="step_id" value='{{$step ?? 0}}'>
  <input type="hidden" id="application_id" name="application_id" value="{{$application_id ?? ''}}">
  <input type="hidden" id="form_id" name="form_id" value="{{$registration->id ?? ''}}">
  @if($type == 1)
  <fieldset class="radio grade" id="canadian-grade">
    <!-- Canadian application only -->
    <label><input type="radio" id="grade-rad" name="grade"
        {{(isset($registration_data->grade) ? ($registration_data->grade=='on'? 'checked' :'') : ( isset($application_data->grade) ? ($application_data->grade=='on'? 'checked' :''):''))}}>Grade</label>
    <label><input name="grade_in" type="number" min="1" max="12" step="1" disabled id="grade-input"
        value="{{$registration_data->grade_in ?? ($application_data->grade_in ??'')}}"><span></label><!-- enable input if above is selected -->
    <label><input type="radio" name="grade" value="Full Time Kindergarten"
        {{(isset($registration_data->grade) ? ($registration_data->grade=='Full Time Kindergarten'? 'checked' :'') : ( isset($application_data->grade)  ? ($application_data->grade=='Full Time Kindergarten'? 'checked' :''):''))}}>*e.g: Full Time
      Kindergarten</label>
    <label><input type="radio" name="grade" value="Part Time Kindergarten"
        {{(isset($registration_data->grade) ? ($registration_data->grade=='Part Time Kindergarten'? 'checked' :'') : (isset($application_data->grade) ? ($application_data->grade=='Part Time Kindergarten'? 'checked' :''):''))}}>*e.g: Part Time
      Kindergarten</label>
    <span id='grade_error'></span>
    <span id='grade_in_error'></span>
  </fieldset>

  @else
  <label id="international-grade">Grade<input type="number" min="1" max="12" step="1" name="int_grade" required
      value="{{$registration_data->int_grade ??$application_data->int_grade ?? ''}}"><span id='int_grade_error'></span></label>
  <!-- International appllication only -->
  @endif
</div>
<h3>Name</h3>
<div class="row">
  <label>Legal Last Name<input name="stu_lname" type="text" value="{{$registration_data->stu_lname ?? ($application_data->stu_lname ??'') }}" required><span
      id='stu_lname_error'></span></label>
      <label>Legal First Name<input name="stu_fname" type="text" value="{{$registration_data->stu_fname ?? $application_data->stu_fname ??''}}" required><span
        id='stu_fname_error'></span></label>
  <label>Legal Middle Name<input name="stu_mname" type="text" value="{{$registration_data->stu_mname ?? $application_data->stu_mname ??''}}"><span
      id='stu_mname_error'></span></label>
  <label>Commonly Goes By<input name="stu_cname" type="text" value="{{$registration_data->stu_cname ?? $application_data->stu_cname ??''}}"><span
      id='stu_cname_error'></span></label>
</div>
<div class="row">
  <fieldset>
    <legend>Date of Birth</legend>
    <label>
      <select name="stu_month_of_birth" required>
        <option value="">-- Month --</option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='01'? 'selected' :''):($application_data->stu_month_of_birth=='01'? 'selected' :''))}}>
          01</option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='02'? 'selected' :''):($application_data->stu_month_of_birth=='02'? 'selected' :''))}}>
          02</option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='03'? 'selected' :''):($application_data->stu_month_of_birth=='03'? 'selected' :''))}}>
          03</option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='04'? 'selected' :''):($application_data->stu_month_of_birth=='04'? 'selected' :''))}}>
          04</option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='05'? 'selected' :''):($application_data->stu_month_of_birth=='05'? 'selected' :''))}}>05
        </option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='06'? 'selected' :''):($application_data->stu_month_of_birth=='06'? 'selected' :''))}}>06
        </option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='07'? 'selected' :''):($application_data->stu_month_of_birth=='07'? 'selected' :''))}}>07
        </option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='08'? 'selected' :''):($application_data->stu_month_of_birth=='08'? 'selected' :''))}}>
          08</option>
        <option
        {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='09'? 'selected' :''):($application_data->stu_month_of_birth=='09'? 'selected' :''))}}>
          09</option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='10'? 'selected' :''):($application_data->stu_month_of_birth=='10'? 'selected' :''))}}>
          10</option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='11'? 'selected' :''):($application_data->stu_month_of_birth=='11'? 'selected' :''))}}>
          11</option>
        <option {{(isset($registration_data->stu_month_of_birth) ? ($registration_data->stu_month_of_birth=='12'? 'selected' :''):($application_data->stu_month_of_birth=='12'? 'selected' :''))}}>
          12</option>
      </select>
      <span id='stu_month_of_birth_error'></span>
    </label>
    <label>
      <select name="stu_date_of_birth" required>
        <option value="">-- Day --</option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='01'? 'selected' :'') : ($application_data->stu_date_of_birth=='01'? 'selected' :''))}}>01
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='02'? 'selected' :'') : ($application_data->stu_date_of_birth=='02'? 'selected' :''))}}>02
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='03'? 'selected' :'') : ($application_data->stu_date_of_birth=='03'? 'selected' :''))}}>03
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='04'? 'selected' :'') : ($application_data->stu_date_of_birth=='04'? 'selected' :''))}}>04
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='05'? 'selected' :'') : ($application_data->stu_date_of_birth=='05'? 'selected' :''))}}>05
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='06'? 'selected' :'') : ($application_data->stu_date_of_birth=='06'? 'selected' :''))}}>06
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='07'? 'selected' :'') : ($application_data->stu_date_of_birth=='07'? 'selected' :''))}}>07
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='08'? 'selected' :'') : ($application_data->stu_date_of_birth=='08'? 'selected' :''))}}>08
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='09'? 'selected' :'') : ($application_data->stu_date_of_birth=='09'? 'selected' :''))}}>09
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='10'? 'selected' :'') : ($application_data->stu_date_of_birth=='10'? 'selected' :''))}}>10
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='11'? 'selected' :'') : ($application_data->stu_date_of_birth=='11'? 'selected' :''))}}>11
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='12'? 'selected' :'') : ($application_data->stu_date_of_birth=='12'? 'selected' :''))}}>12
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='13'? 'selected' :'') : ($application_data->stu_date_of_birth=='13'? 'selected' :''))}}>13
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='14'? 'selected' :'') : ($application_data->stu_date_of_birth=='14'? 'selected' :''))}}>14
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='15'? 'selected' :'') : ($application_data->stu_date_of_birth=='15'? 'selected' :''))}}>15
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='16'? 'selected' :'') : ($application_data->stu_date_of_birth=='16'? 'selected' :''))}}>16
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='17'? 'selected' :'') : ($application_data->stu_date_of_birth=='17'? 'selected' :''))}}>17
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='18'? 'selected' :'') : ($application_data->stu_date_of_birth=='18'? 'selected' :''))}}>18
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='19'? 'selected' :'') : ($application_data->stu_date_of_birth=='19'? 'selected' :''))}}>19
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='20'? 'selected' :'') : ($application_data->stu_date_of_birth=='20'? 'selected' :''))}}>20
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='21'? 'selected' :'') : ($application_data->stu_date_of_birth=='21'? 'selected' :''))}}>21
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='22'? 'selected' :'') : ($application_data->stu_date_of_birth=='22'? 'selected' :''))}}>22
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='23'? 'selected' :'') : ($application_data->stu_date_of_birth=='23'? 'selected' :''))}}>23
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='24'? 'selected' :'') : ($application_data->stu_date_of_birth=='24'? 'selected' :''))}}>24
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='25'? 'selected' :'') : ($application_data->stu_date_of_birth=='25'? 'selected' :''))}}>25
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='26'? 'selected' :'') : ($application_data->stu_date_of_birth=='26'? 'selected' :''))}}>26
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='27'? 'selected' :'') : ($application_data->stu_date_of_birth=='27'? 'selected' :''))}}>27
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='28'? 'selected' :'') : ($application_data->stu_date_of_birth=='28'? 'selected' :''))}}>28
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='29'? 'selected' :'') : ($application_data->stu_date_of_birth=='29'? 'selected' :''))}}>29
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='30'? 'selected' :'') : ($application_data->stu_date_of_birth=='30'? 'selected' :''))}}>30
        </option>
        <option {{(isset($registration_data->stu_date_of_birth) ?($registration_data->stu_date_of_birth=='31'? 'selected' :'') : ($application_data->stu_date_of_birth=='31'? 'selected' :''))}}>31
        </option>
      </select>
      <span id='stu_date_of_birth_error'></span>
    </label>
    <label>
      <select name="stu_year_of_birth" required>
        <option value="">-- Year --</option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2016'? 'selected' :'') : ($application_data->stu_year_of_birth=='2016'? 'selected' :''))}}>2016
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2015'? 'selected' :'') : ($application_data->stu_year_of_birth=='2015'? 'selected' :''))}}>2015
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2014'? 'selected' :'') : ($application_data->stu_year_of_birth=='2014'? 'selected' :''))}}>2014
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2013'? 'selected' :'') : ($application_data->stu_year_of_birth=='2013'? 'selected' :''))}}>2013
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2012'? 'selected' :'') : ($application_data->stu_year_of_birth=='2012'? 'selected' :''))}}>2012
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2011'? 'selected' :'') : ($application_data->stu_year_of_birth=='2011'? 'selected' :''))}}>2011
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2010'? 'selected' :'') : ($application_data->stu_year_of_birth=='2010'? 'selected' :''))}}>2010
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2009'? 'selected' :'') : ($application_data->stu_year_of_birth=='2009'? 'selected' :''))}}>2009
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2008'? 'selected' :'') : ($application_data->stu_year_of_birth=='2008'? 'selected' :''))}}>2008
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2007'? 'selected' :'') : ($application_data->stu_year_of_birth=='2007'? 'selected' :''))}}>2007
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2006'? 'selected' :'') : ($application_data->stu_year_of_birth=='2006'? 'selected' :''))}}>2006
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2005'? 'selected' :'') : ($application_data->stu_year_of_birth=='2005'? 'selected' :''))}}>2005
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2004'? 'selected' :'') : ($application_data->stu_year_of_birth=='2004'? 'selected' :''))}}>2004
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2003'? 'selected' :'') : ($application_data->stu_year_of_birth=='2003'? 'selected' :''))}}>2003
        </option>
        <option {{(isset($registration_data->stu_year_of_birth) ? ($registration_data->stu_year_of_birth=='2002'? 'selected' :'') : ($application_data->stu_year_of_birth=='2002'? 'selected' :''))}}>2002
        </option>
      </select>
      <span id='stu_year_of_birth_error'></span>
    </label>
  </fieldset>
  <label>Gender
    <select name="stu_gender" required>
      <option value="">-- Select --</option>
      <option {{(isset($registration_data->stu_gender) ? ($registration_data->stu_gender=='Male'? 'selected' :'') : ($application_data->stu_gender=='Male'? 'selected' :''))}}>Male</option>
      <option {{(isset($registration_data->stu_gender) ? ($registration_data->stu_gender=='Female'? 'selected' :'') : ($application_data->stu_gender=='Female'? 'selected' :''))}}>Female</option>
    </select>
    <span id='stu_gender_error'></span>
  </label>
</div>
<h3>Home Address</h3>
<div class="row">
  <label>Street Address<input type="text" name="stu_street" value="{{$registration_data->stu_street ?? $application_data->stu_street??''}}" required><span id='stu_street_error'></span></label>
  <label>City<input type="text" name="stu_city" value="{{$registration_data->stu_city ?? $application_data->stu_city??''}}" required>  <span id='stu_city_error'></span></label>
  @if($type==1)
  <label>Province / State
    <select name="stu_province" required>
      <option value="">-- Select --</option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Alberta'? 'selected' :'') : ($application_data->stu_province=='Alberta'? 'selected' :''))}}>Alberta
      </option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='British Columbia'? 'selected' :'') : ($application_data->stu_province=='British Columbia'? 'selected' :''))}}>
        British Columbia
      </option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Manitoba'? 'selected' :'') : ($application_data->stu_province=='Manitoba'? 'selected' :''))}}>Manitoba
      </option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='New Brunswick'? 'selected' :'') : ($application_data->stu_province=='New Brunswick'? 'selected' :''))}}>New
        Brunswick</option>
      <option
        {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Newfoundland and Labrador'? 'selected' :'') : ($application_data->stu_province=='Newfoundland and Labrador'? 'selected' :''))}}>
        Newfoundland and Labrador</option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Nova Scotia'? 'selected' :'') : ($application_data->stu_province=='Nova Scotia'? 'selected' :''))}}>Nova Scotia
      </option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Ontario'? 'selected' :'') : ($application_data->stu_province=='Ontario'? 'selected' :''))}}>Ontario
      </option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Prince Edward Island'? 'selected' :'') : ($application_data->stu_province=='Prince Edward Island'? 'selected' :''))}}>
        Prince Edward Island</option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Quebec'? 'selected' :'') : ($application_data->stu_province=='Quebec'? 'selected' :''))}}>Quebec</option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Saskatchewan'? 'selected' :'') : ($application_data->stu_province=='Saskatchewan'? 'selected' :''))}}>
        Saskatchewan</option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Northwest Territories'? 'selected' :'') : ($application_data->stu_province=='Northwest Territories'? 'selected' :''))}}>
        Northwest Territories</option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Nunavut'? 'selected' :'') : ($application_data->stu_province=='Nunavut'? 'selected' :''))}}>Nunavut
      </option>
      <option {{(isset($registration_data->stu_province) ? ($registration_data->stu_province=='Yukon'? 'selected' :'') : ($application_data->stu_province=='Yukon'? 'selected' :''))}}>Yukon</option>
    </select>
    <span id='stu_province_error'></span>
  </label>
  @else
  <label>Province / State <input type="text" name="stu_province" value="{{$registration_data->stu_province ?? $application_data->stu_province ?? ''}}" required><span id='stu_province_error'></span></label>
  <label>Country<input type="text" name="stu_country" value="{{$registration_data->stu_country ?? $application_data->stu_country ?? ''}}" required><span id='stu_country_error'></span></label>
  @endif
  <label {{$type==2 ? "id=international-grade" : ""}}>Postal Code / Zip Code<input type="text" name="stu_postal" value="{{$registration_data->stu_postal ??$application_data->stu_postal ?? ''}}" required><span id='stu_postal_error'></span></label>
</div>
<div class="row">
  <label>Primary Phone<input class="phone_number_format" required type="text" name="stu_phone" maxlength="12" value="{{$registration_data->stu_phone ??$application_data->stu_phone ?? ''}}"><span id='stu_phone_error'></span></label>
  <label>Primary Family Email<input type="email" name="stu_email" value="{{$registration_data->stu_email ??$application_data->stu_email ?? ''}}"
      required><span id='stu_email_error'></span></label>
</div>

<div class="row">
  <label>Citizenship / Immigration Status
    <select id="option" name="stu_citizenship_status" required>
      <option value="">-- Select --</option>
      @if($type == 1)
      <!-- Canadian application options -->
      <option id="canadian"
        {{(isset($registration_data->stu_citizenship_status) ? ($registration_data->stu_citizenship_status=='Canadian Citizen'? 'selected' :'') : ($application_data->stu_citizenship_status=='Canadian Citizen'? 'selected' :''))}}>
        Canadian Citizen</option>
      <option id="canadian"
        {{(isset($registration_data->stu_citizenship_status) ? ($registration_data->stu_citizenship_status=='Permanent Resident'? 'selected' :'') : ($application_data->stu_citizenship_status=='Permanent Resident'? 'selected' :''))}}>
        Permanent Resident</option>
      <option id="canadian"
        {{(isset($registration_data->stu_citizenship_status) ? ($registration_data->stu_citizenship_status=='Parent Study / Work Permit'? 'selected' :'') : ($application_data->stu_citizenship_status=='Parent Study / Work Permit'? 'selected' :''))}}>
        Parent Study / Work Permit</option>
      @else
      <!-- International application options -->
      <option id="international"
        {{(isset($registration_data->stu_citizenship_status) ? ($registration_data->stu_citizenship_status=='International Student'? 'selected' :'') : ($application_data->stu_citizenship_status=='International Student'? 'selected' :''))}}>
        International Student</option>
      <option id="international"
        {{(isset($registration_data->stu_citizenship_status) ? ($registration_data->stu_citizenship_status=='Parent with Study Permit'? 'selected' :'') : ($application_data->stu_citizenship_status=='Parent with Study Permit'? 'selected' :''))}}>
        Parent with Study Permit</option>
      <option id="international"
        {{(isset($registration_data->stu_citizenship_status) ? ($registration_data->stu_citizenship_status=='Parent with Work Permit'? 'selected' :'') : ($application_data->stu_citizenship_status=='Parent with Work Permit'? 'selected' :''))}}>
        Parent with Work Permit</option>
      @endif
    </select>
    <span id='stu_citizenship_status_error'></span>
  </label>
  <label>Language Spoken at Home<input type="text" name="stu_lang" value="{{$registration_data->stu_lang ??$application_data->stu_lang ?? ''}}"><span id='stu_lang_error'></span></label>
  <label>Country of Birth (If not Canada)<input {{$type==2 ? 'required' : '' }} type="text" name="stu_country_of_birth"
      value="{{$registration_data->stu_country_of_birth ??$application_data->stu_country_of_birth ?? ''}}"><span id='stu_country_of_birth_error'></span></label>
</div>

{{-- <div class="row">
  <label>Current School<input type="text" name="stu_curr_school"
      value="{{$registration_data->stu_curr_school ?? $application_data->stu_curr_school ?? ''}}"><span id='stu_curr_school_error'></span></label>
      <label>Family Church (If applicable)<input name="church" type="text" value="{{$registration_data->church ??$application_data->church ?? ''}}"></label>
</div> --}}
</section>
<footer>
    <a class="button save" id="save_for_later">Save for Later</a>
    <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
</footer>