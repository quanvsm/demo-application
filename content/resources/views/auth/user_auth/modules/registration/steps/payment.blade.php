<header>
    <h1>New Registration - Payment and Submission</h1>
  </header>

  <section>
    <p>*The non-refundable fee of $350 must accompany your submission.</p>
    <table class="payment-invoice">
      <thead>
        <tr>
          <th>Item</th>
          <th>Student</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Fee</td>
          <td>{{($registration_data->stu_fname ??'').' '.($registration_data->stu_lname??'')}}</td>
          <td>$350</td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td><strong>$350</strong></td>
        </tr>
      </tbody>
    </table>
    @if ($paid)
    <div class="payment-message">
      <p>Payment Completed</p>
    </div>
      @else
      <button type="submit" class="button paypal">Pay </button>
    @endif
    
  </section>
  <footer>
    <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
    <a class="button sfl save">Save for Later</a>
    @if ($paid)
    <button type="submit" class="button green submit">Submit</button>
    @endif
  </footer>