<header>
  <h1>Email Notification Authorization</h1>
</header>
<section>
    <div class="row">
      <fieldset class="radio-block">
        <label><input type="checkbox" name="email_auth" {{ (isset($registration_data->email_auth)  && $registration_data->email_auth=="on") ? ' checked' : (old("email_auth" == "on" ? "checked" :"")) }}>I AUTHORIZE... </label>
        <span id="email_auth_error"></span>
      </fieldset>
    </div>
    <div class="row">
      @if (isset($application_data->father))
      <div class="column">
        <label>Father/Guardian Name: <input required type="text" id="pa_sig_email" name="pa_sig_email" value="{{$registration_data->pa_sig_email ?? old("pa_sig_email")}}"><span id='pa_sig_email_error'></span></label>
        <label>Date<input required class="jquery_datepicker" type="text" id="pa_sig_email_date" name="pa_sig_email_date" value="{{($registration_data->pa_sig_email_date ?? old("pa_sig_email_date"))}}"><span id='pa_sig_email_date_error'></span></label>
      </div>
      @endif

      @if (isset($application_data->mother))
      <div class="column">
        <label>Mother/Guardian Name: <input required type="text" id="mo_sig_email" name="mo_sig_email" value="{{$registration_data->mo_sig_email ?? old("mo_sig_email")}}"><span id='mo_sig_email_error'></span></label>
        <label>Date<input required class="jquery_datepicker" type="text" id="mo_sig_email_date" name="mo_sig_email_date" value="{{$registration_data->mo_sig_email_date ?? old("mo_sig_email_date")}}"><span id='mo_sig_email_date_error'></span></label>
      </div>
      @endif
    </div>
  </section>
  <footer>
    <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
    <a class="button save" id="save_for_later">Save for Later</a>
    <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
  </footer>