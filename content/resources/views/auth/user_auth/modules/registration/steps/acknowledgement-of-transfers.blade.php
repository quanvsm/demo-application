<section>
    <h2>Acknowledgement of Transfer of Cumulative File, Pupil Support & Personal Health Information</h2>
    {{-- <p>At such time as a student enrols at Linden Christian School, administration will request from the previous school student cumulative files, pupil support files, and personal health information.  Only personal information and personal health information necessary for the schooling and provision of educational services for the student will be provided to Linden Christian School.</p> --}}
    <div class="row">
      <fieldset class="radio-block">
        <label><input required type="checkbox" name="acknowledgement_of_transfer" {{ (isset($registration_data->acknowledgement_of_transfer)  && $registration_data->acknowledgement_of_transfer=="on") ? ' checked' : '' }}>I hereby acknowledge that my child’s/children’s cumulative file(s), Pupil Support file(s) and any personal health information will be requested and received from the previous school:</label>
        <span id="acknowledgement_of_transfer_error"></span>
      </fieldset>
    </div>
    <div class="row">
      <label>Name of Previous School<input required type="text" name="prev_school" value="{{$registration_data->prev_school ?? old("prev_school")}}"><span id="prev_school_error"></span></label>
    </div>
    <div class="row">
      @if (isset($application_data->father))
      <div class="column">
        <label>Father/Guardian Name: <input required type="text" id="pa_sig_transfer" name="pa_sig_transfer" value="{{$registration_data->pa_sig_transfer ?? old("pa_sig_transfer")}}"><span id='pa_sig_transfer_error'></span></label>
        <label>Date<input required class="jquery_datepicker" type="text" id="pa_sig_transfer_date" name="pa_sig_transfer_date" value="{{($registration_data->pa_sig_transfer_date ?? old("pa_sig_transfer_date"))}}"><span id='pa_sig_transfer_date_error'></span></label>
      </div>
      @endif

      @if (isset($application_data->mother))
      <div class="column">
        <label>Mother/Guardian Name: <input required type="text" id="mo_sig_transfer" name="mo_sig_transfer" value="{{$registration_data->mo_sig_transfer ?? old("mo_sig_transfer")}}"><span id='mo_sig_transfer_error'></span></label>
        <label>Date<input required class="jquery_datepicker" type="text" id="mo_sig_transfer_date" name="mo_sig_transfer_date" value="{{$registration_data->mo_sig_transfer_date ?? old("mo_sig_transfer_date")}}"><span id='mo_sig_transfer_date_error'></span></label>
      </div>
      @endif
    </div>
</section>
  <footer>
    <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
    <a class="button save" id="save_for_later">Save for Later</a>
    <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
  </footer>