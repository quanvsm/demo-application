<header>
    <h1>Medical</h1>
</header>
<section>
    <h2>Medical Information</h2>
    <div class="row">
      <label>Student's Doctor<input required type="text" name="doctor_name" value="{{$registration_data->doctor_name ?? old("doctor_name")}}"><span id="doctor_name_error"></span></label>
      <label>Doctor's Phone<input required class="phone_number_format" type="text" name="doctor_phone" maxlength="12" value="{{$registration_data->doctor_phone ?? old("doctor_phone")}}"><span id="doctor_phone_error"></span></label>
      <label>Manitoba Health Personal 9 Digit ID<input required type="text" name="health_card" maxlength="9" value="{{$registration_data->health_card ?? old("health_card")}}"><span id="health_card_error"></span></label>
    </div>
    <div class="row">
      <fieldset class="radio">
        <legend>Are student's immunizations up to date?</legend>
        <label><input type="radio" name="immunization" value="Yes" {{(isset($registration_data->immunization) && $registration_data->immunization=='Yes'? 'checked' : (old("immunization")=='Yes'? 'checked' : ""))}}>Yes</label>
        <label><input type="radio" name="immunization" value="No" {{(isset($registration_data->immunization) && $registration_data->immunization=='No'? 'checked' :(old("immunization")=='No'? 'checked' : ""))}}>No</label>
        <span id="immunization_error"></span>
      </fieldset>
    </div>
    <div class="row">
      <fieldset class="checkbox">
        <legend>Medical Conditions / Details</legend>
        <label><input type="checkbox" name="stu_medical[]" value="Life Threatening Allergies" {{ (isset($registration_data->stu_medical)  && in_array('Life Threatening Allergies', $registration_data->stu_medical)) ? ' checked' : (is_array(old('stu_medical')) && in_array('Life Threatening Allergies', old("stu_medical")) ? ' checked' : '') }}>Life Threatening Allergies</label>
        <label><input type="checkbox" name="stu_medical[]" value="Asthma" {{ (isset($registration_data->stu_medical)  && in_array('Asthma', $registration_data->stu_medical)) ? ' checked' : (is_array(old('stu_medical')) && in_array('Asthma', old("stu_medical")) ? ' checked' : '') }}>Asthma</label>
        <label><input type="checkbox" name="stu_medical[]" value="Epi-pen" {{ (isset($registration_data->stu_medical)  && in_array('Epi-pen', $registration_data->stu_medical)) ? ' checked' : (is_array(old('stu_medical')) && in_array('Epi-pen', old("stu_medical")) ? ' checked' : '') }}>Epi-pen</label>
        <label><input type="checkbox" name="stu_medical[]" value="Medications" {{ (isset($registration_data->stu_medical)  && in_array('Medications', $registration_data->stu_medical)) ? ' checked' : (is_array(old('stu_medical')) && in_array('Medications', old("stu_medical")) ? ' checked' : '') }}>Medications (specify)</label>
        <label><input type="checkbox" name="stu_medical[]" value="Other" {{ (isset($registration_data->stu_medical)  && in_array('Other', $registration_data->stu_medical)) ? ' checked' : (is_array(old('stu_medical')) && in_array('Other', old("stu_medical")) ? ' checked' : '') }}>Other (specify)</label>
        <span id="stu_medical_error"></span>
      </fieldset>
      <label><textarea name="medical_detail">{{$registration_data->medical_detail ?? old("medical_detail")}}</textarea></label>
    </div>
    <h2>Emergency Medical Permission & Consent for Medical Treatment</h2>
    <p>In case of an emergency, when a parent/guardian cannot be reached, please indicate who should be contacted.</p>
    <div class="row">
      <label>Name<input required type="text" name="emergency_name" value="{{$registration_data->emergency_name ?? old("emergency_name")}}"><span id="emergency_name_error"></span></label>
      <label>Relationship to Student<input required type="text" name="emergency_rela" value="{{$registration_data->emergency_rela ?? old("emergency_rela")}}"><span id="emergency_rela_error"></span></label>
      <label>Daytime Phone<input required class="phone_number_format" type="text" name="emergency_phone" maxlength="12" value="{{$registration_data->emergency_phone ?? old("emergency_phone")}}"><span id="emergency_phone_error"></span></label>
      <label>Mobile Phone<input required class="phone_number_format" type="text" name="emergency_mobile" maxlength="12" value="{{$registration_data->emergency_mobile ?? old("emergency_mobile")}}"><span id="emergency_mobile_error"></span></label>
    </div>
    <p>*consent*</p>
    <div class="row">
      <fieldset class="radio">
        <legend>I/We consent with this emergency medical procedure</legend>
        <label><input type="radio" name="medical_consent" value="Yes" {{(isset($registration_data->medical_consent) && $registration_data->medical_consent=='Yes'? 'checked' :(old("medical_consent")=='Yes'? 'checked' : ""))}}>Yes</label>
        <label><input type="radio" name="medical_consent" value="No" {{(isset($registration_data->medical_consent) && $registration_data->medical_consent=='No'? 'checked' :(old("medical_consent")=='No'? 'checked' : ""))}}>No</label>
        <span id="medical_consent_error"></span>
      </fieldset>
    </div>
    <div class="row">
      <label>If answering No, please provide alternate instructions<textarea id="medical_consent_detail" name="medical_consent_detail">{{$registration_data->medical_consent_detail ?? old("medical_consent_detail")}}</textarea><span id="medical_consent_detail_error"></span></label>
    </div>

    <div class="row">
      <p>*consent*</p>
      @if (isset($application_data->father))
      <div class="column">
        <label>Father/Guardian Name: <input required type="text" id="pa_sig_medical" name="pa_sig_medical" value="{{$registration_data->pa_sig_medical ??old("pa_sig_medical")}}"><span id='pa_sig_medical_error'></span></label>
        <label>Date<input required class="jquery_datepicker" type="text" id="pa_sig_medical_date" name="pa_sig_medical_date" value="{{($registration_data->pa_sig_medical_date ?? old("pa_sig_medical_date"))}}"><span id='pa_sig_medical_date_error'></span></label>
      </div>
      @endif

      @if (isset($application_data->mother))
      <div class="column">
        <label>Mother/Guardian Name: <input required type="text" id="mo_sig_medical" name="mo_sig_medical" value="{{$registration_data->mo_sig_medical ?? old("mo_sig_medical")}}"><span id='mo_sig_medical_error'></span></label>
        <label>Date<input required class="jquery_datepicker" type="text" id="mo_sig_medical_date" name="mo_sig_medical_date" value="{{$registration_data->mo_sig_medical_date ?? old("mo_sig_medical_date")}}"><span id='mo_sig_medical_date_error'></span></label>
      </div>
      @endif
    </div>
    
    {{-- <p><strong>Any related ambulance costs will be the responsibility of the parent/guardian of the student.  Although this is an unlikely event, the cost is significant enough that we recommend that students have insurance coverage (such as Student Accident Insurance).</strong></p> --}}
  </section>
  <footer>
    <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
    <a class="button save" id="save_for_later">Save for Later</a>
    <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
  </footer>