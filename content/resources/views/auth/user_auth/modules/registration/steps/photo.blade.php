<header>
  <h1>Photo</h1>
</header>
<section>
    <h2>Photo / Media Consent</h2>
    <p>*consent*</p>
    <div class="row">
      <fieldset class="radio-block">
        <label><input type="radio" name="media_consent" value="Yes"{{(isset($registration_data->media_consent) && $registration_data->media_consent=='Yes'? 'checked' :'')}}>Yes, I consent....</label>
        <label><input type="radio" name="media_consent" value="No" {{(isset($registration_data->media_consent) && $registration_data->media_consent=='No'? 'checked' :'')}}>No, I do not consent...</label>
        <span id="media_consent_error"></span>
      </fieldset>
    </div>
  </section>
  <section>
    <h2>Family Photo</h2>
    <p>Please upload a current family photo including parents/guardians to assist school staff and administration in becoming acquainted with your family.</p>
    <div>
      <label>Uploaded Image:<span id="family_photo-show">
      @foreach ($file_uploads as $item)
          {{$item->file_name ??''}}
      @endforeach  
      </span></label>
      <span id="family_photo_error"></span>
    </div>
    <div class="row">
      <label><input name="family_photo" id="family_photo" type="file"></label>
    </div>
  </section>
  <footer>
    <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
    <a class="button save" id="save_for_later">Save for Later</a>
    <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
  </footer>