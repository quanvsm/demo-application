<header>
    <h1>Registration</h1>
  </header>
  
    <section>
    <div class="row">
      {{-- <p>Application Type:<span>{{$type == 1 ? "Canadian" : "International"}}</span></p> --}}
      @if ($type==1)
      <p>Grade: <span> {{ $registration_data->grade_in ?? ( $registration_data->grade??'')}}</span></p>
      @else
      <p>Grade: <span>{{$registration_data->int_grade ?? ''}}</span></p>
      @endif
      {{-- <p>Academic Year: <span>{{$academic_year ?? ''}}</span></p> --}}
    </div>
    {{-- <h3>Name</h3> --}}
    <div class="row">
      <p>Legal Last:<span>{{$registration_data->stu_lname ?? ''}}</span></p>
      <p>Legal First:<span>{{$registration_data->stu_fname ?? ''}}</span></p>
      <p>Legal Middle:<span>{{$registration_data->stu_mname ?? ''}}</span></p>
      <p>Commonly Goes by:<span>{{$registration_data->stu_cname ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Date of Birth:<span>{{($registration_data->stu_month_of_birth ??'').'/'.($registration_data->stu_date_of_birth??'').'/'.($registration_data->stu_year_of_birth ?? '')}}</span></p>
      <p>Gender:<span>{{$registration_data->stu_gender ?? ''}}</span></p>
    </div>
    {{-- <h3>Address</h3> --}}
    <div class="row">
      <p>Street Address:<span>{{$registration_data->stu_street ?? ''}}</span></p>
      <p>City:<span>{{$registration_data->stu_city ?? ''}}</span></p>
      <p>Province / Territory:<span>{{$registration_data->stu_province ?? ''}}</span></p>
      @if ($type==2)
      <p>Country:<span>{{$registration_data->stu_country ?? ''}}</span></p>
      @endif
      <p>Postal Code:<span>{{$registration_data->stu_postal ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Primary Phone:<span>{{$registration_data->stu_phone ?? ''}}</span></p>
      <p>Primary Family Email:<span>{{$registration_data->stu_email ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Citizenship / Immigration Status:<span>{{$registration_data->stu_citizenship_status ?? ''}}</span></p>
      <p>Language Spoken at Home:<span>{{$registration_data->stu_lang ?? ''}}</span></p>
      <p>Country of Birth:<span>{{$registration_data->stu_country_of_birth ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Current School:<span>{{$registration_data->stu_curr_school ?? ''}}</span></p>
      {{-- <p>Family Church:<span>{{$registration_data->church ?? ''}}</span></p> --}}
    </div>
  </section>

    <section>
    <h2>Medical Information</h2>
    <div class="row">
      <p>Student's Doctor:<span>{{$registration_data->doctor_name??''}}</span></p>
      <p>Doctor's Phone:<span>{{$registration_data->doctor_phone??''}}</span></p>
      <p>Manitoba Health Personal 9 Digit ID:<span>{{$registration_data->health_card??''}}</span></p>
    </div>
    <div class="row">
      <p>Are student's immunizations up to date?: <span>{{$registration_data->immunization??''}}</span></p>
    </div>
    <div class="row">
      <p>Medical Conditions:
      @if(isset($registration_data->stu_medical))
      @foreach ($registration_data->stu_medical as $item)
      <span>{{$item}}</span>
      @endforeach
      @endif
    </p>
      <p>Details: <span>{{$registration_data->medical_detail??''}}</span></p>
    </div>
    <h2>Emergency Medical Permission & Consent for Medical Treatment</h2>
    <p>In case of an emergency, when a parent/guardian cannot be reached, please indicate who should be contacted.</p>
    <div class="row">
      <p>Name:<span>{{$registration_data->emergency_name??''}}</span></p>
      <p>Relationship to Student:<span>{{$registration_data->emergency_rela??''}}</span></p>
      <p>Daytime Phone:<span>{{$registration_data->emergency_phone??''}}</span></p>
      <p>Mobile Phone:<span>{{$registration_data->emergency_mobile??''}}</span></p>
    </div>
    <p>*consent*</p>
    <div class="row">
      <p>I/We consent with this emergency medical procedure: <span>{{$registration_data->medical_consent??''}}</span></p>
    </div>
    <div class="row">
      @if (isset($application_data->father))
      <div class="column">
        <p>Father/Guardian Name: <span>{{$registration_data->pa_sig_medical ?? old("pa_sig_medical")}}</span></p>
        <p>Date: <span>{{($registration_data->pa_sig_medical_date ?? old("pa_sig_medical_date"))}}</span></p>
      </div>
      @endif

      @if (isset($application_data->mother))
      <div class="column">
        <p>Mother/Guardian Name: <span>{{$registration_data->mo_sig_medical ?? old("mo_sig_medical")}}</span></p>
        <p>Date: <span>{{$registration_data->mo_sig_medical_date ?? old("mo_sig_medical_date")}}</span></p>
      </div>
      @endif
    </div>
  </section>

    <section>
      <h2>Family Information</h2>
      {{-- <h3>Name</h3> --}}
      <div class="row">
        <p>Relationship to Student:<span>{{$registration_data->father ?? ''}}</span></p>
        <p>Last Name:<span>{{$registration_data->pa_lname ?? ''}}</span></p>
        <p>First Name:<span>{{$registration_data->pa_fname ?? ''}}</span></p>
      </div>
      {{-- <h3>Address</h3> --}}
      @if ($registration_data->pa_addr_check_box ??'' == 'on')
      <div class="row">
        <p>Street Address:<span>{{$registration_data->stu_street ?? ""}}</span></p>
        <p>City:<span>{{$registration_data->stu_city ??""}}</span></p>
        <p>Province / State:<span>{{$registration_data->stu_province ??""}}</span></p>
        <p>Postal Code / Zip:<span>{{$registration_data->stu_postal ??""}}</span></p>
      </div>
      @else
      <div class="row">
        <p>Street Address:<span>{{$registration_data->pa_street ?? ''}}</span></p>
        <p>City:<span>{{$registration_data->pa_city ??""}}</span></p>
        <p>Province / State:<span>{{$registration_data->pa_province ??""}}</span></p>
        <p>Postal Code / Zip:<span>{{$registration_data->pa_postal ??""}}</span></p>
      </div>
      @endif
   
  
      {{-- <h3>Contact</h3> --}}
      <div class="row">
        <p>Primary Phone:<span>{{$registration_data->pa_hphone ?? ''}}</span></p>
        <p>Mobile Phone:<span>{{$registration_data->pa_mphone ?? ''}}</span></p>
        <p>Email Address:<span>{{$registration_data->pa_email ?? ''}}</span></p>
      </div>
      {{-- <h3>Employment</h3> --}}
      <div class="row">
        <p>Occupation:<span>{{$registration_data->pa_occupation ?? ''}}</span></p>
        <p>Employer:<span>{{$registration_data->pa_employer ?? ''}}</span></p>
      </div>
      <div class="row">
        <p>Alumnus:<span>{{$registration_data->alumnus ?? ''}} - {{$registration_data->alumnus_input ?? ''}}</span></p>
      </div>
      <hr>
      {{-- <h3>Name</h3> --}}
      <div class="row">
        <p>Relationship to Student:<span>{{$registration_data->mother ?? ''}}</span></p>
        <p>Last Name:<span>{{$registration_data->mo_lname ?? ''}}</span></p>
        <p>First Name:<span>{{$registration_data->mo_fname ?? ''}}</span></p>
      </div>
      {{-- <h3>Address</h3> --}}
      @if ($registration_data->mo_addr_check_box ??'' == 'on')
      <div class="row">
        <p>Street Address:<span>{{$registration_data->stu_street ?? ""}}</span></p>
        <p>City:<span>{{$registration_data->stu_city ??""}}</span></p>
        <p>Province / State:<span>{{$registration_data->stu_province ??""}}</span></p>
        <p>Postal Code / Zip:<span>{{$registration_data->stu_postal ??""}}</span></p>
      </div>
      @else
      <div class="row">
        <p>Street Address:<span>{{$registration_data->mo_street ?? ''}}</span></p>
        <p>City:<span>{{$registration_data->mo_city ??""}}</span></p>
        <p>Province / State:<span>{{$registration_data->mo_province ??""}}</span></p>
        <p>Postal Code / Zip:<span>{{$registration_data->mo_postal ??""}}</span></p>
      </div>
      @endif
      {{-- <h3>Contact</h3> --}}
      <div class="row">
        <p>Primary Phone:<span>{{$registration_data->mo_hphone ?? ''}}</span></p>
        <p>Mobile Phone:<span>{{$registration_data->mo_mphone ?? ''}}</span></p>
        <p>Email Address:<span>{{$registration_data->mo_email ?? ''}}</span></p>
      </div>
      {{-- <h3>Employment</h3> --}}
      <div class="row">
        <p>Occupation:<span>{{$registration_data->mo_occupation ?? ''}}</span></p>
        <p>Employer:<span>{{$registration_data->mo_employer ?? ''}}</span></p>
      </div>
      <div class="row">
        <p>Alumna:<span>{{$registration_data->alumna ?? ''}} - {{$registration_data->alumna_input ?? ''}}</span></p>
      </div>
      <div class="row">
        <p>Student's Parents are:<span>{{$registration_data->marriage_status ?? ''}}</span></p>
        <p>Student Lives with:<span>{{$registration_data->custody_status ?? ''}}</span></p>
      </div>
      <hr>
      @if(isset($registration_data->sibling_fname) && isset($registration_data->sibling_lname) &&  isset($registration_data->sibling_birthday) && isset($registration_data->sibling_school) )
      {{-- <h3>Sibling Information</h3> --}}
      @for ($i = 0; $i < count($registration_data->sibling_fname); $i++)
        <div class="row">
          <p>First Name:<span>{{$registration_data->sibling_fname[$i]}}</span></p>
          <p>Last Name:<span>{{$registration_data->sibling_lname[$i]}}</span></p>
          <p>Date of Birth:<span>{{$registration_data->sibling_birthday[$i]}}</span></p>
          <p>School:<span>{{$registration_data->sibling_school[$i]}}</span></p>
        </div>
        @endfor
        @endif
    </section>

    @if (isset($registration->type))
    @if ($registration->type == 1)
    <section>
    <h2>Indigenous Identity Declaration</h2>
    <p><strong>Indigenous Identity Declaration helps to support the efforts of Manitoba Education and Advanced Learning to plan and improve programs in a way that is responsive to Indigenous learners.</strong></p>
    {{-- <p>Providing this personal information is voluntary and optional. It is being collected in compliance with section 36(1)(b) of The Freedom of Information and Protection of Privacy Act. If you have any questions regarding the collection of this personal information, please contact the school principal.</p> --}}
    <div class="row">
      <p><span>{{$registration_data->indigenous_declaration??''}}</span></p>
    </div>
    <div class="row">
      <p>If your child is an Indigenous person, please select the option(s) that best describe your child now:
        @if(isset($registration_data->indigenous_child))
        @foreach ($registration_data->indigenous_child as $item)
        <span>{{$item}}</span>
        @endforeach
        @endif
      </p>
    </div>
    <div class="row">
        <p>Which best describes your child’s Indigenous cultural-linguistic identity? Please select up to two choices:
          @if(isset($registration_data->cultural_linguistic))
        @foreach ($registration_data->cultural_linguistic as $item)
        <span>{{$item}}</span>
        @endforeach
        @endif
      </p>
    </div>
  </section>
  @endif
    @endif

  <section>
    <h2>Photo / Media Consent</h2>
    <p>*consent*</p>
    <div class="row">
      @if (isset($registration_data->media_consent))
        @if ($registration_data->media_consent =='Yes')
        <p><span>✓ Yes, I consent.</span></p>
       @else
          <p><span>✓ No, I do not consent.</span></p>
        @endif   
      @endif
    </div>
    <h3>Family Photo</h3>
    <div class="row">
      <p>File: <span>    
        @foreach ($file_uploads as $item)
         <a href='{{route('user.registration.download',['id'=>$registration->id])}}'>{{$item->file_name ??''}}</a> 
        @endforeach  
    </span></p>
    </div>
  </section>

  <section>
    <h2>Email Notification Authorization</h2>
    <div class="row">
      <p>*email consent*</p>
    </div>
    <div class="row">
      @if (isset($application_data->father))
      <div class="column">
        <p>Father/Guardian Name: <span>{{$registration_data->pa_sig_email ?? old("pa_sig_email")}}</span></p>
        <p>Date: <span>{{($registration_data->pa_sig_email_date ?? old("pa_sig_email_date"))}}</span></p>
      </div>
      @endif

      @if (isset($application_data->mother))
      <div class="column">
        <p>Mother/Guardian Name: <span>{{$registration_data->mo_sig_email ?? old("mo_sig_email")}}</span></p>
        <p>Date: <span>{{$registration_data->mo_sig_email_date ?? old("mo_sig_email_date")}}</span></p>
      </div>
      @endif
    </div>
  </section>

    @if (isset($registration->type))
        @if ($registration->type == 1)
        <section>
            <h2>Acknowledgement of Transfer of Cumulative File, Pupil Support & Personal Health Information</h2>
            <p>*consent*</p>
            <div class="row">
              <p><span>✓ I hereby acknowledge that my child’s/children’s cumulative file(s), Pupil Support file(s) and any personal health information will be requested and received from the previous school:</span></p>
            </div>
            <div class="row">
              <p>Name of Previous School:<span>{{$registration_data->prev_school??''}}</span></p>
            </div>
            <div class="row">
              @if (isset($application_data->father))
              <div class="column">
                <p>Father/Guardian Name: <span>{{$registration_data->pa_sig_transfer ?? old("pa_sig_transfer")}}</span></p>
                <p>Date: <span>{{($registration_data->pa_sig_transfer_date ?? old("pa_sig_transfer_date"))}}</span></p>
              </div>
              @endif
        
              @if (isset($application_data->mother))
              <div class="column">
                <p>Mother/Guardian Name: <span>{{$registration_data->mo_sig_transfer ?? old("mo_sig_transfer")}}</span></p>
                <p>Date: <span>{{$registration_data->mo_sig_transfer_date ?? old("mo_sig_transfer_date")}}</span></p>
              </div>
              @endif
            </div>
          </section>
      @endif
    @endif
    
<section>
    <h2>Responsibility for Payment of Registration, Capital and Tuition Fees</h2>
    <div class="row">
      <p><span>✓ consent*.</span></p>
    </div>
    <h3>Withdrawal & Refund Policy</h3>
    <div class="row">
        <p><span>✓ consent*.</span></p>
    </div>
    <div class="row">
      @if (isset($application_data->father))
      <div class="column">
        <p>Father/Guardian Initial: <span>{{$registration_data->pa_ini_tuition ?? old("pa_ini_tuition")}}</span> </p>
      </div>
      @endif

      @if (isset($application_data->mother))
      <div class="column">
        <p>Mother/Guardian Initial: <span>{{$registration_data->mo_ini_tuition ?? old("mo_ini_tuition")}}</span></p>
      </div>
      @endif
    </div>
  </section>

  <section>
    <h2>Accuracy Of Information</h2>
    <div class="row">
      <p><span>✓ I/We affirm by initialling, signing and submitting this completed Form, that I/we have thoroughly read and understand the information contained and confirm the accuracy of all responses.</span></p>
    </div>

    <div class="row">
      @if (isset($application_data->father))
      <div class="column">
        <p>Father/Guardian Name: <span>{{$registration_data->pa_sig_accuracy ?? old("pa_sig_accuracy")}}</span></p>
        <p>Date: <span>{{($registration_data->pa_sig_accuracy_date ?? old("pa_sig_accuracy_date"))}}</span></p>
      </div>
      @endif

      @if (isset($application_data->mother))
      <div class="column">
        <p>Mother/Guardian Name: <span>{{$registration_data->mo_sig_accuracy ?? old("mo_sig_accuracy")}}</span></p>
        <p>Date: <span>{{$registration_data->mo_sig_accuracy_date ?? old("mo_sig_accuracy_date")}}</span></p>
      </div>
      @endif
    </div>
  </section>
@if (isset($registration->status))
@if ($registration->status == "Pending")
<footer>
  <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
  <a class="button save" id="save_for_later">Save for Later</a>
  <a class="button green continue" onclick="nextPrev(1,null)">Continue to Payment</a>
</footer>
@else
<footer>
  <a class="button red exit back" href="{{route("user.dashboard")}}">Exit</a>
  @if (isset($registration->id))
  <a class="button green print" href="{{route("user.registration.print",$registration->id)}}" target="_blank">Print</a>
  @endif
</footer>
@endif 
@endif
</div>