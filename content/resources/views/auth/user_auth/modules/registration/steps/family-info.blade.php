<header>
    <h1>Family Information</h1>
</header>
<section>
    <div class="row">
      <div class="column">
        <h3>Name</h3>
        <fieldset class="radio">
          <label><input type="radio" name="father" value="Father"
              {{(isset($registration_data->father) ? ($registration_data->father=='Father'? 'checked' :'') : (isset($application_data->father) ? ($application_data->father=='Father'? 'checked' :'') :''))}}>Father</label>
          <label><input type="radio" name="father" value="Guardian"
              {{(isset($registration_data->father) ? ($registration_data->father=='Guardian'? 'checked' :'') : (isset($application_data->father)  ? ($application_data->father=='Guardian'? 'checked' :''): ''))}}>Guardian</label>
        </fieldset>
        <span id='father_error'></span>
        <label>Last Name<input type="text" id="pa_lname" name="pa_lname" value="{{$registration_data->pa_lname ??$application_data->pa_lname ?? ''}}"><span
            id="pa_lname_error"></span></label>
        <label>First Name<input type="text" id="pa_fname" name="pa_fname" value="{{$registration_data->pa_fname ??$application_data->pa_fname ?? ''}}"><span id="pa_fname_error"></span></label>
        <h3>Address</h3>
        <fieldset class="checkbox" id="father-addr-checkbox">
          @if ($registration_data)
            <label><input type="checkbox" name="pa_addr_check_box"
              {{(isset($registration_data->pa_addr_check_box) && $registration_data->pa_addr_check_box=='on'? 'checked' :'')}}>Same as
            Student</label>
          @else
            <label><input type="checkbox" name="pa_addr_check_box"
              {{(isset($application_data->pa_addr_check_box) && $application_data->pa_addr_check_box=='on'? 'checked' :'')}}>Same as
            Student</label>
          @endif
          
        </fieldset>
        <fieldset disabled id="father-addr">
          <!-- this fieldset is toggled by above checkbox-->
          <label>Street Address<input type="text" id="pa_street" name="pa_street" value="{{$registration_data->pa_street ?? $application_data->pa_street ?? ''}}"><span id="pa_street_error"></span></label>
          <label>City<input type="text" name="pa_city" id="pa_city" value="{{$registration_data->pa_city ?? $application_data->pa_city ?? ''}}"><span id="pa_city_error"></span></label>
          <label>Province / State<input type="text" id="pa_province" name="pa_province" value="{{$registration_data->pa_province ?? $application_data->pa_province ?? ''}}"><span id="pa_province_error"></span></label>
          <label>Country<input type="text" id="pa_country" name="pa_country" value="{{$registration_data->pa_country ?? $application_data->pa_country ?? ''}}"><span id="pa_country_error"></span></label>
          <label>Postal Code / Zip<input type="text" id="pa_postal" name="pa_postal" value="{{$registration_data->pa_postal ?? $application_data->pa_postal ?? ''}}"><span id="pa_postal_error"></span></label>
        </fieldset>
        <h3>Contact</h3>
        <label>Primary Phone<input type="text" class="phone_number_format" maxlength="12" name="pa_hphone" id="pa_hphone" value="{{$registration_data->pa_hphone ?? $application_data->pa_hphone ?? ''}}"><span id="pa_hphone_error"></span></label>
        <label>Mobile Phone<input type="text" class="phone_number_format" maxlength="12" name="pa_mphone" value="{{$registration_data->pa_mphone ?? $application_data->pa_mphone ?? ''}}"><span id="pa_mphone_error"></span></label>
        <label>Email Address<input type="email" id='pa_email' name="pa_email" value="{{$registration_data->pa_email ?? $application_data->pa_email ?? ''}}"><span id='pa_email_error'></span></label>
        <h3>Employment</h3>
        <label>Occupation<input type="text" id="pa_occupation" name="pa_occupation" value="{{$registration_data->pa_occupation ?? $application_data->pa_occupation ?? ''}}"><span id='pa_occupation_error'></span></label>
        <label>Employer<input type="text" id="pa_employer" name="pa_employer" value="{{$registration_data->pa_employer ?? $application_data->pa_employer ?? ''}}"><span id='pa_employer_error'></span></label>
        @if ($type ==1)
        <fieldset class="radio">
          <legend>Alumnus?</legend>
          <label><input type="radio" name="alumnus" value="yes"
              {{(isset($registration_data->alumnus) ? ($registration_data->alumnus=='yes'? 'checked' :'') : (isset($application_data->alumnus) ? ($application_data->alumnus=='yes'? 'checked' :''):''))}}>Yes</label>
          <label><input type="radio" name="alumnus" value="no"
              {{(isset($registration_data->alumnus) ? ($registration_data->alumnus=='no'? 'checked' :'') : (isset($application_data->alumnus)  ? ($application_data->alumnus=='no'? 'checked' :''):''))}}>No</label>
              <span id='alumnus_error'></span>
        </fieldset>
        <label><input type="text" id="alumnus_input" disabled placeholder="If yes, graduating year and last name, if different" name="alumnus_input" value="{{$registration_data->alumnus_input ??$application_data->alumnus_input ?? ''}}"><span id='alumnus_input_error'></span></label>
        @endif
      </div>

      <div class="column">
        <h3>Name</h3>
        <fieldset class="radio">
          <label><input type="radio" name="mother" value="Mother"
              {{(isset($registration_data->mother) ? ($registration_data->mother=='Mother'? 'checked' :'') : (isset($application_data->mother) ? ( $application_data->mother=='Mother'? 'checked' : '') :''))}}>Mother</label>
          <label><input type="radio" name="mother" value="Guardian"
              {{(isset($registration_data->mother) ? ($registration_data->mother=='Guardian'? 'checked' :'') : (isset($application_data->mother) ? ($application_data->mother=='Guardian'? 'checked' :'') : ''))}}>Guardian</label>
        </fieldset>
        <label>Last Name<input type="text" id="mo_lname" name="mo_lname" value="{{$registration_data->mo_lname ?? $application_data->mo_lname ?? ''}}"><span id='mo_lname_error'></span></label>
        <label>First Name<input type="text" id="mo_fname" name="mo_fname" value="{{$registration_data->mo_fname ?? $application_data->mo_fname ?? ''}}"><span id='mo_fname_error'></span></label>
        <h3>Address</h3>
        <fieldset class="checkbox" id="mother-addr-checkbox">
          @if ($registration_data)
          <label><input type="checkbox" name="mo_addr_check_box"
            {{(isset($registration_data->mo_addr_check_box) && $registration_data->mo_addr_check_box=='on'? 'checked' :'')}}>Same as
          Student</label>
        @else
          <label><input type="checkbox" name="mo_addr_check_box"
            {{(isset($application_data->mo_addr_check_box) && $application_data->mo_addr_check_box=='on'? 'checked' :'')}}>Same as
          Student</label>
        @endif
        </fieldset>
        <fieldset disabled id="mother-addr">
          <!-- this fieldset is toggled by above checkbox-->
          <label>Street Address<input id="mo_street" type="text" name="mo_street" value="{{$registration_data->mo_street ??$application_data->mo_street ?? ''}}"><span id='mo_street_error'></span></label>
          <label>City<input type="text" id="mo_city" name="mo_city" value="{{$registration_data->mo_city ??$application_data->mo_city ?? ''}}"><span id='mo_city_error'></span></label>
          <label>Province / State<input id="mo_province" type="text" name="mo_province" value="{{$registration_data->mo_province ??$application_data->mo_province ?? ''}}"><span id='mo_province_error'></span></label>
          <label>Country<input type="text" id="mo_country" name="mo_country" value="{{$registration_data->mo_country ??$application_data->mo_country ?? ''}}"><span id='mo_country_error'></span></label>
          <label>Postal Code / Zip<input id="mo_postal" type="text" name="mo_postal" value="{{$registration_data->mo_postal ??$application_data->mo_postal ?? ''}}"><span id='mo_postal_error'></span></label>
        </fieldset>
        <h3>Contact</h3>
        <label>Primary Phone<input type="text" class="phone_number_format" id="mo_hphone" maxlength="12" name="mo_hphone" value="{{$registration_data->mo_hphone ??$application_data->mo_hphone ?? ''}}"><span id='mo_hphone_error'></span></label>
        <label>Mobile Phone<input type="text" class="phone_number_format" name="mo_mphone" maxlength="12" value="{{$registration_data->mo_mphone ??$application_data->mo_mphone ?? ''}}"><span id='mo_mphone_error'></span></label>
        <label>Email Address<input type="email" id='mo_email' name="mo_email" value="{{$registration_data->mo_email ??$application_data->mo_email ?? ''}}"><span id='mo_email_error'></span></label>
        <h3>Employment</h3>
        <label>Occupation<input type="text" id="mo_occupation" name="mo_occupation" value="{{$registration_data->mo_occupation ??$application_data->mo_occupation ?? ''}}"><span id='mo_occupation_error'></span></label>
        <label>Employer<input type="text" id="mo_employer" name="mo_employer" value="{{$registration_data->mo_employer ??$application_data->mo_employer ?? ''}}"><span id='mo_employer_error'></span></label>
        @if ($type==1)
        <fieldset class="radio">
          <legend>Alumna?</legend>
          <label><input type="radio" name="alumna" value="yes"
              {{isset($registration_data->alumna) ? ($registration_data->alumna=='yes'? 'checked' :'') : (isset($application_data->alumna) && $application_data->alumna=='yes'? 'checked' :'')}}>Yes</label>
          <label><input type="radio" name="alumna" value="no"
              {{isset($registration_data->alumna) ? ($registration_data->alumna=='no'? 'checked' :'') : (isset($application_data->alumna) && $application_data->alumna=='no'? 'checked' :'')}}>No</label>
              <span id='alumna_error'></span>
        </fieldset>
        <label><input type="text" id="alumna_input" disabled placeholder="If yes, graduating year and last name, if different" name="alumna_input" value="{{$application_data->alumna_input ?? ''}}"><span id='alumna_input_error'></span></label>
        @endif
      </div>
    </div>

    <div class="row">
      <fieldset class="radio stretch" name="marriage_status">
        <legend>Student's Parents are:</legend>
        <label><input type="radio" name="marriage_status" value="Married"
            {{isset($registration_data->marriage_status) ? ($registration_data->marriage_status=='Married'? 'checked' :''):(isset($application_data->marriage_status) && $application_data->marriage_status=='Married'? 'checked' :'')}}>Married</label>
        <label><input type="radio" name="marriage_status" value="Separated"
            {{isset($registration_data->marriage_status) ? ($registration_data->marriage_status=='Separated'? 'checked' :''):(isset($application_data->marriage_status) && $application_data->marriage_status=='Separated'? 'checked' :'')}}>Separated</label>
        <label><input type="radio" name="marriage_status" value="Divorced"
            {{isset($registration_data->marriage_status) ? ($registration_data->marriage_status=='Divorced'? 'checked' :''):(isset($application_data->marriage_status) && $application_data->marriage_status=='Divorced'? 'checked' :'')}}>Divorced</label>
        <label><input type="radio" name="marriage_status" value="Single"
            {{isset($registration_data->marriage_status) ? ($registration_data->marriage_status=='Single'? 'checked' :'') : (isset($application_data->marriage_status) && $application_data->marriage_status=='Single'? 'checked' :'')}}>Single</label>
        <label><input type="radio" name="marriage_status" value="Widowed"
            {{isset($registration_data->marriage_status) ? ($registration_data->marriage_status=='Widowed'? 'checked' :''):(isset($application_data->marriage_status) && $application_data->marriage_status=='Widowed'? 'checked' :'')}}>Widowed</label>
        <label><input type="radio" name="marriage_status" value="Common-Law"
            {{isset($registration_data->marriage_status) ? ($registration_data->marriage_status=='Common-Law'? 'checked' :''):(isset($application_data->marriage_status) && $application_data->marriage_status=='Common-Law'? 'checked' :'')}}>Common-Law</label>
      </fieldset>
      <span id="marriage_status_error"></span>
    </div>

    <div class="row">
      <fieldset class="radio stretch" name="custody-status">
        <legend>Student Lives with:</legend>
        <label><input type="radio" name="custody_status" value="Both Parents"
            {{isset($registration_data->custody_status) ? ($registration_data->custody_status=='Both Parents'? 'checked' :''):(isset($application_data->custody_status) && $application_data->custody_status=='Both Parents'? 'checked' :'')}}>Both
          Parents</label>
        <label><input type="radio" name="custody_status" value="Father"
            {{isset($registration_data->custody_status) ? ($registration_data->custody_status=='Father'? 'checked' :''):(isset($application_data->custody_status) && $application_data->custody_status=='Father'? 'checked' :'')}}>Father</label>
        <label><input type="radio" name="custody_status" value="Mother"
            {{isset($registration_data->custody_status) ? ($registration_data->custody_status=='Mother'? 'checked' :''):(isset($application_data->custody_status) && $application_data->custody_status=='Mother'? 'checked' :'')}}>Mother</label>
        <label><input type="radio" name="custody_status" value="Custodian"
            {{isset($registration_data->custody_status) ? ($registration_data->custody_status=='Custodian'? 'checked' :''):(isset($application_data->custody_status) && $application_data->custody_status=='Custodian'? 'checked' :'')}}>Custodian</label>
        <label><input type="radio" name="custody_status" value="Guardian"
            {{isset($registration_data->custody_status) ? ($registration_data->custody_status=='Guardian'? 'checked' :''):(isset($application_data->custody_status) && $application_data->custody_status=='Guardian'? 'checked' :'')}}>Guardian</label>
        <label><input type="radio" name="custody_status" value="Other"
            {{isset($registration_data->custody_status) ? ($registration_data->custody_status=='Other'? 'checked' :'') : (isset($application_data->custody_status) && $application_data->custody_status=='Other'? 'checked' :'')}}>Other</label>
        <label><input type="text" disabled name="student_lives_with" id="student_lives_with"
            value="{{$registration_data->student_lives_with ??$application_data->student_lives_with ?? ''}}"></label>
            <span id='student_lives_with_error'></span>
        <!-- selecting 'other' option enables this field -->
      </fieldset>
    </div>
    <div>
      <p>In case of an emergency, which parent/guardian should be contacted first?</p>
      <label>Contact #1<input type="text" id="emergency_contact_1" name="emergency_contact_1" value="{{$registration_data->emergency_contact_1 ??$application_data->emergency_contact_1 ?? ''}}"><span id='emergency_contact_1_error'></span></label>
      <label>Contact #2<input type="text" id="emergency_contact_2" name="emergency_contact_2" value="{{$registration_data->emergency_contact_2 ??$application_data->emergency_contact_2 ?? ''}}"><span id='emergency_contact_2_error'></span></label>
    </div>
  </section>
  <section>
    <h3 id="after-sibling">Sibling Information</h3>
    @if(isset($registration_data->sibling_fname) && isset($registration_data->sibling_lname) &&  isset($registration_data->sibling_birthday) && isset($registration_data->sibling_school) )
    @for ($i = 0; $i < count($registration_data->sibling_fname); $i++)
    <div class="row sibling">
      <fieldset class="sibling_info">
        <label>Last Name<input type="text" name="sibling_lname[]" value="{{$registration_data->sibling_lname[$i] ?? ''}}"></label>
        <label>First Name<input type="text" name="sibling_fname[]" value="{{$registration_data->sibling_fname[$i]??''}}"></label>
        <label>Date of Birth<input type="text" maxlength="10" class="birthday_mask" placeholder="MM/DD/YYYY" name="sibling_birthday[]"
            value="{{$registration_data->sibling_birthday[$i]}}"></label>
        <label>School<input type="text" name="sibling_school[]" value="{{$registration_data->sibling_school[$i]}}"></label>
        <span class="remove remove_sibling"></span>
      </fieldset>
    </div>
    @endfor
    @else
    @if(isset($application_data->sibling_fname) && isset($application_data->sibling_lname) &&  isset($application_data->sibling_birthday) && isset($application_data->sibling_school) )
    @for ($i = 0; $i < count($application_data->sibling_fname); $i++)
      <div class="row sibling">
        <fieldset class="sibling_info">
          <label>Last Name<input type="text" name="sibling_lname[]" value="{{$application_data->sibling_lname[$i] ?? ''}}"></label>
          <label>First Name<input type="text" name="sibling_fname[]" value="{{$application_data->sibling_fname[$i]??''}}"></label>
          <label>Date of Birth<input type="text" maxlength="10" class="birthday_mask" placeholder="MM/DD/YYYY" name="sibling_birthday[]"
              value="{{$application_data->sibling_birthday[$i]}}"></label>
          <label>School<input type="text" name="sibling_school[]" value="{{$application_data->sibling_school[$i]}}"></label>
          <span class="remove remove_sibling"></span>
        </fieldset>
      </div>
      @endfor
      @endif
      @endif
      <div>
        <span class="button add" id="add_sibling">Add</span>
      </div>
  </section>
<footer>
    <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
    <a class="button save" id="save_for_later">Save for Later</a>
    <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
</footer>