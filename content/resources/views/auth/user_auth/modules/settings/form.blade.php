@extends('admin.layouts.admin_form')
<?
  $class = 'user';
  $active = 'settings';

  $form_action = route('user.settings.save');
?>

@section('content')
    <h1>Edit: {{$user->first_name ?? '' }}</h1>

    <div class="section">
        <label>First Name<input type="text" id="first_name" name="first_name" value="{{$user->first_name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Last Name<input type="text" id="last_name" name="last_name" value="{{$user->last_name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Email<input type="text" name="email" value="{{$user->email ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Password<input type="password" name="password"></label>
    </div>
@endsection

@section('inspector')
  <div id="inspector">
      <button type="submit" class="button accept" >save</button>
      <hr>
  </div>
@endsection
