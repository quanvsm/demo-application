<script>
    var x = document.getElementsByClassName("tab");
    x[0].style.display = "none";

    var currentStep = <?=$step?>; 
    showTab(currentStep);

    function showTab(n) {
    window.scrollTo(0,0);
    var x = document.getElementsByClassName("tab");
    x[currentStep].style.display = "none";
    currentStep=n;
    if(currentStep != 0){
      document.getElementById('step_id').value = currentStep;
    }
    var x = document.getElementsByClassName("tab");
    if(currentStep == 0 ){
      x[n].style.display = "block";
    }
    else{
      x[n].style.display = "flex";
    }
   
    fixStepIndicator(n)
    }

    function nextPrev(n,y) {              
    var x = document.getElementsByClassName("tab");
    if (n == 1 && !validateForm()) return false;
    x[currentStep].style.display = "none";
    currentStep = currentStep + n;
    document.getElementById('step_id').value = currentStep;
    if (currentStep >= x.length) {
        document.getElementsByClassName("continue").className += "disabled";
        return false;
    }
    showTab(currentStep);
    }

    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
      }

    function validateForm() {
     
      var x, y, i, valid = true;
      x = document.getElementsByClassName("tab");
      y = x[currentStep].querySelectorAll("[required]")
      //get grade radio buttons
      var btns = document.querySelectorAll('input[type="radio"][name="grade"]')
        //Grade validation
        if(btns.length >0){
        if(!btns[0].checked && !btns[1].checked  && !btns[2].checked){
        valid = false;
        document.getElementById('grade_error').innerHTML ="* Required Field";
        document.getElementById('grade_error').style.color = "#FF0000";
        btns[0].focus();
        }
       else{
         document.getElementById('grade_error').innerHTML ="";
       }
      }
   

    //Student information validation
    if(currentStep ==1){
      var has_recieved = x[currentStep].querySelectorAll('input[name="stu_support[]"]:checked')
      var check =  x[currentStep].querySelectorAll('input[name="stu_support[]"')

     if(check.length > 0){
          if(has_recieved.length >0){
            document.getElementById('stu_support_doc_error').innerHTML ="";
          }
          else{
            valid = false;
              document.getElementById('stu_support_doc_error').innerHTML ="*Required Field";
              document.getElementById('stu_support_doc_error').style.color = "#FF0000";
          }
        }

        var medical = document.querySelectorAll('input[type="radio"][name="allegies"]:checked')
        if(medical.length > 0){
          document.getElementById('medical_condition_error').innerHTML ="";
        }
        else{
          valid = false;
              document.getElementById('medical_condition_error').innerHTML ="*Required Field";
              document.getElementById('medical_condition_error').style.color = "#FF0000";
        }
      }

    //parent information validation
    if(currentStep ==2){
    var father_radio = document.querySelectorAll('input[type="radio"][name="father"]:checked')
    var mother_radio = document.querySelectorAll('input[type="radio"][name="mother"]:checked')
     if(father_radio.length > 0 || mother_radio.length > 0){
      document.getElementById('father_error').innerHTML ="";
      var alumna_check = $("input[name='alumna']");
      if(alumna_check.length >0){
        if(father_radio.length > 0){
          var father_alumnus = document.querySelectorAll('input[type="radio"][name="alumnus"]:checked')
            if(father_alumnus.length > 0){
              document.getElementById('alumnus_error').innerHTML ="";
            }
            else{
              valid = false;
                  document.getElementById('alumnus_error').innerHTML ="*Required Field";
                  document.getElementById('alumnus_error').style.color = "#FF0000";
            }
      } 
      if(mother_radio.length > 0){
          var mother_alumna = document.querySelectorAll('input[type="radio"][name="alumna"]:checked')
          if(mother_alumna.length > 0){
               document.getElementById('alumna_error').innerHTML ="";
          }
          else{
              valid = false;
              document.getElementById('alumna_error').innerHTML ="*Required Field";
              document.getElementById('alumna_error').style.color = "#FF0000";
          }
      }
      }
    }
   else if(father_radio.length > 0 && mother_radio.length > 0){
      document.getElementById('father_error').innerHTML ="";
      var mother_alumna = document.querySelectorAll('input[type="radio"][name="alumna"]:checked')
      var alumna_check = $("input[name='alumna']");
      if(alumna_check.length >0){
        if(mother_alumna.length > 0){
        document.getElementById('alumna_error').innerHTML ="";
      }
      else{
        valid = false;
        document.getElementById('alumna_error').innerHTML ="*Required Field";
        document.getElementById('alumna_error').style.color = "#FF0000";
      }

        var father_alumnus = document.querySelectorAll('input[type="radio"][name="alumnus"]:checked')
        if(father_alumnus.length > 0){
          document.getElementById('alumnus_error').innerHTML ="";
        }
        else{
          valid = false;

              document.getElementById('alumnus_error').innerHTML ="*Required Field";
              document.getElementById('alumnus_error').style.color = "#FF0000";
          }
      }
    }
    else{
      valid = false;
          document.getElementById('father_error').innerHTML ="*Please select Parent/Guardian";
          document.getElementById('father_error').style.color = "#FF0000";
    }

    var marriage_status = document.querySelectorAll('input[type="radio"][name="marriage_status"]:checked')
    if(marriage_status.length > 0){
      document.getElementById('marriage_status_error').innerHTML ="";
    }
    else{
      valid = false;
          document.getElementById('marriage_status_error').innerHTML ="*Required Field";
          document.getElementById('marriage_status_error').style.color = "#FF0000";
    }

    var custody_status = document.querySelectorAll('input[type="radio"][name="custody_status"]:checked')
    if(marriage_status.length > 0){
      document.getElementById('student_lives_with_error').innerHTML ="";
    }
    else{
      valid = false;
          document.getElementById('student_lives_with_error').innerHTML ="*Required Field";
          document.getElementById('student_lives_with_error').style.color = "#FF0000";
    }
    }

      //additional information validation
      if(currentStep ==3){
      var heard_from = x[currentStep].querySelectorAll('input[name="heard_from[]"]:checked')
      if(heard_from.length ==0){
          valid = false;
          document.getElementById('heard_from_error').innerHTML ="*Required Field";
          document.getElementById('heard_from_error').style.color = "#FF0000";
      }
      else{
        document.getElementById('heard_from_error').innerHTML ="";
      }
      }

       //file upload validation
       if(currentStep == 4){
        var canada_photo = document.getElementById('photo-show');
        var canada_passport = document.getElementById('pass-show')
        if(canada_photo!= null && canada_photo.innerText == '' ){
          valid = false;
            document.getElementById('photo_error').innerHTML ="*Required Field";
            document.getElementById('photo_error').style.color = "#FF0000";
        }

        if(canada_passport!=null && canada_passport.innerText == '' ){
          valid = false;
            document.getElementById('passport_error').innerHTML ="*Required Field";
            document.getElementById('passport_error').style.color = "#FF0000";
        }

        var international_photo = document.getElementById('in-photo-show');
          var international_passport = document.getElementById('in-pass-show')
          if(international_photo!=null && international_photo.innerText == '' ){
          valid = false;
            document.getElementById('int_photo_error').innerHTML ="*Required Field";
            document.getElementById('int_photo_error').style.color = "#FF0000";
        }

        if(international_passport!=null && international_passport.innerText == '' ){
          valid = false;
            document.getElementById('int_passport_error').innerHTML ="*Required Field";
            document.getElementById('int_passport_error').style.color = "#FF0000";
        }
      }

      // A loop that checks every input field in the current tab:
      for (i = 0; i < y.length; i++) {
        // If a field is empty
        if (y[i].value == "" ) {
          var error_id = y[i].name+'_error';
          document.getElementById(error_id).innerHTML ="*Required Field";
          document.getElementById(error_id).style.color = "#FF0000";
          y[i].style.backgroundColor = "#ffdddd";
          y[i].focus();
          valid = false;
          //y[i].placeholder = "Can not be blank!"
          if(y[i].name != 'pa_email' && y[i].name != 'mo_email'){
            if(y[i].type =='email'){
            if(!validateEmail(y[i].value)){
              valid = false;
              y[i].style.backgroundColor = "#ffdddd";
              y[i].focus();
              var error_id = y[i].name+'_error';
              document.getElementById(error_id).innerHTML ="*Email format is not correct";
              document.getElementById(error_id).style.color = "#FF0000";
              // y[i].placeholder = "Email format is not correct"
              }
            }
          }
        }
        else{
          var error_id = y[i].name+'_error';
          document.getElementById(error_id).innerHTML ="";
          y[i].style.backgroundColor = "white";
          if(y[i].type =='email'){
            if(!validateEmail(y[i].value)){
              valid = false;
              y[i].style.backgroundColor = "#ffdddd";
              y[i].focus();
              var error_id = y[i].name+'_error';
              document.getElementById(error_id).innerHTML ="*Email format is not correct";
            document.getElementById(error_id).style.color = "#FF0000";
          }
        }
        }
      }
      // If the valid status is true, mark the step as finished and valid:
      if (valid) {
        document.getElementsByClassName("step")[currentStep].className += " finish";
        var class_name = document.getElementsByClassName("step")[currentStep].className.split(' active').join('')
        document.getElementsByClassName("step")[currentStep].className = class_name;
      }
      return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            if(i <= n){
                x[i].className = x[i].className.replace(" disabled", "");
                x[i].className = x[i].className.split(' active').join('');
                if(!x[i].classList.contains('active')){
                    x[n].className += " active";
                }
            }else{
                x[i].className = x[i].className.split(' active').join('');
                if(!x[i].classList.contains('disabled')){
                    x[i].className += " disabled";
                }
            }
        }
    }    
 </script>  

 <script type="text/javascript">
      $('input[type=file]').change(function () {
        var id = $(this).attr('id')+"-show";
        var size = this.files[0].size;
            var val = $(this).val().toLowerCase(),
                regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|png|rtf|jpg|jpeg|PDF)$");
            if(size >31457280){
              $(this).val('');
              $('#'+id).empty().append('')
                alert('Your files have not been uploaded. The maximum upload file size is 30 MB. Please check your file sizes and try again.');
            }
            if (!(regex.test(val))) {
                $(this).val('');
                $('#'+id).empty().append('')
                alert('This file type is not accepted. Please save your file as one of the listed supported types.');
            }
            else{
              var file = $(this)[0].files[0].name;
              $('#'+id).empty().append(file);
            }
        });
 </script>