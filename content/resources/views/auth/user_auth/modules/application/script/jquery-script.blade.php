<script>
    $(document).ready(function(){
        //$('input').attr("autocomplete",'nope');

        if($("input[name='custody_status']:checked").val() == "Other"){
              $("#student_lives_with").prop("disabled",false);
              $("#student_lives_with").prop("required",true);
            }
            else{
              $("#student_lives_with").prop("disabled",true);
              $("#student_lives_with").prop("required",false);
            }
             
        if( $('#canadian-citizen').find('input:checkbox:checked').length >0){
          $("#stu_support_details").prop("disabled",false);
            }
            else{
          $("#stu_support_details").prop("disabled",true);
          }

      if($('.heard_from_field').find('input[value="Other"]:checked').length >0){
            $("#heard_from_other").prop("disabled",false);
          }
          else{
            $("#heard_from_other").prop("disabled",true);
          }

      $('.heard_from_field').change(function(){
        if($(this).find('input[value="Other"]:checked').length >0){
              $("#heard_from_other").prop("disabled",false);
            }
            else{
              $("#heard_from_other").prop("disabled",true);
              $("#heard_from_other").val("")
            }
      })


      //set required fields for step 3
        $("input[type='radio'][name='father']").change(function(){
            $('#pa_fname').prop('required',true);
            $('#pa_lname').prop('required',true);
            $('#pa_street').prop('required',true);
            $('#pa_city').prop('required',true);
            $('#pa_province').prop('required',true);
            $('#pa_country').prop('required',true);
            $('#pa_postal').prop('required',true);
            $('#pa_hphone').prop('required',true);
            $('#pa_occupation').prop('required',true);
            $('#pa_employer').prop('required',true);
        })
        $("input[type='radio'][name='mother']").change(function(){                    
            $('#mo_fname').prop('required',true);
            $('#mo_lname').prop('required',true);
            $('#mo_street').prop('required',true);
            $('#mo_city').prop('required',true);
            $('#mo_province').prop('required',true);
            $('#mo_country').prop('required',true);
            $('#mo_postal').prop('required',true);
            $('#mo_hphone').prop('required',true);
            $('#mo_occupation').prop('required',true);
            $('#mo_employer').prop('required',true);
        })

        if($("input[type='radio'][name='father']:checked").length >0){
            $('#pa_fname').prop('required',true);
            $('#pa_lname').prop('required',true);
            $('#pa_street').prop('required',true);
            $('#pa_city').prop('required',true);
            $('#pa_province').prop('required',true);
            $('#pa_country').prop('required',true);
            $('#pa_postal').prop('required',true);
            $('#pa_hphone').prop('required',true);
            $('#pa_occupation').prop('required',true);
            $('#pa_employer').prop('required',true);
        }

        if($("input[type='radio'][name='mother']:checked").length >0){                  
            $('#mo_fname').prop('required',true);
            $('#mo_lname').prop('required',true);
            $('#mo_street').prop('required',true);
            $('#mo_city').prop('required',true);
            $('#mo_province').prop('required',true);
            $('#mo_country').prop('required',true);
            $('#mo_postal').prop('required',true);
            $('#mo_hphone').prop('required',true);
            $('#mo_occupation').prop('required',true);
            $('#mo_employer').prop('required',true);
        }

        //toggle parent address fields step 3
        if ($('#mother-addr-checkbox').find('input:checkbox:checked').length > 0) {
            $("#mother-addr").prop("disabled", true);
            $("#mother-addr").prop("required", false);
            $('#mo_street').prop('required',false);
            $('#mo_city').prop('required',false);
            $('#mo_province').prop('required',false);
            $('#mo_country').prop('required',false);
            $('#mo_postal').prop('required',false);
        } else {
            $("#mother-addr").prop("disabled", false);
            $("#mother-addr").prop("required", true);
        }

        if ($('#father-addr-checkbox').find('input:checkbox:checked').length > 0) {
            $("#father-addr").prop("disabled", true);
            $("#father-addr").prop("required", false);
            $('#pa_street').prop('required',false);
            $('#pa_city').prop('required',false);
            $('#pa_province').prop('required',false);
            $('#pa_country').prop('required',false);
            $('#pa_postal').prop('required',false);
        } else {
            $("#father-addr").prop("disabled", false);
            $("#father-addr").prop("required", true);
        }

        //phone number mask
        $('.phone_number_format')
            .keydown(function (e) {
              var key = e.which || e.charCode || e.keyCode || 0;
              $phone = $(this);

              if (key !== 8 && key !== 9) {
                if ($phone.val().length === 3) {
                  $phone.val($phone.val() + '-');
                }			
                if ($phone.val().length === 7) {
                  $phone.val($phone.val() + '-');
                }
              }
              return (key == 8 || 
                  key == 9 ||
                  key == 46 ||
                  (key >= 48 && key <= 57) ||
                  (key >= 96 && key <= 105));	
            })

            .bind('focus click', function () {
              $phone = $(this);
              
                var val = $phone.val();
                $phone.val('').val(val); 
            })

            .blur(function () {
              $phone = $(this);
              
            });

        //hide iep upload field, show when iep is checked on step 2
       $("#iep").hide().find(':input').attr('required', false);
        
          var iep_check = $('#canadian-citizen').find('input:checkbox:checked').map(function(){
          return $(this).val();
          }).get();
          iep_check.forEach(element => {
          if(element == "IEP / AEP" ){
            console.log("here")
              $("#iep").show();
            }
          });
        //toggle grade input box
        var grade = $("input[name='grade']:checked").attr("id");
        if(grade == "grade-rad"){
          $("#grade-input").prop("disabled",false);
          $("#grade-input").prop("required",true);
        }
        else{
          $("#grade-input").prop("disabled",true);
          $("#grade-input").prop("required",false);
        }

        //toggle medical condition box
        var allergies = $("input[name='allegies']:checked").val();
        if(allergies == "yes"){
          $("#allergies-input").prop("disabled",false);
            $("#allergies-input").prop("required",true);
        }
        else{
          $("#allergies-input").prop("disabled",true);
          $("#allergies-input").prop("required",false);
          $("#allergies-input").val("")
        }

      //toggle alumnus condition box
      var alumnus = $("input[name='alumnus']:checked").val();
        if(alumnus == "yes"){
          $("#alumnus_input").prop("disabled",false);
            $("#alumnus_input").prop("required",true);
        }
        else{
          $("#alumnus_input").prop("disabled",true);
          $("#alumnus_input").prop("required",false);
          $("#alumnus_input").val("")
        }

      //toggle alumna condition box
      var alumna = $("input[name='alumna']:checked").val();
        if(alumna == "yes"){
          $("#alumna_input").prop("disabled",false);
            $("#alumna_input").prop("required",true);
        }
        else{
          $("#alumna_input").prop("disabled",true);
          $("#alumna_input").prop("required",false);
          $("#alumna_input").val("")
        }


        //All button click functions
        $('.sfl').click(function(event) {
          event.preventDefault();
          $('form').attr("action", "{{ route('user.form.saveForLater') }}");  //change the form action
          $('form').submit();  // submit the form
        });

        //form exit
        $('.exit').click(function(event) {
          window.location.href = "/user/dashboard"
        }); 

        //submit button
        $('.submit').click(function(event) {
          event.preventDefault();
            $('form').attr("action", "{{ route('user.form.sutmit',$application->id??0) }}");  //change the form action
          $('form').submit();  // submit the form
        });

          //validate required fields before go to summary page
        $('.summary').click(function(event) {
          event.preventDefault();
          var valid= 0;
          var required_fields=[];
          var exclude = ['pa_sig','pa_sig_date','mo_sig','mo_sig_date'];
          var filtered_fields= [];
          //get all required fields
          $(':input[required]').each(function(){
             required_fields.push($(this).attr("name"));
    
          })

          //exclude parents signature at last step.
          filtered_fields = required_fields.filter(function(val) {
                  return exclude.indexOf(val) == -1;
          });
          for( i = 0; i < filtered_fields.length; ++i){
            if($(":input[name="+filtered_fields[i]+"]").val().length ==0){
                valid++;
            }
          }
        
          //validate file uploads (Canadian application)
        var canada_photo = $('#photo-show');
        var canada_passport = $('#pass-show');
        if(canada_photo.length >0&& canada_passport.length >0){
          if( canada_photo.text().length ==0 ){
          $('#photo_error').text("*Required Field");
          $('#photo_error').prop('style',"color:#FF0000");
        }
        if( canada_passport.text().length ==0 ){
            $('#passport_error').text("*Required Field");
            $('#passport_error').prop('style',"color:#FF0000");
        }
        if( canada_photo.text() != '*Required Field' && canada_passport.text() != '*Required Field' ){
          if(valid == 0){
            $('form').attr("action", "{{ route('user.form.save') }}");  //change the form action
            $('form').submit(); 
          }
          else{
            alert("Please complete the required fields in all steps of the application. Thank you.")
          }
      }
        }

      //validate file uploads (International application)
      var international_photo = $('#in-photo-show');
      var international_passport = $('#in-pass-show');
      var letter = $('#in-letter-show');
      var recent_card = $('#in-card-cur-show');
        if(international_photo.length >0 && international_passport.length >0){
          if( international_photo.text().length ==0 ){
          $('#int_photo_error').text("*Required Field") ;
          $('#int_photo_error').prop('style',"color:#FF0000") ;
        }
        if(international_passport.text().length ==0){
          $('#int_passport_error').text("*Required Field") ;
          $('#int_passport_error').prop('style',"color:#FF0000");
      }  
      if(letter.text().length== 0){
        $('#in_letter_error').text("*Required Field") ;
          $('#in_letter_error').prop('style',"color:#FF0000");
      }
      if(recent_card.text().length == 0){
        $('#in_recent_card_error').text("*Required Field") ;
          $('#in_recent_card_error').prop('style',"color:#FF0000");
      }
      if( international_photo.text() != '*Required Field' && international_passport.text() != '*Required Field'  && letter.text() != '*Required Field' && recent_card.text() != '*Required Field'){
        if(valid == 0){
            $('form').attr("action", "{{ route('user.form.save') }}");  //change the form action
            $('form').submit(); 
          }
          else{
            alert("Please complete the required fields in all steps of the application. Thank you.")
          }
      }
        }          
        });

      //enable + disable fields when check rad button
        $("input[type='radio']").click(function(){
            var grade = $("input[name='grade']:checked").attr("id");

             //custody input on parent information step
            if($("input[name='custody_status']:checked").val() == "Other"){
              $("#student_lives_with").prop("disabled",false);
              $("#student_lives_with").prop("required",true);
            }
            else{
              $("#student_lives_with").prop("disabled",true);
              $("#student_lives_with").prop("required",false);
              $("#student_lives_with").val("");
            }

             //Grrade input on student information step (Canadian application)
            if(grade == "grade-rad"){
              $("#grade-input").prop("disabled",false);
              $("#grade-input").prop("required",true);
            }
            else{
              $("#grade-input").prop("disabled",true);
              $("#grade-input").prop("required",false);
            }

            //Allergies on student information step
            var allergies = $("input[name='allegies']:checked").val();
            if(allergies == "yes"){
              $("#allergies-input").prop("disabled",false);
              $("#allergies-input").prop("required",true);
            }
            else{
              $("#allergies-input").prop("disabled",true);
              $("#allergies-input").prop("required",false);
              $("#allergies-input").val("")
            }
            
            //alumnna + alumnus input on parent information step
            var alumnus = $("input[name='alumnus']:checked").val();
            var alumnus_check = $("input[name='alumnus']");
            if(alumnus_check.length >0){
              if(alumnus == "yes"){
              $("#alumnus_input").prop("disabled",false);
              $("#alumnus_input").prop("required",true);
            }
            else{
              $("#alumnus_input").prop("disabled",true);
              $("#alumnus_input").prop("required",false);
              $("#alumnus_input").val("")
            }

            }
            
            var alumna = $("input[name='alumna']:checked").val();
            var alumna_check = $("input[name='alumna']");
            if(alumna_check.length >0 ){
              if(alumna == "yes"){
              $("#alumna_input").prop("disabled",false);
              $("#alumna_input").prop("required",true);
            }
            else{
              $("#alumna_input").prop("disabled",true);
              $("#alumna_input").prop("required",false);
              $("#alumna_input").val("")
            }
            }
            
        });

        //show and hide Iep file upload field on canadian application - when iep support document checkbox on student information step change
        $('#canadian-citizen').change(function(){
          $("#iep").hide().find(':input').val('');
         
          //Show and hide IEP upload field
          var value = $(this).find('input:checkbox:checked').map(function(){
          return $(this).val();
          }).get();
          value.forEach(element => {
          if(element == "IEP / AEP" ){
              $("#iep").show();
            }
          });

            //Show and hide Canadian Citizenship field
            if($(this).find('input:checkbox:checked').length >0){
              $("#stu_support_details").prop("disabled",false);
            }
            else{
              $("#stu_support_details").prop("disabled",true);
            }
        });

        //enable and disable mother + father address fields on parent information step.
        $('#mother-addr-checkbox').change(function(){
          if($(this).find('input:checkbox:checked').length >0){
            $("#mother-addr").prop("disabled",true);
            $("#mother-addr").prop("required",false);
              $('#mo_street').prop('required',false);
            $('#mo_city').prop('required',false);
            $('#mo_province').prop('required',false);
            $('#mo_country').prop('required',false);
            $('#mo_postal').prop('required',false);

            $('#mo_street_error').text("").prop("style","");
            $('#mo_city_error').text("");
            $('#mo_province_error').text("");
            $('#mo_country_error').text("");
            $('#mo_postal_error').text("");
          }
          else{
            $("#mother-addr").prop("disabled",false);
            $("#mother-addr").prop("required",true);
            if($("input[type='radio'][name='mother']:checked").length >0){
              $('#mo_street').prop('required',true);
              $('#mo_city').prop('required',true);
              $('#mo_province').prop('required',true);
              $('#mo_country').prop('required',true);
              $('#mo_postal').prop('required',true);
            }
          }
        });

        $('#father-addr-checkbox').change(function(){
          if($(this).find('input:checkbox:checked').length >0){
            $("#father-addr").prop("disabled",true);
            $("#father-addr").prop("required",false);

            $('#pa_street').prop('required',false);
            $('#pa_city').prop('required',false);
            $('#pa_province').prop('required',false);
            $('#pa_country').prop('required',false);
            $('#pa_postal').prop('required',false);

            $('#pa_street_error').text("");
            $('#pa_city_error').text("");
            $('#pa_province_error').text("");
            $('#pa_country_error').text("");
            $('#pa_postal_error').text("");
          }
          else{
            $("#father-addr").prop("disabled",false);
            $("#father-addr").prop("required",true);
            if($("input[type='radio'][name='father']:checked").length >0){
              $('#pa_street').prop('required',true);
            $('#pa_city').prop('required',true);
            $('#pa_province').prop('required',true);
            $('#pa_country').prop('required',true);
            $('#pa_postal').prop('required',true);
            }
          }
        });
    });
</script>