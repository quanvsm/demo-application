@extends('auth.user_auth.layouts.application')
<?
$class = 'form';
  $active = 'new_applications';
?>
@section('content')   
<article id="content">
<div class="tab" >
    <header>
        <h1>Apply Now</h1>
      </header>
      <section>
        <div class="application-type-selection">
        <a class="button canadian" href="{{route('user.form.show',1) }}">Canadian Application Form</a>
          <a class="button international" href="{{route('user.form.show',2) }}">International Application Form</a>
        </div> 
      </section>
</div>
</article>
@endsection