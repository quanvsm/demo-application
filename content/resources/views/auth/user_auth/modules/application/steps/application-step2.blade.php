<section>
  {{-- <strong>Please ensure you have the following digital documents on hand to support your application:</strong>
  @if($type == 1)
 <ul>
   <li> Most recent photo of student.</li>
   <li> Student's birth certificate or passport.</li>
   <li> Student's most recent report card.</li>
   <li> Student's high school transcript (If applicable).</li>
   <li> Custody agreement (If applicable).</li>
 </ul>
 <p>There is a $100 non-refundable application fee per student (fee is transferable for one year). A credit card is required to complete the online application process.</p>
 @else
 <ul>
  <li> Most recent photo of student.</li>
  <li> Student's birth certificate or passport (Translated).</li>
  <li> Certified translated copies of transcripts of school report cards -  most recent and previous 2 years of school.</li>
  <li> One letter of recommendation from the principal, director or teacher of the current school.</li>
  <li> Custody agreement (If applicable).</li>
</ul>
<p>There is a $200 non-refundable application fee per student. A credit card is required to complete the online application process.</p>
 @endif
 <p>While school is in session, you may contact a member of the Admissions
  Office at <a href="tel:+12049896739">204-989-6739</a> to ask questions between the hours of 8:00 am
  and 4:00 pm. During non-work hours, you may email
  <a href="mailto:admissions@lindenchristian.org">admissions@lindenchristian.org</a> with your questions.</p> --}}

    <div class="row">
      <input type="hidden" name="type_id" value="{{$type}}">
      <input type="hidden" id="step_id" name="step_id" value='{{$step ?? 1}}'>
      <input type="hidden" id="form_id" name="form_id" value="{{$id ?? ''}}">
      @if($type == 1)
      <fieldset class="radio grade" id="canadian-grade">
        <!-- Canadian application only -->
        <label><input type="radio" id="grade-rad" name="grade"
            {{(isset($data->grade) && $data->grade=='on'? 'checked' :(old("grade") == "on" ? 'checked' : '' ))}}>Grade</label>
        <label><input required name="grade_in" type="number" min="1" max="12" step="1" disabled id="grade-input"
            value="{{$data->grade_in ?? old("grade_in")}}"><span></label><!-- enable input if above is selected -->
        <label><input type="radio" name="grade" value="Full Time Kindergarten"
            {{(isset($data->grade) && $data->grade=='Full Time Kindergarten'? 'checked' :(old("grade") == "Full Time Kindergarten" ? 'checked' : '' ))}}>*e.g: Full Time
          Kindergarten</label>
        <label><input type="radio" name="grade" value="Part Time Kindergarten"
            {{(isset($data->grade) && $data->grade=='Part Time Kindergarten'? 'checked' :(old("grade") == "Part Time Kindergarten" ? 'checked' : '' ))}}>*e.g: Part Time
          Kindergarten</label>
        <span id='grade_error'></span>
        <span id='grade_in_error'></span>
      </fieldset>

      @else
      <label id="international-grade">Grade<input type="number" min="1" max="12" step="1" name="int_grade" required
          value="{{$data->int_grade ?? old("int_grade")}}"><span id='int_grade_error'></span></label>
      <!-- International appllication only -->
      @endif
    </div>
    <h3>Name</h3>
    <div class="row">
      <label>Legal Last Name<input name="stu_lname" type="text" value="{{$data->stu_lname ?? old("stu_lname")}}" required><span
          id='stu_lname_error'></span></label>
          <label>Legal First Name<input name="stu_fname" type="text" value="{{$data->stu_fname ?? old("stu_fname")}}" required><span
            id='stu_fname_error'></span></label>
      <label>Legal Middle Name<input name="stu_mname" type="text" value="{{$data->stu_mname ?? old("stu_mname")}}"><span
          id='stu_mname_error'></span></label>
      <label>Commonly Goes By<input name="stu_cname" type="text" value="{{$data->stu_cname ?? old("stu_cname")}}"><span
          id='stu_cname_error'></span></label>
    </div>
    <div class="row">
      <fieldset>
        <legend>Date of Birth</legend>
        <label>
          <select name="stu_month_of_birth" required>
            <option value="">-- Month --</option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='01'? 'selected' :(old("stu_month_of_birth") == '01' ? 'selected' : '') )}}>
              01</option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='02'? 'selected' :(old("stu_month_of_birth") == '02' ? 'selected' : '') )}}>
              02</option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='03'? 'selected' :(old("stu_month_of_birth") == '03' ? 'selected' : '') )}}>
              03</option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='04'? 'selected' :(old("stu_month_of_birth") == '04' ? 'selected' : '') )}}>
              04</option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='05'? 'selected' :(old("stu_month_of_birth") == '05' ? 'selected' : '') )}}>05
            </option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='06'? 'selected' :(old("stu_month_of_birth") == '06' ? 'selected' : '') )}}>06
            </option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='07'? 'selected' :(old("stu_month_of_birth") == '07' ? 'selected' : '') )}}>07
            </option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='08'? 'selected' :(old("stu_month_of_birth") == '08' ? 'selected' : '') )}}>
              08</option>
            <option
              {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='09'? 'selected' :(old("stu_month_of_birth") == '09' ? 'selected' : '') )}}>
              09</option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='10'? 'selected' :(old("stu_month_of_birth") == '10' ? 'selected' : '') )}}>
              10</option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='11'? 'selected' :(old("stu_month_of_birth") == '11' ? 'selected' : '') )}}>
              11</option>
            <option {{(isset($data->stu_month_of_birth) && $data->stu_month_of_birth=='12'? 'selected' :(old("stu_month_of_birth") == '12' ? 'selected' : '') )}}>
              12</option>
          </select>
          <span id='stu_month_of_birth_error'></span>
        </label>
        <label>
          <select name="stu_date_of_birth" required>
            <option value="">-- Day --</option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='01'? 'selected' :(old("stu_date_of_birth") == '01' ? 'selected' : ''))}}>01
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='02'? 'selected' :(old("stu_date_of_birth") == '02' ? 'selected' : ''))}}>02
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='03'? 'selected' :(old("stu_date_of_birth") == '03' ? 'selected' : ''))}}>03
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='04'? 'selected' :(old("stu_date_of_birth") == '04' ? 'selected' : ''))}}>04
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='05'? 'selected' :(old("stu_date_of_birth") == '05' ? 'selected' : ''))}}>05
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='06'? 'selected' :(old("stu_date_of_birth") == '06' ? 'selected' : ''))}}>06
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='07'? 'selected' :(old("stu_date_of_birth") == '07' ? 'selected' : ''))}}>07
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='08'? 'selected' :(old("stu_date_of_birth") == '08' ? 'selected' : ''))}}>08
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='09'? 'selected' :(old("stu_date_of_birth") == '09' ? 'selected' : ''))}}>09
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='10'? 'selected' :(old("stu_date_of_birth") == '10' ? 'selected' : ''))}}>10
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='11'? 'selected' :(old("stu_date_of_birth") == '11' ? 'selected' : ''))}}>11
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='12'? 'selected' :(old("stu_date_of_birth") == '12' ? 'selected' : ''))}}>12
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='13'? 'selected' :(old("stu_date_of_birth") == '13' ? 'selected' : ''))}}>13
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='14'? 'selected' :(old("stu_date_of_birth") == '14' ? 'selected' : ''))}}>14
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='15'? 'selected' :(old("stu_date_of_birth") == '15' ? 'selected' : ''))}}>15
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='16'? 'selected' :(old("stu_date_of_birth") == '16' ? 'selected' : ''))}}>16
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='17'? 'selected' :(old("stu_date_of_birth") == '17' ? 'selected' : ''))}}>17
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='18'? 'selected' :(old("stu_date_of_birth") == '18' ? 'selected' : ''))}}>18
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='19'? 'selected' :(old("stu_date_of_birth") == '19' ? 'selected' : ''))}}>19
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='20'? 'selected' :(old("stu_date_of_birth") == '20' ? 'selected' : ''))}}>20
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='21'? 'selected' :(old("stu_date_of_birth") == '21' ? 'selected' : ''))}}>21
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='22'? 'selected' :(old("stu_date_of_birth") == '22' ? 'selected' : ''))}}>22
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='23'? 'selected' :(old("stu_date_of_birth") == '23' ? 'selected' : ''))}}>23
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='24'? 'selected' :(old("stu_date_of_birth") == '24' ? 'selected' : ''))}}>24
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='25'? 'selected' :(old("stu_date_of_birth") == '25' ? 'selected' : ''))}}>25
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='26'? 'selected' :(old("stu_date_of_birth") == '26' ? 'selected' : ''))}}>26
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='27'? 'selected' :(old("stu_date_of_birth") == '27' ? 'selected' : ''))}}>27
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='28'? 'selected' :(old("stu_date_of_birth") == '28' ? 'selected' : ''))}}>28
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='29'? 'selected' :(old("stu_date_of_birth") == '29' ? 'selected' : ''))}}>29
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='30'? 'selected' :(old("stu_date_of_birth") == '30' ? 'selected' : ''))}}>30
            </option>
            <option {{(isset($data->stu_date_of_birth) && $data->stu_date_of_birth=='31'? 'selected' :(old("stu_date_of_birth") == '31' ? 'selected' : ''))}}>31
            </option>
          </select>
          <span id='stu_date_of_birth_error'></span>
        </label>
        <label>
          <select name="stu_year_of_birth" required>
            <option value="">-- Year --</option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2016'? 'selected' :(old("stu_year_of_birth") == '2016' ? 'selected' : ""))}}>2016
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2015'? 'selected' :(old("stu_year_of_birth") == '2015' ? 'selected' : ""))}}>2015
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2014'? 'selected' :(old("stu_year_of_birth") == '2014' ? 'selected' : ""))}}>2014
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2013'? 'selected' :(old("stu_year_of_birth") == '2013' ? 'selected' : ""))}}>2013
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2012'? 'selected' :(old("stu_year_of_birth") == '2012' ? 'selected' : ""))}}>2012
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2011'? 'selected' :(old("stu_year_of_birth") == '2011' ? 'selected' : ""))}}>2011
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2010'? 'selected' :(old("stu_year_of_birth") == '2010' ? 'selected' : ""))}}>2010
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2009'? 'selected' :(old("stu_year_of_birth") == '2009' ? 'selected' : ""))}}>2009
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2008'? 'selected' :(old("stu_year_of_birth") == '2008' ? 'selected' : ""))}}>2008
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2007'? 'selected' :(old("stu_year_of_birth") == '2007' ? 'selected' : ""))}}>2007
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2006'? 'selected' :(old("stu_year_of_birth") == '2006' ? 'selected' : ""))}}>2006
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2005'? 'selected' :(old("stu_year_of_birth") == '2005' ? 'selected' : ""))}}>2005
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2004'? 'selected' :(old("stu_year_of_birth") == '2004' ? 'selected' : ""))}}>2004
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2003'? 'selected' :(old("stu_year_of_birth") == '2003' ? 'selected' : ""))}}>2003
            </option>
            <option {{(isset($data->stu_year_of_birth) && $data->stu_year_of_birth=='2002'? 'selected' :(old("stu_year_of_birth") == '2002' ? 'selected' : ""))}}>2002
            </option>
          </select>
          <span id='stu_year_of_birth_error'></span>
        </label>
      </fieldset>
      <label>Gender
        <select name="stu_gender" required>
          <option value="">-- Select --</option>
          <option {{(isset($data->stu_gender) && $data->stu_gender=='Male'? 'selected' : (old("stu_gender") == 'Male' ? "selected" : ''))}}>Male</option>
          <option {{(isset($data->stu_gender) && $data->stu_gender=='Female'? 'selected' :(old("stu_gender") == 'Female' ? "selected" : ''))}}>Female</option>
        </select>
        <span id='stu_gender_error'></span>
      </label>
    </div>
    <h3>Home Address</h3>
    <div class="row">
      <label>Street Address<input type="text" name="stu_street" value="{{$data->stu_street??''}}" required><span id='stu_street_error'></span></label>
      <label>City<input type="text" name="stu_city" value="{{$data->stu_city??''}}" required>  <span id='stu_city_error'></span></label>
      @if($type==1)
      <label>Province / State
        <select name="stu_province" required>
          <option value="">-- Select --</option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Alberta'? 'selected' : (old("stu_province") == 'Alberta'? 'selected' : ''))}}>Alberta
          </option>
          <option {{(isset($data->stu_province) && $data->stu_province=='British Columbia'? 'selected' : (old("stu_province") == 'British Columbia'? 'selected' : ''))}}>
            British Columbia
          </option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Manitoba'? 'selected' : (old("stu_province") == 'Manitoba' ? 'selected' : ''))}}>Manitoba
          </option>
          <option {{(isset($data->stu_province) && $data->stu_province=='New Brunswick'? 'selected' : (old("stu_province") == 'New Brunswick' ? 'selected' : ''))}}>New
            Brunswick</option>
          <option
            {{(isset($data->stu_province) && $data->stu_province=='Newfoundland and Labrador'? 'selected' : (old("stu_province") == 'Newfoundland and Labrador' ? 'selected' : ''))}}>
            Newfoundland and Labrador</option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Nova Scotia'? 'selected' : (old("stu_province") == 'Nova Scotia'? 'selected' : ''))}}>Nova Scotia
          </option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Ontario'? 'selected' : (old("stu_province") == 'Ontario'? 'selected' : ''))}}>Ontario
          </option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Prince Edward Island'? 'selected' : (old("stu_province") == 'Prince Edward Island' ? 'selected' : ''))}}>
            Prince Edward Island</option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Quebec'? 'selected' : (old("stu_province") == 'Quebec'? 'selected' : ''))}}>Quebec</option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Saskatchewan'? 'selected' : (old("stu_province") =='Saskatchewan' ? 'selected' : ''))}}>
            Saskatchewan</option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Northwest Territories'? 'selected' : (old("stu_province") == 'Northwest Territories'? 'selected' : ''))}}>
            Northwest Territories</option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Nunavut'? 'selected' : (old("stu_province") == 'Nunavut'? 'selected' : ''))}}>Nunavut
          </option>
          <option {{(isset($data->stu_province) && $data->stu_province=='Yukon'? 'selected' : (old("stu_province") =='Yukon' ? 'selected' : ''))}}>Yukon</option>
        </select>
        <span id='stu_province_error'></span>
      </label>
      @else
      <label>Province / State <input type="text" name="stu_province" value="{{$data->stu_province ?? old("stu_province")}}" required><span id='stu_province_error'></span></label>
      <label>Country<input type="text" name="stu_country" value="{{$data->stu_country ?? old("stu_country")}}" required><span id='stu_country_error'></span></label>
      @endif
      <label {{$type==2 ? "id=international-grade" : ""}}>Postal Code / Zip Code<input type="text" name="stu_postal" value="{{$data->stu_postal ?? old("stu_postal")}}" required><span id='stu_postal_error'></span></label>
    </div>
    <div class="row">
      <label>Primary Phone<input class="phone_number_format" required type="text" name="stu_phone" maxlength="12" value="{{$data->stu_phone ?? old("stu_phone")}}"><span id='stu_phone_error'></span></label>
      <label>Primary Family Email<input type="email" name="stu_email" value="{{$data->stu_email ?? old("stu_email")}}"
          required><span id='stu_email_error'></span></label>
    </div>
    {{-- <h3>History</h3> --}}
    <div class="row">
      <label>Citizenship / Immigration Status
        <select id="option" name="stu_citizenship_status" required>
          <option value="">-- Select --</option>
          @if($type == 1)
          <!-- Canadian application options -->
          <option id="canadian"
            {{(isset($data->stu_citizenship_status) && $data->stu_citizenship_status=='Canadian Citizen'? 'selected' :(old("stu_citizenship_status") == "Canadian Citizen" ? 'selected' : ""))}}>
            Canadian Citizen</option>
          <option id="canadian"
            {{(isset($data->stu_citizenship_status) && $data->stu_citizenship_status=='Permanent Resident'? 'selected' :(old("stu_citizenship_status") == "Permanent Resident" ? 'selected' : ""))}}>
            Permanent Resident</option>
          <option id="canadian"
            {{(isset($data->stu_citizenship_status) && $data->stu_citizenship_status=='Parent Study / Work Permit'? 'selected' :(old("stu_citizenship_status") == "Parent Study / Work Permit" ? 'selected' : ""))}}>
            Parent Study / Work Permit</option>
          @else
          <!-- International application options -->
          <option id="international"
            {{(isset($data->stu_citizenship_status) && $data->stu_citizenship_status=='International Student'? 'selected' :(old("stu_citizenship_status") == "International Student" ? 'selected' : ""))}}>
            International Student</option>
          <option id="international"
            {{(isset($data->stu_citizenship_status) && $data->stu_citizenship_status=='Parent with Study Permit'? 'selected' :(old("stu_citizenship_status") == "Parent with Study Permit" ? 'selected' : ""))}}>
            Parent with Study Permit</option>
          <option id="international"
            {{(isset($data->stu_citizenship_status) && $data->stu_citizenship_status=='Parent with Work Permit'? 'selected' :(old("stu_citizenship_status") == "Parent with Work Permit" ? 'selected' : ""))}}>
            Parent with Work Permit</option>
          @endif
        </select>
        <span id='stu_citizenship_status_error'></span>
      </label>
      <label>Language Spoken at Home<input required type="text" name="stu_lang" value="{{$data->stu_lang ?? old("stu_lang")}}"><span id='stu_lang_error'></span></label>
      <label>Country of Birth (If not Canada)<input {{$type==2 ? 'required' : '' }} type="text" name="stu_country_of_birth"
          value="{{$data->stu_country_of_birth ?? old("stu_country_of_birth")}}"><span id='stu_country_of_birth_error'></span></label>
    </div>
    <div class="row">
      <label>Current School<input type="text" name="stu_curr_school"
          value="{{$data->stu_curr_school ?? old("stu_curr_school")}}"><span id='stu_curr_school_error'></span></label>
      <label>City and Country<input type="text" name="stu_city_country"
          value="{{$data->stu_city_country ?? old("stu_city_country")}}"><span id='stu_city_country_error'></span></label>
    </div>
    <div class="row">
      <fieldset class="radio">
        <legend>Does student have any medical conditions or allergies?</legend>
        <label><input type="radio" name="allegies" value="yes"
            {{(isset($data->allegies) && $data->allegies=='yes'? 'checked' :(old("allegies") == 'yes'? "checked" : ''))}}>Yes</label>
        <label><input type="radio" name="allegies" value="no"
            {{(isset($data->allegies) && $data->allegies=='no'? 'checked' :(old("allegies") == 'no'? "checked" : ''))}}>No</label>
            <span id='medical_condition_error'></span>
      </fieldset>
      <label>If yes, please explain<input type="text" id="allergies-input" disabled name="stu_allegies_details" value="{{$data->stu_allegies_details ?? old("stu_allegies_details")}}"><span id='stu_allegies_details_error'></span></label>
      <!-- enabled by yes selection above -->
    </div>
    @if($type == 1)
    <div class="row" id="canadian-citizen">
      <!-- Canadian Citizen -->
      <fieldset class="checkbox" id="recieved" name="stu_support">
        <legend>Has student received any of the following?</legend>
        <label><input type="checkbox" name="stu_support[]" value="Resource / Special Ed Services"
            {{ (isset($data->stu_support)  && in_array('Resource / Special Ed Services', $data->stu_support)) ? 'checked' : (is_array(old('stu_support')) && in_array('Resource / Special Ed Services', old("stu_support")) ? ' checked' : '')  }}>Resource
          / Special Ed Services</label>
        <label><input type="checkbox" name="stu_support[]" value="IEP / AEP"
            {{ (isset($data->stu_support)  && in_array('IEP / AEP', $data->stu_support)) ? 'checked' : (is_array(old('stu_support')) && in_array('IEP / AEP', old("stu_support")) ? ' checked' : '')  }}>IEP /
          AEP</label>
        <label><input type="checkbox" name="stu_support[]" value="Behavioural Support / BIP"
            {{ (isset($data->stu_support)  && in_array('Behavioural Support / BIP', $data->stu_support)) ? 'checked' : (is_array(old('stu_support')) && in_array('Behavioural Support / BIP', old("stu_support")) ? ' checked' : '')  }}>Behavioural
          Support / BIP</label>
        <label><input type="checkbox" name="stu_support[]" value="Level 2 or 3 Funding"
            {{ (isset($data->stu_support)  && in_array('Level 2 or 3 Funding', $data->stu_support)) ? 'checked' : (is_array(old('stu_support')) && in_array('Level 2 or 3 Funding', old("stu_support")) ? ' checked' : '')  }}>Level
          2 or 3 Funding</label>
        <label><input type="checkbox" name="stu_support[]" value="Counselling"
            {{ (isset($data->stu_support)  && in_array('Counselling', $data->stu_support)) ? 'checked' : (is_array(old('stu_support')) && in_array('Counselling', old("stu_support")) ? ' checked' : '')  }}>Counselling</label>
        <label><input type="checkbox" name="stu_support[]" value="Gifted / Enrichment"
            {{ (isset($data->stu_support)  && in_array('Gifted / Enrichment', $data->stu_support)) ? 'checked' : (is_array(old('stu_support')) && in_array('Gifted / Enrichment', old("stu_support")) ? ' checked' : '')  }}>Gifted
          / Enrichment</label>
        <label><input type="checkbox" name="stu_support[]" value="Formal Assessments"
            {{ (isset($data->stu_support)  && in_array('Formal Assessments', $data->stu_support)) ? 'checked' : (is_array(old('stu_support')) && in_array('Formal Assessments', old("stu_support")) ? ' checked' : '')  }}>Formal
          Assessments &nbsp;<em>(eg. Psychology, Speech, Physical Therapy, etc.)</em></label>
          <label><input type="checkbox" name="stu_support[]" value="none"
            {{ (isset($data->stu_support)  && in_array('none', $data->stu_support)) ? 'checked' : (is_array(old('stu_support')) && in_array('none', old("stu_support")) ? ' checked' : '')  }}>None</label>
      </fieldset>
      <span id='stu_support_doc_error'></span>
      <label>Please provide details of support provided<textarea id="stu_support_details" name="stu_support_details" >{{$data->stu_support_details ?? old("stu_support_details")}}</textarea></label><!-- enabled by any above selection -->
    </div>
    @endif
  </section>