<section>
    <div class="row">
      <div class="column">
        <h3>Name</h3>
        <fieldset class="radio">
          <label><input type="radio" name="father" value="Father"
              {{(isset($data->father) && $data->father=='Father'? 'checked' :(old("father") == "Father" ? 'checked' : ''))}}>Father</label>
          <label><input type="radio" name="father" value="Guardian"
              {{(isset($data->father) && $data->father=='Guardian'? 'checked' :(old("father") == "Guardian" ? 'checked' : ''))}}>Guardian</label>
        </fieldset>
        <span id='father_error'></span>
        <label>Last Name<input type="text" id="pa_lname" name="pa_lname" value="{{$data->pa_lname ?? old("pa_lname")}}"><span
            id="pa_lname_error"></span></label>
        <label>First Name<input type="text" id="pa_fname" name="pa_fname" value="{{$data->pa_fname ?? old("pa_fname")}}"><span id="pa_fname_error"></span></label>
        <h3>Address</h3>
        <fieldset class="checkbox" id="father-addr-checkbox">
          <label><input type="checkbox" name="pa_addr_check_box"
              {{(isset($data->pa_addr_check_box) && $data->pa_addr_check_box=='on'? 'checked' :(old("pa_addr_check_box") == "on" ? "checked" : ''))}}>Same as
            Student</label>
        </fieldset>
        <fieldset disabled id="father-addr">
          <!-- this fieldset is toggled by above checkbox-->
          <label>Street Address<input type="text" id="pa_street" name="pa_street" value="{{$data->pa_street ?? old("pa_street")}}"><span id="pa_street_error"></span></label>
          <label>City<input type="text" name="pa_city" id="pa_city" value="{{$data->pa_city ?? old("pa_city")}}"><span id="pa_city_error"></span></label>
          <label>Province / State<input type="text" id="pa_province" name="pa_province" value="{{$data->pa_province ?? old("pa_province")}}"><span id="pa_province_error"></span></label>
          <label>Country<input type="text" id="pa_country" name="pa_country" value="{{$data->pa_country ?? old("pa_country")}}"><span id="pa_country_error"></span></label>
          <label>Postal Code / Zip<input type="text" id="pa_postal" name="pa_postal" value="{{$data->pa_postal ?? old("pa_postal")}}"><span id="pa_postal_error"></span></label>
        </fieldset>
        <h3>Contact</h3>
        <label>Primary Phone<input type="text" class="phone_number_format" maxlength="12" name="pa_hphone" id="pa_hphone" value="{{$data->pa_hphone ?? old("pa_hphone")}}"><span id="pa_hphone_error"></span></label>
        <label>Mobile Phone<input type="text" class="phone_number_format" maxlength="12" name="pa_mphone" value="{{$data->pa_mphone ?? old("pa_mphone")}}"><span id="pa_mphone_error"></span></label>
        <label>Email Address<input type="email" id='pa_email' name="pa_email" value="{{$data->pa_email ?? old("pa_email")}}"><span id='pa_email_error'></span></label>
        <h3>Employment</h3>
        <label>Occupation<input type="text" id="pa_occupation" name="pa_occupation" value="{{$data->pa_occupation ?? old("pa_occupation")}}"><span id='pa_occupation_error'></span></label>
        <label>Employer<input type="text" id="pa_employer" name="pa_employer" value="{{$data->pa_employer ?? old("pa_employer")}}"><span id='pa_employer_error'></span></label>
        @if ($type ==1)
        <fieldset class="radio">
          <legend>VSM Alumnus?</legend>
          <label><input type="radio" name="alumnus" value="yes"
              {{(isset($data->alumnus) && $data->alumnus=='yes'? 'checked' :(old("alimnus") == "yes" ? 'checked' : ''))}}>Yes</label>
          <label><input type="radio" name="alumnus" value="no"
              {{(isset($data->alumnus) && $data->alumnus=='no'? 'checked' :(old("alimnus") == "no" ? 'checked' : ''))}}>No</label>
              <span id='alumnus_error'></span>
        </fieldset>
        <label><input type="text" id="alumnus_input" disabled placeholder="If yes, graduating year and last name, if different" name="alumnus_input" value="{{$data->alumnus_input ?? old("alumnus_input")}}"><span id='alumnus_input_error'></span></label>
        @endif
      </div>

      <div class="column">
        <h3>Name</h3>
        <fieldset class="radio">
          <label><input type="radio" name="mother" value="Mother"
              {{(isset($data->mother) && $data->mother=='Mother'? 'checked' :(old("mother") == "Mother" ? 'checked' : ''))}}>Mother</label>
          <label><input type="radio" name="mother" value="Guardian"
              {{(isset($data->mother) && $data->mother=='Guardian'? 'checked' :(old("mother") == "Guardian" ? 'checked' : ''))}}>Guardian</label>
        </fieldset>
        <label>Last Name<input type="text" id="mo_lname" name="mo_lname" value="{{$data->mo_lname ?? old("mo_lname")}}"><span id='mo_lname_error'></span></label>
        <label>First Name<input type="text" id="mo_fname" name="mo_fname" value="{{$data->mo_fname ?? old("mo_fname")}}"><span id='mo_fname_error'></span></label>
        <h3>Address</h3>
        <fieldset class="checkbox" id="mother-addr-checkbox">
          <label><input type="checkbox" name="mo_addr_check_box"
              {{(isset($data->mo_addr_check_box) && $data->mo_addr_check_box=='on'? 'checked' :(old("mo_addr_check_box") == "on" ? "checked" : ''))}}>Same as
            Student</label>
        </fieldset>
        <fieldset disabled id="mother-addr">
          <!-- this fieldset is toggled by above checkbox-->
          <label>Street Address<input id="mo_street" type="text" name="mo_street" value="{{$data->mo_street ?? old("mo_street")}}"><span id='mo_street_error'></span></label>
          <label>City<input type="text" id="mo_city" name="mo_city" value="{{$data->mo_city ?? old("mo_city")}}"><span id='mo_city_error'></span></label>
          <label>Province / State<input id="mo_province" type="text" name="mo_province" value="{{$data->mo_province ?? old("mo_province")}}"><span id='mo_province_error'></span></label>
          <label>Country<input type="text" id="mo_country" name="mo_country" value="{{$data->mo_country ?? old("mo_country")}}"><span id='mo_country_error'></span></label>
          <label>Postal Code / Zip<input id="mo_postal" type="text" name="mo_postal" value="{{$data->mo_postal ?? old("mo_postal")}}"><span id='mo_postal_error'></span></label>
        </fieldset>
        <h3>Contact</h3>
        <label>Primary Phone<input type="text" class="phone_number_format" id="mo_hphone" maxlength="12" name="mo_hphone" value="{{$data->mo_hphone ?? old("mo_hphone")}}"><span id='mo_hphone_error'></span></label>
        <label>Mobile Phone<input type="text" class="phone_number_format" name="mo_mphone" maxlength="12" value="{{$data->mo_mphone ?? old("mo_mphone")}}"><span id='mo_mphone_error'></span></label>
        <label>Email Address<input type="email" id='mo_email' name="mo_email" value="{{$data->mo_email ?? old("mo_email")}}"><span id='mo_email_error'></span></label>
        <h3>Employment</h3>
        <label>Occupation<input type="text" id="mo_occupation" name="mo_occupation" value="{{$data->mo_occupation ?? old("mo_occupation")}}"><span id='mo_occupation_error'></span></label>
        <label>Employer<input type="text" id="mo_employer" name="mo_employer" value="{{$data->mo_employer ?? old("mo_employer")}}"><span id='mo_employer_error'></span></label>
        @if ($type==1)
        <fieldset class="radio">
          <legend>VSM Alumna?</legend>
          <label><input type="radio" name="alumna" value="yes"
              {{(isset($data->alumna) && $data->alumna=='yes'? 'checked' :(old("alumna") == "yes" ? 'checked' : ''))}}>Yes</label>
          <label><input type="radio" name="alumna" value="no"
              {{(isset($data->alumna) && $data->alumna=='no'? 'checked' :(old("alumna") == "no" ? 'checked' : ''))}}>No</label>
              <span id='alumna_error'></span>
        </fieldset>
        <label><input type="text" id="alumna_input" disabled placeholder="If yes, graduating year and last name, if different" name="alumna_input" value="{{$data->alumna_input ?? old("alumna_input")}}"><span id='alumna_input_error'></span></label>
        @endif
      </div>
    </div>

    <div class="row">
      <fieldset class="radio stretch" name="marriage_status">
        <legend>Student's Parents are:</legend>
        <label><input type="radio" name="marriage_status" value="Married"
            {{(isset($data->marriage_status) && $data->marriage_status=='Married'? 'checked' :(old("marriage_status") == "Married" ? 'checked' : ''))}}>Married</label>
        <label><input type="radio" name="marriage_status" value="Separated"
            {{(isset($data->marriage_status) && $data->marriage_status=='Separated'? 'checked' :(old("marriage_status") == "Separated" ? 'checked' : ''))}}>Separated</label>
        <label><input type="radio" name="marriage_status" value="Divorced"
            {{(isset($data->marriage_status) && $data->marriage_status=='Divorced'? 'checked' :(old("marriage_status") == "Divorced" ? 'checked' : ''))}}>Divorced</label>
        <label><input type="radio" name="marriage_status" value="Single"
            {{(isset($data->marriage_status) && $data->marriage_status=='Single'? 'checked' :(old("marriage_status") == "Single" ? 'checked' : ''))}}>Single</label>
        <label><input type="radio" name="marriage_status" value="Widowed"
            {{(isset($data->marriage_status) && $data->marriage_status=='Widowed'? 'checked' :(old("marriage_status") == "Widowed" ? 'checked' : ''))}}>Widowed</label>
        <label><input type="radio" name="marriage_status" value="Common-Law"
            {{(isset($data->marriage_status) && $data->marriage_status=='Common-Law'? 'checked' :(old("marriage_status") == "Common-Law" ? 'checked' : ''))}}>Common-Law</label>
      </fieldset>
      <span id="marriage_status_error"></span>
    </div>

    <div class="row">
      <fieldset class="radio stretch" name="custody-status">
        <legend>Student Lives with:</legend>
        <label><input type="radio" name="custody_status" value="Both Parents"
            {{(isset($data->custody_status) && $data->custody_status=='Both Parents'? 'checked' : (old("custody_status") == "Both Parents" ? 'checked' : ''))}}>Both
          Parents</label>
        <label><input type="radio" name="custody_status" value="Father"
            {{(isset($data->custody_status) && $data->custody_status=='Father'? 'checked' : (old("custody_status") == "Father" ? 'checked' : ''))}}>Father</label>
        <label><input type="radio" name="custody_status" value="Mother"
            {{(isset($data->custody_status) && $data->custody_status=='Mother'? 'checked' : (old("custody_status") == "Mother" ? 'checked' : ''))}}>Mother</label>
        <label><input type="radio" name="custody_status" value="Custodian"
            {{(isset($data->custody_status) && $data->custody_status=='Custodian'? 'checked' : (old("custody_status") == "Custodian" ? 'checked' : ''))}}>Custodian</label>
        <label><input type="radio" name="custody_status" value="Guardian"
            {{(isset($data->custody_status) && $data->custody_status=='Guardian'? 'checked' : (old("custody_status") == "Guardian" ? 'checked' : ''))}}>Guardian</label>
        <label><input type="radio" name="custody_status" value="Other"
            {{(isset($data->custody_status) && $data->custody_status=='Other'? 'checked' : (old("custody_status") == "Other" ? 'checked' : ''))}}>Other</label>
        <label><input type="text" disabled name="student_lives_with" id="student_lives_with"
            value="{{$data->student_lives_with ?? ''}}"></label>
            <span id='student_lives_with_error'></span>
        <!-- selecting 'other' option enables this field -->
      </fieldset>
    </div>
    <p>Note: If a legal custody agreement exists, it must be uploaded with the submitted application.</p>
  </section>