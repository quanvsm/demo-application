<header>
    <h1>New Application - Review</h1>
  </header>
  <section>
    <h2>Student Information
      @if(isset($application->status))
      @if ($application->status === 'Pending')
      <a class="button add" onclick="showTab(1)">edit this section</a>
      @endif
      @endif
    </h2>
    <div class="row">
      <p>Application Type:<span>{{$type == 1 ? "Canadian" : "International"}}</span></p>
      @if ($type==1)

      <p>Grade: <span> {{ $data->grade_in ?? ( $data->grade??'')}}</span></p>
      @else
      <p>Grade: <span>{{$data->int_grade ?? ''}}</span></p>
      @endif
      
    </div>
    {{-- <h3>Name</h3> --}}
    <div class="row">
      <p>Legal Last:<span>{{$data->stu_lname ?? ''}}</span></p>
      <p>Legal First:<span>{{$data->stu_fname ?? ''}}</span></p>
      <p>Legal Middle:<span>{{$data->stu_mname ?? ''}}</span></p>
      <p>Commonly Goes by:<span>{{$data->stu_cname ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Date of Birth:<span>{{($data->stu_month_of_birth ??'').'/'.($data->stu_date_of_birth??'').'/'.($data->stu_year_of_birth ?? '')}}</span></p>
      <p>Gender:<span>{{$data->stu_gender ?? ''}}</span></p>
    </div>
    {{-- <h3>Address</h3> --}}
    <div class="row">
      <p>Street Address:<span>{{$data->stu_street ?? ''}}</span></p>
      <p>City:<span>{{$data->stu_city ?? ''}}</span></p>
      <p>Province / Territory:<span>{{$data->stu_province ?? ''}}</span></p>
      @if ($type==2)
      <p>Country:<span>{{$data->stu_country ?? ''}}</span></p>
      @endif
      <p>Postal Code:<span>{{$data->stu_postal ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Primary Phone:<span>{{$data->stu_phone ?? ''}}</span></p>
      <p>Primary Family Email:<span>{{$data->stu_email ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Citizenship / Immigration Status:<span>{{$data->stu_citizenship_status ?? ''}}</span></p>
      <p>Language Spoken at Home:<span>{{$data->stu_lang ?? ''}}</span></p>
      <p>Country of Birth:<span>{{$data->stu_country_of_birth ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Current School:<span>{{$data->stu_curr_school ?? ''}}</span></p>
      <p>City and Country:<span>{{$data->stu_city_country ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Medical Conditions or Allergies:<span>{{$data->allegies ?? ''}}</span></p>
      <p>If yes, please explain:<span>{{$data->stu_allegies_details ?? ''}}</span></p>
    </div>
    @if(isset($data->stu_support))
    <div class="row">
      <p>Has Student Received any of the Following:
        @foreach ($data->stu_support as $item)
        <span>{{$item}}</span>
        @endforeach
      </p>
    </div>
    <div class="row">
      <p>Please provide details of support provided:</p>
      <p class="textarea">{{$data->stu_support_details ?? ''}}</p>
    </div>
    @endif
  </section>

  <section>
    <h2>Parent / Guardian Information
      @if(isset($application->status))
      @if ($application->status === 'Pending')
      <a class="button add" onclick="showTab(2)">edit this section</a>
      @endif
      @endif
    </h2>
    {{-- <h3>Name</h3> --}}
    <div class="row">
      <p>Relationship to Student:<span>{{$data->father ?? ''}}</span></p>
      <p>Last Name:<span>{{$data->pa_lname ?? ''}}</span></p>
      <p>First Name:<span>{{$data->pa_fname ?? ''}}</span></p>
    </div>
    {{-- <h3>Address</h3> --}}
    @if ($data->pa_addr_check_box ??'' == 'on')
    <div class="row">
      <p>Street Address:<span>{{$data->stu_street ?? ""}}</span></p>
      <p>City:<span>{{$data->stu_city ??""}}</span></p>
      <p>Province / State:<span>{{$data->stu_province ??""}}</span></p>
      <p>Postal Code / Zip:<span>{{$data->stu_postal ??""}}</span></p>
    </div>
    @else
    <div class="row">
      <p>Street Address:<span>{{$data->pa_street ?? ''}}</span></p>
      <p>City:<span>{{$data->pa_city ??""}}</span></p>
      <p>Province / State:<span>{{$data->pa_province ??""}}</span></p>
      <p>Postal Code / Zip:<span>{{$data->pa_postal ??""}}</span></p>
    </div>
    @endif
 

    {{-- <h3>Contact</h3> --}}
    <div class="row">
      <p>Primary Phone:<span>{{$data->pa_hphone ?? ''}}</span></p>
      <p>Mobile Phone:<span>{{$data->pa_mphone ?? ''}}</span></p>
      <p>Email Address:<span>{{$data->pa_email ?? ''}}</span></p>
    </div>
    {{-- <h3>Employment</h3> --}}
    <div class="row">
      <p>Occupation:<span>{{$data->pa_occupation ?? ''}}</span></p>
      <p>Employer:<span>{{$data->pa_employer ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Alumnus:<span>{{$data->alumnus ?? ''}} - {{$data->alumnus_input ?? ''}}</span></p>
    </div>
    <hr>
    {{-- <h3>Name</h3> --}}
    <div class="row">
      <p>Relationship to Student:<span>{{$data->mother ?? ''}}</span></p>
      <p>Last Name:<span>{{$data->mo_lname ?? ''}}</span></p>
      <p>First Name:<span>{{$data->mo_fname ?? ''}}</span></p>
    </div>
    {{-- <h3>Address</h3> --}}
    @if ($data->mo_addr_check_box ??'' == 'on')
    <div class="row">
      <p>Street Address:<span>{{$data->stu_street ?? ""}}</span></p>
      <p>City:<span>{{$data->stu_city ??""}}</span></p>
      <p>Province / State:<span>{{$data->stu_province ??""}}</span></p>
      <p>Postal Code / Zip:<span>{{$data->stu_postal ??""}}</span></p>
    </div>
    @else
    <div class="row">
      <p>Street Address:<span>{{$data->mo_street ?? ''}}</span></p>
      <p>City:<span>{{$data->mo_city ??""}}</span></p>
      <p>Province / State:<span>{{$data->mo_province ??""}}</span></p>
      <p>Postal Code / Zip:<span>{{$data->mo_postal ??""}}</span></p>
    </div>
    @endif
    {{-- <h3>Contact</h3> --}}
    <div class="row">
      <p>Primary Phone:<span>{{$data->mo_hphone ?? ''}}</span></p>
      <p>Mobile Phone:<span>{{$data->mo_mphone ?? ''}}</span></p>
      <p>Email Address:<span>{{$data->mo_email ?? ''}}</span></p>
    </div>
    {{-- <h3>Employment</h3> --}}
    <div class="row">
      <p>Occupation:<span>{{$data->mo_occupation ?? ''}}</span></p>
      <p>Employer:<span>{{$data->mo_employer ?? ''}}</span></p>
    </div>
    <div class="row">
      <p>Alumna:<span>{{$data->alumna ?? ''}} - {{$data->alumna_input ?? ''}}</span></p>
    </div>
    <hr>
    <div class="row">
      <p>Student's Parents are:<span>{{$data->marriage_status ?? ''}}</span></p>
      <p>Student Lives with:<span>{{$data->custody_status ?? ''}}</span></p>
    </div>
  </section>

  <section>
    <h2>Additional Information
      @if(isset($application->status))
      @if ($application->status === 'Pending')
      <a class="button add" onclick="showTab(3)">edit this section</a>
      @endif
      @endif
    </h2>
    @if(isset($data->sibling_fname) && isset($data->sibling_lname) &&  isset($data->sibling_birthday) && isset($data->sibling_school) )
    {{-- <h3>Sibling Information</h3> --}}
    @for ($i = 0; $i < count($data->sibling_fname); $i++)
      <div class="row">
        <p>First Name:<span>{{$data->sibling_fname[$i]}}</span></p>
        <p>Last Name:<span>{{$data->sibling_lname[$i]}}</span></p>
        <p>Date of Birth:<span>{{$data->sibling_birthday[$i]}}</span></p>
        <p>School:<span>{{$data->sibling_school[$i]}}</span></p>
      </div>
      @endfor
      @endif
      {{-- <h3>Church</h3> --}}
      {{-- <div class="row">
        <p>Family Church:<span>{{$data->church ?? ''}}</span></p>
        <p>Name of Pastor/Reverend/Priest:<span>{{$data->pastor_name ?? ''}}</span></p>
      </div> --}}
      @if($type==1)
      {{-- <h3>Reference</h3> --}}
      <div class="row">
        <p>Name:<span>{{$data->ref_name ?? ''}}</span></p>
        <p>Phone:<span>{{$data->ref_phone ?? ''}}</span></p>
        <p>Relationship to Applicant:<span>{{$data->ref_rela ?? ''}}</span></p>
      </div>
      @endif
      @if($type==2)
      <h3>Living Arrangements</h3>
      <h4>Custodian's Name</h4>
      <div class="row">
        <p>Legal First Name:<span>{{$data->custodian_fname ?? ''}}</span></p>
        <p>Legal Middle Name:<span>{{$data->custodian_mname ?? ''}}</span></p>
        <p>Legal Last Name:<span>{{$data->custodian_lname ?? ''}}</span></p>
        <p>Commonly Goes by:<span>{{$data->custodian_cname ?? ''}}</span></p>
      </div>
      <div class="row">
        <p>Date of Birth:<span>{{$data->custodian_birthday ?? ''}}</span></p>
        <p>Relationship to Applicant:<span>{{$data->custodian_rela ?? ''}}</span></p>
      </div>
      <h4>Winnipeg Address</h4>
      <div class="row">
        <p>Street
          Address:<span>{{$data->custodian_street ?? ''}}</span>
        </p>
        <p>Postal Code:<span>{{$data->custodian_postal ?? ''}}</span></p>
      </div>
      <div class="row">
        <p>Home Phone:<span>{{$data->custodian_phone ?? ''}}</span></p>
        <p>Alternate Phone:<span>{{$data->custodian_alt_phone ?? ''}}</span></p>
        <p>Email Address:<span>{{$data->custodian_email ?? ''}}</span></p>
      </div>
      @endif
      <h4>How did you hear about Us</h4>
      @if(isset($data->heard_from))
      <div class="row">
        <p>
          @foreach ($data->heard_from as $item)
          <span>{{$item}}</span>
          @endforeach
        </p>
        <p>{{$data->heard_from_other ?? ''}}</p>
      </div>
      @endif
  </section>

  <section>
    <h2>Supporting Material
      @if(isset($application->status))
      @if ($application->status === 'Pending')
      <a class="button add" onclick="showTab(4)">edit this section</a>
      @endif
      @endif
     </h2>
    {{-- <h3>Documents Uploaded</h3> --}}
    @if($type==1)
    <ul>
      @foreach ($file_names as $item => $val)
      <li> <a class="support_file" href='{{route('user.download',['id'=>$application->id, 'field_name'=>$item])}}'>{{$val ?? ''}}</a></li>
      @endforeach
    </ul>
    @endif
    @if($type==2)
    <ul>
      @foreach ($file_names as $item => $val)
      <li><a class="support_file" href='{{route('user.download',['id'=>$application->id, 'field_name'=>$item])}}'>{{$val ?? ''}}</a></li>
      @endforeach
    </ul>
    @endif
  </section>