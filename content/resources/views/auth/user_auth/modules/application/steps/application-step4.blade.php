<section>
    <h2>Additional Information</h2>
    <h3 id="after-sibling">Sibling Information</h3>
    @if(isset($data->sibling_fname) && isset($data->sibling_lname) &&  isset($data->sibling_birthday) && isset($data->sibling_school) )
    @for ($i = 0; $i < count($data->sibling_fname); $i++)
      <div class="row sibling">
        <fieldset class="sibling_info">
          <label>Last Name<input type="text" name="sibling_lname[]" value="{{$data->sibling_lname[$i] ?? ''}}"></label>
          <label>First Name<input type="text" name="sibling_fname[]" value="{{$data->sibling_fname[$i]??''}}"></label>
          <label>Date of Birth<input type="text" maxlength="10" class="birthday_mask" placeholder="MM/DD/YYYY" name="sibling_birthday[]"
              value="{{$data->sibling_birthday[$i]}}"></label>
          <label>School<input type="text" name="sibling_school[]" value="{{$data->sibling_school[$i]}}"></label>
          <span class="remove remove_sibling"></span>
        </fieldset>
      </div>
      @endfor
      @endif
      <div>
        <span class="button add" id="add_sibling">Add</span>
      </div>
      @if($type == 1)
      <h3 id="canadian-ref">Reference</h3><!-- Canadian application only -->
      <p id="canadian-ref-p">Unrelated person who knows student well.</p>
      <div class="row" id="canadian-ref-row">
        <label>Name<input required name="ref_name" type="text" value="{{$data->ref_name ?? old("ref_name")}}"><span id="ref_name_error"></span></label>
        <label>Phone<input required name="ref_phone" maxlength="12" type="text" class="phone_number_format" value="{{$data->ref_phone ?? old("ref_phone")}}"><span id="ref_phone_error"></span></label>
        <label>Relationship to Applicant<input required name="ref_rela" type="text"
            value="{{$data->ref_rela ?? old("ref_rela")}}"><span id="ref_rela_error"></span></label>
      </div>
      @endif
      @if($type == 2)
      <h3 id="international-living-label">Living Arrangements</h3><!-- International application only -->
      <div class="row" id="international-living">
        <fieldset id="international">
          <legend>Custodian's Name</legend>
          <label>Legal Last Name<input required name="custodian_lname" type="text"
              value="{{$data->custodian_lname ?? old("custodian_lname")}}"><span id="custodian_lname_error"></span></label>
              <label>Legal First Name<input required name="custodian_fname" type="text"
                value="{{$data->custodian_fname ?? old("custodian_fname")}}"><span id="custodian_fname_error"></span></label>
          <label>Legal Middle Name<input name="custodian_mname" type="text"
              value="{{$data->custodian_mname ?? old("custodian_mname")}}"></label>
          <label>Commonly Goes By<input name="custodian_cname" type="text"
              value="{{$data->custodian_cname ?? old("custodian_cname")}}"></label>
        </fieldset>
      </div>
      <div class="row">
        <label>Date of Birth<input required name="custodian_birthday" type="text" class="birthday_mask" maxlength="10" placeholder="MM/DD/YYYY"
            value="{{$data->custodian_birthday ?? old("custodian_birthday")}}"><span id="custodian_birthday_error"></span></label>
        <label>Relationship to Applicant<input required name="custodian_rela" type="text"
            value="{{$data->custodian_rela ?? old("custodian_rela")}}"><span id ="custodian_rela_error"></span></label>
      </div>
      <div class="row">
        <fieldset>
          <legend>Winnipeg Address</legend>
          <label>Street Address<input required name="custodian_street" type="text"
              value="{{$data->custodian_street ?? old("custodian_street")}}"><span id="custodian_street_error"></span></label>
          <label>City<input required  name="custodian_city" type="text" value="{{$data->custodian_city ?? (old("custodian_city")??  'Winnipeg')}}"><span id="custodian_city_error"></span></label>
          <label>Province<input required type="text" name="custodian_province"
              value="{{$data->custodian_province ?? (old("custodian_province")  ?? 'Manitoba')}}"><span id="custodian_province_error"></span></label>
          <label>Country<input required type="text" name="custodian_country"
              value="{{$data->custodian_country ?? (old("custodian_country") ?? 'Canada')}}"><span id="custodian_country_error"></span></label>
          <label id="international-grade">Postal Code<input required name="custodian_postal" type="text"
              value="{{$data->custodian_postal ?? old("custodian_postal")}}"><span id="custodian_postal_error"></span></label>
        </fieldset>
      </div>
      <div class="row">
        <label>Primary Phone<input required type="text" class="phone_number_format" maxlength="12" name="custodian_phone" value="{{$data->custodian_phone ?? old("custodian_phone")}}"><span id="custodian_phone_error"></span></label>
        <label>Alternate Phone<input type="text" class="phone_number_format" maxlength="12" name="custodian_alt_phone"
            value="{{$data->custodian_alt_phone ?? old("custodian_alt_phone")}}"></label>
        <label>Email Address<input required type="email" name="custodian_email"
            value="{{$data->custodian_email ?? old("custodian_email")}}"><span id="custodian_email_error"></span></label>
      </div>
      @endif

      <h3>Where did you hear about Us?</h3>
      <div class="row">
        <fieldset class="checkbox heard_from_field">
          <legend>Check all that apply</legend>
          <label><input name="heard_from[]" type="checkbox"
              {{ (isset($data->heard_from)  && in_array('Website', $data->heard_from)) ? 'checked' : (is_array(old('heard_from')) && in_array('Website', old("heard_from")) ? ' checked' : '') }}
              value="Website">Website</label>

            <label><input name="heard_from[]" type="checkbox"
              {{ (isset($data->heard_from)  && in_array('Social Media', $data->heard_from)) ? 'checked' : (is_array(old('heard_from')) && in_array('Social Media', old("heard_from")) ? ' checked' : '') }}
              value="Social Media">Social Media</label>

            <label><input name="heard_from[]" type="checkbox"
              {{ (isset($data->heard_from)  && in_array('Radio', $data->heard_from)) ? 'checked' : (is_array(old('heard_from')) && in_array('Radio', old("heard_from")) ? ' checked' : '') }}
              value="Radio">Radio</label>

            <label><input name="heard_from[]" type="checkbox"
              {{ (isset($data->heard_from)  && in_array('Word Of Mouth', $data->heard_from)) ? 'checked' : (is_array(old('heard_from')) && in_array('Word Of Mouth', old("heard_from")) ? ' checked' : '') }}
              value="Word Of Mouth">Word Of Mouth</label>
          <label><input name="heard_from[]" type="checkbox"
              {{ (isset($data->heard_from)  && in_array('Other', $data->heard_from)) ? 'checked' : (is_array(old('heard_from')) && in_array('Other', old("heard_from")) ? ' checked' : '') }}
              value="Other">Other</label>
              <label><input type="text" disabled name="heard_from_other" id="heard_from_other"
                value="{{$data->heard_from_other ?? old("heard_from_other")}}"></label>
                <span id='heard_from_other_error'></span>
        </fieldset>
        <span id='heard_from_error'></span>
      </div>
  </section>