<section>
    <p>Please upload the supporting material listed below. You can upload these file types: .doc, .docx, .pdf, .png, .jpg and .jpeg. </p>
      <p>Once you have finished uploading the supporting material,
        please click the "Continue to Review" arrow. Advancing to the Review page may take a few
        minutes.</p>
    <table id="upload-table">
      <thead>
        <tr>
          <th>Document</th>
          <th>Filename</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <tr id="iep">
          <!-- visible if option selected on step 2 -->
          <th>IEP / AEP</th>
          <td id="iep-show">{{$file_names['iep'] ?? ''}}</td>
          <td><input type="file" id="iep" name="file_uploads[iep]"></td>
        </tr>
        <!-- Canadian Application -->
        @if($type == 1)
        <tr id="canadian-photo">
          <th>Most Recent Photo of Student</th>
          <td id="photo-show">{{$file_names['photo'] ?? ''}}<span id='photo_error'></span></td>
          <td><input type="file" id="photo" name="file_uploads[photo]"></td>
        </tr>
        <tr id="canadian-pass">
          <th>Birth Certificate or Passport</th>
          <td id="pass-show">{{$file_names['pass'] ?? ''}}<span id='passport_error'></span></td>
          <td><input type="file" id="pass" name="file_uploads[pass]"></td>
        </tr>
        <tr id="canadian-card">
          <th>Most Recent Report Card (If Applicable)</th>
          <td id="report-show">{{$file_names['report'] ?? ''}}</td>
          <td><input type="file" id="report" name="file_uploads[report]"></td>
        </tr>
        <tr id="canadian-trans">
          <!-- display only if relevant to the grade selected -->
          <th>Most Recent High School Transcript (If Applicable)</th>
          <td id="high-trans-show">{{$file_names['high_trans'] ?? ''}}</td>
          <td><input type="file" id="high-trans" name="file_uploads[high_trans]"></td>
        </tr>

        <!-- International Application -->
        @else
        <tr id="international-photo">
          <th>Most Recent Photo of Student</th>
          <td id="in-photo-show">{{$file_names['in_photo'] ?? ''}}<span id='int_photo_error'></span></td>
          <td><input type="file" id="in-photo" name="file_uploads[in_photo]"></td>
        </tr>
        <tr id="international-pass">
          <th>Birth Certificate or Passport (Translated)</th>
          <td id="in-pass-show">{{$file_names['in_pass'] ?? ""}}<span id='int_passport_error'></span></td>
          <td><input type="file" id="in-pass" name="file_uploads[in_pass]"></td>
        </tr>
        {{-- <tr id="international-tuition">
          <th>Agreement and Acceptance of Tuition Schedule and Policies</th>
          <td id="in-tuition-show">{{$file_names['in_tuition'] ?? ''}}</td>
          <td><input type="file" id="in-tuition" name="file_uploads[in_tuition]"></td>
        </tr> --}}
        <tr id="international-letter">
          <th>One letter of recommendation from the principal, director or teacher of your current school.</th>
          <td id="in-letter-show">{{$file_names['in_letter'] ?? ''}}<span id='in_letter_error'></td>
          <td><input type="file" id="in-letter" name="file_uploads[in_letter]"></td>
        </tr>
        <tr id="international-trans-curr">
          <th>Certified Translated Copy of Transcripts or School Report Cards (Most Recent)</th>
          <td id="in-card-cur-show">{{$file_names['in_card_cur'] ?? ''}}<span id='in_recent_card_error'></td>
          <td><input type="file" id="in-card-cur" name="file_uploads[in_card_cur]"></td>
        </tr>
        <tr id="international-trans-last">
          <th>Certified Translated Copy of Transcripts or School Report Cards (Last Year)</th>
          <td id="in-card-l-show">{{$file_names['in_card_l'] ?? ''}}</td>
          <td><input type="file" id="in-card-l" name="file_uploads[in_card_l]"></td>
        </tr>
        <tr id="international-trans-prev">
          <th>Certified Translated Copy of Transcripts or School Report Cards (Previous Year)</th>
          <td id="in-card-p-show">{{$file_names['in_card_p'] ?? ''}}</td>
          <td><input type="file" id="in-card-p" name="file_uploads[in_card_p]"></td>
        </tr>
        @endif
        <tr id="legal-custody">
          <!-- visible based on option selected on step 3 -->
          <th>Legal Custody Documents (If Applicable)</th>
          <td id="custody-show">{{$file_names['custody'] ?? ''}}</td>
          <td><input type="file" id="custody" name="file_uploads[custody]"></td>
        </tr>
      </tbody>
    </table>
  </section>