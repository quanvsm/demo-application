@extends('auth.user_auth.layouts.application')
<?
  $active = 'new_applications';
?>
@section('content')
<article id="content">
  @include('auth.user_auth.inc.messages')
  @if ($type==1)
  <form method="POST" action="{!! URL::route('paypal',100) !!}" enctype="multipart/form-data">
  @else
  <form method="POST" action="{!! URL::route('paypal',200) !!}" enctype="multipart/form-data">
  @endif
 
    @csrf
    {{-- do not delete this div --}}
    <div class="tab" style="display: none;"></div>
    <div class="tab student" style="display: none;">
      <header>
        <h1>New Application - Student Information</h1>
      </header>
        {{-- include step2  --}}
      @include('auth.user_auth.modules.application.steps.application-step2')
      <footer>
        @include('auth.user_auth.inc.footer')
        <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
      </footer>
    </div>

    <div class="tab parent" style="display: none;">
      <header>
        <h1>New Application - Parent/Guardian Information</h1>
      </header>
        {{-- include step3  --}}
      @include('auth.user_auth.modules.application.steps.application-step3')
      <footer>
        @include('auth.user_auth.inc.footer')
        <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
        <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
      </footer>
    </div>

    <div class="tab addition" style="display: none;">
      <header>
        <h1>New Application - Additional Information</h1>
      </header>
      {{-- include step4  --}}
      @include('auth.user_auth.modules.application.steps.application-step4')
      <footer>
        @include('auth.user_auth.inc.footer')
        <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
        <a class="button green continue" onclick="nextPrev(1,null)">Continue to Next Step</a>
      </footer>
    </div>

    <div class="tab upload" style="display: none;">
      <header>
        <h1>New Application - Supporting Material</h1>
      </header>
      {{-- include step5  --}}
      @include('auth.user_auth.modules.application.steps.application-step5')
      <footer>
        @include('auth.user_auth.inc.footer')
        <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
        <a class="button green summary continue">Continue to Review</a>
      </footer>
    </div>

    <div class="tab application-review" id="content" style="display: none;">
      @include('auth.user_auth.modules.application.steps.summary')
    <section>
      {{-- <p>Linden Christian School provides a Christ-centred, biblically-based education. Biblical principles are taught in Bible classes to all grades and integrated across the curricula, in discussions, learning activities and in the general life of the school.  I have read the <strong><a class="color" href='{{route('user.statement.download')}}'> Doctrinal Statement of Faith</a></strong> (click on the words to see the document).  I acknowledge that Christian education is an integral aspect of the school program and realize that the articles of faith arise in the daily process of teaching. I consent to allowing my child to be taught the doctrine outlined in the Statement of Faith.</p>
      <p>The information submitted in this application is accurate to the best of my knowledge. Failure to disclose pertinent information (i.e. resource needs, medical issues, behavioural problems, past or present circumstances that may impact the overall well-being of the student and the school body) may result in the enrolment status of the student being reconsidered and/or the student being asked to leave during the academic year. I understand this policy is a means to ensure the integrity of the teaching environment and to ensure that VSM can plan for and provide adequate resource staffing for the entire student body.</p>
      <p>My signature below affirms my acknowledgement and agreement with the above.</p> --}}
      <div class="row">
        @if (isset($application->status))
        @if ($application->status ==="Pending")
        @if (isset($data->father))
        <div class="column">
          <label>Father/Guardian Signature: <input required type="text" id="pa_sig" name="pa_sig" value="{{$data->pa_sig ?? ''}}"><span id='pa_sig_error'></span></label>
          <label>Date<input required type="text" id="pa_sig_date" name="pa_sig_date" value="{{($data->pa_sig_date ?? '')}}"><span id='pa_sig_date_error'></span></label>
        </div>
        @endif

        @if (isset($data->mother))
        <div class="column">
          <label>Mother/Guardian Signature: <input required type="text" id="mo_sig" name="mo_sig" value="{{$data->mo_sig ?? ''}}"><span id='mo_sig_error'></span></label>
          <label>Date<input required type="text" id="mo_sig_date" name="mo_sig_date" value="{{$data->mo_sig_date ?? ''}}"><span id='mo_sig_date_error'></span></label>
        </div>
        @endif
        @else
        @if (isset($data->father))
        <div class="column">
          <label>Father/Guardian Signature: <input readonly type="text" id="pa_sig" name="pa_sig" value="{{ ($data->pa_sig ?? '')}}"><span id='pa_sig_error'></span></label>
          <label>Date<input readonly type="text" id="pa_sig_date" name="pa_sig_date" value="{{($data->pa_sig_date ?? '')}}"><span id='pa_sig_date_error'></span></label>
        </div>
        @endif
        @if (isset($data->mother))
        <div class="column">
          <label>Mother/Guardian Signature: <input readonly type="text" id="mo_sig" name="mo_sig" value="{{($data->mo_sig ?? '')}}"><span id='mo_sig_error'></span></label>
          <label>Date<input readonly type="text" id="mo_sig_date" name="mo_sig_date" value="{{ ($data->mo_sig_date ?? '')}}"><span id='mo_sig_date_error'></span></label>
        </div>
        @endif
        @endif
        {{-- extra safe.... --}}
        @else
        <div class="column">
          <label>Father/Guardian Signature: <input required type="text" id="pa_sig" name="pa_sig" value="{{$data->pa_sig ?? ''}}"><span id='pa_sig_error'></span></label>
          <label>Date<input required type="text" id="pa_sig_date" name="pa_sig_date" value="{{$data->pa_sig_date ?? ''}}"><span id='pa_sig_date_error'></span></label>
        </div>
        <div class="column">
          <label>Mother/Guardian Signature: <input required type="text" id="mo_sig" name="mo_sig" value="{{$data->pa_sig ?? ''}}"><span id='mo_sig_error'></span></label>
          <label>Date<input required type="text" id="mo_sig_date" name="mo_sig_date" value="{{$data->mo_sig_date ?? ''}}"><span id='mo_sig_date_error'></span></label>
        </div>
        @endif
    </div>

    </section>
    @if(isset($application->status))
      @if ($application->status === 'Pending')
        <footer>
          <a class="button sfl save">Save for Later</a>
          <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
          <a class="button green continue" onclick="nextPrev(1,null)">Continue to Payment</a>
        </footer>
      @else
      <footer>
        <a class="button red exit back">Exit</a>
        @if (isset($application->id))
        <a class="button green print" href="{{route("user.print",$application->id)}}" target="_blank">Print</a>
        @endif
      </footer>
      @endif
    @endif

    </div>
    @if (isset($paid))
    @if ($paid != 1)  
    <div class="tab visible" style="display: none;">
      <header>
        <h1>New Application - Payment and Submission</h1>
      </header>

      <section>
        <p>*The non-refundable application fee of {{$type == 1 ? "$100.00" : "$200.00"}} must accompany your submission.</p>
        <table class="payment-invoice">
          <thead>
            <tr>
              <th>Item</th>
              <th>Student</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$type == 1 ? "Canadian" : "International"}} Application</td>
              <td>{{($data->stu_fname ??'').' '.($data->stu_lname??'')}}</td>
              <td>{{$type == 1 ? "$100.00" : "$200.00"}}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td><strong>{{$type == 1 ? "$100.00" : "$200.00"}}</strong></td>
            </tr>
          </tbody>
        </table>
        {{-- temporary inline style --}}
        <button type="submit" class="button paypal">Pay </button>
      </section>
      <footer>
        <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
        <a class="button sfl save">Save for Later</a>
      </footer>
    </div>
    @else
    <div class="tab visible" style="display: none;">
      <header>
        <h1>New Application - Payment and Submit</h1>
      </header>

      <section>
        <p>*The non-refundable application fee of {{$type == 1 ? "$100.00" : "$200.00"}} must accompany your submission.</p>
        <table class="payment-invoice">
          <thead>
            <tr>
              <th>Item</th>
              <th>Student</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$type == 1 ? "Canadian" : "International"}} Application</td>
              <td>{{($data->stu_fname ??'').' '.($data->stu_lname??'')}}</td>
              <td>{{$type == 1 ? "$100.00" : "$200.00"}}</td>
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td><strong>{{$type == 1 ? "$100.00" : "$200.00"}}</strong></td>
            </tr>
          </tbody>
        </table>
        <div class="payment-message">
          <p>Payment Completed</p>
        </div>
      </section>

      <footer>
        <a class="button yellow back" onclick="nextPrev(-1,null)">Previous</a>
        <a class="button sfl save">Save for Later</a>
        <button type="submit" class="button green submit">Submit Application</button>
      </footer>
    </div>
    @endif
    @endif
  </form>
</article>
</main>
<script type="text/javascript">

  $( "#pa_sig_date" ).datepicker({ minDate:0,
    dateFormat: 'mm/dd/yy'});

    $( "#mo_sig_date" ).datepicker({ minDate:0,
    dateFormat: 'mm/dd/yy'});

    $( ".birthday_mask" ).datepicker({
    dateFormat: 'mm/dd/yy'});
</script>
<script>
  $(document).ready(function() {
    $(".remove_sibling").click(function() {
    var par = $(this).parent();
        par.parent().remove();
    });

        $("#add_sibling").click(function() {
            var field = '<div class="row sibling"><fieldset> <label>Last Name<input type="text" name="sibling_lname[]"></label><label>First Name<input type="text" name="sibling_fname[]"></label></label><label>Date of Birth<input type="text" placeholder="MM/DD/YYYY" class="birthday_mask" maxlength="10" name="sibling_birthday[]"></label><label>School<input type="text" name="sibling_school[]"></label><span class="remove" id="remove_sibling"></span></fieldset></div>';
          
            $("#after-sibling").after(field);
            $( ".birthday_mask" ).datepicker({
            dateFormat: 'mm/dd/yy'});
            $("#remove_sibling").click(function() {
          var par = $(this).parent();
            par.parent().remove();
        });
        });
      });
</script>

</div>
<script>
  </script>
@endsection