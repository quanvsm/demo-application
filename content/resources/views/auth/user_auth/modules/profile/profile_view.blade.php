@extends('auth.user_auth.layouts.general')
@section('content')
<?
  $active = 'profile';
?>
<article id="content">
    <header>
        <h1>Profile</h1>
      </header>
      @include('auth.user_auth.inc.messages')
      <form  method="POST" action="{{ route('user.update.profile') }}" enctype="multipart/form-data" id="loginForm">
        @csrf
        <section>
            <label>First Name<input type="text" id="first_name" name="first_name" value="{{$user->first_name ?? '' }}" required></label>
            <label>Last Name<input type="text" id="last_name" name="last_name" value="{{$user->last_name ?? '' }}" required></label>
            <label>Email Address<input type="text" name="email" value="{{$user->email ?? '' }}" required></label>
            <label>Password<input type="password" name="password" id="password" required></label>
            <label>Confirm Password<input type="password" name="password_confirm" id ="password_confirm" required></label>
          </section>
          <footer>
            <button type="submit" class="button" id="update">Update Profile</button>
          </footer>
    </form>
    {{-- <header>
      <h1>Documents</h1>
    </header> --}}
    {{-- <section>
            <ul>
              @foreach ($documents as $item)
                <li> <a href="{{route('user.download.document',['id'=>$item->id])}}">{{$item->name ??''}}</a> </li>
             @endforeach  
          </ul>
    </section>
    <header>
      <h1>Invoices</h1>
    </header>
    <section>
        <h4>Application Invoices</h4>
            <ul>
              @foreach ($invoices as $item)
                @if (isset($item->application_id))
                  <li> <a href='{{route('user.download.invoice',['id'=>$item->id])}}'>{{$item->name ??''}}</a> </li>
                @endif 
             @endforeach  
          </ul>
          <h4>Registration Invoices</h4>
          <ul>
            @foreach ($invoices as $item)
              @if (isset($item->registration_id))
                <li> <a href='{{route('user.download.invoice',['id'=>$item->id])}}'>{{$item->name ??''}}</a> </li>
              @endif 
           @endforeach  
          </ul>
    </section> --}}
</article>
@endsection
