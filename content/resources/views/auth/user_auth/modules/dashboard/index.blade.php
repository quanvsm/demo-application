@extends('auth.user_auth.layouts.general')
@section('content')
<?
  $active = 'dashboard';
?>
<article id="content">
    <header>
        <h1>Dashboard</h1>
      </header>
      @include('auth.user_auth.inc.messages')
      <section>
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Status</th>
              <th>Last Updated</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($applications as $app)
             <?php $meta = json_decode($app->meta); ?>
            <tr>
                <input type="hidden" class="id" value="{{$app->id}}" required>
                <td class="name">{{$meta->stu_fname ?? ''}} {{$meta->stu_mname ?? ''}} {{$meta->stu_lname ?? ''}}</td>
                
                @if (isset($app->registration_id))
                @foreach($registrations as $reg)
                @if ($reg->application_id == $app->id)
                    <td class="status">{{$reg->status == "Complete" ? $reg->status : $app->status}}</td>
                @endif
              @endforeach
            @else
              <td class="status">{{$app->status}}</td>
            @endif

                <td class="status">{{$app->updated_at}}</td>
            <td class="actions">
              <a class="{{$app->status == "Pending" ? 'button continue' : 'button view'}}" href="{{route('user.form.edit',$app->id)}}">{{$app->status == "Pending" ? 'Continue Application' : 'View Application'}}</a>
            @if (isset($app->registration_id))
                @foreach($registrations as $reg)
                  @if ($app->registration_id == $reg->id)
                  <a class="{{$reg->status == "Pending" ? 'button green continue' : 'button view'}}" href="{{route('user.registration',$app->id)}}">{{$reg->status == "Pending" ? 'Continue Registration' : 'View Registration'}}</a>
                  @endif
              @endforeach
            @else
              @if ($app->status == "Accepted")
              <a class="button continue green" href="{{route('user.registration',$app->id)}}">Register</a>
              @endif
            @endif
            </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </section>
      <section>
        <h2>Invoices</h2>
        <table>
          <thead>
            <tr>
              <th>Invoice</th>
              <th>Uploaded</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
              @foreach($invoices as $invoice)
                  <tr>
                    <td>Registraion Invoice - {{$invoice->name}}</td>
                    <td>{{$invoice->created_at}}</td>
                    <td class="actions">
                      <a class="button file" href='{{route('user.download.invoice',['id'=>$invoice->id])}}'>Download Invoice</a>
                    </td>
                  </tr>
              @endforeach
          </tbody>
        </table>
      </section>
</article>
@endsection
