@extends('auth.user_auth.layouts.general')
@section('content')
<?
  $active = 'documents';
?>
<article id="content">
    <header>
        <h1>Documents</h1>
      </header>
      <section>
        <table>
          <thead>
            <tr>
              <th>Document Name</th>
              <th>Uploaded</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($documents as $item)
            <tr>
                <td>{{$item->name ??''}}</td>
                <td>{{date_format($item->created_at,'F d, Y')}}</td>
                <td class="actions">
                  <a class="button file" href="{{route('user.download.document',['id'=>$item->id])}}">Download Document</a>
                </td>
            </tr>
             @endforeach  

          </tbody>
        </table>
      </section>
</article>
@endsection
