<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="{{(isset($page->seo_description) ? $page->seo_description :'')}}" />
        <title>{{(isset($page->seo_title) ? $page->seo_title :'Vsm')}}</title>
        <link href="/css/theme/style.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    </head>
    <body class="dashboard">
             @include('auth.user_auth.inc.header')
                <main id="site-main">
                @yield('content')
            </main>
        @include('theme.inc.footer')
    </body>
</html>
