<!DOCTYPE html>
<html lang="en">
  <head>
  	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<title>VSM Demo</title>
    <link href="/css/admin/css/style.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
  </head>
  <body>
    <div id="login-wrapper">
    	@yield('content')
    </div>
  </body>
</html>
