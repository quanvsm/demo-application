<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="{{(isset($page->seo_description) ? $page->seo_description :'')}}" />
        <title>{{(isset($page->seo_title) ? $page->seo_title :'Vsm')}}</title>
        <link href="/css/theme/style.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 
    
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">  
    </head>
    <body class="dashboard">
           @include('auth.user_auth.inc.header')
                <main id="site-main">
                  @if(isset($application->status))
                     @if($application->status === 'Pending')
                 <aside id="sidebar">
                    <nav>
                    <a class="step" >Application Type</a>
                    <a class="step" onclick="showTab(1)">Student Information</a>
                    <a class="step" onclick="showTab(2)">Parent / Guardian Information</a>
                    <a class="step" onclick="showTab(3)">Additional Information</a>
                    <a class="step" onclick="showTab(4)">Upload Supporting Material</a>
                    <a class="step">Review</a>
                      <a class="step"> Payment & Submit</a>
                    </nav>
                    <div class="sidebar-extra">
                      <p>Admissions Office:<br>
                      <a href="tel:+123456789">204-123-456</a><br>
                      <a href="mailto:info@viewsource.ca">info@viewsource.ca</a></p>
                    </div>
                </aside>
                @endif
                @else
                <aside id="sidebar">
                  @if ($step!='')
                  <nav>
                    <a class="step" >Application Type</a>
                    <a class="step" onclick="showTab(1)">Student Information</a>
                    <a class="step" onclick="showTab(2)">Parent / Guardian Information</a>
                    <a class="step" onclick="showTab(3)">Additional Information</a>
                    <a class="step" onclick="showTab(4)">Upload Supporting Material</a>
                    <a class="step">Review</a>
                      <a class="step"> Payment & Submit</a>
                    </nav>
                  @else
                  <nav>
                    <a class="step" >Application Type</a>
                    <a class="step">Student Information</a>
                    <a class="step">Parent / Guardian Information</a>
                    <a class="step">Additional Information</a>
                    <a class="step">Upload Supporting Material</a>
                    <a class="step">Review</a>
                      <a class="step"> Payment & Submit</a>
                    </nav>
                  @endif
                  <div class="sidebar-extra">
                    <p>Admissions Office:<br>
                    <a href="tel:+123456789">204-123-456</a><br>
                    <a href="mailto:info@viewsource.ca">info@viewsource.ca</a></p>
                  </div>
              </aside>
                @endif
                @yield('content')
            </main>
            @include('auth.user_auth.modules.application.script.javascript-script')
            @include('auth.user_auth.modules.application.script.jquery-script')
        @include('theme.inc.footer')
    </body>
</html>
