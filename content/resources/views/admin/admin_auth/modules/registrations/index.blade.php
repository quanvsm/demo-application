@extends('admin.layouts.admin')
<?
  $class = 'index submission';
  $active = 'registration';
?>
@section('content')
<h1>Registrations</h1>
    <div class="filterBar"> <!-- for filtering index content (dynamically ordered posts) -->
      <input id="search" type="text" placeholder="search">
    </div>
      <div class="category">
        <ul>
            @foreach($registrations as $app)
            <?php $meta = json_decode($app->meta); ?>
            <?php $date = ($app->submitted_at); ?>
            <?php
            
              if($app->status)
              switch ($app->status) {
                case "Submitted":
                    $status = "pending";
                    break;
                case "Denied":
                    $status = "cancel";
                    break;
                case 'Accepted':
                    $status = "accept";
                    break;
                default: $status = ""; 
                break;
            }

            ?>
            <li class="filter {{$app->status}}">
                <div class="details {{$status}}">
                    <input type="hidden" class="id" value="{{ $app->id }}" required>
                    <span class="status {{$status}}">{{$app->status}}</span> 
                    <span class="name">{{ (isset($meta->stu_fname) ? $meta->stu_fname : '') }} {{ (isset($meta->stu_lname) ? $meta->stu_lname : '') }}</span>
                    @if ($app->type==1)
                    <span class="grade default"> {{(($meta->grade =='on' ? "Grade " .$meta->grade_in ?? '' : $meta->grade ??'') )}}</span>
                   @else
                    <span class="grade default">{{"Grade " .$meta->int_grade ?? ''}}</span>
                   @endif
                    <span class="email">{{ (isset($meta->stu_email) ? $meta->stu_email : '') }}</span>
                    <span class="type status default">{{ (isset($app->type) ? ($app->type ==1 ?  'Canadian' : "International ") : "") }}</span>
                    <span class="date">{{ (isset($app->submitted_at) ? date("M d,Y - h:m A", strtotime($date)) : '') }}</span>
                    <span class="actions">
                      <a class="button" href="{{ route('admin.registration.view',$app->id) }}">view</a>
                      <a class="button cancel registration-delete">delete</a>
                    </span>
                  </div>
            </li>
        @endforeach      
        </ul>
      </div>
@endsection

@section('inspector')
<div id="inspector">
  <a class="button download_all" href="{{route('admin.registration.export.all','all')}}" >export all registrations</a>
</div>

<script type="text/javascript">

  $( "#datepicker" ).datepicker({ 
    dateFormat: 'yy-mm-dd'});

</script>

<script>
  var IDs = [];
  $(document).ready(function(){
    $("#search").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $(".category li").filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
            IDs = []
          $(".filter").each(function() {
              if($(this).css("display") !="none"){
                IDs.push($(this).find(".id").val());
              }
              });
          var route = '{{route("admin.registration.export.all",["id"=>":IDs"])}}';
          route = route.replace(':IDs', IDs);
          $('.download_all').attr("href",route );
    });
  });

  $( ".registration-delete" ).click(function() {
            var registration_id = $(this).closest('.details').find(".id").val();
            showModal('Are You Sure You want to Delete this registration?');

            $( "#modal-accept" ).click(function() {
              $.ajax({
                      url: "{{route('admin.registration.delete')}}",
                      method: 'POST',
                      data: { 'id' : registration_id, '_token' : '{{csrf_token()}}' },
                      success: function(result){
                          $( "#modal" ).hide();
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
            });
        });
  </script>
  
@endsection
