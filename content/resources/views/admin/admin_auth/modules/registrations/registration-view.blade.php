@extends('admin.layouts.admin')
@section('content')
<?php
            
if($registration->status){
  switch ($registration->status) {
  case "Submitted":
      $status = "pending";
      break;
  case "Denied":
      $status = "cancel";
      break;
  case 'Accepted':
      $status = "accept";
      break;
  default: $status = ""; 
  break;
}
}

?>
<div id="main" class="submission">
    @include('auth.user_auth.modules.registration.steps.summary')
    <hr>
    <section>
      <h2>Payment Information</h2>
      @switch($payment->method??'')
      @case('cash')
      <p>Payment Method: Cash</p>
          @break
      @case('cheque')
      <p>Payment Method: Cheque</p>
          @break
      @default
      <p>Payment Method: Paypal</p>
      <p>Transaction Id: {{$payment->transaction_id ??''}}</p>
      <p>Amount: ${{$payment->amount??''}} {{$payment->currency??''}}</p>
      <p>Payer Email: {{$payment->payer_email??''}}</p>
  @endswitch
      <p>Date: {{$payment->created_at ??''}} CST</p>

    </section>
</div>
@endsection
  

@section('inspector')
<div id="inspector">
  <a class="button" href="{{ route('admin.registrations') }}">go back</a>
  <a class="button" href="{{ route('admin.registration.edit',$registration->id) }}">edit</a>
  <a class="button" href="{{ route('admin.registration.print',$registration->id) }}" target="_blank">print</a>
  <a class="button"  href="{{route('admin.registration.export',['id'=>$registration->id,'type'=>$registration->type])}}">export registration</a>
  <a class="button" href="{{ route('admin.application.view',$registration->application_id) }}">view application</a>
  <a class="button cancel registration-delete">delete</a>
  <hr>
  <a class="button" href="{{ route('admin.users.edit',$registration->user_id) }}">upload invoice</a>
</div>


<script type="text/javascript">
  $( "#datepicker" ).datepicker({ 
    dateFormat: 'yy-mm-dd'});
</script>

<script>
$(document).ready(function(){
    $("footer").hide();
})

$( ".registration-delete" ).click(function() {
            var registration_id = '{{$registration->id}}';
            showModal('Are You Sure You want to Delete this registration?');

            $( "#modal-accept" ).click(function() {
              $.ajax({
                      url: "{{route('admin.registration.delete')}}",
                      method: 'POST',
                      data: { 'id' : registration_id, '_token' : '{{csrf_token()}}' },
                      success: function(result){
                          $( "#modal" ).hide();
                          window.location.href = "/admin/registration"
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
            });
        });
  </script>
@endsection
