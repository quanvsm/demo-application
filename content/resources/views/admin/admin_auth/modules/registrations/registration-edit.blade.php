@extends('admin.layouts.admin')
<?
  $class = 'editor';
  $active = 'registration';
?>
@section('content')
    <h1>registration </h1> 
    <article id="content">
      {{-- @include('auth.user_auth.modules.registration.registration-form') --}}
      <form  method="POST" action="{{route('admin.application.save')}}" enctype="multipart/form-data">
        @csrf
        {{-- do not delete this div --}}
        <div class="tab step1" style="display: block;">
            @include('auth.user_auth.modules.registration.steps.student-info')
          </div>
  
          <div class="tab step2" style="display: block;">
            @include('auth.user_auth.modules.registration.steps.medical')
          </div>
  
          <div class="tab step3" style="display: block;">
            @include('auth.user_auth.modules.registration.steps.family-info')
          </div>
  
          @if ($type==1)
              <div class="tab step4" style="display: block;">
                @include('auth.user_auth.modules.registration.steps.indigenous')
              </div>
          @endif
  
          <div class="tab step5" style="display: block;">
            @include('auth.user_auth.modules.registration.steps.photo')
          </div>
  
          <div class="tab step6" style="display: block;">
            @include('auth.user_auth.modules.registration.steps.email-authorization')
          </div>
          
          @if ($type==1)
          <div class="tab step7" style="display: block;">
            @include('auth.user_auth.modules.registration.steps.acknowledgement-of-transfers')
          </div>
      @endif
        <div class="tab step8" style="display: block;">
          @include('auth.user_auth.modules.registration.steps.tuition-consent')
        </div>
  
        <div class="tab step9" style="display: block;">
          @include('auth.user_auth.modules.registration.steps.accuracy')
        </div>
    </form>
</article>   
    <script>
        $(document).ready(function () {
            $("#add_sibling").click(function () {
                var field =
                    '<div class="row"><fieldset><label>Name<input type="text" name="sibling_name[]"></label><label>Date of Birth<input type="date" placeholder="YYYY-MM-DD" name="sibling_birthday[]"></label><label>School<input type="text" name="sibling_school[]"></label><span class="remove" id="remove_sibling"></span></fieldset></div>';

                $("#after-sibling").after(field);
                $("#remove_sibling").click(function () {
                    var par = $(this).parent();
                    par.parent().remove();
                });
            });
        });
    </script>
@endsection

@section('inspector')
  <div id="inspector">
      <a class="button" href="{{ route('admin.registration.view',$registration->id) }}">go back</a>
      <button type="submit" class="button accept" >update registration</button>
      <hr>
  </div>

  <script>
         $(document).ready(function(){
            $("footer").hide();
          $('.accept').click(function(event) {
          $('form').attr("action", "{{ route('admin.registration.save') }}");  //change the form action
          $('form').submit();  // submit the form
    });
  });
  </script>
  
  @include('auth.user_auth.modules.registration.script.jquery-script')
@endsection


