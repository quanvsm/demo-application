@extends('admin.layouts.admin_login')
@section('content')
  <form method="POST" action="{{ route('admin.password.email') }}">
    @csrf
    <div class="loginContainer">
      <img class="logo" src="/css/admin/img/vsm-logo.png" alt="VSM">
      <label>Email
        <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
      </label>
      <button type="submit">Send Password Reset Link</button>
    </div>
  </form>
@endsection
