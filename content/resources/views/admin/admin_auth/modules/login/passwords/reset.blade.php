@extends('admin.layouts.admin_login')
@section('content')
  <form method="POST" action="{{ route('admin.password.request') }}">
    @csrf
    <div class="loginContainer">
      <img class="logo" src="/css/admin/img/vsm-logo.png" alt="VSM">
      <input type="hidden" name="token" value="{{ $token }}">

      <label>Email
        <input id="email" type="email" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
      </label>
      <label>Password
        <input id="password" type="password" name="password" required autocomplete="new-password">
      </label>
      <label>Confirm Password
        <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
      </label>
      <button type="submit">Reset Password</button>
    </div>
  </form>
@endsection
