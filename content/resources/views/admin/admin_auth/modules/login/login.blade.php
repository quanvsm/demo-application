@extends('admin.layouts.admin_login')
@section('content')
  <form method="POST" action="{{ route('admin.login.submit') }}">
    @csrf
    <div class="loginContainer">
      <img class="logo" src="/css/admin/img/vsm-logo.png" alt="VSM">
      <label>Email
        <input id="email" type="email" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
      </label>
      <label>Password
        <input id="password" type="password" name="password" required autocomplete="current-password">
      </label>
      <button type="submit" class="button accept" >login</button>
      @if (Route::has('admin.password.request'))
          <a href="{{ route('admin.password.request') }}">
              {{ __('Forgot Your Password?') }}
          </a>
      @endif
      <br>
        <a class="button" href="{{ route('home') }}">Back</a>
    </div>
  </form>

  @error('email')
      <div id="notification">
          <p>{{ $message }}</p>
          <script>$( "#notification" ).delay(1500).slideUp('slow');</script>
      </div>
  @enderror

  @error('password')
      <div id="notification">
          <p>{{ $message }}</p>
          <script>$( "#notification" ).delay(1500).slideUp('slow');</script>
      </div>
  @enderror

  @if(Session::has('locked'))
      <div id="notification">
          <p>{{Session::get('locked')}}</p>
          <script>$( "#notification" ).delay(3000).slideUp('slow');</script>
      </div>
  @endif
@endsection
