@extends('admin.layouts.admin_login')
@section('content')
  <form method="POST" action="{{ route('admin.create',$admin->hash) }}">
    @csrf
    <div class="loginContainer">
      <img class="logo" src="/css/admin/img/vsm-logo.png" alt="VSM">
      <input type="hidden" name="hash" value="{{ $admin->hash }}">
      <label>Confirm Email
        <input id="email" type="email" name="email" required autocomplete="email" autofocus>
      </label>
      <label>Create Password
        <input id="password" type="password" name="password" required autocomplete="new-password">
      </label>
      <label>Confirm Password
        <input id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
      </label>
      <button type="submit">Activate Account</button>
    </div>
  </form>
@endsection
