@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'settings';

  $form_action = route('admin.settings.save');
?>

@section('content')
    <h1>Edit Admin: {{$admin->first_name ?? '' }}</h1>

    <div class="section">
        <label>First Name<input type="text" id="first_name" name="first_name" value="{{$admin->first_name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Last Name<input type="text" id="last_name" name="last_name" value="{{$admin->last_name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Email<input type="text" name="email" value="{{$admin->email ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Password<input type="password" name="password"></label>
    </div>
@endsection

@section('inspector')
  <div id="inspector">
      <a class="button" href="{{route('admin.admins')}}">go back</a>
      <a id="admin-reset" class="button">reset password</a>
      <button type="submit" class="button accept" >save</button>
      <hr>

      <fieldset>
          <legend>Active</legend>
          <label><input name="active" type="radio" value="yes" {{ ($admin->active=='yes'? 'checked' :'') }} required> Yes</label>
          <label><input name="active" type="radio" value="no" {{ ($admin->active=='no'? 'checked' :'') }} required> No</label>
      </fieldset>
  </div>
  <script>
      $(document).ready(function() {
          $( "#admin-reset" ).click(function() {
              var email = '{{$admin->email}}';

              $.ajax({
                url: "{{route('admin.admins.email')}}",
                method: 'POST',
                data: { 'email' : email, '_token' : '{{csrf_token()}}' },
                success: function(result){

                    var message = "<div id='notification'>";
                        message +=     "<p> Password Rest link sent to "+email+"</p>";
                        message += "</div>";

                    $("body").append(message);
                    $( "#notification" ).delay(3000).slideUp('slow');
                }
              });
          });
      });
  </script>
@endsection
