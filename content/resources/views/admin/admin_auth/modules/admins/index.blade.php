@extends('admin.layouts.admin')
<?
  $class = 'index';
  $active = 'admin';
?>
@section('content')
    <h1>Admins</h1>
    <div class="category">
        <h2>All Admins</h2>
        <ul>
            @foreach($admins as $admin)
                <li class="account" id="{{$admin->id}}">
                    <div class="details">
                        <input type="hidden" class="id" value="{{$admin->id}}" required>
                        <span class="name">{{$admin->first_name.' '.$admin->last_name}}</span>
                        <span class="email">{{$admin->email}}</span>
                        <span class="status">
                            @if($admin->active == 'yes')
                                Active
                            @else
                                Inactive
                            @endif
                        </span>
                        <span class="actions">
                            <a class="button" href="{{ route('admin.admins.edit',$admin->id) }}">edit</a>

                            @if($admin->active=='no' && !empty($admin->hash) && empty($admin->password))
                                <a class="button admin-resend">resend activation email</a>
                            @else
                                <a class="button admin-reset">reset password</a>
                            @endif

                            <a class="button cancel admin-delete">delete</a>
                        </span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection

@section('inspector')
    <div id="inspector">
        <h2>New Admin...</h2>
        <a href="{{ route('admin.admins.new') }}" class="button">create</a>
    </div>

    <script>
      $(document).ready(function() {
          $( ".admin-delete" ).click(function() {
              var name = $(this).closest('.details').find(".name").html();
              var admin_id = $(this).closest('.details').find(".id").val();

              showModal('Are you sure you want to delete '+name+'?');

              $( "#modal-accept" ).click(function() {
                    $.ajax({
                        url: "{{route('admin.admins.delete')}}",
                        method: 'POST',
                        data: { 'id' : admin_id, '_token' : '{{csrf_token()}}' },
                        success: function(result){

                            $( "#modal" ).hide();
                            $( "#"+admin_id ).remove();

                            var message = "<div id='notification'>";
                                message +=     "<p>"+name+" was Deleted. Page will now reload.</p>";
                                message += "</div>";

                            $("body").append(message);
                            $( "#notification" ).delay(1500).slideUp('slow');
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }
                    });
              });
          });

          $( ".admin-reset" ).click(function() {
              var email = $(this).closest('.details').find(".email").html();

              $.ajax({
                url: "{{route('admin.admins.email')}}",
                method: 'POST',
                data: { 'email' : email, '_token' : '{{csrf_token()}}' },
                success: function(result){

                    var message = "<div id='notification'>";
                        message +=     "<p> Password Rest link sent to "+email+"</p>";
                        message += "</div>";

                    $("body").append(message);
                    $( "#notification" ).delay(3000).slideUp('slow');
                }
              });

          });

          $( ".admin-resend" ).click(function() {
              var id = $(this).closest('.details').find(".id").val();

              $.ajax({
                url: "{{route('admin.admins.resend')}}",
                method: 'POST',
                data: { 'id' : id, '_token' : '{{csrf_token()}}' },
                success: function(result){

                    if(result == 1){
                        var message = "<div id='notification'>";
                            message +=     "<p>Activation Email Resent.</p>";
                            message +=     "<a class='button dismiss' href='#'>dismiss</a>";
                            message += "</div>";
                    }else if(result == 2){
                        var message = "<div id='notification'>";
                            message +=     "<p>Error. Activation email was not sent.</p>";
                            message +=     "<a class='button dismiss' href='#'>dismiss</a>";
                            message += "</div>";
                    }

                    $("body").append(message);

                    $( ".dismiss" ).click(function() {
                      $( "#notification" ).delay(500).slideUp('slow');
                    });
                }
              });

          });
      });
    </script>
@endsection
