@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'admin';

  if(isset($admin->id)){
    $form_action = route('admin.admins.save');
  }else{
    $form_action = route('admin.admins.create');
  }
?>

@section('content')

    @if(isset($admin->id))
        <h1>Edit Admin: {{$admin->first_name ?? '' }}</h1>
        <input type="hidden" name="id" value="{{$admin->id}}" required>
    @else
        <h1>New Admin</h1>
    @endif

    <div class="section">
        <label>First Name<input type="text" id="first_name" name="first_name" value="{{$admin->first_name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Last Name<input type="text" id="last_name" name="last_name" value="{{$admin->last_name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Email<input type="text" name="email" value="{{$admin->email ?? '' }}" required></label>
    </div>

@endsection

@section('inspector')
  <div id="inspector">
      <a class="button" href="{{route('admin.admins')}}">go back</a>
      @if(isset($admin->id))

          @if($admin->active=='no' && !empty($admin->hash) && empty($admin->password))
              <a id="admin-resend" class="button">resend activation email</a>
          @else
              <a id="admin-reset" class="button">reset password</a>
          @endif

          <button type="submit" id="delete" formaction="{{route('admin.admins.delete_admin')}}" class="button cancel">delete admin</button>
      @endif
      <button type="submit" class="button accept" >save</button>
      <hr>

      @if(isset($admin->id) && $admin->hash == null)
          <fieldset>
              <legend>Active</legend>
              <label><input name="active" type="radio" value="yes" {{ isset($admin->active) ? ($admin->active=='yes'? 'checked' :'')  :'checked'}} required> Yes</label>
              <label><input name="active" type="radio" value="no" {{ isset($admin->active) ? ($admin->active=='no'? 'checked' :'') :'' }} required> No</label>
          </fieldset>
      @endif
  </div>
  @if(isset($admin->id))
      <script>
          $(document).ready(function() {
              $( "#delete" ).click(function() {
                  event.preventDefault();

                  var full_name = $( "#first_name" ).val()+' '+$( "#last_name" ).val();
                  showModal('Are you sure you want to delete '+full_name+'?');

                  $( "#modal-accept" ).click(function() {
                      $( "#delete" ).click();
                  });
              });

              $( "#admin-reset" ).click(function() {
                  var email = '{{$admin->email}}';

                  $.ajax({
                    url: "{{route('admin.admins.email')}}",
                    method: 'POST',
                    data: { 'email' : email, '_token' : '{{csrf_token()}}' },
                    success: function(result){

                        var message = "<div id='notification'>";
                            message +=     "<p> Password Rest link sent to "+email+"</p>";
                            message += "</div>";

                        $("body").append(message);
                        $( "#notification" ).delay(3000).slideUp('slow');
                    }
                  });

              });

              $( "#admin-resend" ).click(function() {
                  var id = '{{$admin->id}}';

                  $.ajax({
                    url: "{{route('admin.admins.resend')}}",
                    method: 'POST',
                    data: { 'id' : id, '_token' : '{{csrf_token()}}' },
                    success: function(result){

                        if(result == 1){
                            var message = "<div id='notification'>";
                                message +=     "<p>Activation Email Resent.</p>";
                                message +=     "<a class='button dismiss' href='#'>dismiss</a>";
                                message += "</div>";
                        }else if(result == 2){
                            var message = "<div id='notification'>";
                                message +=     "<p>Error. Activation email was not sent.</p>";
                                message +=     "<a class='button dismiss' href='#'>dismiss</a>";
                                message += "</div>";
                        }

                        $("body").append(message);

                        $( ".dismiss" ).click(function() {
                          $( "#notification" ).delay(500).slideUp('slow');
                        });
                    }
                  });

              });
          });
      </script>
  @endif
@endsection
