@extends('admin.layouts.admin')
<?
  $class = 'index';
  $active = 'user';
?>
@section('content')
    <h1>Users</h1>
    <div id='main' class="index category">
        <h2>All Users</h2>
        <ul>
            @foreach($users as $user)
                <li class="account" id="{{$user->id}}">
                    <div class="details">
                        <input type="hidden" class="id" value="{{$user->id}}" required>
                        <span class="name">{{$user->first_name.' '.$user->last_name}}</span>
                        <span class="email">{{$user->email}}</span>
                        <span class="status">
                            @if($user->active == 'yes')
                                Active
                            @else
                                Inactive
                            @endif
                        </span>
                        <span class="actions">
                            <a class="button" href="{{route('admin.users.edit',$user->id)}}">edit</a>

                            @if($user->active=='no' && !empty($user->hash) && empty($user->password))
                                <a class="button user-resend">resend activation email</a>
                            @else
                                <a class="button user-reset">reset password</a>
                            @endif

                            <a class="button cancel user-delete">delete</a>
                        </span>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endsection

@section('inspector')
    <div id="inspector">
        <h2>New User...</h2>
        <a href="{{route('admin.users.new')}}" class="button">create</a>
        <hr>
        {{-- <fieldset>
            <h2>Document...</h2>
            <p>Upload Document To All Users</p>
            @foreach ($documents as $item)
            <a href='{{route('admin.download.document',['id'=>$item->id])}}'>{{$item->name ??''}}</a> 
           @endforeach  
            <form method="POST" action="{{route("admin.upload.document")}}" enctype="multipart/form-data">
              @csrf
              <label>file :<span id="document-show">
              </span>
            </label>
              <label><input class="button" name="document" id="document" type="file"></label>
              <button type="submit" class="button submit">upload document</button>
            </form>
          </fieldset> --}}
    </div>

    <script>
      $(document).ready(function() {
          $( ".user-delete" ).click(function() {
              var name = $(this).closest('.details').find(".name").html();
              var user_id = $(this).closest('.details').find(".id").val();

              showModal('Are you sure you want to delete '+name+'?');

              $( "#modal-accept" ).click(function() {
                    $.ajax({
                        url: "{{route('admin.users.delete')}}",
                        method: 'POST',
                        data: { 'id' : user_id, '_token' : '{{csrf_token()}}' },
                        success: function(result){

                            $( "#modal" ).hide();
                            $( "#"+user_id ).remove();

                            var message = "<div id='notification'>";
                                message +=     "<p>"+name+" was Deleted. Page will now reload.</p>";
                                message += "</div>";

                            $("body").append(message);
                            $( "#notification" ).delay(1500).slideUp('slow');
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }
                    });
              });
          });

          $( ".user-reset" ).click(function() {
              var email = $(this).closest('.details').find(".email").html();

              $.ajax({
                url: "{{route('admin.users.email')}}",
                method: 'POST',
                data: { 'email' : email, '_token' : '{{csrf_token()}}' },
                success: function(result){

                    var message = "<div id='notification'>";
                        message +=     "<p> Password Rest link sent to "+email+"</p>";
                        message += "</div>";

                    $("body").append(message);
                    $( "#notification" ).delay(3000).slideUp('slow');
                }
              });

          });

          $( ".user-resend" ).click(function() {
              var id = $(this).closest('.details').find(".id").val();

              $.ajax({
                url: "{{route('admin.users.resend')}}",
                method: 'POST',
                data: { 'id' : id, '_token' : '{{csrf_token()}}' },
                success: function(result){

                    if(result == 1){
                        var message = "<div id='notification'>";
                            message +=     "<p>Activation Email Resent.</p>";
                            message +=     "<a class='button dismiss' href='#'>dismiss</a>";
                            message += "</div>";
                    }else if(result == 2){
                        var message = "<div id='notification'>";
                            message +=     "<p>Error. Activation email was not sent.</p>";
                            message +=     "<a class='button dismiss' href='#'>dismiss</a>";
                            message += "</div>";
                    }

                    $("body").append(message);

                    $( ".dismiss" ).click(function() {
                      $( "#notification" ).delay(500).slideUp('slow');
                    });
                }
              });

          });
      });
    </script>
@endsection
