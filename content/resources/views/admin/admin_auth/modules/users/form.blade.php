@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'user';

  if(isset($user->id)){
    $form_action = route('admin.users.save');
  }else{
    $form_action = route('admin.users.create');
  }
?>

@section('content')

    @if(isset($user->id))
        <h1>Edit: {{$user->first_name ?? '' }}</h1>
        <input type="hidden" name="id" value="{{$user->id}}" required>
    @else
        <h1>New User</h1>
    @endif

    <div class="section">
        <label>First Name<input type="text" id="first_name" name="first_name" value="{{$user->first_name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Last Name<input type="text" id="last_name" name="last_name" value="{{$user->last_name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Email<input type="text" name="email" value="{{$user->email ?? '' }}" required></label>
    </div>

@endsection

@section('inspector')
  <div id="inspector">
      <a class="button" href="{{route('admin.users')}}">go back</a>
      @if(isset($user->id))

          @if($user->active=='no' && !empty($user->hash) && empty($user->password))
              <a id="user-resend" class="button">resend activation email</a>
          @else
              <a id="user-reset" class="button">reset password</a>
          @endif

          <button type="submit" id="delete" formaction="{{route('admin.users.delete_user')}}" class="button cancel">delete user</button>
      @endif
      <button type="submit" class="button accept" >save</button>
      <hr>

      @if(isset($user->id))
          <fieldset>
              <legend>Active</legend>
              <label><input name="active" type="radio" value="yes" {{ isset($user->active) ? ($user->active=='yes'? 'checked' :'')  :'checked'}} required> Yes</label>
              <label><input name="active" type="radio" value="no" {{ isset($user->active) ? ($user->active=='no'? 'checked' :'') :'' }} required> No</label>
          </fieldset>
          <hr>
          <fieldset>
            <h2>Invoice...</h2>
            @foreach ($invoices as $item)
            <a href='{{route('admin.download.invoice',['id'=>$item->id])}}'>{{$item->name ??''}}</a> 
           @endforeach  
              <label>file :<span id="registration_invoice-show">
              </span>
            </label>
              <label><input class="button" name="registration_invoice" id="registration_invoice" type="file"></label>
              <button type="submit" formaction="{{route("admin.registration.upload.invoice",$user->id)}}" class="button submit" id="invoice-submit">upload invoice</button>
          </fieldset>
      @endif
  </div>
  @if(isset($user->id))
      <script>
          $(document).ready(function() {
              $( "#delete" ).click(function() {
                  event.preventDefault();

                  var full_name = $( "#first_name" ).val()+' '+$( "#last_name" ).val();
                  showModal('Are you sure you want to delete '+full_name+'?');

                  $( "#modal-accept" ).click(function() {
                      $( "#delete" ).click();
                  });
              });

              $( "#user-reset" ).click(function() {
                  var email = '{{$user->email}}';

                  $.ajax({
                    url: "{{route('admin.users.email')}}",
                    method: 'POST',
                    data: { 'email' : email, '_token' : '{{csrf_token()}}' },
                    success: function(result){

                        var message = "<div id='notification'>";
                            message +=     "<p> Password Rest link sent to "+email+"</p>";
                            message += "</div>";

                        $("body").append(message);
                        $( "#notification" ).delay(3000).slideUp('slow');
                    }
                  });

              });

              $( "#user-resend" ).click(function() {
                  var id = '{{$user->id}}';

                  $.ajax({
                    url: "{{route('admin.users.resend')}}",
                    method: 'POST',
                    data: { 'id' : id, '_token' : '{{csrf_token()}}' },
                    success: function(result){

                        if(result == 1){
                            var message = "<div id='notification'>";
                                message +=     "<p>Activation Email Resent.</p>";
                                message +=     "<a class='button dismiss' href='#'>dismiss</a>";
                                message += "</div>";
                        }else if(result == 2){
                            var message = "<div id='notification'>";
                                message +=     "<p>Error. Activation email was not sent.</p>";
                                message +=     "<a class='button dismiss' href='#'>dismiss</a>";
                                message += "</div>";
                        }

                        $("body").append(message);

                        $( ".dismiss" ).click(function() {
                          $( "#notification" ).delay(500).slideUp('slow');
                        });
                    }
                  });

              });
          });

        //   $("#invoice-submit").click(function(e){  
        //         e.preventDefault();
        //         $("#invoice-form").submit();
        //     }
      </script>
  @endif
@endsection
