@extends('admin.layouts.admin')
<?
  $class = 'editor';
  $active = 'application';
?>
@section('content')
    <h1>Application </h1> 
    <article id="content">
      <form  method="POST" action="{{route('admin.application.save')}}" enctype="multipart/form-data">
        @csrf
        {{-- do not delete this div --}}
        <div class="tab" style="display: none;"></div>
        
        <div class="tab student"  style="display: block;">
          @include('auth.user_auth.modules.application.steps.application-step2')
        </div>
        
        <div class="tab parent" style="display: block;"> 
          @include('auth.user_auth.modules.application.steps.application-step3')
        </div>

        <div class= "tab faith"  style="display: block;">
          @include('auth.user_auth.modules.application.steps.application-step4')
        </div>

        <div class="tab addition" style="display: block;"> 
          @include('auth.user_auth.modules.application.steps.application-step5')
        </div>

        {{-- <div class="tab upload" style="display: block;"> 
          @include('auth.user_auth.modules.application.steps.application-step6')
        </div> --}}
        <div class="tab payment" style="display: block;"> 
            <fieldset >
                <h2>Select Payment Method</h2>
                <label><input name="payment" type="radio" value="cash"> Cash</label>
                <label><input name="payment" type="radio" value="cheque"> Cheque</label>
            </fieldset>
          </div>
        <hr>
        {{-- temporary, please change style for this button --}}
        <button type="submit" class="button accept" style="height: 50px;">create application</button>
    </form>
</article>   
          </main>  
          <script>
              $(document).ready(function () {
                  $("#add_sibling").click(function () {
                      var field =
                          '<div class="row"><fieldset><label>Name<input type="text" name="sibling_name[]"></label><label>Date of Birth<input type="date" placeholder="YYYY-MM-DD" name="sibling_birthday[]"></label><label>School<input type="text" name="sibling_school[]"></label><span class="remove" id="remove_sibling"></span></fieldset></div>';

                      $("#after-sibling").after(field);
                      $("#remove_sibling").click(function () {
                          var par = $(this).parent();
                          par.parent().remove();
                      });
                  });
              });
          </script>

<script type="text/javascript">
  $('input[type=file]').change(function () {
    var id = $(this).attr('id')+"-show";
    var size = this.files[0].size;
        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|png|rtf|jpg|jpeg|PDF)$");
        if(size >31457280){
          $(this).val('');
          $('#'+id).empty().append('')
            alert('Your files have not been uploaded. The maximum upload file size is 30 MB. Please check your file sizes and try again.');
        }
        if (!(regex.test(val))) {
            $(this).val('');
            $('#'+id).empty().append('')
            alert('This file type is not accepted. Please save your file as one of the listed supported types.');
        }
        else{
          var file = $(this)[0].files[0].name;
          $('#'+id).empty().append(file);
        }
    });
</script>

<script>
  $(document).ready(function(){
      $('input').attr("autocomplete",'nope');

      if($("input[name='custody_status']:checked").val() == "Other"){
            $("#student_lives_with").prop("disabled",false);
            $("#student_lives_with").prop("required",true);
          }
          else{
            $("#student_lives_with").prop("disabled",true);
            $("#student_lives_with").prop("required",false);
          }
           
      if( $('#canadian-citizen').find('input:checkbox:checked').length >0){
        $("#stu_support_details").prop("disabled",false);
          }
          else{
        $("#stu_support_details").prop("disabled",true);
        }

    if($('.heard_from_field').find('input[value="Other"]:checked').length >0){
          $("#heard_from_other").prop("disabled",false);
        }
        else{
          $("#heard_from_other").prop("disabled",true);
        }

    $('.heard_from_field').change(function(){
      if($(this).find('input[value="Other"]:checked').length >0){
            $("#heard_from_other").prop("disabled",false);
          }
          else{
            $("#heard_from_other").prop("disabled",true);
            $("#heard_from_other").val("")
          }
    })


    //set required fields for step 3
      $("input[type='radio'][name='father']").change(function(){
          // $('#pa_email').prop('required',true);
          $('#pa_fname').prop('required',true);
          $('#pa_lname').prop('required',true);
          $('#pa_street').prop('required',true);
          $('#pa_city').prop('required',true);
          $('#pa_province').prop('required',true);
          $('#pa_country').prop('required',true);
          $('#pa_postal').prop('required',true);
          $('#pa_hphone').prop('required',true);
          $('#pa_occupation').prop('required',true);
          $('#pa_employer').prop('required',true);
      })
      $("input[type='radio'][name='mother']").change(function(){
          // $('#mo_email').prop('required',true);
          $('#mo_fname').prop('required',true);
          $('#mo_lname').prop('required',true);
          $('#mo_street').prop('required',true);
          $('#mo_city').prop('required',true);
          $('#mo_province').prop('required',true);
          $('#mo_country').prop('required',true);
          $('#mo_postal').prop('required',true);
          $('#mo_hphone').prop('required',true);
          $('#mo_occupation').prop('required',true);
          $('#mo_employer').prop('required',true);
      })

      //toggle parent address fields step 3
      if ($('#mother-addr-checkbox').find('input:checkbox:checked').length > 0) {
          $("#mother-addr").prop("disabled", true);
          $("#mother-addr").prop("required", true);
      } else {
          $("#mother-addr").prop("disabled", false);
          $("#mother-addr").prop("required", false);
      }

      if ($('#father-addr-checkbox').find('input:checkbox:checked').length > 0) {
          $("#father-addr").prop("disabled", true);
          $("#father-addr").prop("required", true);
      } else {
          $("#father-addr").prop("disabled", false);
          $("#father-addr").prop("required", false);
      }

      //toggle grade input box
      var grade = $("input[name='grade']:checked").attr("id");
      if(grade == "grade-rad"){
        $("#grade-input").prop("disabled",false);
        $("#grade-input").prop("required",true);
      }
      else{
        $("#grade-input").prop("disabled",true);
        $("#grade-input").prop("required",false);
      }

      //toggle medical condition box
      var allergies = $("input[name='allegies']:checked").val();
      if(allergies == "yes"){
        $("#allergies-input").prop("disabled",false);
          $("#allergies-input").prop("required",true);
      }
      else{
        $("#allergies-input").prop("disabled",true);
        $("#allergies-input").prop("required",false);
        $("#allergies-input").val("")
      }

    //toggle alumnus condition box
    var alumnus = $("input[name='alumnus']:checked").val();
      if(alumnus == "yes"){
        $("#alumnus_input").prop("disabled",false);
          $("#alumnus_input").prop("required",true);
      }
      else{
        $("#alumnus_input").prop("disabled",true);
        $("#alumnus_input").prop("required",false);
        $("#alumnus_input").val("")
      }

    //toggle alumna condition box
    var alumna = $("input[name='alumna']:checked").val();
      if(alumna == "yes"){
        $("#alumna_input").prop("disabled",false);
          $("#alumna_input").prop("required",true);
      }
      else{
        $("#alumna_input").prop("disabled",true);
        $("#alumna_input").prop("required",false);
        $("#alumna_input").val("")
      }


      //All button click functions
      $('.sfl').click(function(event) {
        event.preventDefault();
        $('form').attr("action", "{{ route('user.form.saveForLater') }}");  //change the form action
        $('form').submit();  // submit the form
      });

      //form exit
      $('.exit').click(function(event) {
        window.location.href = "/user/dashboard"
      }); 

      //submit button
      $('.submit').click(function(event) {
        event.preventDefault();
          $('form').attr("action", "{{ route('user.form.sutmit',$application->id??0) }}");  //change the form action
        $('form').submit();  // submit the form
      });

      $('.summary').click(function(event) {
        event.preventDefault();
        $valid= false;
      var canada_photo = $('#photo-show');
      var canada_passport = $('#pass-show')
      if(canada_photo.length >0&& canada_passport.length >0){
        if( canada_photo.text() == '' ){
        $('#photo_error').text("*Required Field") ;
        $('#photo_error').prop('style',"color:#FF0000") ;
      }
      if( canada_passport.text() == '' ){
          $('#passport_error').text("*Required Field") ;
          $('#passport_error').prop('style',"color:#FF0000") ;
      }
      if( canada_photo.text() != '*Required Field' && canada_passport.text() != '*Required Field' ){
        $('form').attr("action", "{{ route('user.form.save') }}");  //change the form action
          $('form').submit(); 
    }
      }

    var international_photo = $('#in-photo-show');
      var international_passport = $('#in-pass-show')

      if(international_photo.length >0 && international_passport.length >0){
        if( international_photo.text() == '' ){
        $('#int_photo_error').text("*Required Field") ;
        $('#int_photo_error').prop('style',"color:#FF0000") ;
      }

      if(international_passport.text() == '' ){
        $('#int_passport_error').text("*Required Field") ;
        $('#int_passport_error').prop('style',"color:#FF0000");
    }  
    if( international_photo.text() != '*Required Field' && international_passport.text() != '*Required Field' ){
        $('form').attr("action", "{{ route('user.form.save') }}");  //change the form action
          $('form').submit(); 
    }
      }          
      });

      //hide iep upload field, show when iep is checked on step 2
     $("#iep").hide().find(':input').attr('required', false);

    //enable + disable fields when check rad button
      $("input[type='radio']").click(function(){
          var grade = $("input[name='grade']:checked").attr("id");

          if($("input[name='custody_status']:checked").val() == "Other"){
            $("#student_lives_with").prop("disabled",false);
            $("#student_lives_with").prop("required",true);
          }
          else{
            $("#student_lives_with").prop("disabled",true);
            $("#student_lives_with").prop("required",false);
            $("#student_lives_with").val("")
          }

          if(grade == "grade-rad"){
            $("#grade-input").prop("disabled",false);
            $("#grade-input").prop("required",true);
          }
          else{
            $("#grade-input").prop("disabled",true);
            $("#grade-input").prop("required",false);
          }


          var allergies = $("input[name='allegies']:checked").val();
          if(allergies == "yes"){
            $("#allergies-input").prop("disabled",false);
            $("#allergies-input").prop("required",true);
          }
          else{
            $("#allergies-input").prop("disabled",true);
            $("#allergies-input").prop("required",false);
            $("#allergies-input").val("")
          }

          var alumnus = $("input[name='alumnus']:checked").val();
          if(alumnus == "yes"){
            $("#alumnus_input").prop("disabled",false);
            $("#alumnus_input").prop("required",true);
          }
          else{
            $("#alumnus_input").prop("disabled",true);
            $("#alumnus_input").prop("required",false);
            $("#alumnus_input").val("")
          }

          var alumna = $("input[name='alumna']:checked").val();
          if(alumna == "yes"){
            $("#alumna_input").prop("disabled",false);
            $("#alumna_input").prop("required",true);
          }
          else{
            $("#alumna_input").prop("disabled",true);
            $("#alumna_input").prop("required",false);
            $("#alumna_input").val("")
          }
      });

      $('#canadian-citizen').change(function(){
        $("#iep").hide().find(':input').attr('required', false);
       
        //Show and hide IEP upload field
        var value = $(this).find('input:checkbox:checked').map(function(){
        return $(this).val();
        }).get();
        value.forEach(element => {
        if(element == "IEP / AEP" ){
            $("#iep").show().find(':input').attr('required', true);
          }
        });

          //Show and hide Canadian Citizenship field
          if($(this).find('input:checkbox:checked').length >0){
            $("#stu_support_details").prop("disabled",false);
          }
          else{
            $("#stu_support_details").prop("disabled",true);
          }
      });

      $('#mother-addr-checkbox').change(function(){
        if($(this).find('input:checkbox:checked').length >0){
          $("#mother-addr").prop("disabled",true);
          $("#mother-addr").prop("required",false);
            $('#mo_street').prop('required',false);
          $('#mo_city').prop('required',false);
          $('#mo_province').prop('required',false);
          $('#mo_country').prop('required',false);
          $('#mo_postal').prop('required',false);
        }
        else{
          $("#mother-addr").prop("disabled",false);
          $("#mother-addr").prop("required",true);
        }
      });

      $('#father-addr-checkbox').change(function(){
        if($(this).find('input:checkbox:checked').length >0){
          $("#father-addr").prop("disabled",true);
          $("#father-addr").prop("required",false);

          $('#pa_street').prop('required',false);
          $('#pa_city').prop('required',false);
          $('#pa_province').prop('required',false);
          $('#pa_country').prop('required',false);
          $('#pa_postal').prop('required',false);
        }
        else{
          $("#father-addr").prop("disabled",false);
          $("#father-addr").prop("required",true);
        }
      });
  });
</script>
    </div>

@endsection
@section('inspector')
  <div id="inspector">
      <a class="button" href="{{ route('admin.applications') }}">go back</a>

      <hr>
  </div>
  
@endsection


