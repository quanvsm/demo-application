@extends('admin.layouts.admin')
<?
  $class = 'editor';
  $active = 'application';
?>
@section('content')
    <h1>Application </h1> 
    <article id="content">
      <form  method="POST" action="{{route('admin.application.save')}}" enctype="multipart/form-data">
        @csrf
        {{-- do not delete this div --}}
        <div class="tab" style="display: none;"></div>
        
        <div class="tab student"  style="display: block;">
          @include('auth.user_auth.modules.application.steps.application-step2')
        </div>
        
        <div class="tab parent" style="display: block;"> 
          @include('auth.user_auth.modules.application.steps.application-step3')
        </div>

        <div class= "tab faith"  style="display: block;">
          @include('auth.user_auth.modules.application.steps.application-step4')
        </div>

        <div class="tab addition" style="display: block;"> 
          @include('auth.user_auth.modules.application.steps.application-step5')
        </div>
    </form>
</article>   
          </main>  
          <script>
              $(document).ready(function () {
                  $("#add_sibling").click(function () {
                      var field =
                          '<div class="row"><fieldset><label>Name<input type="text" name="sibling_name[]"></label><label>Date of Birth<input type="date" placeholder="YYYY-MM-DD" name="sibling_birthday[]"></label><label>School<input type="text" name="sibling_school[]"></label><span class="remove" id="remove_sibling"></span></fieldset></div>';

                      $("#after-sibling").after(field);
                      $("#remove_sibling").click(function () {
                          var par = $(this).parent();
                          par.parent().remove();
                      });
                  });
              });
          </script>

<script type="text/javascript">
  $('input[type=file]').change(function () {
    var id = $(this).attr('id')+"-show";
    var size = this.files[0].size;
        var val = $(this).val().toLowerCase(),
            regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|png|rtf|jpg|jpeg|PDF)$");
        if(size >31457280){
          $(this).val('');
          $('#'+id).empty().append('')
            alert('Your files have not been uploaded. The maximum upload file size is 30 MB. Please check your file sizes and try again.');
        }
        if (!(regex.test(val))) {
            $(this).val('');
            $('#'+id).empty().append('')
            alert('This file type is not accepted. Please save your file as one of the listed supported types.');
        }
        else{
          var file = $(this)[0].files[0].name;
          $('#'+id).empty().append(file);
        }
    });
</script>
@include('auth.user_auth.modules.application.script.jquery-script')
</div>

@endsection
@section('inspector')
  <div id="inspector">
      <a class="button" href="{{ route('admin.application.view',$application->id) }}">go back</a>
      <button type="submit" class="button accept">update application</button>
      <hr>
  </div>
  <script>
         $(document).ready(function(){
          $('.accept').click(function(event) {
          event.preventDefault();
          $('form').attr("action", "{{ route('admin.application.save') }}");  //change the form action
          $('form').submit();  // submit the form
    });
  });
  </script>
  
@endsection


