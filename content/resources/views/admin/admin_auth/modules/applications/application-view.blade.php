@extends('admin.layouts.admin')
@section('content')
<?php
            
if($application->status){
  switch ($application->status) {
  case "Submitted":
      $status = "pending";
      break;
  case "Denied":
      $status = "cancel";
      break;
  case 'Accepted':
      $status = "accept";
      break;
  default: $status = ""; 
  break;
}
}

?>
<div id="main" class="submission">
    @include('auth.user_auth.modules.application.steps.summary')
    <hr>
    <section>
      <h2>Payment Information</h2>
      @switch($payment->method??'')
      @case('cash')
      <p>Payment Method: Cash</p>
          @break
      @case('cheque')
      <p>Payment Method: Cheque</p>
          @break
      @default
      <p>Payment Method: Paypal</p>
      <p>Transaction Id: {{$payment->transaction_id ??''}}</p>
      <p>Amount: ${{$payment->amount??''}} {{$payment->currency??''}}</p>
      <p>Payer Email: {{$payment->payer_email??''}}</p>
  @endswitch
      <p>Date: {{$payment->created_at ??''}} CST</p>

    </section>
</div>
@endsection
  

@section('inspector')
<div id="inspector">
  <a class="button" href="{{ route('admin.applications') }}">go back</a>
  <a class="button" href="{{ route('admin.application.edit',$application->id) }}">edit</a>
  <a class="button" href="{{ route('admin.application.print',$application->id) }}" target="_blank">print</a>
  <a class="button"  href="{{route('admin.export',['id'=>$application->id,'type'=>$data->type_id])}}">export application</a>
  @if (isset($application->registration_id))
    <a class="button" href="{{ route('admin.registration.view',$application->registration_id) }}">view registration</a>
  @endif
  <a class="button cancel application-delete">delete</a>
  <hr>
  <h2>Status...</h2>
  <span class="status {{$status}}">{{$application->status??''}}</span>
  <fieldset>
    <label>Action
      <select id="status_dropdown">
        <option value='Submitted' {{($application->status == "Submitted" ? 'selected' : '')}}>Application Submitted</option>
        <option value='Accepted' {{($application->status == "Accepted" ? 'selected' : '')}}>Application Accepted</option>
        <option value='Denied' {{($application->status == "Denied" ? 'selected' : '')}}>Application Denied</option>
        <option value='Waiting' {{($application->status == "Waiting" ? 'selected' : '')}}>Waiting List</option>
      </select>
    </label>
    <button class="button update_status">update status</button>
  </fieldset>
  <hr>
 <h2>Interview...</h2>
 <fieldset>
  @if (isset($application->interview_date))
  <p>Interviewed on : {{$application->interview_date}}</p>
@endif
<input type="text" id="datepicker" value="{{$application->interview_date??''}}">
<a class="button interview">set interview date</a>
 </fieldset>

{{-- <fieldset>
  <h2>Invoice...</h2>
  @foreach ($invoices as $item)
  <a href='{{route('admin.download.invoice',['id'=>$item->id])}}'>{{$item->name ??''}}</a> 
 @endforeach  
  <form method="POST" action="{{route("admin.upload.invoice",$application->id)}}" enctype="multipart/form-data">
    @csrf
    <label>file :<span id="application_invoice-show">
    </span>
  </label>
    <label><input class="button" name="application_invoice" id="application_invoice" type="file"></label>
    <button type="submit" class="button submit">upload invoice</button>
  </form>
</fieldset> --}}


<script type="text/javascript">

  $( "#datepicker" ).datepicker({ 
    dateFormat: 'yy-mm-dd'});

</script>

<script>
  $(document).ready(function(){
    $('.add').hide();
  });
  </script>

<script>
  $(document).ready(function(){
    $('.support_file').each(function(){
      var route=  $(this).attr('href');
    if(route){
       route = route.replace('user/form','admin/application')
      $(this).attr('href',route)
      }
    })
  });
  </script>
  <script>
      $( ".application-delete" ).click(function() {
            var application_id = '{{$application->id}}';

            showModal('Are You Sure You want to Delete this Application? This will also Delete the Registration.');

            $( "#modal-accept" ).click(function() {
              $.ajax({
                      url: "{{route('admin.application.delete')}}",
                      method: 'POST',
                      data: { 'id' : application_id, '_token' : '{{csrf_token()}}' },
                      success: function(response){

                          $( "#modal" ).hide();
                          window.location.href = "/admin/application"
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
            });
        });

          $( ".update_status" ).click(function() {
             var application_id = '{{$application->id}}';
            var status = ($('#status_dropdown').val());
          showModal('Update status for this application?');
          $( "#modal-accept" ).click(function() {  
                    $.ajax({
                     url: "{{route('admin.application.update.status')}}",
                     method: 'GET',
                     data: { 'id' : application_id, 'value': status},
                     success: function(result){
                         $( "#notification" ).delay(1500).slideUp('slow');
                         setTimeout(function(){
                             location.reload();
                         }, 2000);
                     }
                 });
           });
       });


        $(".interview" ).click(function() {
          var application_id = '{{$application->id}}';
              $.ajax({
                      url: "{{route('admin.application.interview.date')}}",
                      method: 'GET',
                      data: {'id' : application_id,'interview_date': $("#datepicker").val() },
                      success: function(result){
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
        });

        $('input[type=file]').change(function () {
       var id = $(this).attr('id')+"-show";
       var size = this.files[0].size;
           var val = $(this).val().toLowerCase(),
               regex = new RegExp("(.*?)\.(pdf|png|jpg|jpeg)$");
           if(size >31457280){
             $(this).val('');
             $('#'+id).empty().append('')
               alert('Your files have not been uploaded. The maximum upload file size is 30 MB. Please check your file sizes and try again.');
           }
           if (!(regex.test(val))) {
               $(this).val('');
               $('#'+id).empty().append('')
               alert('This file type is not accepted. Please save your file as one of the listed supported types.');
           }
           else{
             var file = $(this)[0].files[0].name;
             $('#'+id).empty().append(file);
           }
       });

    </script>
@endsection
