@extends('admin.layouts.admin')
<?
  $class = 'index submission';
  $active = 'application';
?>
@section('content')
<h1>Applications</h1>
    <div class="filterBar"> <!-- for filtering index content (dynamically ordered posts) -->
      <label> Filter By Status: </label>
      <fieldset> 
        <input type="radio" name="status_filter" id="All" checked ><label for="All" >All Applications</label>
        <input type="radio" name="status_filter" id="Submitted" ><label for="Submitted" >Submitted</label>
        <input type="radio" name="status_filter" id="Accepted" ><label for="Accepted" >Accepted</label>
        <input type="radio" name="status_filter" id="Denied" ><label for="Denied" >Denied</label>
        <input type="radio" name="status_filter" id="Waiting" ><label for="Waiting" >Waiting</label>
      </fieldset>
      <input id="search" type="text" placeholder="search">
    </div>
    
    
      <div class="category">
        <ul>
            @foreach($applications as $app)
            <?php $meta = json_decode($app->meta); ?>
            <?php $date = ($app->submitted_at); ?>
            <?php
            
              if($app->status)
              switch ($app->status) {
                case "Submitted":
                    $status = "pending";
                    break;
                case "Denied":
                    $status = "cancel";
                    break;
                case 'Accepted':
                    $status = "accept";
                    break;
                default: $status = ""; 
                break;
            }

            ?>
            <li class="filter {{$app->status}}">
                <div class="details {{$status}}">
                    <input type="hidden" class="id" value="{{ $app->id }}" required>
                    <span class="status {{$status}}">{{$app->status}}</span> 
                    <span class="name">{{ (isset($meta->stu_fname) ? $meta->stu_fname : '') }} {{ (isset($meta->stu_lname) ? $meta->stu_lname : '') }}</span>
                    @if ($app->type==1)
                    <span class="grade default"> {{(($meta->grade =='on' ? "Grade " .$meta->grade_in ?? '' : $meta->grade ??'') )}}</span>
                   @else
                    <span class="grade default">{{"Grade " .$meta->int_grade ?? ''}}</span>
                   @endif
                    <span class="email">{{ (isset($meta->stu_email) ? $meta->stu_email : '') }}</span>
                    <span class="type status default">{{ (isset($meta->type_id) ? ($meta->type_id ==1 ?  'Canadian' : "International ") : "") }}</span>
                    <span class="date">{{ (isset($app->submitted_at) ? date("M d,Y - h:m A", strtotime($date)) : '') }}</span>
                    <span class="actions">
                      <a class="button" href="{{ route('admin.application.view',$app->id) }}">view</a>
                      <a class="button cancel application-delete">delete</a>
                    </span>
                  </div>
            </li>
        @endforeach      
        </ul>
      </div>
@endsection

@section('inspector')
<div id="inspector">
  <a class="button download_all" href="{{route('admin.export.all','all')}}" >export all applications</a>
  <hr>
  <h2>Create New Application...</h2>
  <fieldset>
    <label><input name="newapp" type="radio" value="1"> Canadian</label>
    <label><input name="newapp" type="radio" value="2"> International</label>
    <a class="button accept create" href="#" >create</a>
    <span class="create_error"></span>
  </fieldset>
  <hr>
  <h2>Set Cut-Off Date...</h2>
  <fieldset>
    <label>Cut-Off Date
      <input type="text" id="datepicker" value="{{$cut_off_date??''}}">
      {{-- <input type="date" name="cut_off_date" id="cut_off_date" value="{{$app->cut_off_date}}"> --}}
    </label>
    <a class="button cut-off">set cut-off date</a>
  </fieldset>
</div>

<script type="text/javascript">

  $( "#datepicker" ).datepicker({ 
    dateFormat: 'yy-mm-dd'});

</script>

<script>
  var IDs = [];
  $(document).ready(function(){
    $("#search").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      var filter = $("input[name='status_filter']:checked").attr("id");
      switch(filter) {
        case "All":
        $(".category li").filter(function(el, idx, array) {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
            IDs = []
          $(".filter").each(function() {
              if($(this).css("display") !="none"){
                IDs.push($(this).find(".id").val());
              }
              });
          var route = '{{route("admin.export.all",["id"=>":IDs"])}}';
          route = route.replace(':IDs', IDs);
          $('.download_all').attr("href",route );
        break;
        default:
           $(".category ."+filter).filter(function() {
              $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
            IDs = []
          $(".filter").each(function() {
              if($(this).css("display") !="none"){
                IDs.push($(this).find(".id").val());
              }
              });
          var route = '{{route("admin.export.all",["id"=>":IDs"])}}';
          route = route.replace(':IDs', IDs);
          $('.download_all').attr("href",route );
      }
    });
  });

  $("input[type='radio']").click(function(){
          var filter = $("input[name='status_filter']:checked").attr("id");
          $("#search").val("");
          $('.filter').hide()
          if(filter=='All'){
            $('.filter').show()
            var route = '{{route("admin.export.all",["id"=>"all"])}}';
            $('.download_all').attr("href",route);
          }
          else{
            $('.'+filter).show()
          IDs=[];
          $('.'+filter).find(".id").each(function(){IDs.push($(this).val())});
          var route = '{{route("admin.export.all",["id"=>":IDs"])}}';
          route = route.replace(':IDs', IDs);
          $('.download_all').attr("href",route );
          }
        });

  $( ".application-delete" ).click(function() {
            var application_id = $(this).closest('.details').find(".id").val();
            showModal('Are You Sure You want to Delete this Application? This will also Delete the Registration.');

            $( "#modal-accept" ).click(function() {
              $.ajax({
                      url: "{{route('admin.application.delete')}}",
                      method: 'POST',
                      data: { 'id' : application_id, '_token' : '{{csrf_token()}}' },
                      success: function(result){
                          $( "#modal" ).hide();
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
            });
        });

        $(".cut-off" ).click(function() {
              $.ajax({
                      url: "{{route('admin.application.set.cutoff')}}",
                      method: 'GET',
                      data: {'cut_off': $("#datepicker").val()},
                      success: function(result){
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
        });

        $(".create" ).click(function() {
          $(".create_error").text("") 
          if( $('input[name="newapp"]:checked').length > 0){
            var id = $('input[name="newapp"]:checked').val();
           var route = '{{route("admin.application.create",["id"=>":IDs"])}}';
           route = route.replace(':IDs', id);
            $('.create').attr("href",route );
          }else{
            $(".create_error").text("*Please select application type!") 
            $(".create_error").prop('style','color:#d4ae16;')
          }

        });
  </script>
  
@endsection
