<div id="inspector">
    <input type="hidden" name="id" value="{{$page->id}}" required>
    <a class="button" href="{{route('admin.page')}}">go back</a>
    <button type="submit" id="delete" formaction="{{route('admin.page.delete_page')}}" class="button cancel">delete page</button>
    <button type="submit" id="save" class="button accept" >save</button>
    @if($page->parent_id)
        <a class="button" href="{{route('public.parent',['parent'=>$parent_page->slug, 'slug'=>$page->slug])}}" target="_blank">view</a>
    @else
        <a class="button" href="{{route('public.page',['slug'=>$page->slug])}}" target="_blank">view</a>
    @endif

    @yield('template-attributes')
    <hr>
    <label>
        Title
        <input type="text" id="title" name="title" value="{{$page->title}}" required>
    </label>
    <label>
        Slug
        <input type="text" id="slug" name="slug" value="{{$page->slug}}" required>
    </label>
    <fieldset>
        <legend>Visibility</legend>
        <label><input name="visible" type="radio" value="published" {{($page->visible=='published'? 'checked' :'')}} >Published</label>
        <label><input name="visible" type="radio" value="draft" {{($page->visible=='draft'? 'checked' :'')}} >Draft</label>
    </fieldset>
    <hr>
    <label>
        Select a Template
        <select name="select_template_id">
          <option value="default">Default</option>
          @foreach($template_types as $type)
              <option value="{{$type->id}}" {{(isset($template) && $template->template_type==$type->id? 'selected' :'')}}>{{$type->description}}</option>
          @endforeach
        </select>
    </label>
    <hr>
    <h2>SEO...</h2>
    <label>
        Head
        <input type="text" id="seo-title" name="seo-title" value="{{$page->seo_title}}">
    </label>
    <label class="description">
        Description
        <textarea name="seo-description">{{$page->seo_description}}</textarea>
    </label>
</div>

<script>
  $(document).ready(function() {

      $('#title').on("change", function() {
          // If the slug has not value, sets the slug value from the title.
          if ($('#slug').val() == ""){
              updateSlug('title', 'slug');
          }
      });

      $('#slug').on("change", function() {
          updateSlug('title', 'slug');
      });

      // Removes spaces and symbols from the slug.
      function updateSlug(title, slug) {
          var newSlug = "";

          if ($('#'+slug).val() == ""){
              newSlug = $('#'+title).val().replace(/\s/g,"-");
          } else {
              newSlug = $('#'+slug).val().replace(/\s/g,"-");
          }

          newSlug = newSlug.replace(/[^A-Za-z0-9\_\-]/g,"");
          newSlug = newSlug.toLowerCase();
          $('#'+slug).val(newSlug);
      }

      $( "#delete" ).click(function() {
          event.preventDefault();
          showModal('Are You Sure You want to Delete this Page?');

          $( "#modal-accept" ).click(function() {
              $( "#delete" ).click();
          });
      });

  });
</script>
