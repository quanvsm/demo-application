<? $admin_name = AdminHelper::get_admin_name(); ?>
<div id="menu">
	<img class="logo" src="/css/admin/img/vsm-logo.png" alt="VSM">
	<ul>
		{{-- <li class="{{ Route::is('admin.page') ? 'active' : '' }}"><a href="{{ route('admin.page') }}">Pages</a></li>
		<li class="{{ Route::is('admin.page') ? 'active' : '' }}"><a href="{{ route('admin.posts') }}">Posts</a></li> --}}
		<li><a href="{{ route('admin.applications') }}">Applications</a></li>
		<li><a href="{{ route('admin.registrations') }}">Registrations</a></li>
		<li><a href="{{ route('admin.documents') }}">Documents</a></li>
		<li><a href="{{ route('admin.users') }}">Users</a></li>
		<li><a href="{{ route('admin.admins') }}">Admins</a></li>
		<li><a href="{{ route('admin.settings') }}">{{$admin_name ?? ''}} Settings</a></li>
		<li><a href="{{ route('admin.logout') }}">Logout</a></li>
	</ul>
</div>
