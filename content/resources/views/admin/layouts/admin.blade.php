<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>VSM Demo</title>
        <link href="/css/admin/css/style.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="/css/admin/js/Sortable.min.js"></script>

        {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  --}}

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 
    
        {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet"> --}}
    
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">   
    
    </head>
    <body>
      @include('admin.inc.messages')

      <div id="interface-wrapper">
        @if(Auth::guard('admin')->check())
            @include('admin.admin_auth.inc.header')
        @endif

      	<div id="main" class="<?=$class ?? '' ?>">
      		@yield('content')
      	</div>
      </div>

      @yield('inspector')

    </body>
</html>
