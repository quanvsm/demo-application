<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>VSM Demo</title>

        <link href="/css/admin/css/style.min.css" rel="stylesheet" >
        <link href="/css/admin/css/jquery-ui.css" rel="stylesheet">
        <link href="/css/admin/css/jquery-ui-timepicker-addon.css" rel="stylesheet" >

        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="/css/admin/js/jquery-ui-timepicker-addon.js"></script>
        <script src="/css/admin/js/Sortable.min.js"></script>
        <script src="/css/admin/js/prism.js"></script>
        <script src='https://cdn.tiny.cloud/1/7fhpe8fjhh42qzk21o54ifrcwizkgxi5e9o7ik7joq29bj05/tinymce/5/tinymce.min.js' referrerpolicy="origin"></script>
    </head>
    <body>
      @include('admin.inc.messages')

      <form method="POST" action="{{$form_action ?? '' }}" enctype='multipart/form-data'>
          @csrf
          <div id="interface-wrapper">
            @if(Auth::guard('admin')->check())
                @include('admin.admin_auth.inc.header')
            @endif

          	<div id="main" class="<?=$class ?? '' ?>">
          		@yield('content')
          	</div>
          </div>

          @yield('inspector')
      </form>
      
    </body>
</html>
