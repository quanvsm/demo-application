@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'page';

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.page.save');
  }
?>

@section('content')
    <h1>Edit SEO: {{$page->title}}</h1>

    <div class="section">
        <label>Head<input type="text" id="seo-title" name="seo-title" value="{{$page->seo_title}}"></label>
    </div>

    <div class="section">
        <label>Description
            <textarea name="seo-description">{{$page->seo_description}}</textarea>
        </label>
    </div>
@endsection

@section('inspector')
      <div id="inspector">
        <input type="hidden" name="id" value="{{$page->id}}" required>

        @if(Auth::guard('admin')->check())
            <a class="button" href="{{ route('admin.page') }}">go back</a>
        @elseif(Auth::guard('user')->check())
            <a class="button" href="{{ route('user.page') }}">go back</a>
        @endif

        <button id="save" type="submit" class="button accept" >save</button>

        @if($page->parent_id)
            <a class="button" href="{{route('public.parent',['parent'=>$parent_page->slug, 'slug'=>$page->slug])}}" target="_blank">view</a>
        @else
            <a class="button" href="{{route('public.page',['slug'=>$page->slug])}}" target="_blank">view</a>
        @endif
      </div>
@endsection
