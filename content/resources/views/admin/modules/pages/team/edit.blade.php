@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'page';
  $file = true;

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.team.save');
  }
?>

@section('content')
    <h1>Edit: {{$team->name}}</h1>
    <input type="hidden" name="id" value="{{$team->id}}" required>
    @if(isset($team->image))
        <input type="hidden" id="remove_image" name="remove_image" value="0">
    @endif

    <div class="section">
        <label>Name<input type="text" name="name" value="{{$team->name ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Image
            <div class="details">
                <input type="file" name="image">
                @if(isset($team->image))
                    <div id="image_thumbnail" class="thumbnail">
                        @if(isset($team->image))
                              <img id="side_img" src="{{env('APP_TEAM').$team->image}}">
                        @endif
                    </div>
                    <a id="image_remove_btn" class="button cancel">remove</a>
                @endif
            </div>
        </label>
    </div>

    <div class="section">
        <label>Position<input type="text" name="position" value="{{$team->position ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Additional Position (Optional)<input type="text" name="additional_position" value="{{$team->additional_position ?? '' }}" ></label>
    </div>

    <div class="section">
        <label>Phone<input type="text" name="phone" value="{{$team->phone ?? '' }}" ></label>
    </div>

    <div class="section">
        <label>Email<input type="email" name="email" value="{{$team->email ?? '' }}" ></label>
    </div>

    <div class="section">
        <label>Quote<textarea name="quote" >{{$team->quote ?? '' }}</textarea></label>
    </div>

    <div class="section">
        <label>Bio<textarea name="bio" >{{$team->bio ?? '' }}</textarea></label>
    </div>
@endsection

@section('inspector')
  <div id="inspector">
      @if(Auth::guard('admin')->check())
          <a class="button" href="{{ route('admin.page.edit',$page->id) }}">go back</a>
      @elseif(Auth::guard('user')->check())
          <a class="button" href="{{ route('user.page.edit',$page->id) }}">go back</a>
      @endif

      <button type="submit" class="button accept" >save</button>

      @if($page->parent_id)
          <a class="button" href="{{route('public.parent',['parent'=>$parent_page->slug, 'slug'=>$page->slug])}}" target="_blank">view</a>
      @else
          <a class="button" href="{{route('public.page',['slug'=>$page->slug])}}" target="_blank">view</a>
      @endif

  </div>
  <script>
      $(document).ready(function(){
          $( "#image_remove_btn" ).click(function(e) {
              e.preventDefault();
              $("#remove_image").val(1);
              $("#image_thumbnail").remove();
          });
      });
  </script>
@endsection
