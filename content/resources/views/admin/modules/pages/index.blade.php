@extends('admin.layouts.admin')
<?
  $class = 'index';
  $active = 'page';

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.page.order');
  }
?>
@section('content')
  <h1>Pages</h1>

  <form method="POST" id="order_form" action="{{$form_action ?? ''}}">
    @csrf
    <div class="category">
      <h2>All Pages</h2>
      <ul class="sortable">
        @foreach($pages as $page)
          @if($page->parent_id == null)
              <?// If the page is not a child page OR is a parent page.?>

              <li class="{{($page->parent == 1 ? 'folder' :'post')}}" id="{{$page->id}}">
                <div class="details">
                    <input type="hidden" class="page_id" name="id" value="{{$page->id}}" required>
                    <input type="hidden" class="sort" name="sort_{{$page->id}}" value="{{$page->sort}}" required>

                    @if($page->parent == 1)
                      <?
                        // If the page is a parent.
                        // This input is not used in the form post.
                        // This input is for child pages to know what parent they are under.
                      ?>
                      <input type="hidden" class="id" value="{{$page->id}}" required>
                    @else
                      <input type="hidden" class="parent_id" name="parent_id_{{$page->id}}" value="{{$page->parent_id}}" required>
                    @endif

                    <span class="title">{{$page->title}}</span>
                    <span class="slug">
                        <a href="{{route('public.page',['slug'=>$page->slug])}}" target="_blank">/{{$page->slug}}</a>
                    </span>
                    <span>
                        @if($page->template_id != null)
                            <? $template = $helper->get_template_info($page->template_id); ?>
                            {{$template->view}}
                        @else
                            default
                        @endif
                    </span>
                    <span class="actions">
                        <span class="status">{{$page->visible}}</span>
                        @if(Auth::guard('admin')->check())
                            @if($page->hard_code == 1)
                                <a class="button" href="{{ route('admin.page.seo',$page->id) }}">edit</a>
                            @elseif($page->hard_code == 0)
                                <a class="button" href="{{ route('admin.page.edit',$page->id) }}">edit</a>
                                <a class="button cancel page-delete">delete</a>
                            @endif
                        @elseif(Auth::guard('user')->check())
                            @if($page->hard_code == 1)
                                <a class="button" href="{{ route('user.page.seo',$page->id) }}">edit</a>
                            @elseif($page->hard_code == 0)
                                <a class="button" href="{{ route('user.page.edit',$page->id) }}">edit</a>
                                <a class="button cancel page-delete">delete</a>
                            @endif
                        @endif
                    </span>
                </div>
                @if($page->parent == 1)
                  <?// If the page is a parent page.?>
                  <ul class="sortable">
                      @foreach($pages as $child)
                        @if($page->id == $child->parent_id)
                            <?// If the child is a child of this parent page.?>
                            <li class="post">
                                <div class="details">
                                    <input type="hidden" class="page_id" name="id" value="{{$child->id}}" required>
                                    <input type="hidden" class="parent_id" name="parent_id_{{$child->id}}" value="{{$child->parent_id}}" required>
                                    <input type="hidden" class="sort" name="sort_{{$child->id}}" value="{{$child->sort}}" required>
                                    <span class="title">{{$child->title}}</span>
                                    <span class="slug">
                                        <a href="{{route('public.parent',['parent'=>$page->slug, 'slug'=>$child->slug])}}" target="_blank">/{{$page->slug}}/{{$child->slug}}</a>
                                    </span>
                                    <span>
                                        @if($child->template_id != null)
                                            <? $template = $helper->get_template_info($child->template_id); ?>
                                            {{$template->view}}
                                        @else
                                            default
                                        @endif
                                    </span>
                                    <span class="actions">
                                        <span class="status">{{$child->visible}}</span>
                                        @if(Auth::guard('admin')->check())
                                            @if($child->hard_code == 1)
                                                <a class="button" href="{{ route('admin.page.seo',$child->id) }}">edit</a>
                                            @elseif($child->hard_code == 0)
                                                <a class="button" href="{{ route('admin.page.edit',$child->id) }}">edit</a>
                                                <a class="button cancel page-delete">delete</a>
                                            @endif
                                        @elseif(Auth::guard('user')->check())
                                            @if($child->hard_code == 1)
                                                <a class="button" href="{{ route('user.page.seo',$child->id) }}">edit</a>
                                            @elseif($child->hard_code == 0)
                                                <a class="button" href="{{ route('user.page.edit',$child->id) }}">edit</a>
                                                <a class="button cancel page-delete">delete</a>
                                            @endif
                                        @endif
                                    </span>
                                </div>
                            </li>
                        @endif
                      @endforeach
                  </ul>
                @endif
              </li>

          @endif
        @endforeach
      </ul>
    </div>
  </form>
@endsection

@section('inspector')
  <div id="inspector">
      <button id="post_order" class="button accept">update page order</button>
      <hr>

      @if(Auth::guard('admin')->check())
          <? $create_page = route('admin.page.create'); ?>
      @endif

      <form method="POST" action="{{$create_page ?? ''}}">
        @csrf
        <h2>New Page...</h2>
        <input type="text" id="title" name="title" placeholder="Title" required>
        <input type="text" id="slug" name="slug" placeholder="Slug" required>
        <button class="button" type="submit" >create</button>
      </form>
      <hr>

      @if(Auth::guard('admin')->check())
          <? $create_parent = route('admin.parent.create'); ?>
      @endif

      <form method="POST" action="{{$create_parent ?? ''}}">
        @csrf
        <h2>New Parent...</h2>
        <input type="text" id="parent_title" name="title" placeholder="Title" required>
        <input type="text" id="parent_slug" name="slug" placeholder="Slug" required>
        <button class="button" type="submit" >create</button>
      </form>
  </div>

  <script>
    $(document).ready(function() {

    	$('#title, #parent_title').on("change", function() {
            // If the slug has not value, sets the slug value from the title.
    		if ($('#slug').val() == ""){
    			updateSlug('title', 'slug');
    		}

            if ($('#parent_slug').val() == ""){
                updateSlug('parent_title', 'parent_slug');
            }
    	});

    	$('#slug').on("change", function() {
    		updateSlug('title', 'slug');
    	});

        $('#parent_slug').on("change", function() {
    		updateSlug('parent_title', 'parent_slug');
    	});

        // Removes spaces and symbols from the slug.
    	function updateSlug(title, slug) {
    		var newSlug = "";

    		if ($('#'+slug).val() == ""){
    			newSlug = $('#'+title).val().replace(/\s/g,"-");
    		} else {
    			newSlug = $('#'+slug).val().replace(/\s/g,"-");
    		}

            newSlug = newSlug.replace(/[^A-Za-z0-9\_\-]/g,"");
    		newSlug = newSlug.toLowerCase();
    		$('#'+slug).val(newSlug);
    	}

        $( "#post_order" ).click(function() {
          $( "#order_form" ).submit();
        });

        $( ".page-delete" ).click(function() {
            var title = $(this).closest('.details').find(".title").html();
            var page_id = $(this).closest('.details').find(".page_id").val();

            showModal('Are You Sure You want to Delete '+title+'?');

            @if(Auth::guard('admin')->check())
                var url = '{{route('admin.page.delete')}}';
            @elseif(Auth::guard('user')->check())
                var url = '{{route('user.page.delete')}}';
            @endif

            $( "#modal-accept" ).click(function() {
                  $.ajax({
                      url: url,
                      method: 'POST',
                      data: { 'id' : page_id, '_token' : '{{csrf_token()}}' },
                      success: function(result){

                          $( "#modal" ).hide();
                          $( "#"+page_id ).remove();

                          var message = "<div id='notification'>";
                              message +=     "<p>"+title+" was Deleted. Page will now reload.</p>";
                              message += "</div>";

                          $("body").append(message);
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
            });
        });

        // Sets the .sortable items to be sortable.
        var sortable = [].slice.call(document.querySelectorAll('.sortable'));
    	for (var i = 0; i < sortable.length; i++) {
    		new Sortable(sortable[i], {
    			group: 'nested',
    			animation: 150
    		});
    	}

        // When ever the .category items are sorted.
        $('.category').change(function() {
            // Clears all the paren_id's.
            $('.parent_id').val("");
            // Goes threw each .category element.
            $(".category").each(function () {

              // If the element is an .post element.
              $($(this).find(".post")).each(function(){
                // If the .post element is nested us a .folder parent element.
                if($(this).parents('.folder').length == 1) {
                  // Gets the parent id.
                  var id = $(this).closest('.folder').find(".id").val();
                  // Sets the child element folder_id to the parent id.
                  $(this).children().closest('.details').find(".parent_id").val(id);
                }
              });

            });

            var x = 1;
            // Goes threw each .sort element.
            $(".sort").each(function () {
              $(this).val(x);
              x++;
            });

        });

    });
  </script>
@endsection
