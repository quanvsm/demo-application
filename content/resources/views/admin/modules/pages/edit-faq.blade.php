@extends('admin.layouts.admin_form')
<?
  $class = 'index';
  $active = 'page';
  $FAQs = $helper->get_FAQS($page->template_id);
  $template = $helper->get_template_info($page->template_id);

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.page.save');
  }
?>

@section('content')
    <h1>Edit: {{$page->title}}</h1>

    <input type="hidden" name="template_id" value="{{$page->template_id}}" required>
    <div class="category">
        <h2>All FAQs</h2>
        <ul class="sortable">
          @foreach($FAQs as $FAQ)
              <li class="post" id="{{$FAQ->id}}">
                  <div class="details">
                      <input type="hidden" class="faq_id" name="id" value="{{$FAQ->id}}" required>
                      <input type="hidden" class="sort" name="sort_{{$FAQ->id}}" value="{{$FAQ->sort}}" required>

                      <span class="title question">{{$FAQ->question}}</span>
                      <span class="actions">
                          @if(Auth::guard('admin')->check())
                              <a class="button" href="{{ route('admin.faq.edit',$FAQ->id) }}">edit</a>
                          @elseif(Auth::guard('user')->check())
                              <a class="button" href="{{ route('user.faq.edit',$FAQ->id) }}">edit</a>
                          @endif
                          <a class="button cancel faq-delete">delete</a>
                      </span>
                  </div>
              </li>
          @endforeach
        </ul>
     </div>
@endsection

@section('inspector')

  @section('template-attributes')
    <hr>
    <h2>Add FAQ...</h2>
    <input type="text" id="question" name="question" placeholder="Question">
    @if(Auth::guard('admin')->check())
        <button id="add_faq" type="submit" formaction="{{route('admin.faq.create')}}" class="button">add</button>
    @elseif(Auth::guard('user')->check())
        <button id="add_faq" type="submit" formaction="{{route('user.faq.create')}}" class="button">add</button>
    @endif
  @endsection

  @if(Auth::guard('admin')->check())
      @include('admin.admin_auth.inc.page-inspector')
  @elseif(Auth::guard('user')->check())
      @include('admin.user_auth.inc.page-inspector')
  @endif

  <script>
    $(document).ready(function() {

        $( "#add_faq" ).click(function() {
            $("#question").attr("required",true);
        });

        $( "#save" ).click(function() {
            $("#question").attr("required",false);
        });


        $( ".faq-delete" ).click(function() {
            var question = $(this).closest('.details').find(".question").html();
            var faq_id = $(this).closest('.details').find(".faq_id").val();

            showModal('Are you sure you want to delete '+question+'?');

            @if(Auth::guard('admin')->check())
                var url = '{{route('admin.faq.delete')}}';
            @elseif(Auth::guard('user')->check())
                var url = '{{route('user.faq.delete')}}';
            @endif

            $( "#modal-accept" ).click(function() {
                  $.ajax({
                      url: url,
                      method: 'POST',
                      data: { 'id' : faq_id, '_token' : '{{csrf_token()}}' },
                      success: function(result){

                          $( "#modal" ).hide();
                          $( "#"+faq_id ).remove();

                          var message = "<div id='notification'>";
                              message +=     "<p>"+question+" was Deleted. Page will now reload.</p>";
                              message += "</div>";

                          $("body").append(message);
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
            });
        });

        // Sets the .sortable items to be sortable.
        var sortable = [].slice.call(document.querySelectorAll('.sortable'));
        for (var i = 0; i < sortable.length; i++) {
            new Sortable(sortable[i], {
                group: 'nested',
                animation: 150
            });
        }

        // When ever the .category items are sorted.
        $('.category').change(function() {
            var x = 1;
            // Goes threw each .sort element.
            $(".sort").each(function () {
              $(this).val(x);
              x++;
            });

        });

    });
  </script>
@endsection
</form>
