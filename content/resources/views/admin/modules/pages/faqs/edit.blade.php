@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'page';

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.faq.save');
  }
?>

@section('content')
    <h1>Edit: {{$FAQ->question}}</h1>
    <input type="hidden" name="id" value="{{$FAQ->id}}" required>

    <div class="section">
        <label>Question<input type="text" name="question" value="{{$FAQ->question ?? '' }}" required></label>
    </div>

    <div class="section">
        <label>Answer<textarea name="answer" id="editor">{{$FAQ->answer ?? '' }}</textarea></label>
    </div>
@endsection

@section('inspector')
  <div id="inspector">
      @if(Auth::guard('admin')->check())
          <a class="button" href="{{ route('admin.page.edit',$page->id) }}">go back</a>
      @elseif(Auth::guard('user')->check())
          <a class="button" href="{{ route('user.page.edit',$page->id) }}">go back</a>
      @endif

      <button type="submit" class="button accept" >save</button>

      @if($page->parent_id)
          <a class="button" href="{{route('public.parent',['parent'=>$parent_page->slug, 'slug'=>$page->slug])}}" target="_blank">view</a>
      @else
          <a class="button" href="{{route('public.page',['slug'=>$page->slug])}}" target="_blank">view</a>
      @endif
  </div>
  @include('admin.inc.tinymce_options')
@endsection
