@extends('admin.layouts.admin_form')
<?
  $class = 'index';
  $active = 'page';
  $all_team = $helper->get_team($page->template_id);
  $template = $helper->get_template_info($page->template_id);

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.page.save');
  }
?>

@section('content')
    <h1>Edit: {{$page->title}}</h1>

    <input type="hidden" name="template_id" value="{{$page->template_id}}" required>
    <div class="category">
        <h2>Team</h2>
        <ul class="sortable">
          @foreach($all_team as $team)
              <li class="post" id="{{$team->id}}">
                  <div class="details">
                      <input type="hidden" class="team_id" name="id" value="{{$team->id}}" required>
                      <input type="hidden" class="sort" name="sort_{{$team->id}}" value="{{$team->sort}}" required>

                      <span class="name question">{{$team->name}}</span>
                      <span class="actions">
                          @if(Auth::guard('admin')->check())
                              <a class="button" href="{{route('admin.team.edit',$team->id)}}">edit</a>
                          @elseif(Auth::guard('user')->check())
                              <a class="button" href="{{route('user.team.edit',$team->id)}}">edit</a>
                          @endif
                          <a class="button cancel team-delete">delete</a>
                      </span>
                  </div>
              </li>
          @endforeach
        </ul>
     </div>

@endsection

@section('inspector')

  @section('template-attributes')
    <hr>
    <h2>Add Team Member...</h2>
    <input type="text" id="name" name="name" placeholder="Name">
    @if(Auth::guard('admin')->check())
        <button id="add_team_member" type="submit" formaction="{{route('admin.team.create')}}" class="button">add</button>
    @elseif(Auth::guard('user')->check())
        <button id="add_team_member" type="submit" formaction="{{route('user.team.create')}}" class="button">add</button>
    @endif
  @endsection
  @if(Auth::guard('admin')->check())
      @include('admin.admin_auth.inc.page-inspector')
  @elseif(Auth::guard('user')->check())
      @include('admin.user_auth.inc.page-inspector')
  @endif

  <script type="text/javascript">
    $(document).ready(function() {

        $( "#add_team_member" ).click(function() {
            $("#name").attr("required",true);
        });

        $( "#save" ).click(function() {
            $("#name").attr("required",false);
        });


        // Sets the .sortable items to be sortable.
        var sortable = [].slice.call(document.querySelectorAll('.sortable'));
        for (var i = 0; i < sortable.length; i++) {
            new Sortable(sortable[i], {
                group: 'nested',
                animation: 150
            });
        }

        // When ever the .category items are sorted.
        $('.category').change(function() {
            var x = 1;
            // Goes threw each .sort element.
            $(".sort").each(function () {
              $(this).val(x);
              x++;
            });

        });

    });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
        $( ".team-delete" ).click(function() {
            var name = $(this).closest('.details').find(".name").html();
            var team_id = $(this).closest('.details').find(".team_id").val();

            showModal('Are you sure you want to delete '+name+'?');

            @if(Auth::guard('admin')->check())
                var url = '{{route('admin.team.delete')}}';
            @elseif(Auth::guard('user')->check())
                var url = '{{route('user.team.delete')}}';
            @endif

            $( "#modal-accept" ).click(function() {
                  $.ajax({
                      url: url,
                      method: 'POST',
                      data: { 'id' : team_id, '_token' : '{{csrf_token()}}' },
                      success: function(result){

                          $( "#modal" ).hide();
                          $( "#"+team_id ).remove();

                          var message = "<div id='notification'>";
                              message +=     "<p>"+name+" was Deleted. Page will now reload.</p>";
                              message += "</div>";

                          $("body").append(message);
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
            });
        });
    });
  </script>

@endsection
