@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'page';

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.page.save');
  }
?>

@section('content')
    <h1>Edit: {{$page->title}}</h1>

    <div class="section">
        <input type="hidden" name="id" value="{{$page->id}}" required>
    </div>
	<label>Content
    <textarea id="editor" name="content">{{$page->content}}</textarea>
</label>
@endsection

@section('inspector')
  @if(Auth::guard('admin')->check())
      @include('admin.admin_auth.inc.page-inspector')
  @endif
  @include('admin.inc.tinymce_options')
@endsection
