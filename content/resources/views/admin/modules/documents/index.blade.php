@extends('admin.layouts.admin')
<?
  $class = 'index';
  $active = 'documents';
?>
@section('content')
    <h1>Documents</h1>
    <div class="filterBar"> <!-- for filtering index content (dynamically ordered posts) -->
        <input id="search" type="text" placeholder="search">
      </div>
  <div class="category primary">
      <h2>All Uploaded Documents</h2>
      <ul>
            @foreach ($documents as $item)
              <li id="{{$item->id}}">
                  <div class="details">
                      <input type="hidden" class="id" name="id" value="{{$item->id}}">
                      <span class="title">{{$item->name}}</span>
                      <span class="date"><strong>{{date_format($item->created_at,'F d, Y')}}</strong></span>
                      <span class="actions">
                          <a class="button document" href='{{route('admin.download.document',['id'=>$item->id])}}'>download</a>
                          <a class="button cancel document-delete">delete</a>
                      </span>
                  </div>
              </li>
          @endforeach
      </ul>
  </div>

@endsection

@section('inspector')
  <div id="inspector">
    <fieldset>
        <h2>Document...</h2>
        <p>Upload Document To All Users</p> 
        <form method="POST" action="{{route("admin.upload.document")}}" enctype="multipart/form-data">
          @csrf
          <label>file :<span id="document-show">
          </span>
        </label>
          <label><input class="button" name="document" id="document" type="file"></label>
          <button type="submit" class="button submit">upload document</button>
        </form>
      </fieldset>
  </div>
  <script>
    $(document).ready(function(){
      $("#search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".category li").filter(function(el, idx, array) {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
  
    $( ".document-delete" ).click(function() {
              var document_id = $(this).closest('.details').find(".id").val();
              showModal('Are You Sure You Want To Delete This Document?');
  
              $( "#modal-accept" ).click(function() {
                $.ajax({
                        url: "{{route('admin.document.delete')}}",
                        method: 'POST',
                        data: { 'id' : document_id, '_token' : '{{csrf_token()}}' },
                        success: function(result){
                            $( "#modal" ).hide();
                            $( "#notification" ).delay(1500).slideUp('slow');
                            setTimeout(function(){
                                location.reload();
                            }, 2000);
                        }
                    });
              });
          });
    </script>
@endsection
