@extends('admin.layouts.admin')
<?
  $class = 'index longNames';
  $active = 'posts';
?>
@section('content')
  @if(isset($slug))
      <h1>Post: {{$helper->get_category_by_slug($slug)}}</h1>
  @else
     <h1>Post</h1>
  @endif

  <div class="category primary">
      <h2>All Post Items</h2>
      <ul>
          @foreach($post_items as $post)
              <li id="{{$post->id}}">
                  <div class="details">
                      <input type="hidden" class="id" name="id" value="{{$post->id}}">
                      <span class="title">{{$post->title}}</span>
                      <span class="category">{{$helper->get_category($post->category_id)}}</span>
                      <span class="date"><strong>{{date_format($post->created_at,'F d, Y')}}</strong></span>
                      <span class="actions">
                          <span class="status">{{$post->visible}}</span>
                          @if(Auth::guard('admin')->check())
                              <a class="button" href="{{ route('admin.posts.edit',$post->id) }}">edit</a>
                          @elseif(Auth::guard('user')->check())
                              <a class="button" href="{{ route('user.posts.edit',$post->id) }}">edit</a>
                          @endif
                          <a class="button" href="{{ route('public.posts.single',$post->slug) }}" target="_blank">view</a>
                          <a class="button cancel post-delete">delete</a>
                      </span>
                  </div>
              </li>
          @endforeach
      </ul>
  </div>

@endsection

@section('inspector')
  <div id="inspector">
      @if(Auth::guard('admin')->check())
          <form method="POST" action="{{ route('admin.posts.create') }}">
            @csrf
            <h2>New Post Item...</h2>
            <input type="text" id="title" name="title" placeholder="Title" required>
            <input type="text" id="slug" name="slug" placeholder="Slug" required>
            <button class="button" type="submit" >create</button>
          </form>
          <hr>
          <a href="{{route('admin.posts.cat')}}" class="button">manage categories</a>
      @elseif(Auth::guard('user')->check())
          <form method="POST" action="{{ route('user.posts.create') }}">
            @csrf
            <h2>New Post Item...</h2>
            <input type="text" id="title" name="title" placeholder="Title" required>
            <input type="text" id="slug" name="slug" placeholder="Slug" required>
            <button class="button" type="submit" >create</button>
          </form>
          <hr>
          <a href="{{route('user.posts.cat')}}" class="button">manage categories</a>
      @endif
  </div>

  <script>
    $(document).ready(function() {
        $('#title').on("change", function() {
            // If the slug has not value, sets the slug value from the title.
    		if ($('#slug').val() == ""){
    			updateSlug('title', 'slug');
    		}
    	});

    	$('#slug').on("change", function() {
    		updateSlug('title', 'slug');
    	});

        // Removes spaces and symbols from the slug.
    	function updateSlug(title, slug) {
    		var newSlug = "";

    		if ($('#'+slug).val() == ""){
    			newSlug = $('#'+title).val().replace(/\s/g,"-");
    		} else {
    			newSlug = $('#'+slug).val().replace(/\s/g,"-");
    		}

            newSlug = newSlug.replace(/[^A-Za-z0-9\_\-]/g,"");
    		newSlug = newSlug.toLowerCase();
    		$('#'+slug).val(newSlug);
    	}

        @if(Auth::guard('admin')->check())
            $( ".post-delete" ).click(function() {
                var title = $(this).closest('.details').find(".title").html();
                var id = $(this).closest('.details').find(".id").val();

                showModal('Are you sure you want to delete '+title+'?');

                @if(Auth::guard('admin')->check())
                    var url = '{{route('admin.posts.delete')}}';
                @elseif(Auth::guard('user')->check())
                    var url = '{{route('user.posts.delete')}}';
                @endif

                $( "#modal-accept" ).click(function() {
                      $.ajax({
                          url: url,
                          method: 'POST',
                          data: { 'id' : id, '_token' : '{{csrf_token()}}' },
                          success: function(result){

                              $( "#modal" ).hide();
                              $( "#"+id ).remove();

                              var message = "<div id='notification'>";
                                  message +=     "<p>"+title+" was Deleted. Page will now reload.</p>";
                                  message += "</div>";

                              $("body").append(message);
                              $( "#notification" ).delay(1500).slideUp('slow');
                              setTimeout(function(){
                                  location.reload();
                              }, 2000);
                          }
                      });
                });
            });
        @endif
    });
  </script>
@endsection
