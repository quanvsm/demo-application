@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'posts';
  $file = true;

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.posts.save');
  }elseif(Auth::guard('user')->check()){
      $form_action = route('user.posts.save');
  }
?>

@section('content')
    <h1>Title: {{$post->title}}</h1>

    <div class="section">
        <label>Content
            <textarea id="editor" name="content">{{$post->content}}</textarea>
        </label>
    </div>

    <div class="section">
        <label>Excerpt
            <input type="text" name="excerpt" value="{{$post->excerpt}}">
        </label>
    </div>
@endsection

@section('inspector')
    <div id="inspector">
      <input type="hidden" name="id" value="{{$post->id}}" required>
      @if(isset($post->img))
          <input type="hidden" id="remove_img" name="remove_img" value="0">
      @endif

      @if(Auth::guard('admin')->check())
          <a class="button" href="{{ route('admin.posts') }}">go back</a>
          <button type="submit" id="delete" formaction="{{route('admin.posts.delete_post')}}" class="button cancel">delete</button>
      @elseif(Auth::guard('user')->check())
          <a class="button" href="{{ route('user.posts') }}">go back</a>
          <button type="submit" id="delete" formaction="{{route('user.posts.delete_post')}}" class="button cancel">delete</button>
      @endif

      <button id="save" type="submit" class="button accept" >save</button>
      <a class="button" href="{{ route('public.posts.single',$post->slug) }}" target="_blank">view</a>
      <hr>
      <label>
          Title
          <input type="text" id="title" name="title" value="{{$post->title}}" required>
      </label>
      <label>
          Slug
          <input type="text" id="slug" name="slug" value="{{$post->slug}}" required>
      </label>
      <fieldset>
          <legend>Visibility</legend>
          <label><input name="visible" type="radio" value="published" {{($post->visible=='published'? 'checked' :'')}} >Published</label>
          <label><input name="visible" type="radio" value="draft" {{($post->visible=='draft'? 'checked' :'')}} >Draft</label>
      </fieldset>
      <hr>
      <label>Date
          <input type="text" id="created_at" name="created_at" value="{{date_format($post->created_at,'F d, Y g:i A')}}" required>
      </label>
      <hr>
      <label>
          Select a Category
          <select name="category">
            <option value="default">Default</option>
            @foreach($categories as $category)
                <option value="{{$category->id}}" {{(isset($post) && $post->category_id==$category->id? 'selected' :'')}}>{{$category->title}}</option>
            @endforeach
          </select>
      </label>
      <hr>
      <label>Featured Image (Recommended Pixel Size 280 x 280)
          <input type="file" id="img" name="img">
          @if(isset($post->img))
              <img id="side_img" src="{{env('APP_POST').$post->img}}">
              <a id="img_remove_btn" class="button cancel">remove image</a>
          @endif
      </label>
      <hr>
      <h2>SEO...</h2>
      <label>
          Head
          <input type="text" id="seo-title" name="seo-title" value="{{$post->seo_title}}">
      </label>
      <label class="description">
          Description
          <textarea name="seo-description">{{$post->seo_description}}</textarea>
      </label>
    </div>
    <script>
      $(document).ready(function() {
          $('#title').on("change", function() {
              // If the slug has not value, sets the slug value from the title.
              if ($('#slug').val() == ""){
                  updateSlug('title', 'slug');
              }
          });

          $('#slug').on("change", function() {
              updateSlug('title', 'slug');
          });

          // Removes spaces and symbols from the slug.
          function updateSlug(title, slug) {
              var newSlug = "";

              if ($('#'+slug).val() == ""){
                  newSlug = $('#'+title).val().replace(/\s/g,"-");
              } else {
                  newSlug = $('#'+slug).val().replace(/\s/g,"-");
              }

              newSlug = newSlug.replace(/[^A-Za-z0-9\_\-]/g,"");
              newSlug = newSlug.toLowerCase();
              $('#'+slug).val(newSlug);
          }

          $( "#created_at" ).datetimepicker({
              dateFormat:'MM dd, yy',
              timeFormat:'h:mm TT',
          });

          $( "#img_remove_btn" ).click(function(e) {
              e.preventDefault();
              $("#remove_img").val(1);
              $("#side_img").remove();
          });


          $( "#delete" ).click(function() {
              event.preventDefault();
              showModal('Are you sure you want to delete this post item?');

              $( "#modal-accept" ).click(function() {
                  $( "#delete" ).click();
              });
          });

      });
    </script>
    @include('admin.inc.tinymce_options')
@endsection
