@extends('admin.layouts.admin_form')
<?
  $class = 'editor';
  $active = 'posts';

  if(Auth::guard('admin')->check()){
      $form_action = route('admin.posts.cat.save');
  }elseif(Auth::guard('user')->check()){
      $form_action = route('user.posts.cat.save');
  }
?>

@section('content')
    <h1>Post Category: {{$category->title}}</h1>

    <div class="section">
        <label>Title
            <input type="text" id="title" name="title" value="{{$category->title}}" >
        </label>
    </div>

    <div class="section">
        <label>Slug
            <input type="text" id="slug" name="slug" value="{{$category->slug}}" >
        </label>
    </div>

@endsection

@section('inspector')
    <div id="inspector">
      <input type="hidden" name="id" value="{{$category->id}}" required>

      @if(Auth::guard('admin')->check())
          <a class="button" href="{{ route('admin.posts.cat') }}">go back</a>
          <button type="submit" id="delete" formaction="{{route('admin.posts.cat.delete_cat')}}" class="button cancel">delete category</button>
      @elseif(Auth::guard('user')->check())
          <a class="button" href="{{ route('user.posts.cat') }}">go back</a>
          <button type="submit" id="delete" formaction="{{route('user.posts.cat.delete_cat')}}" class="button cancel">delete category</button>
      @endif

      <button id="save" type="submit" class="button accept" >save</button>
    </div>
    <script>
      $(document).ready(function() {
          $('#title').on("change", function() {
              // If the slug has not value, sets the slug value from the title.
              if ($('#slug').val() == ""){
                  updateSlug('title', 'slug');
              }
          });

          $('#slug').on("change", function() {
              updateSlug('title', 'slug');
          });

          // Removes spaces and symbols from the slug.
          function updateSlug(title, slug) {
              var newSlug = "";

              if ($('#'+slug).val() == ""){
                  newSlug = $('#'+title).val().replace(/\s/g,"-");
              } else {
                  newSlug = $('#'+slug).val().replace(/\s/g,"-");
              }

              newSlug = newSlug.replace(/[^A-Za-z0-9\_\-]/g,"");
              newSlug = newSlug.toLowerCase();
              $('#'+slug).val(newSlug);
          }

          $( "#delete" ).click(function() {
              event.preventDefault();
              showModal('Are you sure you want to delete this Category?');

              $( "#modal-accept" ).click(function() {
                  $( "#delete" ).click();
              });
          });

      });
    </script>
@endsection
