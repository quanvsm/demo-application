@extends('admin.layouts.admin')
<?
  $class = 'index';
  $active = 'posts';
?>
@section('content')
  <h1>Posts Categories</h1>

  <div class="category primary">
      <h2>All Categories</h2>
      <ul class="sortable">
        @foreach($categories as $category)
          <li>
            <div class="details">
                <input type="hidden" class="id" name="id" value="{{$category->id}}" required>
                <span class="title">{{$category->title}}</span>

                <span class="actions">
                    @if(Auth::guard('admin')->check())
                        <a class="button" href="{{ route('admin.posts.cat.edit',$category->id) }}">edit</a>
                    @elseif(Auth::guard('user')->check())
                        <a class="button" href="{{ route('user.posts.cat.edit',$category->id) }}">edit</a>
                    @endif
                    <a class="button cancel cat-delete">delete</a>
                </span>
            </div>
          </li>
        @endforeach
      </ul>
  </div>

@endsection

@section('inspector')
  <div id="inspector">
      @if(Auth::guard('admin')->check())
          <a class="button" href="{{ route('admin.posts') }}">go back</a>
          <hr>
          <form method="POST" action="{{ route('admin.posts.cat.create') }}">
            @csrf
            <h2>Add Category...</h2>
            <input type="text" id="title" name="title" placeholder="title" required>
            <input type="text" id="slug" name="slug" placeholder="Slug" required>
            <button class="button" type="submit" >add</button>
          </form>
          <hr>
      @elseif(Auth::guard('user')->check())
          <a class="button" href="{{ route('user.posts') }}">go back</a>
          <hr>
          <form method="POST" action="{{ route('user.posts.cat.create') }}">
            @csrf
            <h2>Add Category...</h2>
            <input type="text" id="title" name="title" placeholder="title" required>
            <input type="text" id="slug" name="slug" placeholder="Slug" required>
            <button class="button" type="submit" >add</button>
          </form>
          <hr>
      @endif
  </div>

  <script>
    $(document).ready(function() {
        $('#title').on("change", function() {
            // If the slug has not value, sets the slug value from the title.
            if ($('#slug').val() == ""){
                updateSlug('title', 'slug');
            }
        });

        $('#slug').on("change", function() {
            updateSlug('title', 'slug');
        });

        // Removes spaces and symbols from the slug.
        function updateSlug(title, slug) {
            var newSlug = "";

            if ($('#'+slug).val() == ""){
                newSlug = $('#'+title).val().replace(/\s/g,"-");
            } else {
                newSlug = $('#'+slug).val().replace(/\s/g,"-");
            }

            newSlug = newSlug.replace(/[^A-Za-z0-9\_\-]/g,"");
            newSlug = newSlug.toLowerCase();
            $('#'+slug).val(newSlug);
        }


        $( ".cat-delete" ).click(function() {
            var title = $(this).closest('.details').find(".title").html();
            var id = $(this).closest('.details').find(".id").val();

            showModal('Are you sure you want to delete '+title+'?');

            @if(Auth::guard('admin')->check())
                var url = '{{route('admin.posts.cat.delete')}}';
            @elseif(Auth::guard('user')->check())
                var url = '{{route('user.posts.cat.delete')}}';
            @endif

            $( "#modal-accept" ).click(function() {
                  $.ajax({
                      url: url,
                      method: 'POST',
                      data: { 'id' : id, '_token' : '{{csrf_token()}}' },
                      success: function(result){

                          $( "#modal" ).hide();

                          var message = "<div id='notification'>";
                              message +=     "<p>"+title+" was Deleted. Page will now reload.</p>";
                              message += "</div>";

                          $("body").append(message);
                          $( "#notification" ).delay(1500).slideUp('slow');
                          setTimeout(function(){
                              location.reload();
                          }, 2000);
                      }
                  });
            });
        });


    });
  </script>
@endsection
