@if($errors->any())
    @foreach($errors->all() as $error)
      <div id="notification">
        <p>{{$error}}</p>
        <a class="button dismiss" href="#">dismiss</a>
      </div>
    @endforeach
@endif

@if(Session::has('success'))
    <div id="notification">
        <p>{{Session::get('success')}}</p>
        <script>$( "#notification" ).delay(1500).slideUp('slow');</script>
    </div>
@endif

@if(Session::has('error'))
    <div id="notification">
        <p>{{Session::get('error')}}</p>
        <script>$( "#notification" ).delay(1500).slideUp('slow');</script>
    </div>
@endif

@if(session('status'))
    <div id="notification">
        <p>{{ session('status') }}</p>
        <a class="button dismiss" href="#">dismiss</a>
    </div>
@endif


<div id="modal" style="display: none;">
    <div class="modal-content">
        <p id="modal-message" ></p>
        <p style="display: none;"><strong>would you like to continue?</strong></p>
        <a id="modal-cancel" class="button cancel" href="#">no</a>
        <a id="modal-accept" class="button accept" href="#">yes</a>
    </div>
</div>

<script>
  $(document).ready(function() {
      $( ".dismiss" ).click(function() {
        $( "#notification" ).delay(500).slideUp('slow');
      });

      $( "#modal-cancel" ).click(function() {
        $( "#modal" ).hide();
      });
  });

  function showModal(message){
      $("#modal").show();
      $("#modal-message").html(message);
  }

</script>
