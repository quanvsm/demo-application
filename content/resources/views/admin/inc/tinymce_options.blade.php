<script>
$(document).ready(function(){

    // https://www.tiny.cloud/docs/configure/editor-appearance/
    tinymce.init({
        selector: "#editor",
        height: "calc(100vh - 250px)",
        //content_css : "/css/admin/css/wysiwyg.min.css",
        plugins: [
            "responsivefilemanager code lists table link charmap"
        ],
        menubar: false,
        toolbar: "code | responsivefilemanager | undo redo | bold strikethrough underline italic | fontsizeselect forecolor backcolor styleselect | alignleft aligncenter alignright  alignjustify | bullist numlist | outdent indent | table | link charmap",
        style_formats: [
            { title: 'Paragraph', format: 'p' },
            { title: 'Heading 1', format: 'h1' },
            { title: 'Heading 2', format: 'h2' },
            { title: 'Heading 3', format: 'h3' },
            { title: 'Heading 4', format: 'h4' },
            { title: 'Blockquote', format: 'blockquote' },
        ],
        fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt 48pt 60pt 72pt 96pt',
        external_filemanager_path:"/lib/responsive_filemanager/filemanager/",
        filemanager_title:"Responsive Filemanager" ,
        filemanager_access_key: "{{env('APP_UPLOADS_KEY')}}",
        external_plugins: {
            "responsivefilemanager": "/lib/responsive_filemanager/filemanager/plugin.min.js",
            "filemanager": "/lib/responsive_filemanager/tinymce/plugins/responsivefilemanager/plugin.min.js"
        }
    });

});
</script>
