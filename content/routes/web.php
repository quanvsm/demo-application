<?php
use App\Http\Controllers\UserAuth\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Public Routes
Route::get('/', 'UserAuth\UserLoginController@showLoginForm' )->name('home');
Route::get('/signup','UserAuth\UserLoginController@showSignUp')->name('user.signup');
// Route::get('/', 'PublicController@showHomePage' )->name('home');

// Admin Routes
Route::group(['prefix' => 'admin'], function () {

    // Admin Login Routes.
    Route::get('/','AdminAuth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/','AdminAuth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout', 'AdminAuth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/account/{hash}', 'AdminAuth\AdminAccountController@adminAccount')->name('admin.account');
    Route::post('/account/{hash}', 'AdminAuth\AdminAccountController@adminCreate')->name('admin.create');

    // Admin Password Reset Routes.
    Route::get('/password/reset','AdminAuth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/email','AdminAuth\AdminForgotPasswordController@check')->name('admin.password.email');
    Route::get('/password/reset/{token}','AdminAuth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('/password/reset','AdminAuth\AdminResetPasswordController@resetAdmin')->name('admin.password.check');

    // Admin Pages.
    Route::get('/pages', 'AdminAuth\PageController@page')->name('admin.page');
    Route::post('/pages', 'AdminAuth\PageController@createPage')->name('admin.page.create');
    Route::post('/parent', 'AdminAuth\PageController@createParent')->name('admin.parent.create');
    Route::get('/pages/{id}/edit', 'AdminAuth\PageController@editPage')->name('admin.page.edit');
    Route::post('/pages/edit', 'AdminAuth\PageController@savePage')->name('admin.page.save');
    Route::get('/pages/{id}/edit_seo', 'AdminAuth\PageController@editPageSEO')->name('admin.page.seo');
    Route::post('/pages/edit_seo', 'AdminAuth\PageController@savePageSEO')->name('admin.page.save.seo');
    Route::post('/pages/delete', 'AdminAuth\PageController@delete')->name('admin.page.delete');
    Route::post('/pages/delete_page', 'AdminAuth\PageController@deletePage')->name('admin.page.delete_page');
    Route::post('/pages/save_order', 'AdminAuth\PageController@saveOrder')->name('admin.page.order');

    // Admin FAQ Template.
    Route::post('/pages/faq', 'AdminAuth\FAQController@createFAQ')->name('admin.faq.create');
    Route::get('/pages/faq/{id}/edit', 'AdminAuth\FAQController@editFAQ')->name('admin.faq.edit');
    Route::post('/pages/faq/edit', 'AdminAuth\FAQController@saveFAQ')->name('admin.faq.save');
    Route::post('/pages/faq/delete', 'AdminAuth\FAQController@delete')->name('admin.faq.delete');

    // Admin Team Template.
    Route::post('/pages/team', 'AdminAuth\TeamController@create')->name('admin.team.create');
    Route::get('/pages/team/{id}/edit', 'AdminAuth\TeamController@edit')->name('admin.team.edit');
    Route::post('/pages/team/edit', 'AdminAuth\TeamController@save')->name('admin.team.save');
    Route::post('/pages/team/delete', 'AdminAuth\TeamController@delete')->name('admin.team.delete');

    // Admin Posts.
    Route::get('/posts', 'AdminAuth\PostsController@posts')->name('admin.posts');
    Route::post('/posts', 'AdminAuth\PostsController@create')->name('admin.posts.create');
    Route::get('/posts/{id}/edit', 'AdminAuth\PostsController@edit')->name('admin.posts.edit');
    Route::post('/posts/edit', 'AdminAuth\PostsController@save')->name('admin.posts.save');
    Route::post('/posts/delete', 'AdminAuth\PostsController@delete')->name('admin.posts.delete');
    Route::post('/posts/delete_post', 'AdminAuth\PostsController@deletePost')->name('admin.posts.delete_post');

    // Admin Posts Categories.
    Route::get('/posts/categories', 'AdminAuth\PostsCategoriesController@index')->name('admin.posts.cat');
    Route::post('/posts/categories', 'AdminAuth\PostsCategoriesController@create')->name('admin.posts.cat.create');
    Route::get('/posts/categories/{id}/edit', 'AdminAuth\PostsCategoriesController@edit')->name('admin.posts.cat.edit');
    Route::post('/posts/categories/edit', 'AdminAuth\PostsCategoriesController@save')->name('admin.posts.cat.save');
    Route::post('/posts/categories/delete', 'AdminAuth\PostsCategoriesController@delete')->name('admin.posts.cat.delete');
    Route::post('/posts/categories/delete_category', 'AdminAuth\PostsCategoriesController@deleteCategory')->name('admin.posts.cat.delete_cat');

    // Admin Admins
    Route::get('/admins', 'AdminAuth\AdminController@admin')->name('admin.admins');
    Route::get('/admins/new', 'AdminAuth\AdminController@newAdmin')->name('admin.admins.new');
    Route::post('/admins/new', 'AdminAuth\AdminController@createAdmin')->name('admin.admins.create');
    Route::get('/admins/{id}/edit', 'AdminAuth\AdminController@editAdmin')->name('admin.admins.edit');
    Route::post('/admins/edit', 'AdminAuth\AdminController@saveAdmin')->name('admin.admins.save');
    Route::post('/admins/delete', 'AdminAuth\AdminController@delete')->name('admin.admins.delete');
    Route::post('/admins/delete_admin', 'AdminAuth\AdminController@deleteAdmin')->name('admin.admins.delete_admin');
    Route::post('/admins/email','AdminAuth\AdminController@sendResetLinkEmail')->name('admin.admins.email');
    Route::post('/admins/resend','AdminAuth\AdminController@reSendActivationEmail')->name('admin.admins.resend');

    // Admin Users
    Route::get('/users', 'AdminAuth\UserController@users')->name('admin.users');
    Route::get('/users/new', 'AdminAuth\UserController@newUser')->name('admin.users.new');
    Route::post('/users/new', 'AdminAuth\UserController@createUser')->name('admin.users.create');
    Route::get('/users/{id}/edit', 'AdminAuth\UserController@editUser')->name('admin.users.edit');
    Route::post('/users/edit', 'AdminAuth\UserController@saveUser')->name('admin.users.save');
    Route::post('/users/delete', 'AdminAuth\UserController@delete')->name('admin.users.delete');
    Route::post('/users/delete_user', 'AdminAuth\UserController@deleteUser')->name('admin.users.delete_user');
    Route::post('/users/email','AdminAuth\UserController@sendResetLinkEmail')->name('admin.users.email');
    Route::post('/users/resend','AdminAuth\UserController@reSendActivationEmail')->name('admin.users.resend');

    //Applications Route
    Route::get('/application', 'AdminAuth\ApplicationController@applications')->name('admin.applications');
    Route::get('/application/{id}/view', 'AdminAuth\ApplicationController@viewApplication')->name('admin.application.view');
    Route::get('/application/{id}/create', 'AdminAuth\ApplicationController@create')->name('admin.application.create');
    Route::get('/application/{id}/edit', 'AdminAuth\ApplicationController@editApplication')->name('admin.application.edit');
    Route::post('/application/delete', 'AdminAuth\ApplicationController@delete')->name('admin.application.delete');
    Route::get('/application/{id}/download/{field_name}', 'AdminAuth\ApplicationController@download')->name('admin.application.download');
    Route::get('/application/download/all/{id}', 'AdminAuth\ApplicationController@downloadAll')->name('admin.application.download.all');
    Route::get('/application/export/{id}/type/{type}', 'PublicController@exportCSV')->name('admin.export');
    Route::get('/application/all/export/{id}', 'PublicController@exportAllCSV')->name('admin.export.all');
    Route::post('/application/save', 'AdminAuth\ApplicationController@save')->name('admin.application.save');
    Route::get('/application/update/status', 'AdminAuth\ApplicationController@updateStatus')->name('admin.application.update.status');
    Route::get('/application/set/interview/date', 'AdminAuth\ApplicationController@setInterviewDate')->name('admin.application.interview.date');
    Route::get('/application/set/cut/off', 'AdminAuth\ApplicationController@setCutOff')->name('admin.application.set.cutoff');
    Route::get('/application/{id}/print', 'AdminAuth\ApplicationController@print')->name('admin.application.print');
    Route::post('/application/upload/invoice/{id}', 'AdminAuth\ApplicationController@uploadInvoice')->name('admin.upload.invoice');

    //Registrations Route
    Route::get('/registration', 'AdminAuth\RegistrationController@registrations')->name('admin.registrations');
    Route::get('/registration/{id}/view', 'AdminAuth\RegistrationController@viewRegistration')->name('admin.registration.view');
    Route::get('/registration/{id}/edit', 'AdminAuth\RegistrationController@editRegistration')->name('admin.registration.edit');
    Route::post('/registration/save', 'AdminAuth\RegistrationController@save')->name('admin.registration.save');
    Route::get('/registration/export/{id}/type/{type}', 'PublicController@exportIndividualRegistrationCSV')->name('admin.registration.export');
    Route::get('/registration/all/export/{id}', 'PublicController@exportAllRegistrationsCSV')->name('admin.registration.export.all');
    Route::get('/registration/{id}/print', 'AdminAuth\RegistrationController@print')->name('admin.registration.print');
    Route::get('/registration/update/status', 'AdminAuth\RegistrationController@updateStatus')->name('admin.registration.update.status');
    Route::post('/registration/upload/invoice/{id}', 'AdminAuth\RegistrationController@uploadInvoice')->name('admin.registration.upload.invoice');
    Route::post('/registration/delete', 'AdminAuth\RegistrationController@delete')->name('admin.registration.delete');

    Route::get('admin/download/invoice/{id}', 'PublicController@downloadInvoice')->name('admin.download.invoice');

    //documents Routes
    Route::get('/documents','AdminAuth\DocumentsController@documents')->name('admin.documents');
    Route::post('/document/upload/document', 'AdminAuth\DocumentsController@uploadDocument')->name('admin.upload.document');
    Route::get('admin/download/document/{id}', 'AdminAuth\DocumentsController@downloadDocument')->name('admin.download.document');
    Route::post('/document/delete', 'AdminAuth\DocumentsController@delete')->name('admin.document.delete');


    // Admin Settings
    Route::get('/settings','AdminAuth\AdminController@settings')->name('admin.settings');
    Route::post('/settings','AdminAuth\AdminController@saveSettings')->name('admin.settings.save');
    
});

// User Routes
Route::group(['prefix' => 'user'], function () {

    // User Login Routes.
    Route::get('/','UserAuth\UserLoginController@showLoginForm')->name('user.login');
    // Route::get('/signup','UserAuth\UserLoginController@showSignUp')->name('user.signup');
    Route::post('/login','UserAuth\UserLoginController@login')->name('user.login.submit');
    Route::get('/logout', 'UserAuth\UserLoginController@logout')->name('user.logout');
    Route::post('/create', 'UserAuth\UserAccountController@createUser')->name('user.create');
    Route::get('/account/{hash}', 'UserAuth\UserAccountController@create')->name('user.activate');
    // Route::post('/account/{hash}', 'UserAuth\UserAccountController@create')->name('user.create');

    // User Password Reset Routes.
    Route::get('/password/reset','UserAuth\UserForgotPasswordController@showLinkRequestForm')->name('user.password.request');
    Route::post('/password/email','UserAuth\UserForgotPasswordController@sendResetLinkEmail')->name('user.password.email');
    Route::get('/password/reset/{token}','UserAuth\UserResetPasswordController@showResetForm')->name('user.password.reset');
    Route::post('/password/reset','UserAuth\UserResetPasswordController@resetUser')->name('user.password.check');


    //user application routes
    Route::get('/form','UserAuth\FormController@form')->name('user.form');
    Route::get('/form/{id}','UserAuth\FormController@showForm')->name('user.form.show');
    Route::post('/form/save','UserAuth\FormController@save')->name('user.form.save');
    Route::post('/form/submit/{id}','UserAuth\FormController@submit')->name('user.form.sutmit');
    Route::get('/form/edit/{id}','UserAuth\FormController@editForm')->name('user.form.edit');
    Route::post('/form/saveforlater','UserAuth\FormController@saveForLater')->name('user.form.saveForLater');
    Route::get('/form/summary/{id}','UserAuth\FormController@showSummary')->name('user.form.summary');
    Route::get('/form/payment/{id}','UserAuth\FormController@showPaymentPage')->name('user.form.payment');
    Route::get('/form/download/statementoffaith', 'UserAuth\FormController@statementDownload')->name('user.statement.download');
    Route::get('/form/{id}/download/{field_name}', 'UserAuth\FormController@download')->name('user.download');
    Route::get('/form/{id}/print', 'UserAuth\FormController@print')->name('user.print');

    //user registration routes
    Route::get('/registration/{id}','UserAuth\RegistrationFormController@form')->name('user.registration');
    Route::post('/registration/save/summary','UserAuth\RegistrationFormController@saveBeforeSummary')->name('user.registration.save');
    Route::post('/registration/save/for/later','UserAuth\RegistrationFormController@saveForLater')->name('user.registration.later.save');
    Route::get('/registration/summary/{id}','UserAuth\RegistrationFormController@showSummary')->name('user.registration.summary');
    Route::get('/registration/payment/{id}','UserAuth\RegistrationFormController@showPaymentStep')->name('user.registration.payment');
    Route::get('/registration/form/download/{id}', 'UserAuth\RegistrationFormController@download')->name('user.registration.download');
    Route::post('registration/form/submit/{id}','UserAuth\RegistrationFormController@submit')->name('user.registration.sutmit');
    Route::get('/registration/form/{id}/print', 'UserAuth\RegistrationFormController@print')->name('user.registration.print');

    //Profile Routes
    Route::get('/profile/{id}','UserAuth\UserController@profile')->name('user.profile');
    Route::post('/profile/update','UserAuth\UserController@updateProfile')->name('user.update.profile');
    Route::get('user/download/invoice/{id}', 'PublicController@downloadInvoice')->name('user.download.invoice');
    Route::get('user/download/document/{id}', 'PublicController@downloadDocument')->name('user.download.document');

    //dashboard Routes
    Route::get('/dashboard','UserAuth\FormController@dashboard')->name('user.dashboard');

    //documents Routes
    Route::get('/documents','UserAuth\FormController@documents')->name('user.documents');

    //paypal
    Route::get('/paywithpaypal', array('as' => 'paywithpaypal','uses' => 'PaypalController@payWithPaypal',));
    Route::post('/paypal/{amount}', array('as' => 'paypal','uses' => 'PaypalController@postPaymentWithpaypal',));
    Route::get('/paypal', array('as' => 'status','uses' => 'PaypalController@getPaymentStatus',));
    Route::get('/updatePayment/{id}/application/{application}', [UserController::class, 'updateApplicationPayment'])->name('user.update.payment');

    // User Pages.
    // Route::get('/pages', 'UserAuth\PageController@page')->name('user.page');
    // Route::post('/pages', 'UserAuth\PageController@createPage')->name('user.page.create');
    // Route::post('/parent', 'UserAuth\PageController@createParent')->name('user.parent.create');
    // Route::get('/pages/{id}/edit', 'UserAuth\PageController@editPage')->name('user.page.edit');
    // Route::post('/pages/edit', 'UserAuth\PageController@savePage')->name('user.page.save');
    // Route::get('/pages/{id}/edit_seo', 'UserAuth\PageController@editPageSEO')->name('user.page.seo');
    // Route::post('/pages/edit_seo', 'UserAuth\PageController@savePageSEO')->name('user.page.save.seo');
    // Route::post('/pages/delete', 'UserAuth\PageController@delete')->name('user.page.delete');
    // Route::post('/pages/delete_page', 'UserAuth\PageController@deletePage')->name('user.page.delete_page');
    // Route::post('/pages/save_order', 'UserAuth\PageController@saveOrder')->name('user.page.order');

    // // User FAQ Template.
    // Route::post('/pages/faq', 'UserAuth\FAQController@createFAQ')->name('user.faq.create');
    // Route::get('/pages/faq/{id}/edit', 'UserAuth\FAQController@editFAQ')->name('user.faq.edit');
    // Route::post('/pages/faq/edit', 'UserAuth\FAQController@saveFAQ')->name('user.faq.save');
    // Route::post('/pages/faq/delete', 'UserAuth\FAQController@delete')->name('user.faq.delete');

    // // User Team Template.
    // Route::post('/pages/team', 'UserAuth\TeamController@create')->name('user.team.create');
    // Route::get('/pages/team/{id}/edit', 'UserAuth\TeamController@edit')->name('user.team.edit');
    // Route::post('/pages/team/edit', 'UserAuth\TeamController@save')->name('user.team.save');
    // Route::post('/pages/team/delete', 'UserAuth\TeamController@delete')->name('user.team.delete');

    // // User Posts.
    // Route::get('/posts', 'UserAuth\PostsController@posts')->name('user.posts');
    // Route::post('/posts', 'UserAuth\PostsController@create')->name('user.posts.create');
    // Route::get('/posts/{id}/edit', 'UserAuth\PostsController@edit')->name('user.posts.edit');
    // Route::post('/posts/edit', 'UserAuth\PostsController@save')->name('user.posts.save');
    // Route::post('/posts/delete', 'UserAuth\PostsController@delete')->name('user.posts.delete');
    // Route::post('/posts/delete_post', 'UserAuth\PostsController@deletePost')->name('user.posts.delete_post');

    // // User Posts Categories.
    // Route::get('/posts/categories', 'UserAuth\PostsCategoriesController@index')->name('user.posts.cat');
    // Route::post('/posts/categories', 'UserAuth\PostsCategoriesController@create')->name('user.posts.cat.create');
    // Route::get('/posts/categories/{id}/edit', 'UserAuth\PostsCategoriesController@edit')->name('user.posts.cat.edit');
    // Route::post('/posts/categories/edit', 'UserAuth\PostsCategoriesController@save')->name('user.posts.cat.save');
    // Route::post('/posts/categories/delete', 'UserAuth\PostsCategoriesController@delete')->name('user.posts.cat.delete');
    // Route::post('/posts/categories/delete_category', 'UserAuth\PostsCategoriesController@deleteCategory')->name('user.posts.cat.delete_cat');

    // // User Settings
    // Route::get('/settings','UserAuth\SettingsController@settings')->name('user.settings');
    // Route::post('/settings','UserAuth\SettingsController@saveSettings')->name('user.settings.save');
});

// Public Posts
// Please note these routes must be after the admin and user routes.
Route::get('/posts', 'PublicPostsController@index')->name('public.posts');
Route::get('/posts/{cat_slug}/page/{page}', 'PublicPostsController@categoryPage')->name('public.posts.cat.page');
Route::get('/posts/page/{page}', 'PublicPostsController@page')->name('public.posts.page');
Route::get('/posts/{cat_slug}', 'PublicPostsController@category')->name('public.posts.cat');
Route::get('/posts/{slug}', 'PublicPostsController@single')->name('public.posts.single');

// Public Routes Continued.
// Please note these routes must be after the admin and user routes.
Route::get('/{slug}', 'PublicController@show')->name('public.page');
Route::get('/{parent}/{slug}', 'PublicController@showParent')->name('public.parent');
