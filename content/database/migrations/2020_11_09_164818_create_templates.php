<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemplates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->string('view');
            $table->bigInteger('page_id')->nullable($value = false)->unsigned();
            $table->bigInteger('template_type')->nullable($value = false)->unsigned();
            $table->timestamps();
        });

        Schema::table('templates', function (Blueprint $table) {
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->foreign('template_type')->references('id')->on('template_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('templates', function (Blueprint $table) {
            $table->dropForeign(['page_id']);
            $table->dropForeign(['template_type']);
        });

        Schema::dropIfExists('templates');
    }
}
