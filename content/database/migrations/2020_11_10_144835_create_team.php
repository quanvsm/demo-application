<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable($value = false);
            $table->text('image')->nullable();
            $table->text('image_mime')->nullable();
            $table->string('position')->nullable();
            $table->string('additional_position')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->text('quote')->nullable();
            $table->text('bio')->nullable();
            $table->integer('sort')->unique()->nullable();
            $table->bigInteger('template_id')->nullable()->unsigned();
            $table->timestamps();
        });

        Schema::table('team', function (Blueprint $table) {
            $table->foreign('template_id')->references('id')->on('templates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team', function (Blueprint $table) {
            $table->dropForeign(['template_id']);
        });

        Schema::dropIfExists('team');
    }
}
