<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('content')->nullable($value = true);
            $table->string('seo_title')->nullable($value = true);
            $table->string('seo_description')->nullable($value = true);
            $table->integer('parent')->default(0);
            $table->integer('parent_id')->nullable($value = true);
            $table->string('visible')->nullable($value = true);
            $table->integer('sort')->unique()->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
