<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TemplateSortBugFix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('team', function (Blueprint $table) {
            $table->dropUnique(['sort']);
        });

        Schema::table('faqs', function (Blueprint $table) {
            $table->dropUnique(['sort']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('team', function (Blueprint $table) {
            $table->unique('sort');
        });

        Schema::table('faqs', function (Blueprint $table) {
            $table->unique('sort');
        });
    }
}
